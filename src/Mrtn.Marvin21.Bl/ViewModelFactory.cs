﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Bl;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Interfaces;
using Mrtn.Marvin21.DbModel.Run;
using Mrtn.Marvin21.DbModel.Run.Support;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.DbModel.Tasks;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Run;
using Mrtn.Marvin21.Model.RunTrack;
using Mrtn.Marvin21.Model.Si;
using Mrtn.Marvin21.Model.Tasks;
using Mrtn.Marvin21.Model.Teams;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl
{
    public static class ViewModelFactory
    {
        public static PackViewModel PackViewModel(Pack pack)
        {
            return new PackViewModel
            {
                Id = pack.Id,
                Name = pack.Name,
                Town = pack.Town,
                Abbr = pack.Abbr,
                Active = pack.Active,
                InCompetition = pack.InCompetition
            };
        }

        public static TeamViewModel TeamViewModel(Team team)
        {
            var model = new TeamViewModel
            {
                No = team.No,
                SiCode = team.SiCode,
                Active = team.Active,
                Id = team.Id,
                InCompetition = team.InCompetition,
                Name = team.Name,
                CategoryName = team.Category.Name,
                CategoryId = team.Category.Id,
                PackName = team.Pack.Name,
                PackAbbr = team.Pack.Abbr,
                PackId = team.Pack.Id,
                PrivateSiCode = team.PrivateSiCode
            };
            if (team.Result != null)
            {
                model.Points = team.Result.Points.ToString();
                model.Place = team.Result.Place.ToString();
                model.Percent= team.Result.Percent;
            }
            return model;
        }
        /// <summary>
        /// Za polnjenje dropdown selectorja. Iz podanega seznama entitet naredi seznam za v combo.
        /// </summary>
        /// <param name="dbList"></param>
        /// <param name="addEmptyEntity"></param>
        /// <returns></returns>
        public static List<NamedEntity> NamedEntityList(IEnumerable<INamedEntity> dbList, bool addEmptyEntity)
        {
            var list = new List<NamedEntity>();
            if (addEmptyEntity) list.Add(new NamedEntity { Id = 0, Name = "..." });
            foreach (var dbEntity in dbList)
            {
                var entity = new NamedEntity { Id = dbEntity.Id, Name = dbEntity.Name };
                //different treatment for category
                if (dbEntity is Category category) 
                    entity.Name = $"{category.Name} ({category.Abbr})";
                //different treatment for pack
                if (dbEntity is Pack pack) 
                    entity.Name = $"{pack.Name} ({pack.Abbr} {pack.Town})";
                //different treatment for station
                if (dbEntity is Station station) 
                    entity.Name = station.ToString();
                list.Add(entity);
            }
            return list;
        }

        public static SiChipViewModel SiChipViewModel(SiChip siChip)
        {
            return new SiChipViewModel
            {
                Id = siChip.Id,
                SiCode = siChip.SiCode,
                Active = siChip.Active,
                Private = siChip.Private,
                Comment = siChip.Comment
            };
        }

        public static TrackCheckpointViewModel TrackCheckpointViewModel(TrackCheckpoint dbCheckpoint, Marvin20Db db)
        {
            var model = new TrackCheckpointViewModel
            {
                Id = dbCheckpoint.Id,
                Description = dbCheckpoint.Description,
                FromPreviousDistance = dbCheckpoint.FromPrevious.Distance,
                FromPreviousDownhill = dbCheckpoint.FromPrevious.Downhill,
                FromPreviousUphill = dbCheckpoint.FromPrevious.Uphill,
                FromPreviousZisDirty = dbCheckpoint.FromPrevious.ZisDirty,
                FromPreviousZlist = dbCheckpoint.FromPrevious.Zlist,
                Locked = dbCheckpoint.Locked,
                Name = dbCheckpoint.Name,
                No = dbCheckpoint.No,
                Points = dbCheckpoint.Points,
                SpeedTrialIsFinish = dbCheckpoint.SpeedTrial.IsFinish,
                SpeedTrialIsStart = dbCheckpoint.SpeedTrial.IsStart,
                TimeForTask = FormatSecondsToString(dbCheckpoint.Time.ForTask),
                TimeMaxFromStart = FormatSecondsToString(dbCheckpoint.Time.MaxFromStart),
                StationId = dbCheckpoint.Station.Id,
                PathId = dbCheckpoint.PathFromPrevious?.Id ?? 0,
                StationName = dbCheckpoint.Station.Name,
                StationString = dbCheckpoint.Station.ToString(),
                StationSiCodeArrival = dbCheckpoint.Station.SiCodeArrival,
                StationSiCodeDeparture = dbCheckpoint.Station.SiCodeDeparture,
                StationIsStart = dbCheckpoint.Station.IsStart,
                StationIsFinish = dbCheckpoint.Station.IsFinish,
                StationIsAlive = dbCheckpoint.Station.IsAlive,
                StationIsHidden = dbCheckpoint.Station.IsHidden,
                X = dbCheckpoint.Station.MapPosition.X,
                Y = dbCheckpoint.Station.MapPosition.Y,
                TimeFromPrevious = FormatSecondsToString(dbCheckpoint.Time.FromPrevious),
                TimeFromPreviousForDistance =
                                    FormatSecondsToString(dbCheckpoint.Time.FromPreviousForDistance),
                TimeFromPreviousForDownhill =
                                    FormatSecondsToString(dbCheckpoint.Time.FromPreviousForDownhill),
                TimeFromPreviousForUphill =
                                    FormatSecondsToString(dbCheckpoint.Time.FromPreviousForUphill),
                TimeOptimalArrival = FormatSecondsToString(dbCheckpoint.Time.OptimalArrival),
                TimeOptimalDeparture = FormatSecondsToString(dbCheckpoint.Time.OptimalDeparture),
                Tasks = new List<TaskViewModel>()
            };


            var tasksOnThisCheckpoint =
                db.Tasks.Where(
                    x =>
                    x.Station.Id == dbCheckpoint.Station.Id &&
                    x.TaskToCategories.Any(y => y.Category.Id == dbCheckpoint.Track.Category.Id));
            foreach (var task in tasksOnThisCheckpoint)
            {
                model.Tasks.Add(TaskViewModel(task, db));
            }
            return model;
        }


        public static StationViewModel StationViewModel(Station dbStation)
        {
            if (dbStation == null) return null;
            var model = new StationViewModel
            {
                Id = dbStation.Id,
                Day = dbStation.Day,
                Description = dbStation.Description,
                IsAlive = dbStation.IsAlive,
                IsHidden = dbStation.IsHidden,
                IsFinish = dbStation.IsFinish,
                IsStart = dbStation.IsStart,
                Name = dbStation.Name,
                SiCodeArrival = dbStation.SiCodeArrival,
                SiCodeDeparture = dbStation.SiCodeDeparture,
                Locked = dbStation.Locked,
                DefaultPoints = dbStation.DefaultPoints
            };
            if (dbStation.MapPosition != null)
            {
                model.X = dbStation.MapPosition.X;
                model.Y = dbStation.MapPosition.Y;
                model.Z = dbStation.MapPosition.Z;
                model.ZisDirty = dbStation.MapPosition.ZisDirty;
            }
            if (dbStation.TrackCheckpoints != null)
            {
                model.Tracks = String.Join(", ", dbStation.TrackCheckpoints.Select(x => x.Track.ToString()).ToList());
            }
            return model;
        }

        public static PathViewModel PathViewModel(Path dbPath)
        {
            var pathViewModel = new PathViewModel
            {
                Id = dbPath.Id,
                IsManual = dbPath.IsManual,
                Locked = dbPath.Locked,

                FirstStationId = dbPath.FirstStation.Id,
                FirstStationName = dbPath.FirstStation.Name,
                FirstStationX = dbPath.FirstStation.MapPosition.X,
                FirstStationY = dbPath.FirstStation.MapPosition.Y,
                FirstStationZ = dbPath.FirstStation.MapPosition.Z,
                FirstStationZisDirty = dbPath.FirstStation.MapPosition.ZisDirty,

                SecondStationId = dbPath.SecondStation.Id,
                SecondStationName = dbPath.SecondStation.Name,
                SecondStationX = dbPath.SecondStation.MapPosition.X,
                SecondStationY = dbPath.SecondStation.MapPosition.Y,
                SecondStationZ = dbPath.SecondStation.MapPosition.Z,
                SecondStationZisDirty = dbPath.SecondStation.MapPosition.ZisDirty,

                AutomaticDataDistance = dbPath.AutomaticData.Distance,
                AutomaticDataDownhill = dbPath.AutomaticData.Downhill,
                AutomaticDataUphill = dbPath.AutomaticData.Uphill,
                AutomaticDataZisDirty = dbPath.AutomaticData.ZisDirty,
                AutomaticDataZlist = dbPath.AutomaticData.Zlist,

                ManualDataDistance = dbPath.ManualData.Distance,
                ManualDataZlist = dbPath.ManualData.Zlist,
                ManualDataDownhill = dbPath.ManualData.Downhill,
                ManualDataUphill = dbPath.ManualData.Uphill,
                ManualDataZisDirty = dbPath.ManualData.ZisDirty,

                PathPoints = new List<PathPointViewModel>(),

            };
            if (pathViewModel.IsManual)
            {
                pathViewModel.ValidDataDistance = dbPath.ManualData.Distance;
                pathViewModel.ValidDataZlist = dbPath.ManualData.Zlist;
                pathViewModel.ValidDataDownhill = dbPath.ManualData.Downhill;
                pathViewModel.ValidDataUphill = dbPath.ManualData.Uphill;
                pathViewModel.ValidDataZisDirty = false;
            }
            else
            {
                pathViewModel.ValidDataDistance = dbPath.AutomaticData.Distance;
                pathViewModel.ValidDataZlist = dbPath.AutomaticData.Zlist;
                pathViewModel.ValidDataDownhill = dbPath.AutomaticData.Downhill;
                pathViewModel.ValidDataUphill = dbPath.AutomaticData.Uphill;
                pathViewModel.ValidDataZisDirty = dbPath.AutomaticData.ZisDirty;

            }
            foreach (var point in dbPath.PathPoints.OrderBy(x => x.No))
            {
                pathViewModel.PathPoints.Add(new PathPointViewModel
                {
                    Id = point.Id,
                    Locked = point.Locked,
                    No = point.No,
                    MapPositionX = point.MapPosition.X,
                    MapPositionY = point.MapPosition.Y,
                    MapPositionZ = point.MapPosition.Z,
                    MapPositionZisDirty = point.MapPosition.ZisDirty
                });
            }
            return pathViewModel;
        }

        public static TrackViewModel TrackViewModel(Track dbTrack, Marvin20Db db)
        {
            var tm = new TrackViewModel();
            FillTrackViewmodel(tm, dbTrack);
            return tm;
        }



        public static TrackWithCheckpointsViewModel TrackWithCheckpointsViewModel(Track dbTrack, Marvin20Db db)
        {
            var tm = new TrackWithCheckpointsViewModel();
            FillTrackViewmodel(tm, dbTrack);
            tm.TrackCheckpoints = new List<TrackCheckpointViewModel>();
            tm.RemainingStations = new List<StationViewModel>();
            foreach (var dbCheckpoint in dbTrack.TrackCheckpoints.OrderBy(x => x.No))
            {
                tm.TrackCheckpoints.Add(TrackCheckpointViewModel(dbCheckpoint, db));
            }
            var cps = db.Stations
                .Where(x => x.Day == dbTrack.Day)
                .OrderByDescending(x => x.IsStart)
                .ThenBy(x => x.IsFinish)
                .ThenBy(x => x.Name);
            foreach (var station in cps)
            {
                if (dbTrack.TrackCheckpoints.Any(x => x.Station == station)) continue;
                tm.RemainingStations.Add(StationViewModel(station));
            }
            return tm;
        }

        public static CategoryViewModel CategoryViewModel(Category category)
        {
            if (category == null) return null;
            var cat = new CategoryViewModel
            {
                Id = category.Id,
                Name = category.Name,
                Abbr = category.Abbr,
                Active = category.Active,
                No = category.No,
                TeamCount = category.Teams?.Count ?? 0,
                Color = category.Color,
                Speed = category.Speed,
                UphillSpeed = category.UphillSpeed,
                DownhillSpeed = category.DownhillSpeed,
                TrackCount = category.Tracks?.Count ?? 0,
                Tracks = FillTracksForCategory(category),
                MaxPoints=category.MaxPoints,
                MaxPointsDescription = category.MaxPointsDescription
            };
            return cat;
        }

        private static Dictionary<int, string> FillTracksForCategory(Category category)
        {
            var tracks = new Dictionary<int, string>();
            if (category.Tracks != null) foreach (var track in category.Tracks.OrderBy(x => x.Day))
            {
                tracks.Add(track.Id, $"Dan {track.Day}");
            }
            return tracks;

        }

        /************ private stuff *************/




        private static void FillTrackViewmodel(TrackViewModel tm, Track dbTrack)
        {
            tm.Id = dbTrack.Id;
            tm.Locked = dbTrack.Locked;
            tm.CategoryId = dbTrack.Category.Id;
            tm.CategoryNo = dbTrack.Category.No;
            tm.CategoryAbbr = dbTrack.Category.Abbr;
            tm.CumulativeDistance = dbTrack.CumulativeDistance;
            tm.CumulativeDownhill = dbTrack.CumulativeDownhill;
            tm.CumulativeUphill = dbTrack.CumulativeUphill;
            tm.OptimalTime = FormatSecondsToString(dbTrack.OptimalTime);
            tm.OptimalPathTime = FormatSecondsToString(dbTrack.OptimalPathTime);
            tm.TrackCheckpointsCount = dbTrack.TrackCheckpoints.Count;
            tm.Day = dbTrack.Day;
            tm.MinTimeForSpeedTrial = FormatSecondsToString(dbTrack.MinTimeForSpeedTrial);
        }

        private static string FormatSecondsToString(float seconds)
        {
            return FormatSecondsToString((long)seconds);
        }

        private static string FormatSecondsToString(long? seconds)
        {
            if (!seconds.HasValue) return "";
            return FormatSecondsToString(seconds.Value);
        }
        private static string FormatSecondsToString(long seconds)
        {
            var minusSign = "";
            if (seconds < 0)
            {
                seconds = -seconds;
                minusSign = "-";
            }
            var secs = seconds;
            var hours = secs / 3600;
            secs = secs - hours * 3600;
            var minutes = secs / 60;
            secs = secs - minutes * 60;
            return String.Format("{3}{0}:{1:00}:{2:00}", hours, minutes, secs, minusSign);
            //return hours == 0
            //    ? String.Format("{0}:{1:00}", minutes, secs)
            //    : String.Format("{0}:{1:00}:{2:00}", hours, minutes, secs);
        }

        public static TaskViewModel TaskViewModel(Task dbTask, Marvin20Db db)
        {
            var task = new TaskViewModel
            {
                Abbr = dbTask.Abbr,
                AfterFinish = dbTask.AfterFinish,
                BeforeStart = dbTask.BeforeStart,
                CalculatingAlgorythmId = dbTask.CalculatingAlgorythmId,
                Day = dbTask.Day,
                Description = dbTask.Description,
                FirstEntryName = dbTask.FirstEntryName,
                Id = dbTask.Id,
                Locked = dbTask.Locked,
                Name = dbTask.Name,
                OnTrack = dbTask.OnTrack,
                SecondEntryIsTime = dbTask.SecondEntryIsTime,
                SecondEntryName = dbTask.SecondEntryName,
                MaxPoints = dbTask.MaxPoints,
                Station = StationViewModel(dbTask.Station),
                TimeForTaskString = (new TimeSpan(0, 0, dbTask.TimeForTask)).ToString(@"m\:ss"),
                FirstEntryDefault = dbTask.FirstEntryDefault.ToString(),
                FirstEntryMax = dbTask.FirstEntryMax.ToString(),
                FirstEntryMin = dbTask.FirstEntryMin.ToString(),
            };

            if (dbTask.SecondEntryName != "")
            {
                if (dbTask.SecondEntryIsTime)
                {
                    task.SecondEntryDefault = (new TimeSpan(0, 0, dbTask.SecondEntryDefault)).ToString(@"m\:ss");
                    task.SecondEntryMax = (new TimeSpan(0, 0, dbTask.SecondEntryMax)).ToString(@"m\:ss");
                    task.SecondEntryMin = (new TimeSpan(0, 0, dbTask.SecondEntryMin)).ToString(@"m\:ss");
                }
                else
                {
                    task.SecondEntryDefault = dbTask.SecondEntryDefault.ToString();
                    task.SecondEntryMax = dbTask.SecondEntryMax.ToString();
                    task.SecondEntryMin = dbTask.SecondEntryMin.ToString();
                }
            }



            task.Categories = new Dictionary<int, string>();
            task.LockedCategories = new Dictionary<int, string>();
            foreach (var taskToCategory in dbTask.TaskToCategories)
            {
                task.Categories.Add(taskToCategory.Category.Id, taskToCategory.Category.Abbr);
                if (taskToCategory.Locked)
                    task.LockedCategories.Add(taskToCategory.Category.Id, taskToCategory.Category.Abbr);
            }
            return task;
            //     TaskToCategories       = dbTask.TaskToCategories


        }

        public static RunTrackViewModel RunTrackViewModel(RunTrack dbModel)
        {
            var model = new RunTrackViewModel
            {
                Id = dbModel.Id,
                Locked = dbModel.Locked,

                Day = dbModel.Day,

                StartTime = TrackTimeViewModel(dbModel.StartTime),
                FinishTime = TrackTimeViewModel(dbModel.FinishTime),
                TimeOnTrack = FormatSecondsToString(dbModel.TimeOnTrack),
                TimeForPath = FormatSecondsToString(dbModel.TimeForPath),
                TimeDelayForPath = FormatSecondsToString(dbModel.TimeDelayForPath),
                TimeForTask = FormatSecondsToString(dbModel.TimeForTask),
                TimeForWait = FormatSecondsToString(dbModel.TimeForWait),

                CheckpointsFound = dbModel.CheckpointsFound,
                PointsForCheckpoints = dbModel.PointsForCheckpoints,
                PointsForTime = dbModel.PointsForTime,
                TrackPoints = dbModel.TrackPoints,

                SpeedTrialStart = TrackTimeViewModel(dbModel.SpeedTrialStart),
                SpeedTrialFinish = TrackTimeViewModel(dbModel.SpeedTrialFinish),
                SpeedTrialTimeOnTrack = FormatSecondsToString(dbModel.SpeedTrialTimeOnTrack),

                SpeedTrialPoints = dbModel.SpeedTrialPoints,
            };
            if (dbModel.SpeedTrialTimeOnTrack.HasValue && dbModel.Track.MinTimeForSpeedTrial.HasValue)
                model.SpeedTrialTimeDelay =
                    FormatSecondsToString(
                        dbModel.SpeedTrialTimeOnTrack.Value -
                        dbModel.Track.MinTimeForSpeedTrial.Value);

            return model;
        }


        public static RunTrackCheckpointViewModel RunTrackCheckpointViewModel(RunTrackCheckpoint dbModel, Marvin20Db db)
        {
            return new RunTrackCheckpointViewModel
            {
                Id = dbModel.Id,
                Locked = dbModel.Locked,
                No = dbModel.No,

                IsRejectedManualy = dbModel.IsRejectedManualy,
                IsRejectedManualyReason = dbModel.IsRejectedManualyReason,

                IsConfirmedManualy = dbModel.IsConfirmedManualy,
                IsConfirmedManualyReason = dbModel.IsConfirmedManualyReason,

                Arrival = TrackTimeViewModel(dbModel.Arrival),
                Departure = TrackTimeViewModel(dbModel.Departure),
                TimeOnPath = FormatSecondsToString(dbModel.TimeOnPath),
                TimeWaitHere = FormatSecondsToString(dbModel.TimeWaitHere),
                TimeTotal = FormatSecondsToString(dbModel.TimeTotal),

                IsConfirmedByArrival = dbModel.IsConfirmedByArrival,
                IsConfirmedByTask = dbModel.IsConfirmedByTask,

                Points = dbModel.Points,
                SiPunchArrivalId =
                                dbModel.SiPunchArrival?.Id,
                SiPunchDepartureId =
                                dbModel.SiPunchDeparture?.Id,
                TrackCheckpoint = TrackCheckpointViewModel(dbModel.TrackCheckpoint, db)
            };
        }

        public static TrackTimeViewModel TrackTimeViewModel(TrackTime tt)
        {
            var model = new TrackTimeViewModel();
            if (tt.Predicted.HasValue)
                model.Predicted =
                    tt.Predicted.Value.ToString("H:mm:ss");
            if (tt.MaxPredicted.HasValue)
                model.MaxPredicted =
                    tt.MaxPredicted.Value.ToString("H:mm:ss");
            if (tt.Manual.HasValue)
                model.Manual =
                    tt.Manual.Value.ToString("H:mm:ss");
            if (tt.FromSi.HasValue)
                model.FromSi =
                    tt.FromSi.Value.ToString("H:mm:ss");
            return model;
        }


        public static SiReadViewModel SiReadViewModel(SiRead siRead)
        {
            return new SiReadViewModel
            {
                Check = FormatDateTimeToHms(siRead.Check),
                Clear = FormatDateTimeToHms(siRead.Clear),
                Day = siRead.Day,
                Finish = FormatDateTimeToHms(siRead.Finish),
                HasBeenProcessed = siRead.HasBeenProcessed,
                Id = siRead.Id,
                Locked = siRead.Locked,
                ReadAt = FormatDateTimeToHms(siRead.ReadAt),
                SiCode = siRead.SiCode,
                Start = FormatDateTimeToHms(siRead.Start),
            };

        }

        internal static string FormatDateTimeToHms(DateTime? dt)
        {
            return dt == null
                ? ""
                : dt.Value.ToString("H:mm:ss");
        }

        public static SiPunchViewModel SiPunchViewModel(SiPunch dbPunch)
        {
            return new SiPunchViewModel
            {
                Id = dbPunch.Id,
                Locked = dbPunch.Locked,
                No = dbPunch.No,
                IsValid = (dbPunch.RunTrackCheckpoint != null),
                ValidFor =
                                dbPunch.RunTrackCheckpoint == null
                                ? ""
                                : dbPunch.RunTrackCheckpoint.TrackCheckpoint.Name,
                SiCode = dbPunch.SiCode,
                Time = FormatDateTimeToHms(dbPunch.Time)
            };

        }

        public static ViewModelForTrackTables ViewModelForTrackTables(Track dbTrack, Marvin20Db db)
        {
            var result = new ViewModelForTrackTables();
            var runTracks = dbTrack.RunTracks.ToList();
            foreach (var runTrack in runTracks)
            {
                var table = new ViewModelForTrackTable
                {
                    No = runTrack.Team.No,
                    CategoryName = runTrack.Track.Category.Name,
                    CategoryAbbr = runTrack.Track.Category.Abbr,
                    PredictedStart = FormatDateTimeToHms(runTrack.StartTime.Predicted)
                };

                foreach (var runTrackCheckpoint in runTrack.RunTrackCheckpoints.OrderBy(x => x.No))
                {
                    var model = RunTrackCheckpointViewModel(runTrackCheckpoint, db);
                    table.RunTrackCheckPoints.Add(model);




                }
                result.Tables.Add(table);
            }
            return result;
        }

        public static StationDetailsViewModel StationDetailsViewModel(Station station, Marvin20Db db)
        {
            var model = new StationDetailsViewModel
            {
                Name = station.Name,
                Day = station.Day,
                DefaultPoints = station.DefaultPoints,
                Description = station.Description,
                IsStart = station.IsStart,
                IsFinish = station.IsFinish,
                IsHidden = station.IsHidden
            };

            model.IsFinish = station.IsFinish;
            model.SiCodeArrival = station.SiCodeArrival;
            model.SiCodeDeparture = station.SiCodeDeparture;

            var lines = new List<StationDetailsLineViewModel>();

            foreach (var trackCheckpoint in station.TrackCheckpoints)
            {

                var runTrackCheckpoints = db.RunTrackCheckpoints
                    .Where(x => x.TrackCheckpoint.Id == trackCheckpoint.Id)
                    .ToList();
                foreach (var runTrackCheckpoint in runTrackCheckpoints)
                {
                    var line = new StationDetailsLineViewModel
                    {
                        TeamNo = runTrackCheckpoint.RunTrack.Team.No,
                        CategoryAbbr = trackCheckpoint.Track.Category.Abbr,
                        CheckPointName = trackCheckpoint.Name,
                        Arrival = FormatDateTimeToHms(runTrackCheckpoint.Arrival.Predicted),
                        ArrivalMax = FormatDateTimeToHms(runTrackCheckpoint.Arrival.MaxPredicted)
                    };

                    lines.Add(line);
                }
            }

            model.Lines = lines.OrderBy(x => x.Arrival).ThenBy(x => x.ArrivalMax).ThenBy(x => x.TeamNo).ToList();
            return model;
        }
    }

}
