﻿using System.Data.Entity;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.Dao.DbInitializers;

namespace Mrtn.Marvin21.Bl
{
    public static class Initalizer
    {
        private static bool _isInitialized = false;
        public static void InitalizeDatabase()
        {
            if (_isInitialized) return;

            // Database.SetInitializer(new NewMarvin20DbInitializer());
            // Database.SetInitializer(new OnChangeMarvin20DbInitializer());
             Database.SetInitializer<Marvin20Db>(null);
             _isInitialized = true;
        }
    }
}
