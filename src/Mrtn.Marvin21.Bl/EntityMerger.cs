﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Bl
{
    public static class EntityMerger
    {
        public static void MergeIgnoringId(Object destination, Object source)
        {
            if (destination == null) return;
            if (source == null) return;
            var destinationProperties = destination.GetType().GetProperties();
            foreach (var propertyInfo in source.GetType().GetProperties())
            {
                var info = propertyInfo;
                if (info.Name == "Id") continue;
                var prop = destinationProperties.Where(x => x.Name == info.Name).FirstOrDefault();
                if (prop == null) continue;
                prop.SetValue(destination, info.GetValue(source, null), null);

            }
        }
        public static void Merge(Object destination, Object source)
        {
            if (destination == null) return;
            if (source == null) return;
            var destinationProperties = destination.GetType().GetProperties();
            foreach (var propertyInfo in source.GetType().GetProperties())
            {
                var info = propertyInfo;
                var prop = destinationProperties.Where(x => x.Name == info.Name).FirstOrDefault();
                if (prop == null) continue;
                prop.SetValue(destination, info.GetValue(source, null), null);

            }
        }
    }
}
