using System.Collections.Generic;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Tasks;

namespace Mrtn.Marvin21.Bl.Interface.Tasks
{
    public interface ITaskService
    {
        // basic getter
        List<TaskViewModel> Get();

        // adding, editing
        TaskEditModel Edit(int? id);
        SuccessResponse Save(TaskEditModel model);

        // actions
        SuccessResponse Lock(int id);
        SuccessResponse Unlock(int id);

        //SuccessResponse Deactivate(int id);
        //SuccessResponse Activate(int id);
        //SuccessResponse SetSiCode(int id, string siCode);
        //SuccessResponse SetNo(int id, int? no);

        //// Aditional services
        //int GetInactiveTeamsCount();
        //FiltersForTeams GetDataForFilters();


        bool Add(string name, string abbr, string description, int day, 
            bool beforeStart, bool onTrack, bool afterFinish, string firstEntryName, int firstEntryMin, int firstEntryMax, 
            int firstEntryDefault, string secondEntryName, int secondEntryMin, int secondEntryMax, int secondEntryDefault, bool secondEntryIsTime, int timeForTask, int calculatingAlgorythmId, bool locked, object trackCheckpoint, object station, string categories);

    }
}