using System.Collections.Generic;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Si;

namespace Mrtn.Marvin21.Bl.Interface.Si
{
    public interface ISiChipService
    {
        // basic getter
        List<SiChipViewModel> Get(SiChipQueryModel queryModel);

        // adding, editing, deleting
        SiChipEditModel Edit(int? id);
        SuccessResponse Save(SiChipEditModel model);
        SuccessResponse Delete(int id);

        // actions
        SuccessResponse Deactivate(int id);
        SuccessResponse Activate(int id);

        // Aditional services
        SuccessResponse AddPacket(string siCodes, bool @private);
    }
}