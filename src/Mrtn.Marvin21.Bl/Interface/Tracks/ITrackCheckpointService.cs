﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Interface.Tracks
{
    public interface ITrackCheckpointService
    {
        List<TrackCheckpointViewModel> Get(TrackCheckpointQueryModel queryModel);
        SuccessResponse Add(int id, int stationId);
        SuccessResponse MoveUp(int id);
        SuccessResponse MoveDown(int id);
        SuccessResponse Delete(int id);

        SuccessResponse Lock(int id);
        SuccessResponse Unlock(int id);
        TrackCheckpointEditModel Edit(int id);
        SuccessResponse Save(TrackCheckpointEditModel model);
        SuccessResponse Save(TrackCheckpointEditModel model, double timeFromPrevious);
    }
}
