﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.Model.RunTrack;
using Mrtn.Marvin21.Model.Teams;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Interface.Tracks
{
    public interface IStationService
    {
        // basic getter
        List<StationViewModel> Get();
        List<StationViewModel> Get(StationQueryModel queryModel);

        // adding, editing
        StationEditModel Edit(int id);
        SuccessResponse Save(StationEditModel model);

        // actions
        SuccessResponse ChangeCoordinates(int id, float x, float y);
        SuccessResponse Lock(int id);
        SuccessResponse Unlock(int id);
        SuccessResponse LockAll(int day);
        SuccessResponse UnlockAll(int day);

        // Aditional services
//        int GetInactiveTeamsCount();
        SuccessResponse Add(string type, float x, float y, int day);
        //List<StationViewModel> GetAll();
        StationEditModel GetModelForAdd(int day);
        SuccessResponse Delete(int id);
        StationDetailsViewModel GetModelForStation(int id);
    }
}
