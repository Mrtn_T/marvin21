﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.Model.Teams;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Interface.Tracks
{
    public interface IPathPointService
    {
        // basic getter
        //        List<PathViewModel> Get(PathQueryModel queryModel);

        // adding, editing
        SuccessResponse Add(int pathId, int putAfter, float x, float y);
        SuccessResponse Move(int id, float x, float y);

        PathPointEditModel Edit(int id);
        SuccessResponse Save(PathPointEditModel model);

        // actions
        SuccessResponse LockAll(int pathId);
        SuccessResponse UnlockAll(int pathId);
        SuccessResponse Lock(int id);
        SuccessResponse Unlock(int id);

        //        // Aditional services
        ////        int GetInactiveTeamsCount();
        //        SuccessResponse Add(string type, float x, float y, int day);
        SuccessResponse Delete(int id);
    }
}
