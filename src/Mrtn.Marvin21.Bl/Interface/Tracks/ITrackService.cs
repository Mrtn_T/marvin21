﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Interface.Tracks
{
    public interface ITrackService
    {
        //List<TrackCheckpointViewModel> Get(TrackCheckpointQueryModel queryModel);
        List<TrackViewModel> GetTrackForEachCategorByDay(TrackQueryModel queryModel);
        SuccessResponse Add (int categoryId, int day);
        TrackWithCheckpointsViewModel Get(int id);
        List<TrackViewModel> GetTracks(TrackQueryModel queryModel);
        SuccessResponse Lock(int id);
        SuccessResponse Unlock(int id);
        SuccessResponse RemoveSpeedTrial(int id);
        SuccessResponse SetSpeedTrial(int checkpointId);
    }
}
