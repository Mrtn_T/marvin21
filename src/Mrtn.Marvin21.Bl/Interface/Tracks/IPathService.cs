﻿using System.Collections.Generic;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Interface.Tracks
{
    public interface IPathService
    {
        // basic getter
        List<PathViewModel> Get(PathQueryModel queryModel);

        // adding, editing
        SuccessResponse Add(int id1, int id2);
        PathEditModel Edit(int id);
        SuccessResponse Save(PathEditModel model);

        // actions
        SuccessResponse LockAll(int day);
        SuccessResponse UnlockAll(int day);
        SuccessResponse Lock(int id);
        SuccessResponse Unlock(int id);

        SuccessResponse Invert(int id);
        PathViewModel Get(int id);
    }
}
