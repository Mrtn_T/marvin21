using System.Collections.Generic;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Bl.Interface.Support
{
    public interface ISettingsService
    {
        int GetDays();
        int GetMapDpi();
        int GetMapScale();
        double PixelsPerMeter();
        double MetersPerPixel();
    }
}