using System.Collections.Generic;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Bl.Interface.Teams
{
    public interface ITeamService
    {
        // basic getter
        List<TeamViewModel> Get(TeamQueryModel queryModel);
        
        // adding, editing
        TeamEditModel Edit(int? id);
        SuccessResponse Save(TeamEditModel model);

        // actions
        SuccessResponse Deactivate(int id);
        SuccessResponse Activate(int id);
        SuccessResponse SetSiCode(int id, string siCode);
        SuccessResponse SetNo(int id, int? no);
        
        // Aditional services
        int GetInactiveTeamsCount();
        FiltersForTeams GetDataForFilters();
    }
}