﻿using System.Collections.Generic;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Bl.Interface.Teams
{
    public interface IPackService
    {

        // basic getter
        List<PackViewModel> Get(PackQueryModel queryModel);

        // adding, editing
        PackEditModel Edit(int? id);
        SuccessResponse Save(PackEditModel model);

        // actions
        SuccessResponse Deactivate(int id);
        SuccessResponse Activate(int id);

        // Aditional services


        bool AddIfNew(string name, bool active, bool inCompetition, string abbr, string town, bool locked);
    }
}