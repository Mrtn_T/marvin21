using System.Collections.Generic;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Bl.Interface.Teams
{
    public interface ICategoryService
    {
        List<CategoryViewModel> GetAll();
        CategoryEditModel Edit(int id);
        SuccessResponse Save(CategoryEditModel model);
        CategoryViewModel Get(int categoryId);

        void AddCategory(int no, string name, bool active, string abbr, float speed, float uphillSpeed,
            float downhillSpeed, string color, bool locked);
    }
}