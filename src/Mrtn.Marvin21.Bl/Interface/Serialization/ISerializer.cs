﻿using Mrtn.Marvin21.Model.Common;

namespace Mrtn.Marvin21.Bl.Interface.Serialization
{
    public interface ISerializer
    {
        string SerializeTracks();
        string SerializeTeams();
        string SerializeTasks();
        SuccessResponse DeserializeTracks(string myJson);


        SuccessResponse DeserializeTeams(string myJson);
        SuccessResponse DeserializeTasks(string myJson);
        SuccessResponse DeleteAllEntites();
    }
        
}