﻿using System.Collections.Generic;
using Mrtn.Marvin21.Model.Result;

namespace Mrtn.Marvin21.Bl.Interface.Result
{
    public interface IResultService
    {
        List<PackResultViewModel> PackResults();
        TeamResultsViewModel TeamResults();
        TeamResultsViewModel TeamPpResults();
        TeamResultsViewModel TeamTopoResults();
        TeamResultsViewModel TeamTtResults();
        TeamResultsByDayViewModel TeamsByDayResult(int day, bool showInactive = false);
    }
}