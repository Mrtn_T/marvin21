﻿using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Run;

namespace Mrtn.Marvin21.Bl.Interface.Run
{
    public interface IRunTaskService
    {
        RunTasksEditModel GetEditModels(int id);
        SuccessResponse TrySaveCell(int teamId, int taskId, string firstEntry, string secondEntry);
        SuccessResponse Lock(int id);
        SuccessResponse Unlock(int id);
        SuccessResponse LockCategory(int taskId, int categoryId);
        SuccessResponse UnlockCategory(int taskId, int categoryId);
    }
}