﻿using Mrtn.Marvin21.Bl.Services.Run;
using Mrtn.Marvin21.Model.Run;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.RunTrack;
using Mrtn.Marvin21.Model.RunTrack.Support;

namespace Mrtn.Marvin21.Bl.Interface.Run
{
    public interface IRunTrackService
    {
        RunTrackViewModel Get(int id);
        RunTrackViewModel Add(int teamId, int day);
        SuccessResponse AddAll(int day);
        
        PredictedStartTimeEditModel GetModelForPredictedStartTime(int id);
        SuccessResponse Save(PredictedStartTimeEditModel model);

        RejectedManualyEditModel EditRejectManualy(int id);
        SuccessResponse Save(RejectedManualyEditModel model);
        
        ConfirmManualyEditModel EditConfirmManualy(int id);
        SuccessResponse Save(ConfirmManualyEditModel model);

        ManualDepartureEditModel ManualDeparture(int id);
        SuccessResponse Save(ManualDepartureEditModel model);
        
        ManualArrivalEditModel ManualArrival(int id);
        SuccessResponse Save(ManualArrivalEditModel model);
        
        SuccessResponse Lock(int id);
        SuccessResponse Unlock(int id);
        SuccessResponse DeleteResults(int id);
        ViewModelForTrackTables GetModelForTrackTables(int id);
    }
}