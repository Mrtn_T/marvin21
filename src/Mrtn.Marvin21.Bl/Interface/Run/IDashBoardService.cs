﻿using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Run;

namespace Mrtn.Marvin21.Bl.Interface.Run
{
    public interface IDashBoardService
    {
        DashBoardViewModel Get(int? day, bool showInactive = false);
        SiReadReportResponse CheckForSiReads(int day);
        SuccessResponse DeleteUnusedSiReads();
        void RecalculateTeams();
    }
}
