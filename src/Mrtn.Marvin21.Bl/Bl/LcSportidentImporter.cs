﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.Model.Common;

namespace Mrtn.Marvin21.Bl.Bl
{
    internal static class LcSportidentImporter
    {
        /// <summary>
        /// Ta metoda uvozi podatke iz "lcsportident_evets" baze.
        /// Uvozi podatke iz vseh čipov, ki še niso uvoženi za izbrani dan!
        /// Podatke zapiše v tabelo SiRead in SiPunch
        /// 
        /// </summary>
        /// <param name="day"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static ImportFromSiReadReport ImportFromSiRead(int day, Marvin20Db db)
        {
            var report = new ImportFromSiReadReport();
            var cs = ConfigurationManager.ConnectionStrings["SiConnection"].ConnectionString;
            report.ConfigurationString = cs;
            try
            {
                int idEvent;
                using (var connection = GetConnection(cs))
                {
                    // najprej poiščemo event z "event_foreign_id" == day
                    using (
                        var command = connection.GetCommand(
                            "SELECT id_event FROM lcevents where event_foreign_id=" + day + "", CommandType.Text))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                idEvent = Int32.Parse(reader["id_event"].ToString());
                            }
                            else
                            {
                                throw new Exception(
                                    String.Format(
                                    "V bazi lcsportident_events v tabeli lcevents ne najdem dogodka s event_foreign_key={0}", day));
                            }
                        }
                    }


                    //create the command    
                    using (var command = connection
                        .GetCommand("SELECT * FROM stamps WHERE id_event = " + idEvent + " ORDER BY id_stamp", CommandType.Text))
                    {

                        // initialize the reader and execute the command 
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            var reportLine = new SiReadReportLine();
                            report.Lines.Add(reportLine);

                            var currentChip = "";
                            var ignoreTheRest = false;
                            SiRead siRead = null;
                            var counter = 0;
                            var lastTime = new TimeSpan();
                            var addHours = 0;
                            while (reader.Read())
                            {
                                if (reader["stamp_card_id"].ToString() != currentChip)
                                {
                                    // Sem priletim, če je to PRVI zapis od NOVEGA čipa
                                    if (siRead != null)
                                    {
                                        // če sem v prejšnjih iteracijah kaj  prebral in ustvaril entiteto siRead, 
                                        // jo v tej točki POSPRAVIM V BAZO
                                        reportLine.Text += String.Format("");
                                        reportLine = new SiReadReportLine();
                                        report.Lines.Add(reportLine);
                                        db.SaveChanges();
                                        siRead = null;
                                    }
                                    // Vzamem ident naslednjega čipa
                                    currentChip = reader["stamp_card_id"].ToString();
                                    reportLine.SiChip = currentChip;

                                    // preverim, če že obstaja zapis za ta dan
                                    siRead = db.SiReads.FirstOrDefault(x => x.SiCode == currentChip && x.Day == day);
                                    if (siRead != null)
                                    {
                                        // če že obstaja, bom ignoriral vse njegove punche...
                                        reportLine.IsNew = false;
                                        reportLine.Text += String.Format("{0} že obstaja, ignoriram.", currentChip);

                                        report.Lines.Remove(reportLine);
                                        //reportLine = new SiReadReportLine();
                                        //report.Lines.Add(reportLine);
                                        ignoreTheRest = true;
                                        continue;
                                    }

                                    // Gre za nov zapis, se pravi začnem prepisovat v svojo bazo
                                    siRead = new SiRead
                                    {
                                        SiCode = reader["stamp_card_id"].ToString(),
                                        ReadAt = (DateTime?)reader["stamp_readout_datetime"],
                                        SiPunches = new List<SiPunch>(),
                                        Day = day
                                    };
                                    reportLine.IsNew = true;
                                    reportLine.SiChip = currentChip;
                                    reportLine.Text += String.Format("berem: ");

                                    //resetiram dodatne ure
                                    lastTime = new TimeSpan();
                                    addHours = 0;

                                    db.SiReads.Add(siRead);
                                    counter = 1;
                                    ignoreTheRest = false;
                                }
                                if (ignoreTheRest)
                                {
                                    // če že obstaja, še naprej ignoriram vse njegove punche...
                                    continue;
                                }

                                // Analiza trenutnega puncha
                                var tsFromLcDb = ((DateTime)reader["stamp_punch_datetime"]).TimeOfDay;

                                //// toDo: sranje z zgodnjimi punchi
                                //var incremetHour = (dt.Hour < 9);
                                //if (incremetHour)
                                //{
                                //    dt = dt.AddHours(12);
                                //    reportLine.Text += "*";
                                //}

                                //sranje z zgodnjimi urami
                                var tsCorrected = tsFromLcDb.Add(new TimeSpan(0, addHours, 0, 0));
                                if (tsCorrected < lastTime)
                                {
                                    addHours += 12;
                                    reportLine.Text += "(+12)";
                                    tsCorrected = tsFromLcDb.Add(new TimeSpan(0, addHours, 0, 0));
                                }
                                lastTime = tsCorrected;
                                var stampControlMode = (int)reader["stamp_control_mode"];
                                if (stampControlMode == 3 || stampControlMode == 10)
                                {
                                    //Hack, martin 5.1.2018: letos je start kar naenkrat postal 10 ?!?
                                    // stampControlMode 3 pomeni START
                                    siRead.Start = ToNullableDateTime(tsCorrected);
                                    reportLine.Text += (String.Format("start, "));
                                }
                                else if (stampControlMode == 4)
                                {
                                    // stampControlMode 4 pomeni FINISH
                                    siRead.Finish = ToNullableDateTime(tsCorrected);
                                    reportLine.Text += (String.Format("cilj "));
                                    ignoreTheRest = true;
                                }
                                else if (stampControlMode == 2)
                                {
                                    // stampControlMode 2 pomeni PUNCH
                                    var siPunch = new SiPunch
                                    {
                                        No = counter,
                                        SiCode = reader["stamp_control_code"].ToString(),
                                        //SiRead = siRead,
                                        Time = ToDateTime(tsCorrected)
                                    };
                                    reportLine.Text += (String.Format("{0}, ", siPunch.SiCode));
                                    // Punch dodam samo če ni start ali cilj!
                                    counter++;
                                    siRead.SiPunches.Add(siPunch);
                                }
                            }
                            db.SaveChanges();

                        } // end using (SqlDataReader reader = command.ExecuteReader())
                    } // end using (var command = connection.GetCommand("SELECT * FROM stamps ORDER BY id_stamp", CommandType.Text))
                } //end using (var connection = GetConnection(cs))
                report.Success = true;
            }
            catch (Exception ee)
            {
                report.Success = false;
                report.ErrorMessage = ee.Message;

                throw;
            }
            return report;
        }

        private static DateTime ToDateTime(TimeSpan tsCorrected)
        {
            return new DateTime(2000, 1, 1).Add(tsCorrected);
        }

        private static DateTime? ToNullableDateTime(TimeSpan tsCorrected)
        {
            return new DateTime(2000, 1, 1).Add(tsCorrected);
        }

        private static SqlConnection GetConnection(string connectionString)
        {
            SqlConnection connection = null;
            try
            {
                connection = new SqlConnection(connectionString);
                connection.Open();
            }
            catch (Exception ex)
            {
                //ex should be written into a error log

                // dispose of the connection to avoid connections leak
                if (connection != null)
                {
                    connection.Dispose();
                }
                throw ex;
            }
            return connection;
        }

        private static SqlCommand GetCommand(this SqlConnection connection, string commandText, CommandType commandType)
        {
            SqlCommand command = connection.CreateCommand();
            command.CommandTimeout = connection.ConnectionTimeout;
            command.CommandType = commandType;
            command.CommandText = commandText;
            return command;
        }
    }
}