﻿using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Teams;

namespace Mrtn.Marvin21.Bl.Bl
{
    static class CategoryCalculator
    {
        /// <summary>
        /// Martin, 10.1.2020:
        /// Ta metoda je dodana za izračun skupnega števila točk za neko kategorijo
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        /// <param name="category"></param>
        /// <param name="db"></param>
        public static void RecalculateMaxPoints(Category category, Marvin20Db db)
        {
            if (db == null) return;
            if (category == null) return;

            CalculateMaxPoints(category, out var maxpoints, out var description);
            category.MaxPoints = maxpoints;
            category.MaxPointsDescription = description;
        }







        internal static void CalculateMaxPoints(Category category,
            out int points,
            out string description)
        {
            description = "";
            points = 0;
            if (category == null) return;
            var strs = new List<string>();
            foreach (var ttc in category.TaskToCategories)
            {
                if (ttc.Task ==null || ttc.Task.MaxPoints == 0)
                    continue;
                points += ttc.Task.MaxPoints;
                strs.Add($"{ttc.Task.Name}:{ttc.Task.MaxPoints}");
            }
            if (category.Tracks != null)
                foreach (var track in category.Tracks.OrderBy(x => x.Day))
                {
                    var maxKtPoints = track.TrackCheckpoints.Select(x => x.Points).Sum();
                    if (maxKtPoints > 0)
                    {
                        points += maxKtPoints;
                        strs.Add($"Proga-D{track.Day}:{maxKtPoints}");
                    }

                    if (track.TrackCheckpoints.Any(x => x.SpeedTrial.IsStart))
                    {
                        points += 60;
                        strs.Add($"HE-D{track.Day}:{60}");
                    }
                }
            description = string.Join(", ", strs);
        }
    }
}