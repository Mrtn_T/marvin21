﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.Bl.Services;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Tracks;

namespace Mrtn.Marvin21.Bl.Bl
{
    static class TrackCalculator
    {
        /// <summary>
        /// Ta metoda preimenuje KT-je na podani poti. poimenuje jih takole:
        ///  - Točka, ki je start, se imenuje "Start"
        ///  - Točka, ki je cilj, se imenuje "Cilj" :-)
        ///  - Ostale točke dobijo po vrsti imena: KT1, KT2, KT3,...
        /// </summary>
        /// <param name="track"></param>
        public static void RenameTracksCheckpoints(Track track)
        {
            var i = 1;
            var no = 1;
            foreach (var checkpoint in track.TrackCheckpoints.OrderBy(x => x.No))
            {
                checkpoint.No = no;
                no++;
                if (checkpoint.Station.IsStart) checkpoint.Name = "Start";
                else if (checkpoint.Station.IsFinish) checkpoint.Name = "Cilj";
                else
                {
                    checkpoint.Name = "KT" + i;
                    i++;
                }
            }
        }

        public static void RecalculateTrackDistances(Track track, Marvin20Db db)
        {
            //izhodišče: 
            track.CumulativeDistance = 0;
            track.CumulativeDownhill = 0;
            track.CumulativeUphill = 0;

            for (var i = 1; i < track.TrackCheckpoints.Count; i++)
            {
                //v to zanko vsopim, če sta VSAJ dve točki. 
                var previousCheckpoint = track.TrackCheckpoints.Where(x => x.No == i).Single();
                var previousStation = previousCheckpoint.Station;
                var checkpoint = track.TrackCheckpoints.Where(x => x.No == i + 1).Single();
                var station = checkpoint.Station;
                var path = PathCalculator.GetOrCreatePathBetween(previousStation, station, db);


                if (path.IsManual)
                {
                    checkpoint.FromPrevious.Distance = path.ManualData.Distance;
                    checkpoint.FromPrevious.Downhill = path.ManualData.Downhill;
                    checkpoint.FromPrevious.Uphill = path.ManualData.Uphill;
                    checkpoint.FromPrevious.Zlist = path.ManualData.Zlist;
                    checkpoint.FromPrevious.ZisDirty = path.ManualData.ZisDirty;
                }
                else
                {
                    checkpoint.FromPrevious.Distance = path.AutomaticData.Distance;
                    checkpoint.FromPrevious.Downhill = path.AutomaticData.Downhill;
                    checkpoint.FromPrevious.Uphill = path.AutomaticData.Uphill;
                    checkpoint.FromPrevious.Zlist = path.AutomaticData.Zlist;
                    checkpoint.FromPrevious.ZisDirty = path.AutomaticData.ZisDirty;
                }
                checkpoint.PathFromPrevious = path;
                //checkpoint.Time. //later
                track.CumulativeDistance += checkpoint.FromPrevious.Distance;
                track.CumulativeDownhill += checkpoint.FromPrevious.Downhill;
                track.CumulativeUphill += checkpoint.FromPrevious.Uphill;
                //ToDo: ugotovi, če so podatki o razdaljh zanesljivi in to nekam shrani
            }

        }

        public static string InvertCsv(string zlist)
        {
            if (zlist == null) zlist = "";
            var arr = zlist.Split(',');
            var newArr = new List<string>();
            for (var i = 0; i < arr.Count(); i++)
            {
                var s = arr[arr.Count() - 1 - i];
                newArr.Add(s.Trim());
            }
            return String.Join(", ", newArr);
        }

        /// <summary>
        /// Metoda preračuna časovnico za izbrano pot. Predpostavlja se par stvari:
        ///  - predhodno so bile tako ali drugače določene razdalje (distance, uphill, downihll)
        ///  - v kategoriji so vnešene hitrosti
        /// Če pogoji niso izpolnjeni, se časovnice NE izračunajo
        /// 
        /// </summary>
        /// <param name="track"></param>
        /// <param name="db"></param>
        public static void RecalculateTrackTimes(Track track, Marvin20Db db)
        {
            if (db == null) return;
            if (track == null) return;
            if (track.Category == null) return;
            if (track.Category.Speed <= 0) return;
            if (track.Category.UphillSpeed <= 0) return;
            if (track.Category.DownhillSpeed <= 0) return;
            if (track.TrackCheckpoints == null || track.TrackCheckpoints.Count < 2) return;

            double speedInMetersPerSecond = track.Category.Speed * 1000 / 3600;
            double uphillSpeedInMetersPerSecond = track.Category.UphillSpeed / 3600;
            double downhillSpeedInMetersPerSecond = track.Category.DownhillSpeed / 3600;
            //--------------------------------------------
            // Izvedejo se trije prehodi: 
            //  - V prvem se določijo časi za naloge (checkpoint.Time.ForTask)
            //  - V drugem se izračunajo optimalni časi.
            //  - V tretjem se določi časovnica za individualni umik
            //--------------------------------------------

            //--------------------------------------------
            // prvi prehod, časi za naloge
            //--------------------------------------------
            foreach (var trackCheckpoint in track.TrackCheckpoints.OrderBy(x => x.No))
            {
                if (trackCheckpoint.Station == null) continue;
                var tasksOnThisCheckpoint =
                    db.Tasks.Where(
                        x =>
                        x.Station.Id == trackCheckpoint.Station.Id &&
                        x.TaskToCategories.Where(y => y.Category.Id == track.Category.Id).Any());
                var timeForTask = 0;
                foreach (var task in tasksOnThisCheckpoint)
                {
                    timeForTask += task.TimeForTask;
                }
                trackCheckpoint.Time.ForTask = timeForTask;

            }

            //--------------------------------------------
            // drugi prehod, določanje optimalnih časov
            //--------------------------------------------
            double cumulativeTime = 0.0; // skupen čas, skupaj s časi za naloge (Time.ForTask)
            double cumulativePathTime = 0.0; // čas, potreben za pot, brez časov za naloge (Time.ForTask)
            //var cumulativeUphillTime = 0.0;
            //var cumulativeDownhillTime = 0.0;
            //var cumulativeDistanceTime = 0.0;

            //prva točka, start
            var previousCheckpoint = track.TrackCheckpoints.OrderBy(x => x.No).First();
            previousCheckpoint.Time.OptimalArrival = (long)cumulativeTime;
            previousCheckpoint.Time.OptimalDeparture = (long)(previousCheckpoint.Time.ForTask + cumulativeTime);
            cumulativeTime += previousCheckpoint.Time.ForTask; // prištejem čas za naloge
            foreach (var trackCheckpoint in
                track.TrackCheckpoints.OrderBy(x => x.No).Where(trackCheckpoint => trackCheckpoint.No > 1))
            {
                double time;
                if (AppSettings.UseMaps) // če se uporablja zemljevide
                {
                    if (trackCheckpoint.FromPrevious == null) return;
                    if (!trackCheckpoint.FromPrevious.Distance.HasValue) return;

                    // izračunam podatke od prejšnje točke do sem
                    double distanceTime = trackCheckpoint.FromPrevious.Distance.Value / speedInMetersPerSecond;
                    double uphillTime = trackCheckpoint.FromPrevious.Uphill / uphillSpeedInMetersPerSecond;
                    double downhillTime = trackCheckpoint.FromPrevious.Downhill / downhillSpeedInMetersPerSecond;
                    time = distanceTime + uphillTime + downhillTime;

                    // Vpišem izračunane podatke
                    trackCheckpoint.Time.FromPrevious = (int)time;
                    trackCheckpoint.Time.FromPreviousForDistance = (int)distanceTime;
                    trackCheckpoint.Time.FromPreviousForUphill = (int)uphillTime;
                    trackCheckpoint.Time.FromPreviousForDownhill = (int)downhillTime;
                }
                else
                {
                    // če se čase vpisuje ročno...
                    time = trackCheckpoint.Time.FromPrevious;
                }
                // povečam kumulativne čase
                cumulativeTime += time;
                cumulativePathTime += time;
                //cumulativeDistanceTime += distanceTime;
                //cumulativeDownhillTime += distanceTime;
                //cumulativeUphillTime += uphillTime;

                // določim optimalni čas prihoda in odhoda
                trackCheckpoint.Time.OptimalArrival = (long)cumulativeTime;
                cumulativeTime += trackCheckpoint.Time.ForTask;
                trackCheckpoint.Time.OptimalDeparture = (long)cumulativeTime;
            }
            track.OptimalTime = (int?)cumulativeTime;
            track.OptimalPathTime = (int?)cumulativePathTime;

            //--------------------------------------------
            // tretji prehod, časovnica za individualni umik
            //--------------------------------------------

             double maxFactor = AppSettings.MaxFactor;
            // ToDo: preberi iz nastavitev
            double n = Math.Min(1800, cumulativePathTime / maxFactor);
            double k = (maxFactor * cumulativePathTime - n) / cumulativePathTime;
            double cumulativeTaskTime = 0.0;
            cumulativePathTime = 0;
            foreach (var trackCheckpoint in track.TrackCheckpoints.OrderBy(x => x.No))
            {
                cumulativePathTime += trackCheckpoint.Time.FromPrevious;
                trackCheckpoint.Time.MaxFromStart = (long)(k * cumulativePathTime + n + cumulativeTaskTime);
                cumulativeTaskTime += trackCheckpoint.Time.ForTask * AppSettings.TaskFactor;
            }
        }
    }
}
