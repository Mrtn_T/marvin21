﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Services.Support;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.DbModel.Tracks.Support;

namespace Mrtn.Marvin21.Bl.Bl
{
    static class PathCalculator
    {
        /// <summary>
        /// Ta metoda izračuna razdaljo, višine in višinske razlike.
        /// 
        /// Če je Manual, izračuna edinole Uphill in Downhill, če je tako določeno
        /// 
        /// Če ni manual, izvede celoten preračun:
        /// Predpostavlja, da imata start in cilj določne vse tri koordinate
        /// in da imajo vse vmesne točke določeni vsaj X in Y koordinate.
        /// Če česa od tega ni, potem ne naredi nič
        /// </summary>
        /// <param name="path"></param>
        public static void CalculatePathValues(Path path)
        {
            if (path.IsManual) CalculateManualPathValues(path);
            else CalculateAutomaticPathValues(path);
        }
        #region private CalculateAutomaticPathValues, CalculateManualPathValues
        private static void CalculateAutomaticPathValues(Path path)
        {
            //distance is already entered manualy
            if (path.FirstStation == null) return;
            if (!path.FirstStation.MapPosition.X.HasValue) return;
            if (!path.FirstStation.MapPosition.Y.HasValue) return;
            if (!path.FirstStation.MapPosition.Z.HasValue) return;
            if (path.SecondStation == null) return;
            if (!path.SecondStation.MapPosition.X.HasValue) return;
            if (!path.SecondStation.MapPosition.Y.HasValue) return;
            if (!path.SecondStation.MapPosition.Z.HasValue) return;

            var zCoordinates = new List<int>();
            var zIsDirty = path.FirstStation.MapPosition.ZisDirty
                           || path.SecondStation.MapPosition.ZisDirty;
            //izračun razdalje in dvigov/spustov
            var distance = 0.0;
            var uphill = 0.0;
            var downhill = 0.0;
            var previousZ = path.FirstStation.MapPosition.Z.Value;
            float currentZ;
            var previousPosition = path.FirstStation.MapPosition;
            if (path.PathPoints != null) foreach (var point in path.PathPoints.OrderBy(x => x.No))
                {
                    if (!point.MapPosition.X.HasValue) return;
                    if (!point.MapPosition.Y.HasValue) return;
                    distance += MetersBetween(previousPosition, point.MapPosition);
                    previousPosition = point.MapPosition;
                    //Seznam višin
                    if (point.MapPosition.Z.HasValue)
                    {
                        currentZ = point.MapPosition.Z.Value;
                        // ugotovim ali gre gor ali dol
                        if (currentZ < previousZ) downhill = downhill + (previousZ - currentZ);
                        if (currentZ > previousZ) uphill = uphill + (currentZ - previousZ);
                        zCoordinates.Add((int)currentZ);
                        zIsDirty = zIsDirty || point.MapPosition.ZisDirty;
                        previousZ = currentZ;
                    }
                }
            // in še zadnji, do konca:
            distance += MetersBetween(previousPosition, path.SecondStation.MapPosition);
            currentZ = path.SecondStation.MapPosition.Z.Value;
            // ugotovim ali gre gor ali dol
            if (currentZ < previousZ) downhill = downhill + (previousZ - currentZ);
            if (currentZ > previousZ) uphill = uphill + (currentZ - previousZ);

            // potem pa zapišem podatke v path
            path.AutomaticData.Zlist = String.Join(", ", zCoordinates);
            path.AutomaticData.Distance = (int)distance;
            path.AutomaticData.Uphill = (int)uphill;
            path.AutomaticData.Downhill = (int)downhill;
            path.AutomaticData.ZisDirty = zIsDirty;
            //ToDo: shranit je treba še wayDown in wayUp, zIsDirty
        }

        private static void CalculateManualPathValues(Path path)
        {
            //distance is already entered manualy
            if (path.ManualData.ManualUpDown) return; //Uphill, downhill določen ročno

            //uphill in downhill se izračunata iz Zliste
            path.ManualData.Downhill = 0;
            path.ManualData.Uphill = 0;
            //preverim, če imam začetno in končno višino. Če nimam, izhod!
            if (!path.FirstStation.MapPosition.Z.HasValue) return;
            if (!path.SecondStation.MapPosition.Z.HasValue) return;

            //izračunam vzpone/spuste na osnovi zListe
            var uphill = 0.0;
            var downhill = 0.0;
            double previousZ = path.FirstStation.MapPosition.Z.Value;
            double currentZ;
            if (path.ManualData.Zlist == null) path.ManualData.Zlist = "";
            var arr = path.ManualData.Zlist.Split(',');
            foreach (var s in arr)
            {
                if (!Double.TryParse(s, out currentZ)) continue;
                // ugotovim ali gre gor ali dol
                if (currentZ < previousZ) downhill = downhill + (previousZ - currentZ);
                if (currentZ > previousZ) uphill = uphill + (currentZ - previousZ);
                previousZ = currentZ;
            }
            // in še zadnji, do konca:
            currentZ = path.SecondStation.MapPosition.Z.Value;
            // ugotovim ali gre gor ali dol
            if (currentZ < previousZ) downhill = downhill + (previousZ - currentZ);
            if (currentZ > previousZ) uphill = uphill + (currentZ - previousZ);
            path.ManualData.Uphill = (int)uphill;
            path.ManualData.Downhill = (int)downhill;
        }
        #endregion

        /// <summary>
        /// Na osnovi podane poti skreira OBRATNO pot in jo vrne.
        /// POT NI ZAPISANA V BAZO!!!!
        /// </summary>
        /// <param name="path">obstoječa pot iz točke A b točko B</param>
        /// <returns>nova pot iz točke B v točko A</returns>
        public static Path GetInversePath(Path path)
        {
            var inversePath = new Path
            {
                FirstStation = path.SecondStation,
                SecondStation = path.FirstStation,
                IsManual = path.IsManual,
                Locked = path.Locked,
                PathPoints = PathCalculator.DuplicateAndInvert(path.PathPoints),
                ManualData = new PathData
                {
                    Distance = path.ManualData.Distance,
                    Downhill = path.ManualData.Uphill,
                    ManualUpDown = path.ManualData.ManualUpDown,
                    Uphill = path.ManualData.Downhill,
                    ZisDirty = path.ManualData.ZisDirty,
                    Zlist = TrackCalculator.InvertCsv(path.ManualData.Zlist)
                },
                AutomaticData = new PathData()
            };
            PathCalculator.CalculatePathValues(inversePath);
            return inversePath;
        }


        public static double MetersBetween(Station ts1, Station ts2)
        {
            if (ts1 == null) return 0;
            if (ts2 == null) return 0;
            if (ts1.MapPosition == null) return 0;
            if (ts2.MapPosition == null) return 0;
            return MetersBetween(ts1.MapPosition, ts2.MapPosition);
        }
        public static double MetersBetween(MapPosition poz1, MapPosition poz2)
        {
            if (!poz1.X.HasValue) return 0;
            if (!poz1.Y.HasValue) return 0;
            if (!poz2.X.HasValue) return 0;
            if (!poz2.Y.HasValue) return 0;
            var settings = new SettingsService();
            return Math.Sqrt((poz2.X.Value - poz1.X.Value) * (poz2.X.Value - poz1.X.Value) +
                             (poz2.Y.Value - poz1.Y.Value) * (poz2.Y.Value - poz1.Y.Value)) *
                             settings.MetersPerPixel();
        }

        public static void FixOrderNumbersInPath(Path path)
        {
            var no = 1;
            if (path.PathPoints == null || path.PathPoints.Count == 0) return;
            foreach (var point in path.PathPoints.OrderBy(x => x.No))
            {
                point.No = no;
                no++;
            }
        }


        /// <summary>
        /// Ta metoda poskuša najtio pot med dvema Checkpointoma
        ///  - če obstaja, jo vrne
        ///  - če obstaja obratna, skreira "obrnjeno" in jo vrne
        ///  - če sta točki bliže kot 50m, ustvari "direktno" pot med njima
        ///  - poišče točke "v bližini" obeh točk in preveri, če obstaja kakšna "podobna" pot. Če jo najde, jo vrne
        ///  - naredi ravno pot med podanima točkama
        /// </summary>
        /// <param name="ts1"></param>
        /// <param name="ts2"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Path GetOrCreatePathBetween(Station ts1, Station ts2, Marvin20Db db)
        {
            if (ts1 == null || ts2 == null || ts1.Id == ts2.Id)
                throw new Exception("Točki nista pravi!");
            if (ts1.Day != ts2.Day)
                throw new Exception("Točki ne pripadata istemu dnevu!");
            Path path;

            // preverim, če pot obstaja
            path = GetPathBetween(ts1, ts2, db);
            if (path != null) return path; // najdena prava pot

            //preverim, če obstaja INVERZNA pot
            path = GetPathBetween(ts2, ts1, db);
            if (path != null)
            {
                //najdena inverzna pot
                var inversePath = GetInversePath(path);
                db.Paths.Add(inversePath);
                return inversePath;
            }

            // preverim razdaljo
            var distance = MetersBetween(ts1, ts2);
            if (distance < 50)
            {
                //razdalja manjša od 50m, ustvarim direktno pot
                path = CreateDirectPathBetween(ts1, ts2);
                db.Paths.Add(path);
                return path;
            }

            //poiščem "bližnje" točke in alternativne poti
            var tps1 = GetVicinityStations(ts1, db, 10);
            var tps2 = GetVicinityStations(ts2, db, 10);

            //poiščem "podobno" pot
            Path similarPath = null;
            var minDistance = 0.0;
            foreach (var t1 in tps1)
            {
                foreach (var t2 in tps2)
                {
                    path = GetPathBetween(t1, t2, db);
                    if (path == null) continue;
                    distance = MetersBetween(ts1, t1) + MetersBetween(ts2, t2);
                    if (similarPath != null && distance >= minDistance) continue;
                    similarPath = path;
                    minDistance = distance;
                }
            }
            if (similarPath != null)
            {
                path = DuplicatePath(similarPath);
                path.FirstStation = ts1;
                path.SecondStation = ts2;
                CalculatePathValues(path);
                db.Paths.Add(path);
                return path;
            }
            //poskusim najti "obratno" pot
            foreach (var t1 in tps1)
            {
                foreach (var t2 in tps2)
                {
                    path = GetPathBetween(t2, t1, db);
                    if (path == null) continue;
                    distance = MetersBetween(ts1, t1) + MetersBetween(ts2, t2);
                    if (similarPath != null && distance >= minDistance) continue;
                    similarPath = path;
                    minDistance = distance;
                }
            }
            if (similarPath != null)
            {
                path = GetInversePath(similarPath);
                path.FirstStation = ts1;
                path.SecondStation = ts2;
                CalculatePathValues(path);
                db.Paths.Add(path);
                return path;
            }
            //če ni nič od tega, naredim direktno pot
            path = CreateDirectPathBetween(ts1, ts2);
            db.Paths.Add(path);
            return path;
        }

        /// <summary>
        /// Vrne duplikat obstoječe poti (deep clone)
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static Path DuplicatePath(Path path)
        {
            var newpath = new Path
                              {
                                  FirstStation = path.FirstStation,
                                  SecondStation = path.SecondStation,
                                  IsManual = path.IsManual,
                                  Locked = path.Locked,
                                  PathPoints = PathCalculator.Duplicate(path.PathPoints),
                                  ManualData = new PathData
                                  {
                                      Distance = path.ManualData.Distance,
                                      Downhill = path.ManualData.Downhill,
                                      ManualUpDown = path.ManualData.ManualUpDown,
                                      Uphill = path.ManualData.Uphill,
                                      ZisDirty = path.ManualData.ZisDirty,
                                      Zlist = path.ManualData.Zlist
                                  },
                                  AutomaticData = new PathData()

                              };
            return newpath;

        }

        /// <summary>
        /// Vrne kopijo seznama točk na zemljevidu
        /// </summary>
        /// <param name="pathPoints"></param>
        /// <returns></returns>
        private static List<PathPoint> Duplicate(List<PathPoint> pathPoints)
        {
            var list = new List<PathPoint>();
            if (pathPoints == null || pathPoints.Count == 0) return list;
            var orderedPathPoints = pathPoints.OrderBy(x => x.No).ToList();
            var i = 0;
            foreach (var pathPoint in orderedPathPoints)
            {
                i++;
                var point = new PathPoint
                {
                    Locked = pathPoint.Locked,
                    No = i,
                    MapPosition = new MapPosition
                    {
                        X = pathPoint.MapPosition.X,
                        Y = pathPoint.MapPosition.Y,
                        Z = pathPoint.MapPosition.Z,
                        ZisDirty = pathPoint.MapPosition.ZisDirty
                    }
                };
                list.Add(point);
            }
            return list;
        }
        /// <summary>
        /// Vrne Kopijo tok na zemljevidu, ki so invertirane (prva je zadnja, druga predzadnja,...)
        /// </summary>
        /// <param name="pathPoints"></param>
        /// <returns></returns>
        public static List<PathPoint> DuplicateAndInvert(List<PathPoint> pathPoints)
        {
            var list = new List<PathPoint>();
            if (pathPoints == null || pathPoints.Count == 0) return list;
            var orderedPathPoints = pathPoints.OrderByDescending(x => x.No).ToList();
            var i = 0;
            foreach (var pathPoint in orderedPathPoints)
            {
                i++;
                var point = new PathPoint
                {
                    Locked = pathPoint.Locked,
                    No = i,
                    MapPosition = new MapPosition
                    {
                        X = pathPoint.MapPosition.X,
                        Y = pathPoint.MapPosition.Y,
                        Z = pathPoint.MapPosition.Z,
                        ZisDirty = pathPoint.MapPosition.ZisDirty
                    }
                };
                list.Add(point);
            }
            return list;
        }

        /// <summary>
        /// Vrne vse točke, ki so v neposredni bližini podane točke in pripadajo istemu dnevu
        /// </summary>
        /// <param name="ts1">podana točka</param>
        /// <param name="db"></param>
        /// <param name="i">koliko pikslov stran je lahko točka</param>
        /// <returns></returns>
        private static List<Station> GetVicinityStations(Station ts1, Marvin20Db db, int i)
        {
            if (!ts1.MapPosition.X.HasValue || !ts1.MapPosition.Y.HasValue) return new List<Station>();
            var x = ts1.MapPosition.X.Value;
            var y = ts1.MapPosition.Y.Value;

            return db.Stations.Where(t =>
                t.Day == ts1.Day &&
                t.MapPosition.X.HasValue &&
                t.MapPosition.X.Value > x - i &&
                t.MapPosition.X.Value < x + i &&
                t.MapPosition.Y.HasValue &&
                t.MapPosition.Y.Value > y - i &&
                t.MapPosition.Y.Value < y + i).ToList();

        }

        /// <summary>
        /// >Ustvari "direktno" pot med dvema točkama
        /// </summary>
        /// <param name="ts1"></param>
        /// <param name="ts2"></param>
        /// <returns></returns>
        private static Path CreateDirectPathBetween(Station ts1, Station ts2)
        {
            var path = new Path
            {
                FirstStation = ts1,
                SecondStation = ts2,
                ManualData = new PathData(),
                AutomaticData = new PathData()
            };
            CalculatePathValues(path);
            return path;
        }

        private static Path GetPathBetween(Station ts1, Station ts2, Marvin20Db db)
        {
            return db.Paths.Where(x => x.FirstStation.Id == ts1.Id && x.SecondStation.Id == ts2.Id).FirstOrDefault();
        }

        public static void DeleteUnusedAutomaticPaths(Marvin20Db db)
        {
            var paths = db.Paths.Where(x => x.PathPoints.Count == 0);
            foreach (var path in paths)
            {
                if (db.TrackCheckpoints.Where(x => x.PathFromPrevious.Id == path.Id).Any()) 
                    continue;
                var ppToBeRemoved = path.PathPoints.ToList();
                foreach (var pathPoint in ppToBeRemoved)
                {
                    db.PathPoints.Remove(pathPoint);
                }
                db.Paths.Remove(path);
            }
        }
    }
}
