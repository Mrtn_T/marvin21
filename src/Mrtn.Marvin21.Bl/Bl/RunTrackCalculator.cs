﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Configuration;
using Mrtn.Marvin21.Bl.Services;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Run;
using Mrtn.Marvin21.DbModel.Run.Support;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.Model.Common;

namespace Mrtn.Marvin21.Bl.Bl
{
    static class RunTrackCalculator
    {
        /// <summary>
        /// Za določeno ekipo izdelam prazen "okostnjak" za vnos-branje  proge za določen dan
        /// </summary>
        /// <param name="team"></param>
        /// <param name="day"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static RunTrack CreateRunTrackForTeam(Team team, int day, Marvin20Db db)
        {
            //preverim, če slučajno tes ne obstaja
            var runTrack = team.RunTracks.FirstOrDefault(x => x.Day == day);
            if (runTrack != null) return runTrack;

            //poiščem track
            var track = db.Tracks.SingleOrDefault(x => x.Day == day &&
                                                       x.Category.Id == team.Category.Id);
            if (track == null) return null;

            //izdelam nov runTrack
            runTrack = new RunTrack
                {
                    Day = day,
                    Team = team,
                    Track = track,
                    CheckpointsFound = 0,
                    PointsForCheckpoints = 0,
                    PointsForTime = 0,
                    SpeedTrialPoints = 0,
                    TrackPoints = 0,
                    StartTime = new TrackTime(),
                    FinishTime = new TrackTime(),
                    SpeedTrialStart = new TrackTime(),
                    SpeedTrialFinish = new TrackTime(),
                    RunTrackCheckpoints = new List<RunTrackCheckpoint>()

                };
            foreach (var checkpoint in track.TrackCheckpoints.OrderBy(x => x.No))
            {
                var runTrackCheckpoint = new RunTrackCheckpoint
                    {
                        IsConfirmedByArrival = false,
                        IsConfirmedByTask = false,
                        IsConfirmedManualy = false,
                        No = checkpoint.No,
                        TrackCheckpoint = checkpoint,
                        RunTrack = runTrack,
                        Arrival = new TrackTime(),
                        Departure = new TrackTime()

                    };
                runTrack.RunTrackCheckpoints.Add(runTrackCheckpoint);
            }
            team.RunTracks.Add(runTrack);
            db.SaveChanges();
            return runTrack;
        }

        /// <summary>
        /// Ta metoda zgolj preveri kateri KT-ji so priznani in sešteje točke za KT-je
        /// </summary>
        /// <param name="runTrack"></param>
        /// <param name="db"></param>
        public static void RecalculateRunTrackPointsForCheckpoints(RunTrack runTrack, Marvin20Db db)
        {
            if (runTrack == null) return;
            if (runTrack.RunTrackCheckpoints == null || runTrack.RunTrackCheckpoints.Count == 0)
            {
                runTrack.PointsForCheckpoints = 0;
                runTrack.TrackPoints = runTrack.PointsForCheckpoints + runTrack.PointsForTime;
                runTrack.CheckpointsFound = 0;
                db.SaveChanges();
                return;
            }
            var points = 0;
            var checkpointsFound = 0;
            foreach (var rtcp in runTrack.RunTrackCheckpoints)
            {
                if (rtcp.IsRejectedManualy)
                {
                    rtcp.Points = 0;
                    continue;
                }
                if (rtcp.IsConfirmedByArrival ||
                    rtcp.IsConfirmedByTask ||
                    rtcp.IsConfirmedManualy)
                {
                    rtcp.Points = rtcp.TrackCheckpoint.Points;
                    points = points + rtcp.Points;
                    if (!rtcp.TrackCheckpoint.Station.IsFinish
                        && !rtcp.TrackCheckpoint.Station.IsStart)
                        //KT "prištejemo" samo če ni start ali cilj
                        checkpointsFound++;
                    continue;
                }
                rtcp.Points = 0;
            }
            runTrack.PointsForCheckpoints = points;
            runTrack.TrackPoints = runTrack.PointsForCheckpoints + runTrack.PointsForTime;
            runTrack.CheckpointsFound = checkpointsFound;
            db.SaveChanges();
        }

        public static void CalculatePredictionsForTrack(RunTrack runTrack, Marvin20Db db)
        {
            if (!runTrack.StartTime.Predicted.HasValue)
            {
                // ni predikcije!!!
                runTrack.StartTime.MaxPredicted = null;
                runTrack.FinishTime.Predicted = null;
                runTrack.FinishTime.MaxPredicted = null;
                runTrack.SpeedTrialStart.Predicted = null;
                runTrack.SpeedTrialStart.MaxPredicted = null;
                runTrack.SpeedTrialFinish.Predicted = null;
                runTrack.SpeedTrialFinish.MaxPredicted = null;
                foreach (var checkpoint in runTrack.RunTrackCheckpoints)
                {
                    checkpoint.Arrival.Predicted = null;
                    checkpoint.Arrival.MaxPredicted = null;
                    checkpoint.Departure.Predicted = null;
                    checkpoint.Departure.MaxPredicted = null;
                }
                db.SaveChanges();
                return;
            }
            var startTime = runTrack.StartTime.Predicted.Value;

            runTrack.StartTime.MaxPredicted = null;
            runTrack.FinishTime.Predicted = startTime
                    .AddSeconds(runTrack.Track.OptimalTime.GetValueOrDefault());
            runTrack.FinishTime.MaxPredicted = startTime
                .AddSeconds(runTrack.Track.OptimalTime.GetValueOrDefault())
                .AddSeconds((AppSettings.MaxFactor - 1) * runTrack.Track.OptimalPathTime.GetValueOrDefault());
            //ROT 2016: testiranje za dvojno časovnico za naloge
            //.AddSeconds(
            //      (runTrack.Track.OptimalTime.GetValueOrDefault() - runTrack.Track.OptimalPathTime.GetValueOrDefault())
            //    * (AppSettings.TaskFactor - 1));

            foreach (var checkpoint in runTrack.RunTrackCheckpoints)
            {
                checkpoint.Arrival.Predicted = startTime
                    .AddSeconds(checkpoint.TrackCheckpoint.Time.OptimalArrival);
                checkpoint.Arrival.MaxPredicted = startTime
                    .AddSeconds(checkpoint.TrackCheckpoint.Time.MaxFromStart);
                checkpoint.Departure.Predicted = startTime
                    .AddSeconds(checkpoint.TrackCheckpoint.Time.OptimalArrival)
                    .AddSeconds(checkpoint.TrackCheckpoint.Time.ForTask);
                checkpoint.Departure.MaxPredicted = startTime
                    .AddSeconds(checkpoint.TrackCheckpoint.Time.MaxFromStart)
                    .AddSeconds(checkpoint.TrackCheckpoint.Time.ForTask);
                if (checkpoint.TrackCheckpoint.SpeedTrial.IsStart)
                {
                    runTrack.SpeedTrialStart.Predicted = checkpoint.Departure.Predicted;
                    runTrack.SpeedTrialStart.MaxPredicted = checkpoint.Departure.MaxPredicted;
                }
                if (checkpoint.TrackCheckpoint.SpeedTrial.IsFinish)
                {
                    runTrack.SpeedTrialFinish.Predicted = checkpoint.Arrival.Predicted;
                    runTrack.SpeedTrialFinish.MaxPredicted = checkpoint.Arrival.MaxPredicted;
                }
            }
            db.SaveChanges();
            return;

        }

        public static void RecalculateRunTrackTimes(RunTrack runTrack, Marvin20Db db)
        {
            // ****** prevrjam, pogoje za izračun:******/
            // obstajati morajo RunTrackCheckpoints
            if (runTrack.RunTrackCheckpoints == null || runTrack.RunTrackCheckpoints.Count < 2)
                //ali bomo pred izhodom še kaj pobrisali???
                return;
            var rtcps = runTrack.RunTrackCheckpoints.OrderBy(x => x.No).ToList();
            // prvi punch mora biti start)
            if (!rtcps[0].TrackCheckpoint.Station.IsStart)
                //prvi ni start
                return;
            var startTime = rtcps[0].GetTimeArrival();
            if (startTime == null)
                //start nima časa 
                return;
            //start očitno ima čas, zato ga priznamo
            rtcps[0].IsConfirmedByArrival = true;

            runTrack.StartTime.FromSi = rtcps[0].Departure.FromSi;
            runTrack.StartTime.Manual = rtcps[0].Departure.Manual;
            runTrack.StartTime.Predicted = rtcps[0].Departure.Predicted;

            runTrack.SpeedTrialStart = new TrackTime();
            runTrack.SpeedTrialFinish = new TrackTime();


            var timeForTask = 0;
            var timeDead = 0;
            foreach (var rtcp in rtcps)
                if (true || !rtcp.TrackCheckpoint.Station.IsStart) // Rot 2014, naloga je lahko tudi na startu
                {
                    var arrival = rtcp.GetTimeArrival();
                    if (arrival == null)
                    {
                        //ne morem ugotoviti, kdaj je ekipa prispela na to piko
                        rtcp.IsConfirmedByArrival = false;
                        rtcp.TimeOnPath = null;
                        rtcp.TimeWaitHere = null;
                        rtcp.TimeTotal = null;
                        continue;
                    }
                    var toHere = arrival.Value.TotalSeconds - timeForTask - timeDead - startTime.Value.TotalSeconds;
                    if (toHere < 0)
                    {
                        //izgleda da so hodili NEGATIVNO količino časa. 
                        //Trenutno ni podprt izračun tekmovanj čez polnoč
                        rtcp.IsConfirmedByArrival = false;
                        rtcp.TimeOnPath = null;
                        rtcp.TimeWaitHere = null;
                        rtcp.TimeTotal = null;
                        continue;
                    }
                    rtcp.TimeOnPath = (int?)toHere;
                    //ugotovim, če so iz individualne časovnice
                    var toHereAllowed = rtcp.TrackCheckpoint.Time.MaxFromStart;
                    if (toHereAllowed + 1 > (toHere + timeDead + timeForTask))
                    {
                        rtcp.IsConfirmedByArrival = true;
                    }
                    else
                    {
                        rtcp.IsConfirmedByArrival = false;
                    }
                    var timeHere = (int)rtcp.TrackCheckpoint.Time.ForTask;
                    timeForTask += (int)rtcp.TrackCheckpoint.Time.ForTask;
                    var departure = rtcp.GetTimeDeparture();
                    if (departure != null)
                    {
                        var dt = departure.Value.TotalSeconds - arrival.Value.TotalSeconds;
                        if (dt > 0)
                        {
                            timeDead += (int)dt;
                            timeHere += (int)dt;
                        }
                    }
                    rtcp.TimeWaitHere = timeHere == 0 ? (int?)null : timeHere;
                    rtcp.TimeTotal = (int?)(toHere + timeDead + timeForTask);
                    if (rtcp.TrackCheckpoint.SpeedTrial.IsStart)
                        runTrack.SpeedTrialStart = rtcp.GetDeparture();
                    if (rtcp.TrackCheckpoint.SpeedTrial.IsFinish)
                        runTrack.SpeedTrialFinish = rtcp.GetArrival();


                }
            //Spped trial
            var stStart = runTrack.SpeedTrialStart.Manual ?? runTrack.SpeedTrialStart.FromSi;
            var stFinish = runTrack.SpeedTrialFinish.Manual ?? runTrack.SpeedTrialFinish.FromSi;

            if (stStart.HasValue
                && stFinish.HasValue
                && stFinish.Value.TimeOfDay.TotalSeconds > stStart.Value.TimeOfDay.TotalSeconds)
            {
                runTrack.SpeedTrialTimeOnTrack = (int)(stFinish.Value.TimeOfDay.TotalSeconds - stStart.Value.TimeOfDay.TotalSeconds);
            }
            else
            {
                runTrack.SpeedTrialTimeOnTrack = null;
            }

            var last = rtcps[rtcps.Count - 1];
            var lastDeparture = last.GetTimeDeparture();
            if (last.TrackCheckpoint.Station.IsFinish && lastDeparture.HasValue)
            {
                //imamo cilj
                runTrack.FinishTime = last.GetArrival();
                runTrack.TimeForPath = last.TimeOnPath;
                runTrack.TimeForTask = timeForTask;
                runTrack.TimeForWait = timeDead;
                runTrack.TimeOnTrack = last.TimeTotal;
                runTrack.TimeDelayForPath = (int?)(last.TimeTotal - last.TrackCheckpoint.Time.OptimalArrival - timeDead);
                runTrack.PointsForTime =
                    runTrack.TimeDelayForPath > 0
                        ? -2 * ((int)((runTrack.TimeDelayForPath + 59) / 60))
                        : 0;
            }
            else
            {
                //ni cilja!

                runTrack.FinishTime = new TrackTime();
                runTrack.TimeForPath = null;
                runTrack.TimeForTask = null;
                runTrack.TimeForWait = null;
                runTrack.TimeOnTrack = null;
                runTrack.TimeDelayForPath = null;
                runTrack.PointsForTime = 0;
            }

            db.SaveChanges();
        }

        public static void RecalculateRunTrackSpeedTrial(Track track, Marvin20Db db)
        {
            int? minTimeTrial = null;
            // v prvem prehodu najdem najmanjšo vrednost časa za hitrostno
            foreach (var runTrack in track.RunTracks)
            {
                if (!runTrack.SpeedTrialTimeOnTrack.HasValue) continue;
                if (!minTimeTrial.HasValue)
                {
                    minTimeTrial = runTrack.SpeedTrialTimeOnTrack.Value;
                    continue;
                }
                if (runTrack.SpeedTrialTimeOnTrack < minTimeTrial.Value)
                    minTimeTrial = runTrack.SpeedTrialTimeOnTrack.Value;
            }
            track.MinTimeForSpeedTrial = minTimeTrial;
            //drugi pass podeli točke
            foreach (var runTrack in track.RunTracks)
            {
                if (!minTimeTrial.HasValue
                    || !runTrack.SpeedTrialTimeOnTrack.HasValue)
                {
                    runTrack.SpeedTrialPoints = 0;
                    continue;
                }
                if (runTrack.SpeedTrialTimeOnTrack.Value > 2 * minTimeTrial.Value)
                {
                    runTrack.SpeedTrialPoints = 0;
                    continue;
                }
                runTrack.SpeedTrialPoints = (int)(120 - 120 / (2 * (double)minTimeTrial.Value) * runTrack.SpeedTrialTimeOnTrack);
            }
            db.SaveChanges();


            //ToDo: naredi!!! throw new NotImplementedException();
        }

        public static void ProcessSiRead(RunTrack runTrack, Marvin20Db db)
        {
            if (runTrack == null) return;
            if (runTrack.SiRead == null) return;
            //tu zrihtaj še start in cilj!!!
            foreach (var runTrackCheckpoint in runTrack.RunTrackCheckpoints)
            {
                if (runTrackCheckpoint.TrackCheckpoint.Station.IsStart)
                {
                    runTrackCheckpoint.Arrival.FromSi = runTrack.SiRead.Start;
                    runTrackCheckpoint.Departure.FromSi = runTrack.SiRead.Start;
                }
                else if (runTrackCheckpoint.TrackCheckpoint.Station.IsFinish)
                {
                    runTrackCheckpoint.Arrival.FromSi = runTrack.SiRead.Finish;
                    runTrackCheckpoint.Departure.FromSi = null;
                }
                else
                {
                    var siCodeArrival = runTrackCheckpoint.TrackCheckpoint.Station.SiCodeArrival;
                    var siCodeDeparture = runTrackCheckpoint.TrackCheckpoint.Station.SiCodeDeparture;

                    if (String.IsNullOrWhiteSpace(siCodeArrival))
                        runTrackCheckpoint.Arrival.FromSi = null;
                    else
                    {
                        //poskusim najti prihod
                        var siPunch = runTrack.SiRead.SiPunches
                            .Where(x => x.SiCode == siCodeArrival).FirstOrDefault();
                        if (siPunch != null) siPunch.RunTrackCheckpoint = runTrackCheckpoint;
                        runTrackCheckpoint.SiPunchArrival = siPunch;
                        runTrackCheckpoint.Arrival.FromSi =
                            siPunch != null
                            ? (DateTime?)siPunch.Time
                            : null;
                    }
                    if (String.IsNullOrWhiteSpace(siCodeDeparture))
                        runTrackCheckpoint.Departure.FromSi = null;
                    else
                    {
                        //poskusim najti prihod
                        var siPunch = runTrack.SiRead.SiPunches
                            .Where(x => x.SiCode == siCodeDeparture).FirstOrDefault();
                        if (siPunch != null) siPunch.RunTrackCheckpoint = runTrackCheckpoint;
                        runTrackCheckpoint.SiPunchDeparture = siPunch;
                        runTrackCheckpoint.Departure.FromSi =
                            siPunch != null
                            ? (DateTime?)siPunch.Time
                            : null;
                    }
                }
            }
            db.SaveChanges();
        }
    }
}
