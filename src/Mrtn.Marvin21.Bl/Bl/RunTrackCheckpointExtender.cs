﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.DbModel.Run;
using Mrtn.Marvin21.DbModel.Run.Support;

namespace Mrtn.Marvin21.Bl.Bl
{
    static class RunTrackCheckpointExtender
    {
        //poišče čas prihoda po najboljših močeh
        public static TimeSpan? GetTimeArrival(this RunTrackCheckpoint rcp)
        {
            if (rcp == null) return null;
            if (rcp.Arrival == null && rcp.Departure == null)
                return null;
            if (rcp.Arrival != null)
            {
                if (rcp.Arrival.Manual.HasValue)
                    return rcp.Arrival.Manual.Value.TimeOfDay;
                if (rcp.Arrival.FromSi.HasValue)
                    return rcp.Arrival.FromSi.Value.TimeOfDay;
            }
            if (rcp.Departure != null)
            {
                if (rcp.Departure.Manual.HasValue)
                    return rcp.Departure.Manual.Value.TimeOfDay;
                if (rcp.Departure.FromSi.HasValue)
                    return rcp.Departure.FromSi.Value.TimeOfDay;
            }
            return null;
        }

        //poišče čas prihoda po najboljših močeh
        public static TimeSpan? GetTimeDeparture(this RunTrackCheckpoint rcp)
        {
            if (rcp == null) return null;
            if (rcp.Departure != null)
            {
                if (rcp.Departure.Manual.HasValue)
                    return rcp.Departure.Manual.Value.TimeOfDay;
                if (rcp.Departure.FromSi.HasValue)
                    return rcp.Departure.FromSi.Value.TimeOfDay;
            }
            if (rcp.Arrival != null)
            {
                if (rcp.Arrival.Manual.HasValue)
                    return rcp.Arrival.Manual.Value.TimeOfDay;
                if (rcp.Arrival.FromSi.HasValue)
                    return rcp.Arrival.FromSi.Value.TimeOfDay;
            }
            return null;
        }

        public static TrackTime GetDeparture(this RunTrackCheckpoint rcp)
        {
            var model = new TrackTime
                            {
                                FromSi = rcp.Departure.FromSi ?? rcp.Arrival.FromSi,
                                Manual = rcp.Departure.Manual ?? rcp.Arrival.Manual,
                                MaxPredicted = rcp.Departure.MaxPredicted ?? rcp.Arrival.MaxPredicted,
                                Predicted = rcp.Departure.Predicted ?? rcp.Arrival.Predicted
                            };
            return model;
        }
        public static TrackTime GetArrival(this RunTrackCheckpoint rcp)
        {
            var model = new TrackTime
                            {
                                FromSi = rcp.Arrival.FromSi ?? rcp.Departure.FromSi,
                                Manual = rcp.Arrival.Manual ?? rcp.Departure.Manual,
                                MaxPredicted = rcp.Arrival.MaxPredicted ?? rcp.Departure.MaxPredicted,
                                Predicted = rcp.Arrival.Predicted ?? rcp.Departure.Predicted
                            };
            return model;
        }
    }
}
