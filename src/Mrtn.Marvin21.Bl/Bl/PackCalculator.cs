﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Results;

namespace Mrtn.Marvin21.Bl.Bl
{
    static class PackCalculator
    {
        public static void CalculatePackResults(Marvin20Db db)
        {
            foreach (var teamResult in db.TeamResults)
                teamResult.PackResult = null;
            db.SaveChanges();

            foreach (var pack in db.Packs)
                if (pack.Result == null) pack.Result = new PackResult();
            db.SaveChanges();

            foreach (var pack in db.Packs)
                pack.Result.Pack = pack;
            db.SaveChanges();

            var packs = db.Packs.ToList();

            //Prvi prehod: Naberem 

            //***************** 2020 ***************************
            // Tole spodaj zakomentirano je KLASIČNA verzija.
            // predelano 2020 za GSJ zaradi novih pripozicij
            //***************** 2020 ***************************
            //foreach (var pack in packs)
            //{
            //    var result = pack.Result;
            //    var points = 0;
            //    foreach (var category in db.Categories)
            //    {
            //        var teamResult = db.TeamResults
            //            .Where(
            //                x =>
            //                    x.Team.Pack.Id == pack.Id &&
            //                    x.Team.Category.Id == category.Id &&
            //                    x.Team.Active &&
            //                    x.Team.InCompetition)
            //            .OrderByDescending(y => y.Points)
            //            .FirstOrDefault();
            //        if (teamResult != null)
            //        {
            //            if (result.TeamResults == null) result.TeamResults = new List<TeamResult>();
            //            result.TeamResults.Add(teamResult);
            //            points += teamResult.Points;
            //        }
            //    }
            //    result.Points = points;
            //}
            foreach (var pack in packs)
            {
                //var result = pack.Result;
                var tuples = new List<Tuple<Single, string>>();

                foreach (var category in db.Categories)
                {
                    var teamResults = db.TeamResults
                        .Where(
                            x =>
                                x.Team.Pack.Id == pack.Id &&
                                x.Team.Category.Id == category.Id &&
                                x.Team.Active &&
                                x.Team.InCompetition);
                    if (teamResults?.Count() > 0)
                    {
                        var percent = teamResults.Average(x => x.Percent);
                        var s = $"<b>{category.Abbr}:{percent:P}</b> " +
                                $"[{string.Join(", ", teamResults.ToList().Select(x => $"{x.Team.No}: {x.Percent:P}")).ToString()}]";
                        tuples.Add(new Tuple<float, string>(percent,s));
                    }
                }
                if (tuples.Count < 3)
                {
                    pack.Result = null;
                    continue;
                }

                var first3 = tuples.OrderByDescending(x => x.Item1).Take(3).ToList();
                var rest = tuples.OrderByDescending(x => x.Item1).Skip(3).ToList();
                if (pack.Result == null)
                    pack.Result = new PackResult();
                var percent1= first3.Sum(x => x.Item1);
                var desc = string.Join(" <b>|</b> ", first3.Select(x => x.Item2).ToList()).ToString();
                if (rest.Count > 0)
                {
                    desc += $"<br/><strike>";
                    desc += string.Join(" <b>|</b> ", rest.Select(x => x.Item2).ToList()).ToString();
                    desc += $"</strike>";
                }

                pack.Result.Percent = percent1;
                pack.Result.Description = desc;
                pack.Result.Points = (int)(pack.Result.Percent * 1000);
            }
            db.SaveChanges();

            var packResults = db.PackResults.OrderByDescending(x => x.Points).ToList();
            var place = 1;
            var lastPlace = 1;
            var lastPoints = 0;
            foreach (var packResult in packResults)
            {
                if (place == 1) lastPoints = packResult.Points;
                if (packResult.Points == lastPoints)
                {
                    //prvič ali izenačenje
                    packResult.Place = lastPlace;
                }
                else
                {
                    packResult.Place = place;
                    lastPlace = place;
                    lastPoints = packResult.Points;
                }
                place++;

            }
            db.SaveChanges();

        }
    }
}
