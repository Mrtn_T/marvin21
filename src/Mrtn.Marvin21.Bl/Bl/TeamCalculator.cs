﻿using System.Linq;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Results;
using Mrtn.Marvin21.DbModel.Teams;

namespace Mrtn.Marvin21.Bl.Bl
{
    class TeamCalculator
    {
        public static void CalculateResult(Team team, Marvin20Db db)
        {
            var points = 0;
            var isAnyResult = false;

            foreach (var runTrack in team.RunTracks)
            {
                isAnyResult = true;
                points = points + runTrack.TrackPoints + runTrack.SpeedTrialPoints;
            }

            foreach (var runTask in team.RunTasks)
            {
                isAnyResult = true;
                points = points + runTask.Points;
            }
            if (isAnyResult)
            {
                if (team.Result == null)
                {
                    team.Result = new TeamResult ();
                    db.SaveChanges();
                    team.Result.Team = team;
                    db.SaveChanges();
                }
                team.Result.Points = points;
                var catMax = team.Category.MaxPoints;

                team.Result.Percent = catMax != 0 
                    ? (float) team.Result.Points / catMax 
                    : 0;
            }
            else
            {
                if (team.Result != null) db.TeamResults.Remove(team.Result);
            }

            db.SaveChanges();

        }
        public static void CalculatePlacesInCategory(Category category, Marvin20Db db)
        {
            var results = db.TeamResults
                .Where(x => x.Team.Category.Id == category.Id)
                .OrderByDescending(y => y.Points)
                .ToList();
            var place = 1;
            var lastPlace = 1;
            var lastPoints = 0;
            foreach (var teamResult in results)
            {
                if (place == 1) lastPoints = teamResult.Points;
                if (teamResult.Points == lastPoints)
                {
                    //prvič ali izenačenje
                    teamResult.Place = lastPlace;
                }
                else
                {
                    teamResult.Place = place;
                    lastPlace = place;
                    lastPoints = teamResult.Points;
                }
                place++;
            }
            db.SaveChanges();

        }
    }
}
