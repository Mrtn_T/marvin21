﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Mrtn.Marvin21.Bl.Services.Support;
using Mrtn.Marvin21.Bl.Interface.Si;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Si;

namespace Mrtn.Marvin21.Bl.Services.Si
{
    public class SiChipService : ISiChipService
    {
        private class SiCodeCount
        {
            public string Code { get; set; }
            public int Count { get; set; }
        }

        public List<SiChipViewModel> Get(SiChipQueryModel queryModel)
        {
            var siChips = new List<SiChipViewModel>();
            using (var db = new Marvin20Db())
            {
                var siCodeCounts = db.Database.SqlQuery<SiCodeCount>(
                    " SELECT SiCode AS Code, COUNT(*) AS Count" +
                    " FROM Teams " +
                    " GROUP BY SiCode" +
                    " HAVING (NOT (SiCode IS NULL))").ToList();
                foreach (var siChip in db.SiChips.OrderBy(x => x.SiCode))
                {
                    var siChipModel = ViewModelFactory.SiChipViewModel(siChip);
                    var siCodeCount = siCodeCounts.Where(x => x.Code == siChip.SiCode).FirstOrDefault();
                    if (siCodeCount != null) siChipModel.TeamCount = siCodeCount.Count;
                    siChips.Add(siChipModel);
                }
            }

            return siChips;
        }

        public SiChipEditModel Edit(int? id)
        {
            SiChipEditModel teamEditModel;
            using (var db = new Marvin20Db())
            {
                SiChip siChip;
                if (id.HasValue)
                {
                    siChip = db.SiChips.Where(x => x.Id == id).FirstOrDefault();
                    if (siChip == null) throw new Exception("SI čip s tem id ne obstaja!");
                    if (siChip.Locked) throw new Exception("SI čip je zaklenjen!");
                }
                else siChip = new SiChip();
                teamEditModel = EditModelFactory.SiChipEditModel(siChip, db);
            }
            return teamEditModel;
        }

        public SuccessResponse Save(SiChipEditModel model)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                if (string.IsNullOrWhiteSpace(model.SiCode))
                    return new SuccessResponse(false, "SI koda ne sme biti prazna!");
                model.SiCode = model.SiCode.Trim();
                if (db.SiChips
                    .Where(x => x.SiCode == model.SiCode && x.Id != model.Id)
                    .Count() > 0)
                    return new SuccessResponse(false, "SI koda že obstaja v bazi!");
                SiChip siChip;
                if (model.Id == 0)
                {
                    //dodajam 
                    siChip = new SiChip();
                    db.SiChips.Add(siChip);
                }
                else
                {
                    siChip = db.SiChips.Where(x => x.Id == model.Id).FirstOrDefault();
                    if (siChip == null) return new SuccessResponse(false, "SI čip s tem id ne obstaja!");
                    if (siChip.Locked) return new SuccessResponse(false, "SI čip je zaklenjen!");
                }
                EntityMerger.MergeIgnoringId(siChip, model);
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Deactivate(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var siChip = db.SiChips.Where(x => x.Id == id).FirstOrDefault();
                if (siChip == null)
                {
                    response.Message = "SI čip s tem id ne obstaja!";
                    return response;
                }
                if (siChip.Locked)
                {
                    response.Message = "SI čip je zaklenjen!";
                    return response;
                }
                if (db.Teams.Where(x => x.SiCode == siChip.SiCode).Count() > 0)
                {
                    response.Message = "Ta SI čip je uporabljen vsaj pri eni ekipi!";
                    return response;
                }
                siChip.Active = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Activate(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var siChip = db.SiChips.Where(x => x.Id == id).FirstOrDefault();
                if (siChip == null)
                {
                    response.Message = "SI čip s tem id ne obstaja!";
                    return response;
                }
                if (siChip.Locked)
                {
                    response.Message = "SI čip je zaklenjen!";
                    return response;
                }
                siChip.Active = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse AddPacket(string siCodes, bool @private)
        {
            var response = new SuccessResponse();
            var codes = siCodes
                .Replace(" ", ",")
                .Replace("\r", ",")
                .Replace("\n", ",")
                .Replace(";", ",")
                .Replace(":", ",")
                .Split(',')
                .Where(x => x != "")
                .ToList();
            var rex = new Regex("\\d+");
            var invalidCodes = new List<string>();
            var existingCodes = new List<string>();
            var newCodes = new List<string>();
            using (var db = new Marvin20Db())
            {
                foreach (var code in codes)
                {
                    if (!rex.IsMatch(code) || code.Length <= 5)
                        invalidCodes.Add(code);
                    else if (db.SiChips.Where(x => x.SiCode == code).Count() > 0)
                        existingCodes.Add(code);
                    else
                    {
                        db.SiChips.Add(new SiChip { SiCode = code, Private = @private });
                        db.SaveChanges();
                        newCodes.Add(code);
                    }
                }
            }
            if (newCodes.Count > 0)
                response.AddToMessage(
                    "Dodane so bile naslednje SI kode: " +
                    String.Join(", ", newCodes) + "<br />");
            if (invalidCodes.Count > 0)
                response.AddToMessage(
                    "Napačno formatirane SI kode: " +
                    String.Join(", ", invalidCodes));
            if (existingCodes.Count > 0)
                response.AddToMessage(
                    "SI kode, ki že obstajajo v bazi: " +
                    String.Join(", ", existingCodes));
            response.Ok = true;
            return response;
        }

        public SuccessResponse Delete(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var siChip = db.SiChips.Where(x => x.Id == id).FirstOrDefault();
                if (siChip == null)
                {
                    response.Message = "SI čip s tem id ne obstaja!";
                    return response;
                }
                if (siChip.Locked)
                {
                    response.Message = "SI čip je zaklenjen!";
                    return response;
                }
                if (db.Teams.Where(x => x.SiCode == siChip.SiCode).Count() > 0)
                {
                    response.Message = "Ta SI čip je uporabljen vsaj pri eni ekipi!";
                    return response;
                }
                db.SiChips.Remove(siChip);
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }
    }
}