﻿using Mrtn.Marvin21.Model.Common;

namespace Mrtn.Marvin21.Bl.Services.DatabaseInit
{
    public interface IInitializer
    {
        SuccessResponse LoadCategoriesForGsj();
        SuccessResponse LoadCategoriesForRot();
        SuccessResponse LoadPacks();
        SuccessResponse LoadTasksForGsj();
        SuccessResponse LoadTasksForRot();
        SuccessResponse StationsTemplateForGsj();
        SuccessResponse StationsTemplateForRot();
    }
}