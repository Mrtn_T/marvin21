﻿using Mrtn.Marvin21.Bl.Interface.Tasks;
using Mrtn.Marvin21.Bl.Interface.Teams;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Tasks;
using Mrtn.Marvin21.Bl.Services.Teams;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Model.Common;

namespace Mrtn.Marvin21.Bl.Services.DatabaseInit
{
    public class Initializer : IInitializer
    {
        public SuccessResponse LoadCategoriesForGsj()
        {
            ICategoryService service = new CategoryService();
            var all = service.GetAll();
            if (all.Count > 0) return new SuccessResponse()
            {
                Ok = false,
                Message = "Kategorije niso prazne!"
            };
            service.AddCategory(1, "GG mlajši", true, "GGM", (float)4, 350, 550, "33ff00", false);
            service.AddCategory(2, "GG starejši", true, "GGS", (float)4.5, 400, 600, "00cc00", false);
            service.AddCategory(3, "Popotniki", true, "PP", (float)5.5, 500, 900, "0000cc", false);
            service.AddCategory(4, "RR+Grče", true, "RG", (float)6, 550, 1000, "cc00cc", false);
            service.AddCategory(5, "40plus", true, "40+", (float)5.5, 500, 800, "996633", false);
            return new SuccessResponse()
            {
                Ok = true,
                Message = "Kategorije dodane!"
            };

        }

        public SuccessResponse LoadCategoriesForRot()
        {
            ICategoryService service = new CategoryService();
            var all = service.GetAll();
            if (all.Count > 0) return new SuccessResponse()
            {
                Ok = false,
                Message = "Kategorije niso prazne!"
            };
            service.AddCategory(1, "Popotniki", true, "PPM", (float)5.5, 500, 900, "0000cc", false);
            service.AddCategory(2, "Popotnice", true, "PPŽ", (float)5, 450, 700, "3333ff", false);
            service.AddCategory(3, "Grče", true, "GRM", (float)6, 550, 1000, "cc00cc", false);
            service.AddCategory(4, "Grčice", true, "GRŽ", (float)5.5, 500, 800, "cc33cc", false);
            return new SuccessResponse()
            {
                Ok = true,
                Message = "Kategorije dodane!"
            };
        }

        public SuccessResponse LoadPacks()
        {
            IPackService service = new PackService();
            service.AddIfNew("Rašiški rod", true, true, "RaR", "Ljubljana", false);
            service.AddIfNew("Rod argonitnih ježkov", true, true, "RAJ", "Cerkno", false);
            service.AddIfNew("Rod bičkove skale", true, true, "RBS", "Ljubljana", false);
            service.AddIfNew("Rod bistriških gamsov", true, true, "RBG", "Kamnik", false);
            service.AddIfNew("Rod črni mrav", true, true, "RČM", "Ljubljana", false);
            service.AddIfNew("Rod dobre volje", true, true, "RDV", "Ljubljana", false);
            service.AddIfNew("Rod druge grupe odredov", true, true, "RDGO", "Celje", false);
            service.AddIfNew("Rod gorjanskih tabornikov", true, true, "RGT", "Novo mesto", false);
            service.AddIfNew("Rod heroj vitez", true, true, "RHV", "Ljubljana-Črnuče", false);
            service.AddIfNew("Rod hudi potok", true, true, "RHP", "Šmartno pri Paki", false);
            service.AddIfNew("Rod II. grupe odredov", true, true, "RDGO", "Celje", false);
            service.AddIfNew("Rod jezerske ščuke", true, true, "RJŠ", "Cerknica", false);
            service.AddIfNew("Rod jezerski zmaj", true, true, "RJZ", "Velenje", false);
            service.AddIfNew("Rod kranjskega jegliča", true, true, "RKJ", "Spodnja Idrija", false);
            service.AddIfNew("Rod lilijski grič", true, true, "RLG", "Pesje", false);
            service.AddIfNew("Rod močvirski tulipan", true, true, "RMT", "Ljubljana", false);
            service.AddIfNew("Rod morskih viharnikov", true, true, "RMV", "Portorož", false);
            service.AddIfNew("Rod podkovani krap", true, true, "RPK", "Ljubljana", false);
            service.AddIfNew("Rod Polde Ebrl jamski", true, true, "RPEj", "Zagorje ob Savi", false);
            service.AddIfNew("Rod pusti grad", true, true, "RPG", "Šoštanj", false);
            service.AddIfNew("Rod Rožnik", true, true, "RR", "Ljubljana", false);
            service.AddIfNew("Rod sivi dim", true, true, "RSD", "Leskovec", false);
            service.AddIfNew("Rod sivi volk", true, true, "RSV", "Ljubljana", false);
            service.AddIfNew("Rod skalnih taborov", true, true, "RST", "Domžale", false);
            service.AddIfNew("Rod snežniških ruševcev", true, true, "RSR", "Ilirska Bistrica", false);
            service.AddIfNew("Rod soških mejašev", true, true, "RSM", "Nova Gorica", false);
            service.AddIfNew("Rod Stane Žagar mlajši", true, true, "RSŽ-ml", "Kranj", false);
            service.AddIfNew("Rod stražnih ognjev", true, true, "RSO", "Kranj", false);
            service.AddIfNew("Rod svobodnega Kamnitnika", true, true, "RSK", "Škofja Loka", false);
            service.AddIfNew("Rod tabornikov Topolšica", true, true, "RTT", "Topolšica", false);
            service.AddIfNew("Rod trnovskih regljačev", true, true, "RTR", "Ljubljana", false);
            service.AddIfNew("Rod tršati tur", true, true, "RTT", "Ljubljana", false);
            service.AddIfNew("Rod upornega plamena", true, true, "RUP", "Mengeš", false);
            service.AddIfNew("Rod XI SNOUB", true, true, "RXI", "Maribor", false);
            service.AddIfNew("Rod zelene sreče", true, true, "RZS", "Železniki", false);
            service.AddIfNew("Rod zelenega Žirka", true, true, "RZŽ", "Žiri", false);
            return new SuccessResponse()
            {
                Ok = true,
                Message = "Rodovi dodani!"
            };
        }

        public SuccessResponse LoadTasksForGsj()
        {
            ICategoryService categoryService = new CategoryService();
            ITaskService taskService = new TaskService();
            var allCategories = categoryService.GetAll();
            if (allCategories.Count == 0) return new SuccessResponse()
            {
                Ok = false,
                Message = "Kategorije še niso vnešene!"
            };
            taskService.Add("Vrisovanje", "Vris", "", 1, true, false, false, "Točke", 0, 100, 0, "", 0, 0, 0, false,
                1200, 0, false, null, null, "PP, RG, 40+");
            taskService.Add("Topografski test", "Topo", "", 1, true, false, false, "Točke", 0, 125, 0, "", 0, 0, 0, false,
                300, 0, false, null, null, "GGM, GGS, PP, RG, 40+");
            taskService.Add("Test prva pomoč", "PP", "", 1, false, true, false, "Točke", 0, 100, 0, "", 0, 0, 0, false,
                300, 0, false, null, null, "GGM, GGS, PP, RG, 40+");
            taskService.Add("Tematski test", "TT", "", 1, false, true, false, "Točke", 0, 100, 0, "", 0, 0, 0, false,
                300, 0, false, null, null, "GGM, GGS, PP, RG, 40+");
            taskService.Add("Skica terena", "ST", "", 1, false, true, false, "Točke", 0, 100, 0, "", 0, 0, 0, false,
                300, 0, false, null, null, "GGS, PP, RG, 40+");
            taskService.Add("Ženska kvota", "Ž", "Dodatnih 20 točk za vsako žensko predstavnico v ekipi", 1, true, false, false, "Točke", 0,
                120, 0, "", 0, 0, 0, false, 0, 0, false, null, null, "GGM, GGS, PP, RG, 40+");
            taskService.Add("dodatki, odbitki", "+/-", "Dodatki in odbitki po odločitvi komisije", 1, false, false, true, "Točke",
                -200, 200, 0, "", 0, 0, 0, false, 0, 0, false, null, null, "GGM, GGS, PP, RG, 40+");
            return new SuccessResponse()
            {
                Ok = true,
                Message = "Naloge dodane!"
            };
        }

        public SuccessResponse LoadTasksForRot()
        {
            ICategoryService categoryService = new CategoryService();
            ITaskService taskService = new TaskService();
            var allCategories = categoryService.GetAll();
            if (allCategories.Count == 0) return new SuccessResponse()
            {
                Ok = false,
                Message = "Kategorije še niso vnešene!"
            };


            taskService.Add("Topo testi", "Topo", "",
                1, true, false, false,
                "Točke", 0, 125, 0, "", 0, 0, 0, false, 0, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Vrisovanje 1.", "Vris.1", "",
                1, true, false, false,
                "Točke", -400, 0, 0, "", 0, 0, 0, false, 0, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Skica poti", "S. poti", "",
                2, false, true, false,
                "Točke", 0, 200, 0, "", 0, 0, 0, false, 1800, 0, false, null, null, "PPm, PPž, GRm, GRž");

            //60 točk za najhitrejšo. Linearno do 3min. Nad 3min 0 točk za čas.
            taskService.Add("Semafor", "Semaf.", "60 točk za najhitrejšo. Linearno do 3min. Nad 3min 0 točk za čas.",
                2, false, true, false,
                "Napake", 0, 60, 0, "Čas", 0, 180, 180, true, 600, 0, false, null, null, "PPm, PPž, GRm, GRž");

            //60 točk za najhitrejšo. Linearno do 4min. Nad 3min 0 točk za čas.
            taskService.Add("Morse", "Morse", "60 točk za najhitrejšo. Linearno do 4min. Nad 3min 0 točk za čas.",
                2, true, false, false,
                "Napake", 0, 60, 0, "Čas", 0, 240, 240, true, 240, 1, false, null, null, "PPm, PPž, GRm, GRž");

            taskService.Add("Opis poti", "Opis", "",
                2, false, true, false,
                "Točke", 0, 60, 0, "", 0, 0, 0, false, 600, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Skica terena", "Skica", "",
                2, false, true, false,
                "Točke", 0, 150, 0, "", 0, 0, 0, false, 1200, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Profil terena", "Profil", "",
                2, false, true, false,
                "Točke", 0, 100, 0, "", 0, 0, 0, false, 900, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Bivak", "Bivak", "",
                2, false, false, true,
                "Točke", 0, 100, 0, "", 0, 0, 0, false, 0, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Ogenj", "Ogenj", "",
                2, false, false, true,
                "Točke", 0, 50, 0, "", 0, 0, 0, false, 0, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Topel obrok", "Golaž", "",
                2, false, false, true,
                "Točke", 0, 50, 0, "", 0, 0, 0, false, 0, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Lokostrelstvo", "Lok", "",
                2, false, true, false,
                "Točke", 0, 50, 0, "", 0, 0, 0, false, 600, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Prva pomoč", "PP", "",
                2, false, true, false,
                "Točke", 0, 60, 0, "", 0, 0, 0, false, 600, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Kroki", "Kroki", "",
                3, false, true, false,
                "Točke", 0, 150, 0, "", 0, 0, 0, false, 1800, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Minsko polje", "Minsko", "",
                3, false, true, false,
                "Točke", 0, 100, 0, "", 0, 0, 0, false, 600, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Skica minskega polja", "S. minca", "",
                3, false, true, false,
                "Točke", 0, 80, 0, "", 0, 0, 0, false, 0, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Pregled opreme", "Oprema", "",
                3, false, true, false,
                "Točke", -575, 0, 0, "", 0, 0, 0, false, 300, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Vrisovanje 2.", "Vris.2", "",
                3, true, false, false,
                "Točke", -400, 0, 0, "", 0, 0, 0, false, 1200, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Popravki", "+/-", "Dodatki in odbitki po odločitvi komisije.",
                3, false, false, true,
                "Točke", -400, 400, 0, "", 0, 0, 0, false, 0, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Prihod pod kotom", "PPK", "",
                3, false, true, false,
                "Točke", 0, 30, 0, "", 0, 0, 0, false, 0, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Signalni stolp", "Stolp", "",
                3, false, true, false,
                "", 0, 100, 0, "", 0, 0, 0, false, 900, 0, false, null, null, "PPm, PPž, GRm, GRž");
            taskService.Add("Neoznačen KT", "Ne. KT", "",
                3, false, true, false,
                "Točke", 0, 100, 0, "", 0, 0, 0, false, 0, 0, false, null, null, "PPm, PPž, GRm, GRž");


            return new SuccessResponse()
            {
                Ok = true,
                Message = "Naloge dodane!"
            };

        }

        public SuccessResponse StationsTemplateForGsj()
        {
            IStationService stationService = new StationService();
            var allStations = stationService.Get();
            if (allStations.Count > 0) return new SuccessResponse()
            {
                Ok = false,
                Message = "Točke že obstajajo!"
            };
            stationService.Add("start", 0, 0, 1);
            stationService.Add("kt", 0, 0, 1);
            stationService.Add("kt", 0, 0, 1);
            stationService.Add("live", 0, 0, 1);
            stationService.Add("kt", 0, 0, 1);
            stationService.Add("live", 0, 0, 1);
            stationService.Add("kt", 0, 0, 1);
            stationService.Add("finish", 0, 0, 1);
            return new SuccessResponse()
            {
                Ok = true,
                Message = "Točke dodane!"
            };

        }

        public SuccessResponse StationsTemplateForRot()
        {
            IStationService stationService = new StationService();
            var allStations = stationService.Get();
            if (allStations.Count > 0) return new SuccessResponse()
            {
                Ok = false,
                Message = "Točke že obstajajo!"
            };
            stationService.Add("start", 0, 0, 2);
            stationService.Add("kt", 0, 0, 2);
            stationService.Add("kt", 0, 0, 2);
            stationService.Add("live", 0, 0, 2);
            stationService.Add("kt", 0, 0, 2);
            stationService.Add("live", 0, 0, 2);
            stationService.Add("kt", 0, 0, 2);
            stationService.Add("finish", 0, 0, 2);
            stationService.Add("start", 0, 0, 3);
            stationService.Add("kt", 0, 0, 3);
            stationService.Add("kt", 0, 0, 3);
            stationService.Add("live", 0, 0, 3);
            stationService.Add("kt", 0, 0, 3);
            stationService.Add("live", 0, 0, 3);
            stationService.Add("kt", 0, 0, 3);
            stationService.Add("finish", 0, 0, 3);

            return new SuccessResponse()
            {
                Ok = true,
                Message = "Točke dodane!"
            };
        }
    }
}
