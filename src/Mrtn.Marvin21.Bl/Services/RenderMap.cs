﻿namespace Mrtn.Marvin21.Bl.Services
{
    public class RenderMap
    {

        // nastavitve za krogec
        public int KtMarkAlpha
        {
            get
            {
                return AppSettings.RenderMapKtMarkAlpha;
            }
        }
        public int KtMarkDiameter
        {
            get
            {
                return AppSettings.RenderMapKtMarkDiameter;
            }
        }
        public int KtMarkCenterDiameter
        {
            get
            {
                return AppSettings.RenderMapKtMarkCenterDiameter;
            }
        }
        public string KtMarkColor
        {
            get
            {
                return AppSettings.RenderMapKtMarkColor;
            }
        }
        public int KtMarkLineWidth
        {
            get
            {
                return AppSettings.RenderMapKtMarkLineWidth;
            }
        }

        //Nastavitve za besedilo
        public int KtTextAlpha
        {
            get
            {
                return AppSettings.RenderMapKtTextAlpha;
            }
        }
        public string KtTextColor
        {
            get
            {
                return AppSettings.RenderMapKtTextColor;
            }
        }
        public int KtTextBgAlpha
        {
            get
            {
                return AppSettings.RenderMapKtTextBgAlpha;
            }
        }
        public string KtTextBgColor
        {
            get
            {
                return AppSettings.RenderMapKtTextBgColor;
            }
        }
        public string KtTextFontName
        {
            get
            {
                return AppSettings.RenderMapKtTextFontName;
            }
        }
        public float KtTextFontSize
        {
            get
            {
                return AppSettings.RenderMapKtTextFontSize;
            }
        }
        public int KtTextBgOffset
        {
            get
            {
                return AppSettings.RenderMapKtTextFontSize;
            }
        }
        public int KtTextXOffset
        {
            get
            {
                return AppSettings.RenderMapKtTextXOffset;
            }
        }
        public int KtTextYOffset
        {
            get
            {
                return AppSettings.RenderMapKtTextYOffset;
            }
        }
        public int StartTextXOffset
        {
            get
            {
                return AppSettings.RenderMapStartTextXOffset;
            }
        }
        public int StartTextYOffset
        {
            get
            {
                return AppSettings.RenderMapStartTextYOffset;
            }
        }
        public int FinishTextXOffset
        {
            get
            {
                return AppSettings.RenderMapFinishTextXOffset;
            }
        }
        public int FinishTextYOffset
        {
            get
            {
                return AppSettings.RenderMapFinishTextYOffset;
            }
        }

        // nastavitev za kvadratek za ranjenca
        public int KtHiddenRectangleDimension
        {
            get
            {
                return AppSettings.RenderMapKtHiddenRectangleDimension;
            }
        }
    }
}