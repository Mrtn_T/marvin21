﻿using System;
using System.Linq;
using Mrtn.Marvin21.Dao;

namespace Mrtn.Marvin21.Bl.Services
{

    public static class AppSettings
    {
        private static bool _isInitialized;

        private static float _maxFactor;
        private static float _taskFactor;
        private static int _days;
        private static int _mapDpi;
        private static int _mapScale;
        private static int _renderMapKtMarkAlpha;
        private static int _renderMapKtMarkDiameter;
        private static int _renderMapKtMarkCenterDiameter;
        private static string _renderMapKtMarkColor;
        private static int _renderMapKtMarkLineWidth;
        private static int _renderMapKtTextAlpha;
        private static string _renderMapKtTextColor;
        private static int _renderMapKtTextBgAlpha;
        private static string _renderMapKtTextBgColor;
        private static string _renderMapKtTextFontName;
        private static int _renderMapKtTextFontSize;
        private static int _renderMapKtTextXOffset;
        private static int _renderMapKtTextYOffset;
        private static int _renderMapStartTextXOffset;
        private static int _renderMapStartTextYOffset;
        private static int _renderMapFinishTextXOffset;
        private static int _renderMapFinishTextYOffset;
        private static int _renderMapKtHiddenRectangleDimension;
        private static bool _useMaps;
        private static string _competitionName;
        private static int _renderMapKtTextBgOffset;


        public static int Days
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _days;
            }
        }

        public static int MapDpi
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _mapDpi;
            }
        }


        public static int MapScale
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _mapScale;
            }
        }

        public static int RenderMapKtMarkAlpha
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtMarkAlpha;
            }
        }

        public static int RenderMapKtMarkDiameter
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtMarkDiameter;
            }
        }

        public static int RenderMapKtMarkCenterDiameter
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtMarkCenterDiameter;
            }
        }

        public static string RenderMapKtMarkColor
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtMarkColor;
            }
        }

        public static int RenderMapKtMarkLineWidth
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtMarkLineWidth;
            }
        }

        public static int RenderMapKtTextAlpha
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtTextAlpha;
            }
        }

        public static string RenderMapKtTextColor
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtTextColor;
            }
        }

        public static int RenderMapKtTextBgAlpha
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtTextBgAlpha;
            }
        }

        public static string RenderMapKtTextBgColor
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtTextBgColor;
            }
        }

        public static string RenderMapKtTextFontName
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtTextFontName;
            }
        }

        public static int RenderMapKtTextFontSize
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtTextFontSize;
            }
        }

        public static int RenderMapKtTextXOffset
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtTextXOffset;
            }
        }

        public static int RenderMapKtTextYOffset
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtTextYOffset;
            }
        }

        public static int RenderMapStartTextXOffset
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapStartTextXOffset;
            }
        }

        public static int RenderMapStartTextYOffset
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapStartTextYOffset;
            }
        }

        public static int RenderMapFinishTextXOffset
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapFinishTextXOffset;
            }
        }

        public static int RenderMapFinishTextYOffset
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapFinishTextYOffset;
            }
        }

        public static int RenderMapKtHiddenRectangleDimension
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtHiddenRectangleDimension;
            }
        }



        public static float MaxFactor
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _maxFactor;
            }
        }

        public static float TaskFactor
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _taskFactor;
            }
        }


        public static string CompetitionName
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _competitionName;
            }
        }

        public static bool UseMaps
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _useMaps;
            }
        }

        public static int RenderMapKtTextBgOffset
        {
            get
            {
                if (!_isInitialized) Initialize();
                return _renderMapKtTextBgOffset;
            }
        }








        //****** initializer *************
        private static void Initialize()
        {
            using (var db = new Marvin20Db())
            {
                _days = GetInt("Days", db);
                _mapDpi = GetInt("MapDpi", db);
                _mapScale = GetInt("MapScale", db);
                _maxFactor = GetFloat("MaxFactor", db);
                _taskFactor = GetFloat("TaskFactor", db);

                _renderMapKtMarkAlpha = GetInt("RenderMap.KtMarkAlpha", db);
                _renderMapKtMarkDiameter = GetInt("RenderMap.KtMarkDiameter", db);
                _renderMapKtMarkCenterDiameter = GetInt("RenderMap.KtMarkCenterDiameter", db);
                _renderMapKtMarkColor = GetString("RenderMap.KtMarkColor", db);
                _renderMapKtMarkLineWidth = GetInt("RenderMap.KtMarkLineWidth", db);
                _renderMapKtTextAlpha = GetInt("RenderMap.KtTextAlpha", db);
                _renderMapKtTextColor = GetString("RenderMap.KtTextColor", db);
                _renderMapKtTextBgAlpha = GetInt("RenderMap.KtTextBgAlpha", db);
                _renderMapKtTextBgColor = GetString("RenderMap.KtTextBgColor", db);
                _renderMapKtTextFontName = GetString("RenderMap.KtTextFontName", db);
                _renderMapKtTextFontSize = GetInt("RenderMap.KtTextFontSize", db);
                _renderMapKtTextXOffset = GetInt("RenderMap.KtTextXOffset", db);
                _renderMapKtTextYOffset = GetInt("RenderMap.KtTextYOffset", db);
                _renderMapStartTextXOffset = GetInt("RenderMap.StartTextXOffset", db);
                _renderMapStartTextYOffset = GetInt("RenderMap.StartTextYOffset", db);
                _renderMapFinishTextXOffset = GetInt("RenderMap.FinishTextXOffset", db);
                _renderMapFinishTextYOffset = GetInt("RenderMap.FinishTextYOffset", db);
                _renderMapKtHiddenRectangleDimension = GetInt("RenderMap.KtHiddenRectangleDimension", db);
                _renderMapKtTextBgOffset = GetInt("RenderMap.KtTextBgOffset", db);

                _useMaps = GetBool("UseMaps", db);
                _competitionName = GetString("CompetitionName", db);
            }
            _isInitialized = true;
        }

        // ****** calculated properties *************
        public static double PixelsPerMeter
        {
            get { return (MapDpi * 1000) / (25.4 * MapScale); }
        }
        public static double MetersPerPixel
        {
            get { return (25.4 * MapScale) / (MapDpi * 1000); }
        }



        // ****** getters *************
        private static float GetFloat(string code, Marvin20Db db)
        {
            var setting = db.Settings.Single(x => x.Code == code);
            return float.Parse(setting.Value);
        }

        private static string GetString(string code, Marvin20Db db)
        {
            var setting = db.Settings.Single(x => x.Code == code);
            return setting.Value;
        }

        private static int GetInt(string code, Marvin20Db db)
        {
            var setting = db.Settings.Single(x => x.Code == code);
            return Int32.Parse(setting.Value);
        }

        private static bool GetBool(string code, Marvin20Db db)
        {
            var setting = db.Settings.Single(x => x.Code == code);
            return bool.Parse(setting.Value);
        }
    }



}
