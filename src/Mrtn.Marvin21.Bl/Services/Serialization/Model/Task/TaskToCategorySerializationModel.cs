﻿using Mrtn.Marvin21.DbModel;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Task
{
    internal class TaskToCategorySerializationModel : EntityBase
    {
        public int TaskId { get; set; }
        public string CategoryAbbr { get; set; }
    }
}