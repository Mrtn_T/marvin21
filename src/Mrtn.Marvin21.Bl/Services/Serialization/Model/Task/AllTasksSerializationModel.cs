﻿using System.Collections.Generic;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Task
{
    class AllTasksSerializationModel
    {
        public List<TaskSerializationModel> Tasks { get; set; }
        public List<TaskToCategorySerializationModel> TaskToCategories { get; set; }
    }
}
