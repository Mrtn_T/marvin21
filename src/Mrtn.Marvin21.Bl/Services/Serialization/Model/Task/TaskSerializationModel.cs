﻿using Mrtn.Marvin21.DbModel;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Task
{
    internal class TaskSerializationModel : EntityBase
    {
        // Podatki o sami nalogi
        public string Name { get; set; }
        public string Abbr { get; set; }
        public string Description { get; set; }

        // Podatki tem, kdaj in kje se naloga izvaja
        public int Day { get; set; }
        public bool BeforeStart { get; set; }
        public bool OnTrack { get; set; }
        public bool AfterFinish { get; set; }
        public string StationName { get; set; }

        // Kaj se vnaša
        public string FirstEntryName { get; set; }
        public int FirstEntryMin { get; set; }
        public int FirstEntryMax { get; set; }
        public int FirstEntryDefault { get; set; }

        public string SecondEntryName { get; set; }
        public int SecondEntryMin { get; set; }
        public int SecondEntryMax { get; set; }
        public int SecondEntryDefault { get; set; }
        public bool SecondEntryIsTime { get; set; }

        // Algoritem za računanje točk. 0 pomeni preprost vnos!
        public int CalculatingAlgorythmId { get; set; }

        public int TimeForTask { get; set; }

    }
}