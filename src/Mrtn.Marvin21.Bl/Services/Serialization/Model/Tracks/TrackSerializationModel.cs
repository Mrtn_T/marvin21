﻿using Mrtn.Marvin21.DbModel;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Tracks
{
    class TrackSerializationModel : EntityBase
    {
        public virtual string CategoryAbbr { get; set; }
        public virtual int Day { get; set; }

        public virtual float? CumulativeDistance { get; set; }
        public virtual float? CumulativeUphill { get; set; }
        public virtual float? CumulativeDownhill { get; set; }

        public virtual int? OptimalTime { get; set; }
        public int? OptimalPathTime { get; set; }
    }
}
