﻿using Mrtn.Marvin21.DbModel;
using Mrtn.Marvin21.DbModel.Tracks.Support;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Tracks
{
    class StationSerializationModel : EntityBase
    {
        public string Name { get; set; }
        public string SiCodeArrival { get; set; }
        public string SiCodeDeparture { get; set; }
        public bool IsStart { get; set; }
        public bool IsFinish { get; set; }
        public MapPosition MapPosition { get; set; }
        public bool IsAlive { get; set; }
        public bool IsHidden { get; set; }
        public string Description { get; set; }
        public int Day { get; set; }
        public int DefaultPoints { get; set; }
    }
}
