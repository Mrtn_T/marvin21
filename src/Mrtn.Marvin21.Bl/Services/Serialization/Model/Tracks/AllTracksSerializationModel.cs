﻿using System.Collections.Generic;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Tracks
{
    class AllTracksSerializationModel
    {
        public List<StationSerializationModel> Stations { get; set; }
        public List<PathSerializationModel> Paths { get; set; }
        public List<PathPointSerializationModel> PathPoints { get; set; }
        public List<TrackSerializationModel> Tracks { get; set; }
        public List<TrackCheckpointSerializationModel> TrackCheckpoints { get; set; }
    }
}
