﻿using Mrtn.Marvin21.DbModel;
using Mrtn.Marvin21.DbModel.Tracks.Support;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Tracks
{
    class PathPointSerializationModel : EntityBase
    {
        public int No { get; set; }
        public MapPosition MapPosition { get; set; }
        public int PathId { get; set; }
    }
}
