﻿using Mrtn.Marvin21.DbModel;
using Mrtn.Marvin21.DbModel.Tracks.Support;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Tracks
{
    class TrackCheckpointSerializationModel : EntityBase
    {
        public  int No { get; set; }
        public  string Name { get; set; }
        public  int Points { get; set; }
        public  string Description { get; set; }

        public  Time Time { get; set; }
        public  PathData FromPrevious { get; set; }
        public  SpeedTrial SpeedTrial { get; set; }

        //linked entites
        public  int TrackId { get; set; }
        public  int StationId { get; set; }
        public  int? PathFromPreviousId { get; set; }

    }
}
