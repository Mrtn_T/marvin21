﻿using Mrtn.Marvin21.DbModel;
using Mrtn.Marvin21.DbModel.Tracks.Support;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Tracks
{
    class PathSerializationModel : EntityBase
    {
        public int FirstStationId { get; set; }
        public int SecondStationId { get; set; }
        public bool IsManual { get; set; }
        public PathData ManualData { get; set; }
        public PathData AutomaticData { get; set; }
    }
}
