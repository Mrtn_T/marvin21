﻿using System.Collections.Generic;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Teams
{
    class AllTeamsSerializationModel
    {
        public List<SiChipSerializationModel> SiChips { get; set; }
        public List<PackSerializationModel> Packs { get; set; }
        public List<CategorySerializationModel> Categories { get; set; }
        public List<TeamSerializationModel> Teams { get; set; }
    }
}
