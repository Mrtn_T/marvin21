﻿using Mrtn.Marvin21.DbModel;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Teams
{
    public class TeamSerializationModel : EntityBase
    {
        public int CategoryId { get; set; }
        public int PackId { get; set; }
        public string Name { get; set; }
        public int? No { get; set; }
        public string SiCode { get; set; }
        public string PrivateSiCode { get; set; }
        //public virtual Pack Pack { get; set; }
        //public virtual Category Category { get; set; }
        public bool Active { get; set; }
        public bool InCompetition { get; set; }
    }
}