﻿using Mrtn.Marvin21.DbModel;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Teams
{
    public class PackSerializationModel : EntityBase
    {
        public string Name { get; set; }
        public bool Active { get; set; }
        public bool InCompetition { get; set; }
        public string Abbr { get; set; }
        public string Town { get; set; }
        //public virtual IList<Team> Teams { get; set; }
    }
}