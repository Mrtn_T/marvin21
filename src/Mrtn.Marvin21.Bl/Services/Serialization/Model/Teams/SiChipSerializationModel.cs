﻿using Mrtn.Marvin21.DbModel;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Teams
{
    public class SiChipSerializationModel : EntityBase
    {
        public bool Private { get; set; }
        public string SiCode { get; set; }
        public string Comment { get; set; }
        public bool Active { get; set; }
    }
}