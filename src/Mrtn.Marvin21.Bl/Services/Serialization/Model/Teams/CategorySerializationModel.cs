﻿using Mrtn.Marvin21.DbModel;

namespace Mrtn.Marvin21.Bl.Services.Serialization.Model.Teams
{
    public class CategorySerializationModel : EntityBase
    {
        public int No { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string Abbr { get; set; }
        //public virtual IList<Team> Teams { get; set; }
        //public virtual IList<Track> Tracks { get; set; }
        public float Speed { get; set; }
        public float UphillSpeed { get; set; }
        public float DownhillSpeed { get; set; }
        public string Color { get; set; }
        //public virtual List<TaskToCategory> TaskToCategories { get; set; }
    }
}