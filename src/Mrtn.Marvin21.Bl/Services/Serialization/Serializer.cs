﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Mrtn.Marvin21.Bl.Services.Serialization.Model;
using Mrtn.Marvin21.Bl.Interface.Serialization;
using Mrtn.Marvin21.Bl.Services.Serialization.Model.Task;
using Mrtn.Marvin21.Bl.Services.Serialization.Model.Teams;
using Mrtn.Marvin21.Bl.Services.Serialization.Model.Tracks;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.DbModel.Tasks;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.Model.Common;
using Newtonsoft.Json;

namespace Mrtn.Marvin21.Bl.Services.Serialization
{
    public class Serializer : ISerializer
    {
        public string SerializeTracks()
        {
            using (var db = new Marvin20Db())
            {
                var model = new AllTracksSerializationModel();

                //Dodam statione
                model.Stations = new List<StationSerializationModel>();
                foreach (var station in db.Stations)
                {
                    var ssm = new StationSerializationModel();
                    EntityMerger.Merge(ssm, station);
                    model.Stations.Add(ssm);
                }

                //Dodam Pathe
                model.Paths = new List<PathSerializationModel>();
                foreach (var path in db.Paths)
                {
                    var psm = new PathSerializationModel();
                    EntityMerger.Merge(psm, path);
                    psm.FirstStationId = path.FirstStation.Id;
                    psm.SecondStationId = path.SecondStation.Id;
                    model.Paths.Add(psm);
                }

                //Dodam PathPointe
                model.PathPoints = new List<PathPointSerializationModel>();
                foreach (var pathPoint in db.PathPoints)
                {
                    var ppsm = new PathPointSerializationModel();
                    EntityMerger.Merge(ppsm, pathPoint);
                    ppsm.PathId = pathPoint.Path.Id;
                    model.PathPoints.Add(ppsm);
                }

                //Dodam tracke
                model.Tracks = new List<TrackSerializationModel>();
                foreach (var track in db.Tracks)
                {
                    var tsm = new TrackSerializationModel();
                    EntityMerger.Merge(tsm, track);
                    tsm.CategoryAbbr = track.Category.Abbr;
                    model.Tracks.Add(tsm);
                }

                //Dodam KT-je
                model.TrackCheckpoints = new List<TrackCheckpointSerializationModel>();
                foreach (var checkpoint in db.TrackCheckpoints)
                {
                    var tpsm = new TrackCheckpointSerializationModel();
                    EntityMerger.Merge(tpsm, checkpoint);
                    if (checkpoint.PathFromPrevious != null)
                        tpsm.PathFromPreviousId = checkpoint.PathFromPrevious.Id;
                    tpsm.StationId = checkpoint.Station.Id;
                    tpsm.TrackId = checkpoint.Track.Id;
                    model.TrackCheckpoints.Add(tpsm);
                }


                var setings = new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    };

                return JsonConvert.SerializeObject(model, Formatting.Indented, setings);
            }
        }

        public string SerializeTeams()
        {
            //(Json format: Categories, Packs, SiChips, Teams)
            using (var db = new Marvin20Db())
            {
                var model = new AllTeamsSerializationModel();

                //Dodam statione
                model.SiChips = new List<SiChipSerializationModel>();
                foreach (var siChip in db.SiChips)
                {
                    var sicsm = new SiChipSerializationModel();
                    EntityMerger.Merge(sicsm, siChip);
                    model.SiChips.Add(sicsm);
                }
                model.Packs = new List<PackSerializationModel>();
                foreach (var pack in db.Packs)
                {
                    var psm = new PackSerializationModel();
                    EntityMerger.Merge(psm, pack);
                    model.Packs.Add(psm);
                }
                model.Categories = new List<CategorySerializationModel>();
                foreach (var category in db.Categories)
                {
                    var catsm = new CategorySerializationModel();
                    EntityMerger.Merge(catsm, category);
                    model.Categories.Add(catsm);
                }
                model.Teams = new List<TeamSerializationModel>();
                foreach (var team in db.Teams)
                {
                    var tsm = new TeamSerializationModel();
                    EntityMerger.Merge(tsm, team);
                    tsm.CategoryId = team.Category.Id;
                    tsm.PackId = team.Pack.Id;
                    model.Teams.Add(tsm);
                }

                var setings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                };

                return JsonConvert.SerializeObject(model, Formatting.Indented, setings);
            }
        }

        public string SerializeTasks()
        {
            //Json format: Tasks, TaskToCategories)
            using (var db = new Marvin20Db())
            {
                var model = new AllTasksSerializationModel();

                //Dodam statione
                model.Tasks = new List<TaskSerializationModel>();
                foreach (var task in db.Tasks)
                {
                    var tsm = new TaskSerializationModel();
                    EntityMerger.Merge(tsm, task);
                    if (task.Station != null)
                        tsm.StationName = task.Station.Name;
                    model.Tasks.Add(tsm);
                }
                model.TaskToCategories = new List<TaskToCategorySerializationModel>();
                foreach (var taskToCategory in db.TasksToCategories)
                {
                    var ttscm = new TaskToCategorySerializationModel
                        {
                            CategoryAbbr =
                                taskToCategory.Category != null
                                ? taskToCategory.Category.Abbr
                                : "",
                            TaskId =
                                taskToCategory.Task != null
                                ? taskToCategory.Task.Id
                                : 0
                        };
                    model.TaskToCategories.Add(ttscm);
                }

                var setings = new JsonSerializerSettings
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                };

                return JsonConvert.SerializeObject(model, Formatting.Indented, setings);
            }

        }




        public SuccessResponse DeserializeTracks(string myJson)
        {
            try
            {
                // deserializacija modela
                var model = JsonConvert
                    .DeserializeObject<AllTracksSerializationModel>(myJson);

                //nekaj preverjanj na modelu
                if (model == null) throw new Exception("Deserializacija ni bila uspešna");
                if (model.Stations == null || model.Stations.Count == 0)
                    throw new Exception("Niti ene točke ni na karti");
                if (model.Paths == null || model.Paths.Count == 0)
                    throw new Exception("Niti ene poti ni na karti");
                if (model.PathPoints == null || model.PathPoints.Count == 0)
                    throw new Exception("Niti ene točke na poti ni na karti");

                // Brisanej vsega v zvezi s potmi
                using (var db = new Marvin20Db())
                {
                    foreach (var item in db.TrackCheckpoints)
                        db.TrackCheckpoints.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.Tracks)
                        db.Tracks.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.PathPoints)
                        db.PathPoints.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.Paths)
                        db.Paths.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.Stations)
                        db.Stations.Remove(item);
                    db.SaveChanges();

                    //repopulacija
                    var stationMapping = new Dictionary<int, Station>();
                    foreach (var stationSerializationModel in model.Stations)
                    {
                        var station = new Station();
                        EntityMerger.MergeIgnoringId(station, stationSerializationModel);
                        db.Stations.Add(station);
                        db.SaveChanges();
                        stationMapping.Add(stationSerializationModel.Id, station);
                    }

                    var pathMapping = new Dictionary<int, Path>();
                    foreach (var pathSerializationModel in model.Paths)
                    {
                        var path = new Path();
                        EntityMerger.MergeIgnoringId(path, pathSerializationModel);
                        path.FirstStation = stationMapping[pathSerializationModel.FirstStationId];
                        path.SecondStation = stationMapping[pathSerializationModel.SecondStationId];
                        db.Paths.Add(path);
                        db.SaveChanges();
                        pathMapping.Add(pathSerializationModel.Id, path);
                    }

                    foreach (var pathPointSerializationModel in model.PathPoints)
                    {
                        var pathPoint = new PathPoint();
                        EntityMerger.MergeIgnoringId(pathPoint, pathPointSerializationModel);
                        pathPoint.Path = pathMapping[pathPointSerializationModel.PathId];
                        db.PathPoints.Add(pathPoint);
                        db.SaveChanges();
                    }

                    var trackMapping = new Dictionary<int, Track>();
                    foreach (var trackSerializationModel in model.Tracks)
                    {
                        var track = new Track();
                        EntityMerger.MergeIgnoringId(track, trackSerializationModel);
                        track.Category = db.Categories
                            .Where(x => x.Abbr == trackSerializationModel.CategoryAbbr).Single();
                        db.Tracks.Add(track);
                        db.SaveChanges();
                        trackMapping.Add(trackSerializationModel.Id, track);
                    }
                    foreach (var cpsm in model.TrackCheckpoints)
                    {
                        var checkpoint = new TrackCheckpoint();
                        EntityMerger.MergeIgnoringId(checkpoint, cpsm);
                        if (cpsm.PathFromPreviousId.HasValue)
                            checkpoint.PathFromPrevious =
                                pathMapping[cpsm.PathFromPreviousId.Value];
                        checkpoint.Station = stationMapping[cpsm.StationId];
                        checkpoint.Track = trackMapping[cpsm.TrackId];
                        db.TrackCheckpoints.Add(checkpoint);
                        db.SaveChanges();
                    }

                }
                return new SuccessResponse(true, "Uvoz je bil uspešen");
            }
            catch (Exception ee)
            {
                return new SuccessResponse(false, ee.Message);
            }
        }

        public SuccessResponse DeserializeTeams(string myJson)
        {
            try
            {
                // deserializacija modela
                var model = JsonConvert
                    .DeserializeObject<AllTeamsSerializationModel>(myJson);

                //nekaj preverjanj na modelu
                if (model == null) throw new Exception("Deserializacija ni bila uspešna");
                if (model.Packs == null || model.Packs.Count == 0)
                    throw new Exception("Niti enega rodu ni v datoteki");
                if (model.Categories == null || model.Categories.Count == 0)
                    throw new Exception("Niti ene kategorije ni v datoteki");
                if (model.Teams == null || model.Teams.Count == 0)
                    throw new Exception("Niti ene ekipe ni v datoteki");

                var mergeCategories = true;

                using (var db = new Marvin20Db())
                {
                    if (!mergeCategories)
                        foreach (var category in db.Categories)
                        {
                            if (category.TaskToCategories != null && category.TaskToCategories.Count > 0)
                                throw new Exception("Vsaj ena kategorija vsebuje naloge");
                            if (category.Tracks != null && category.Tracks.Count > 0)
                                throw new Exception("Vsaj ena kategorija ima dodeljeno progo");
                        }

                    // Brisanej vsega v zvezi s potmi
                    foreach (var item in db.Teams)
                        db.Teams.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.SiChips)
                        db.SiChips.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.Packs)
                        db.Packs.Remove(item);
                    db.SaveChanges();

                    if (!mergeCategories)
                    {
                        foreach (var item in db.Categories)
                            db.Categories.Remove(item);
                        db.SaveChanges();
                    }

                    // Si chips
                    if (model.SiChips != null)
                        foreach (var siChipSerializationModel in model.SiChips)
                        {
                            var siChip = new SiChip();
                            EntityMerger.MergeIgnoringId(siChip, siChipSerializationModel);
                            db.SiChips.Add(siChip);
                            db.SaveChanges();
                        }

                    //kategorije
                    var categoryMapping = new Dictionary<int, Category>();
                    foreach (var categorySerializationModel in model.Categories)
                    {
                        Category category;
                        if (mergeCategories)
                        {
                            category = db.Categories.SingleOrDefault(x => x.Abbr == categorySerializationModel.Abbr);
                            if(category==null) throw new Exception(
                                string.Format(
                                    "Med kategorijami '{0}' ne najdem {1}",
                                    string.Join(", ", db.Categories.Select(x=>x.Abbr)),
                                    categorySerializationModel.Abbr)
                                    );
                        }
                        else
                        {
                            category = new Category();
                            EntityMerger.MergeIgnoringId(category, categorySerializationModel);
                            db.Categories.Add(category);
                            db.SaveChanges();
                        }
                        categoryMapping.Add(categorySerializationModel.Id, category);
                    }

                    // rodovi
                    var packMapping = new Dictionary<int, Pack>();
                    foreach (var pathSerializationModel in model.Packs)
                    {
                        var pack = new Pack();
                        EntityMerger.MergeIgnoringId(pack, pathSerializationModel);
                        db.Packs.Add(pack);
                        db.SaveChanges();
                        packMapping.Add(pathSerializationModel.Id, pack);
                    }

                    //ekipe
                    foreach (var teamSerializationModel in model.Teams)
                    {
                        var team = new Team();
                        EntityMerger.MergeIgnoringId(team, teamSerializationModel);
                        team.Pack = packMapping[teamSerializationModel.PackId];
                        team.Category = categoryMapping[teamSerializationModel.CategoryId];
                        db.Teams.Add(team);
                        db.SaveChanges();
                    }
                }
                return new SuccessResponse(true, "Uvoz je bil uspešen");
            }
            catch (Exception ee)
            {
                return new SuccessResponse(false, ee.Message);
            }
        }

        public SuccessResponse DeserializeTasks(string myJson)
        {
            try
            {
                // deserializacija modela
                var model = JsonConvert
                    .DeserializeObject<AllTasksSerializationModel>(myJson);

                //nekaj preverjanj na modelu
                if (model == null) throw new Exception("Deserializacija ni bila uspešna");
                if (model.TaskToCategories == null || model.TaskToCategories.Count == 0)
                    throw new Exception("Niti ene povezave med nalogo in kategorijo ni v datoteki");
                if (model.Tasks == null || model.Tasks.Count == 0)
                    throw new Exception("Niti ene naloge ni v datoteki");

                using (var db = new Marvin20Db())
                {

                    // Brisanej vsega v zvezi s potmi
                    foreach (var item in db.TasksToCategories)
                        db.TasksToCategories.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.Tasks)
                        db.Tasks.Remove(item);
                    db.SaveChanges();

                    // taski
                    var taskMapping = new Dictionary<int, Task>();
                    foreach (var taskSerializationModel in model.Tasks)
                    {
                        var task = new Task();
                        EntityMerger.MergeIgnoringId(task, taskSerializationModel);

                        if (taskSerializationModel.StationName != "")
                        {
                            var station = db.Stations
                                .Where(x => x.Name == taskSerializationModel.StationName
                                    && x.Day == taskSerializationModel.Day)
                                .FirstOrDefault();
                            if (station != null) task.Station = station;
                        }
                        db.Tasks.Add(task);
                        db.SaveChanges();
                        taskMapping.Add(taskSerializationModel.Id, task);
                    }

                    //tazki za kategorije
                    foreach (var taskToCategorySerializationModel in model.TaskToCategories)
                    {
                        var taskToCategory = new TaskToCategory();
                        taskToCategory.Task = taskMapping[taskToCategorySerializationModel.TaskId];

                        var category = db.Categories
                            .Where(x => x.Abbr == taskToCategorySerializationModel.CategoryAbbr)
                            .FirstOrDefault();
                        if (category != null) taskToCategory.Category = category;
                        db.TasksToCategories.Add(taskToCategory);
                        db.SaveChanges();
                    }

                }
                return new SuccessResponse(true, "Uvoz je bil uspešen");
            }
            catch (Exception ee)
            {
                return new SuccessResponse(false, ee.Message);
            }
        }

        public SuccessResponse DeleteAllEntites()
        {
            try
            {
                using (var db = new Marvin20Db())
                {

                    foreach (var item in db.TasksToCategories)
                        db.TasksToCategories.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.Tasks)
                        db.Tasks.Remove(item);
                    db.SaveChanges();

                    foreach (var item in db.TrackCheckpoints)
                        db.TrackCheckpoints.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.Tracks)
                        db.Tracks.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.PathPoints)
                        db.PathPoints.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.Paths)
                        db.Paths.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.Stations)
                        db.Stations.Remove(item);
                    db.SaveChanges();


                    foreach (var item in db.Teams)
                        db.Teams.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.SiChips)
                        db.SiChips.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.Packs)
                        db.Packs.Remove(item);
                    db.SaveChanges();
                    foreach (var item in db.Categories)
                        db.Categories.Remove(item);
                    db.SaveChanges();

                }
            }
            catch (Exception ee)
            {
                return new SuccessResponse(false, ee.Message);
            }

            return new SuccessResponse(true, "Baza je prazna!");

        }
    }

}
