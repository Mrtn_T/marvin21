﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Interface.Result;
using Mrtn.Marvin21.Bl.Services.Tasks;
using Mrtn.Marvin21.Bl.Services.Teams;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.Model.Result;
using Mrtn.Marvin21.Model.Tasks;
using Mrtn.Marvin21.Model.Teams;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Services.Result
{
    public class ResultService : IResultService
    {
        public TeamResultsByDayViewModel TeamsByDayResult(int day, bool showInactive = false)
        {
            
            var model = new TeamResultsByDayViewModel
            {
                Headers = new List<TeamResultsHeaderViewModel>()
            };
            using (var db = new Marvin20Db())
            {
                //------------------------------
                // dodam ekipe (vrstice)
                //------------------------------
                model.Rows = new List<TeamResultsRowViewModel>();
                var teams = new TeamService().Get(new TeamQueryModel { ShowInactive = showInactive,SortByPlace=true });
                foreach (var team in teams)
                {
                    model.Rows.Add(new TeamResultsRowViewModel
                    {
                        Team = team,
                        Cells = new List<TeamResultsCellViewModel>()
                    });
                }
                var tasks = new TaskService().Get();
                var tracks = new TrackService().GetTracks(new TrackQueryModel { });

                //grem čez stolpce

                for (var currentDay = 1; currentDay < AppSettings.Days + 1; currentDay++)
                {
                    if ( currentDay == day)
                        InsertColumnsForDay(tracks, currentDay, model, db, tasks);
                    InsertSumsForDay(tracks, currentDay, model, db, tasks);
                }

            }
            return model;
        }

        private void InsertSumsForDay(List<TrackViewModel> tracks, int day, TeamResultsByDayViewModel model, Marvin20Db db, List<TaskViewModel> tasks)
        {
            model.Headers.Add(new TeamResultsHeaderViewModel
            {
                Day = day,
                Name = String.Format("&sum; d{0}", day),
                Sum = true
            });
            var dayTracks = tracks.Where(x => x.Day == day).ToList();
            foreach (var row in model.Rows)
            {
                var sumPoints = 0;
                if (dayTracks.Count > 0)
                {
                    var track =
                    tracks.SingleOrDefault(x => x.Day == day &&
                                                x.CategoryId == row.Team.CategoryId);
                    if (track != null)
                    {
                        var runTrack = db.RunTracks.FirstOrDefault(x => x.Team.Id == row.Team.Id &&
                                                                        x.Track.Id == track.Id);
                        if (runTrack != null)
                        {
                            //sumPoints += runTrack.PointsForCheckpoints;
                            //sumPoints += runTrack.PointsForTime;
                            sumPoints += runTrack.TrackPoints;
                            sumPoints += runTrack.SpeedTrialPoints;
                        }
                    }
                }
                var dayTasks = tasks.Where(x => x.Day == day).ToList();
                foreach (var task in dayTasks)
                {
                    if (task.Categories.Any(x => x.Key == row.Team.CategoryId))
                    {
                        var runTask = db.RunTasks.SingleOrDefault(x => x.Task.Id == task.Id &&
                                                                       x.Team.Id == row.Team.Id);
                        if (runTask != null)
                        {
                            sumPoints += runTask.Points;
                        }
                    }
                }
                var cell = new TeamResultsCellViewModel { TaskPoints = sumPoints };
                row.Cells.Add(cell);
            }

        }

        private void InsertColumnsForDay(List<TrackViewModel> tracks, int day, TeamResultsByDayViewModel model, Marvin20Db db, List<TaskViewModel> tasks)
        {
            //------------------------------
            // za vsak dan...
            //------------------------------
            var dayTracks = tracks.Where(x => x.Day == day).ToList();
            if (dayTracks.Count > 0)
            {
                //---------------------------------------------------------
                //
                //   ta dan ima progo, treba je dodat vse za progo
                //
                //---------------------------------------------------------
                model.Headers.Add(new TeamResultsHeaderViewModel
                {
                    Day = day,
                    Name = String.Format("Proga, dan {0}", day),
                    IsTrack = true
                });
                foreach (var row in model.Rows)
                {
                    var cell = new TeamResultsCellViewModel()
                    {
                        IsTrack = true,
                        Day = day,
                        TeamId = row.Team.Id
                    };

                    var track =
                        tracks.SingleOrDefault(x => x.Day == day &&
                                                    x.CategoryId == row.Team.CategoryId);
                    if (track == null)
                    {
                        //sploh ni proge za to ekipo za ta dan
                        cell.HasNoEntry = true;
                    }
                    else
                    {
                        var runTrack = db.RunTracks.FirstOrDefault(x => x.Team.Id == row.Team.Id &&
                                                                        x.Track.Id == track.Id);
                        if (runTrack == null)
                        {
                            //proga je, vendar še ni vnosa
                            cell.NoResultYet = true;
                        }
                        else
                        {
                            //proga je, vnos je, treba je prikazat...
                            cell.CheckpointsFound = runTrack.CheckpointsFound;
                            cell.PointsForCheckpoints = runTrack.PointsForCheckpoints;
                            cell.PointsForTime = runTrack.PointsForTime;
                            cell.TrackPoints = runTrack.TrackPoints;
                            cell.SpeedTrialPoints = runTrack.SpeedTrialPoints;
                            cell.Locked = runTrack.Locked;
                            cell.RunTrackId = runTrack.Id;
                        }
                    }
                    row.Cells.Add(cell);
                }
            }
            //--------------------------------------------
            //
            //  dodam še vse naloge za izbrani dan
            //
            //--------------------------------------------
            var dayTasks = tasks.Where(x => x.Day == day).ToList();
            foreach (var task in dayTasks)
            {
                //ToDo: kasneje ZDRUŽI naloge z enako kratico

                //------------------------------
                // header za nalogo
                //------------------------------
                var header = new TeamResultsHeaderViewModel
                {
                    Day = day,
                    Name = String.Format(task.Abbr),
                    TaskId = task.Id,
                };
                if (task.BeforeStart) header.Location = "S";
                else if (task.AfterFinish) header.Location = "C";
                else if (task.Station != null) header.Location = "" + task.Station.ToString();
                else header.Location = "?";
                model.Headers.Add(header);
                foreach (var row in model.Rows)
                {
                    //------------------------------
                    // rezultati za nalogo
                    //------------------------------
                    var cell = new TeamResultsCellViewModel();
                    if (task.Categories.Where(x => x.Key == row.Team.CategoryId).Any())
                    {
                        var runTask = db.RunTasks.Where(
                            x =>
                                x.Task.Id == task.Id &&
                                x.Team.Id == row.Team.Id).SingleOrDefault();
                        if (runTask == null)
                        {
                            cell.NoResultYet = true;
                        }
                        else
                        {
                            cell.TaskPoints = runTask.Points;
                            cell.Locked = runTask.Locked;
                        }
                    }
                    else
                    {
                        //ta kategorija NIMA tega taska
                        cell.HasNoEntry = true;
                    }
                    row.Cells.Add(cell);
                }
            }
        }

        public List<PackResultViewModel> PackResults()
        {
            var model = new List<PackResultViewModel>();
            using (var db = new Marvin20Db())
            {
                foreach (var packResult in db.PackResults
                    .Where(x=>x.TeamResults.Count>0)
                    .OrderBy(x => x.Place))
                {
                    var result = new PackResultViewModel
                                     {
                                         Place = packResult.Place,
                                         Points = packResult.Points,
                                         PackAbbr = packResult.Pack.Abbr,
                                         PackNam = packResult.Pack.Name,
                                         PackTown = packResult.Pack.Town,
                                         Percent= packResult.Percent
                    };
                    //******************************************************
                    //***** predelava 2020
                    //******************************************************
                    //var teams = "";
                    //foreach (var teamResult in packResult.TeamResults)
                    //{
                    //    if (teams != "") teams += ", ";
                    //    teams += String.Format("{0} <i>({1} točk)</i>",teamResult.Team.No, teamResult.Team.Result.Points);
                    //}
                    //result.Teams = teams;

                    result.Teams = packResult.Description;
                    model.Add(result);
                }
            }
            return model;
        }

        public TeamResultsViewModel TeamResults()
        {
            var model = new TeamResultsViewModel
                            {
                                Categories = new List<TeamResultsByCategoryViewModel>(),
                                TimeGenerated = DateTime.Now.ToString("H:mm:ss")
                            };
            using (var db = new Marvin20Db())
            {
                foreach (var category in db.Categories.OrderBy(x => x.No))
                {
                    var catModel = new TeamResultsByCategoryViewModel
                        {
                            Category = ViewModelFactory.CategoryViewModel(category),
                            TeamResults = new List<TeamResultViewModel>()
                        };
                    foreach (var teamResult in db.TeamResults
                        .Where(x => x.Team.Category.Id == category.Id && x.Team.Active)
                        .OrderBy(y => y.Place))
                    {
                        var team = teamResult.Team;
                        var trModel = new TeamResultViewModel
                                          {
                                              Name = team.Name,
                                              Place = teamResult.Place,
                                              Points = teamResult.Points,
                                              No = team.No,
                                              SpeedTrialPoints = 0,
                                              TaskPoints = null,
                                              VrisPoints = 0,
                                              TimePoints = 0,
                                              TrackPoints = 0,
                                              PpPoints = 0,
                                              FemalePoints = 0,
                                              SemPoints = 0,
                                              TopoPoints = 0,
                                              TtPoints = 0

                                          };
                        if (team.RunTasks != null)
                            foreach (var runTask in team.RunTasks)
                            {
                                if (trModel.TaskPoints == null) trModel.TaskPoints = runTask.Points;
                                else trModel.TaskPoints += runTask.Points;

                                if (runTask.Task.Abbr.StartsWith("Vris"))
                                {
                                    trModel.VrisPoints += runTask.Points;
                                }
                                if (runTask.Task.Abbr.StartsWith("Topo"))
                                {
                                    trModel.TopoPoints += runTask.Points;
                                }
                                if (runTask.Task.Abbr.StartsWith("SEM"))
                                {
                                    trModel.SemPoints += runTask.Points;
                                }
                                if (runTask.Task.Abbr.StartsWith("PP"))
                                {
                                     trModel.PpPoints += runTask.Points;
                                }
                                if (runTask.Task.Abbr.StartsWith("TT"))
                                {
                                     trModel.TtPoints += runTask.Points;
                                }
                                if (runTask.Task.Abbr.StartsWith("Ž"))
                                {
                                    trModel.FemalePoints += runTask.Points;
                                }

                            }
                        ;
                        if (team.RunTracks != null)
                            foreach (var runTrack in team.RunTracks)
                            {
                                trModel.SpeedTrialPoints += runTrack.SpeedTrialPoints;
                                trModel.TimePoints += runTrack.PointsForTime;
                                trModel.TrackPoints += runTrack.PointsForCheckpoints;
                            }
                        catModel.TeamResults.Add(trModel);
                    }
                    model.Categories.Add(catModel);
                }
            }
            return model;
        }

        public TeamResultsViewModel TeamPpResults()
        {
            var model = new TeamResultsViewModel
            {
                Categories = new List<TeamResultsByCategoryViewModel>(),
                TimeGenerated = DateTime.Now.ToString("H:mm:ss")
            };
            using (var db = new Marvin20Db())
            {
                foreach (var category in db.Categories.OrderBy(x => x.No))
                {
                    var catModel = new TeamResultsByCategoryViewModel
                    {
                        Category = ViewModelFactory.CategoryViewModel(category),
                        TeamResults = new List<TeamResultViewModel>()
                    };
                    foreach (var teamResult in db.TeamResults
                        .Where(x => x.Team.Category.Id == category.Id)
                        .OrderBy(y => y.Place))
                    {
                        var team = teamResult.Team;
                        var trModel = new TeamResultViewModel
                        {
                            Name = team.Name,
                            Place = teamResult.Place,
                            Points = teamResult.Points,
                            No = team.No,
                            SpeedTrialPoints = 0,
                            TaskPoints = null,
                            TimePoints = 0,
                            TrackPoints = 0

                        };
                        if (team.RunTasks != null)
                            foreach (var runTask in team.RunTasks)
                            {
                                if (runTask.Task.Abbr.StartsWith("PP"))
                                {
                                    if (trModel.TaskPoints == null) trModel.TaskPoints = runTask.Points;
                                    else trModel.TaskPoints += runTask.Points;
                                }
                            }
                        catModel.TeamResults.Add(trModel);
                    }
                    model.Categories.Add(catModel);
                }
            }
            return model;
        }

        public TeamResultsViewModel TeamTopoResults()
        {
            var model = new TeamResultsViewModel
            {
                Categories = new List<TeamResultsByCategoryViewModel>(),
                TimeGenerated = DateTime.Now.ToString("H:mm:ss")
            };
            using (var db = new Marvin20Db())
            {
                foreach (var category in db.Categories.OrderBy(x => x.No))
                {
                    var catModel = new TeamResultsByCategoryViewModel
                    {
                        Category = ViewModelFactory.CategoryViewModel(category),
                        TeamResults = new List<TeamResultViewModel>()
                    };
                    foreach (var teamResult in db.TeamResults
                        .Where(x => x.Team.Category.Id == category.Id)
                        .OrderBy(y => y.Place))
                    {
                        var team = teamResult.Team;
                        var trModel = new TeamResultViewModel
                        {
                            Name = team.Name,
                            Place = teamResult.Place,
                            Points = teamResult.Points,
                            No = team.No,
                            SpeedTrialPoints = 0,
                            TaskPoints = null,
                            TimePoints = 0,
                            TrackPoints = 0

                        };
                        if (team.RunTasks != null)
                            foreach (var runTask in team.RunTasks)
                            {
                                if (runTask.Task.Abbr.StartsWith("Topo"))
                                {
                                    if (trModel.TaskPoints == null) trModel.TaskPoints = runTask.Points;
                                    else trModel.TaskPoints += runTask.Points;
                                }
                            }
                        catModel.TeamResults.Add(trModel);
                    }
                    model.Categories.Add(catModel);
                }
            }
            return model;
        }

        public TeamResultsViewModel TeamTtResults()
        {
            var model = new TeamResultsViewModel
            {
                Categories = new List<TeamResultsByCategoryViewModel>(),
                TimeGenerated = DateTime.Now.ToString("H:mm:ss")
            };
            using (var db = new Marvin20Db())
            {
                foreach (var category in db.Categories.OrderBy(x => x.No))
                {
                    var catModel = new TeamResultsByCategoryViewModel
                    {
                        Category = ViewModelFactory.CategoryViewModel(category),
                        TeamResults = new List<TeamResultViewModel>()
                    };
                    foreach (var teamResult in db.TeamResults
                        .Where(x => x.Team.Category.Id == category.Id)
                        .OrderBy(y => y.Place))
                    {
                        var team = teamResult.Team;
                        var trModel = new TeamResultViewModel
                        {
                            Name = team.Name,
                            Place = teamResult.Place,
                            Points = teamResult.Points,
                            No = team.No,
                            SpeedTrialPoints = 0,
                            TaskPoints = null,
                            TimePoints = 0,
                            TrackPoints = 0

                        };
                        if (team.RunTasks != null)
                            foreach (var runTask in team.RunTasks)
                            {
                                if (runTask.Task.Abbr.StartsWith("Tem"))
                                {
                                    if (trModel.TaskPoints == null) trModel.TaskPoints = runTask.Points;
                                    else trModel.TaskPoints += runTask.Points;
                                }
                            }
                        catModel.TeamResults.Add(trModel);
                    }
                    model.Categories.Add(catModel);
                }
            }
            return model;
        }

    }
}
