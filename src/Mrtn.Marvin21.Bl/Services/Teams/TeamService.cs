﻿using System;
using System.Collections.Generic;
using Mrtn.Marvin21.Bl.Services.Support;
using System.Linq;
using Mrtn.Marvin21.Bl.Interface.Teams;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Bl.Services.Teams
{
    public class TeamService : ITeamService
    {
        private class SiCodeCount
        {
            public string Code { get; set; }
            public int Count { get; set; }
        }
        public List<TeamViewModel> Get(TeamQueryModel queryModel)
        {
            var packs = new List<TeamViewModel>();
            using (var db = new Marvin20Db())
            {
                IQueryable<Team> dbSet;
                if (queryModel.SortByPlace)
                    dbSet = db.Teams.OrderBy(x => x.Category.No).ThenBy(x => x.Result.Place);
                else
                    dbSet = db.Teams.OrderBy(x => x.Category.No).ThenBy(x => x.No);

                if (!queryModel.ShowInactive)
                    dbSet = dbSet.Where(x => x.Active);

                if (queryModel.IdCategory > 0)
                    dbSet = dbSet.Where(x => x.Category.Id == queryModel.IdCategory);

                if (queryModel.IdPack > 0)
                    dbSet = dbSet.Where(x => x.Pack.Id == queryModel.IdPack);

                var siCodeCounts = db.Database.SqlQuery<SiCodeCount>(
                                    " SELECT SiChips.SiCode AS Code, COUNT(Teams.Id) AS Count " +
                                    " FROM  SiChips LEFT OUTER JOIN Teams " +
                                    "    ON SiChips.SiCode = Teams.SiCode " +
                                    " GROUP BY SiChips.SiCode").ToList();
                foreach (var dbItem in dbSet)
                {
                    var item = ViewModelFactory.TeamViewModel(dbItem);
                    if (!string.IsNullOrEmpty(item.SiCode))
                    {
                        var codeCount = siCodeCounts.Where(x => x.Code == item.SiCode).FirstOrDefault();
                        if (codeCount == null) item.SiCodeError = "Čip ne obstaja v bazi čipov";
                        else if (codeCount.Count > 1) item.SiCodeError = "Čip je uporabljen več kot enkrat";
                        else item.SiCodeError = "";
                    }
                    packs.Add(item);
                }
            }
            return packs;

        }

        public TeamEditModel Edit(int? id)
        {
            TeamEditModel teamEditModel;
            using (var db = new Marvin20Db())
            {
                Team team;
                if (id.HasValue)
                {
                    team = db.Teams.Where(x => x.Id == id).FirstOrDefault();
                    if (team == null) throw new Exception("Ekipa s tem id ne obstaja!");
                    if (team.Locked) throw new Exception("Ekipa je zaklenjena!");
                }
                else team = new Team();
                teamEditModel = EditModelFactory.TeamEditModel(team, db);
            }
            return teamEditModel;
        }

        SuccessResponse ITeamService.Save(TeamEditModel model)
        {
            if (model.Name == "testError") return new SuccessResponse(false, "testError");
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                Team team;
                var category = db.Categories.Where(x => x.Id == model.CategoryId).FirstOrDefault();
                if (category == null) return new SuccessResponse(false, "Ne najdem kategorije");
                if (!category.Active) return new SuccessResponse(false, "Kategorija je neaktivna!");
                var pack = db.Packs.Where(x => x.Id == model.PackId).FirstOrDefault();
                if (pack == null) return new SuccessResponse(false, "Ne najdem rodu");
                if (!pack.Active) return new SuccessResponse(false, "Rod je neaktiven!");
                if (model.Id == 0)
                {
                    //dodajam 
                    team = new Team();
                    db.Teams.Add(team);
                }
                else
                {
                    team = db.Teams.Where(x => x.Id == model.Id).FirstOrDefault();
                    if (team == null) return new SuccessResponse(false, "Ne najdem ekipe!");
                }
                EntityMerger.MergeIgnoringId(team, model);
                team.Category = category;
                team.Pack = pack;
                team.SiCode = team.SiCode != null ? team.SiCode.Trim() : null;

                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }


        public SuccessResponse Deactivate(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var team = db.Teams.Where(x => x.Id == id).FirstOrDefault();
                if (team == null)
                {
                    response.AddToMessage("Ekipa s tem id ne obstaja!");
                    return response;
                }
                if (team.Locked)
                {
                    response.AddToMessage("Ekipa je zaklenjena!");
                    return response;
                }
                team.Active = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Activate(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var team = db.Teams.Where(x => x.Id == id).FirstOrDefault();
                if (team == null)
                {
                    response.Message = "Ekipa s tem id ne obstaja!";
                    return response;
                }
                if (team.Locked)
                {
                    response.Message = "Ekipa je zaklenjena!";
                    return response;
                }
                team.Active = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse SetSiCode(int id, string siCode)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var team = db.Teams.Where(x => x.Id == id).FirstOrDefault();
                if (team == null) return new SuccessResponse(false, "Ekipa s tem id ne obstaja!");
                if (team.Locked) return new SuccessResponse(false, "Ekipa je zaklenjena!");
                if (team.SiCode == siCode) return new SuccessResponse(false, "Ni spermembe!");
                team.SiCode = siCode != null ? siCode.Trim() : null;
                db.SaveChanges();
                response.Ok = true;
                // some aditional tests and warnings
                if (!String.IsNullOrWhiteSpace(siCode))
                {
                    var otherTeams = db.Teams.Where(x => x.SiCode == siCode && x.Id != team.Id).Count();
                    if (otherTeams > 0)
                        response.AddToMessage(String.Format(
                            "Številka čipa ni unikatna. Obstaja še {0} ekip z isto številko.",
                            otherTeams));
                    if (db.SiChips.Where(x => x.SiCode == siCode).Count() == 0)
                        response.AddToMessage(String.Format(
                            "Čip {0} ne obstaja v bazi čipov.",
                            siCode));
                }
            }
            return response;
        }

        public SuccessResponse SetNo(int id, int? no)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var team = db.Teams.Where(x => x.Id == id).FirstOrDefault();
                if (team == null) return new SuccessResponse(false, "Ekipa s tem id ne obstaja!");
                if (team.Locked) return new SuccessResponse(false, "Ekipa je zaklenjena!");

                if (team.No == no) return new SuccessResponse(false, "Ni spermembe!");
                team.No = no;
                db.SaveChanges();
                response.Ok = true;
                // some aditional tests and warnings
                if (no.HasValue)
                {
                    var categoryNo = team.Category.No;
                    var otherTeams = db.Teams.Where(x => x.No == no.Value && x.Id != team.Id).Count();
                    if (otherTeams > 0)
                        response.AddToMessage(String.Format(
                            "Številka ekipe ni unikatna. Obstaja še {0} ekip z isto številko.",
                            otherTeams));
                    if ((no.Value / 100) != categoryNo)
                        response.AddToMessage(String.Format(
                            "Številka ekipe je običajno 100*številka kategorije + n, v tem primeru {0}00+n",
                            categoryNo));
                    if ((no.Value % 100) == 0)
                        response.AddToMessage(String.Format(
                            "Številka ekipe naj ne bo večkrtanik števila 100. Prva ekipa v tej kategoriji naj bo {0}01",
                            categoryNo));
                }
            }
            return response;
        }


        public int GetInactiveTeamsCount()
        {
            using (var db = new Marvin20Db())
            {
                return db.Teams.Where(x => x.Active == false).Count();
            }
        }

        public FiltersForTeams GetDataForFilters()
        {
            using (var db = new Marvin20Db())
            {
                return new FiltersForTeams
                           {
                               Categories =
                                   ViewModelFactory.NamedEntityList(
                                       db.Categories.Where(x => x.Active).OrderBy(x => x.No), true),
                               Packs =
                                   ViewModelFactory.NamedEntityList(
                                       db.Packs.Where(x => x.Active).OrderBy(x => x.Abbr), true)
                           };
            }
        }
    }
}
