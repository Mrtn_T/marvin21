﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Bl;
using Mrtn.Marvin21.Bl.Interface.Teams;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Bl.Services.Teams
{

    public class CategoryService : ICategoryService
    {
        public List<CategoryViewModel> GetAll()
        {
            var categories = new List<CategoryViewModel>();
            using (var db = new Marvin20Db())
            {
                foreach (var category in db.Categories.OrderBy(x => x.No))
                    categories.Add(ViewModelFactory.CategoryViewModel(category));
            }
            return categories;
        }

        public CategoryViewModel Get(int categoryId)
        {
            using (var db = new Marvin20Db())
            {
                return ViewModelFactory.CategoryViewModel(
                    db.Categories.Where(x => x.Id == categoryId).FirstOrDefault());
            }
        }


        public CategoryEditModel Edit(int id)
        {

            CategoryEditModel model;
            using (var db = new Marvin20Db())
            {
                var category = db.Categories.Where(x => x.Id == id).FirstOrDefault();
                if (category == null) throw new Exception("Kategorija s tem id ne obstaja!");
                if (category.Locked) throw new Exception("Kategorija je zaklenjena!");
                model = EditModelFactory.CategoryEditModel(category);
            }
            return model;
        }

        public SuccessResponse Save(CategoryEditModel model)
        {
            if (model.Name == "testError") return new SuccessResponse(false, "testError");
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var category = db.Categories.Where(x => x.Id == model.Id).FirstOrDefault();
                if (category == null) throw new Exception("Kategorija s tem id ne obstaja!");
                if (category.Locked) throw new Exception("Kategorija je zaklenjena!");
                var needToRecalculateTracks = AreSpeedsDifferent(category, model);

                EntityMerger.MergeIgnoringId(category, model);

                if (needToRecalculateTracks)
                    foreach (var track in category.Tracks)
                        TrackCalculator.RecalculateTrackTimes(track, db);
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }


        private static bool AreSpeedsDifferent(Category category, CategoryEditModel model)
        {
            if (category.Speed != model.Speed) return true;
            if (category.UphillSpeed != model.UphillSpeed) return true;
            if (category.DownhillSpeed != model.DownhillSpeed) return true;
            return false;

        }

        public void AddCategory(int no, string name, bool active, string abbr, float speed, float uphillSpeed, float downhillSpeed, string color, bool locked)
        {
            var model = new Category
            {
                Name = name,
                No = no,
                Abbr=abbr,
                Active=active,
                Color=color,
                DownhillSpeed=downhillSpeed,
                Speed=speed,
                UphillSpeed=uphillSpeed
            };
            using (var db = new Marvin20Db())
            {
                db.Categories.Add(model);
                db.SaveChanges();
            }
        }
    }
}
