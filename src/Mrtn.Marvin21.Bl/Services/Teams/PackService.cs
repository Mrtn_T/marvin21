﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Interface.Teams;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Bl.Services.Teams
{
    public class PackService : IPackService
    {
        public List<PackViewModel> Get(PackQueryModel queryModel)
        {
            var packs = new List<PackViewModel>();
            using (var db = new Marvin20Db())
            {
                IQueryable<Pack> dbPacks = db.Packs;

                if (queryModel.Active.HasValue)
                    dbPacks = dbPacks.Where(x => x.Active == queryModel.Active.Value);

                foreach (var dbPack in dbPacks)
                {
                    var pack = ViewModelFactory.PackViewModel(dbPack);
                    pack.TeamCount = dbPack.Teams.Count;
                    packs.Add(pack);
                }
            }
            return packs;
        }

        public PackEditModel Edit(int? id)
        {
            PackEditModel packEditModel;
            using (var db = new Marvin20Db())
            {
                Pack pack;
                if (id.HasValue)
                {
                    pack = db.Packs.Where(x => x.Id == id).FirstOrDefault();
                    if (pack == null) throw new Exception("Rod s tem id ne obstaja!");
                    if (pack.Locked) throw new Exception("Rod je zaklenjen!");
                }
                else pack = new Pack();
                packEditModel = EditModelFactory.PackEditModel(pack);
            }
            return packEditModel;
        }

        public SuccessResponse Save(PackEditModel model)
        {
            if (model.Name == "testError") return new SuccessResponse(false, "testError");
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                Pack pack;
                if (model.Id == 0)
                {
                    //dodajam 
                    pack = new Pack();
                    db.Packs.Add(pack);
                }
                else
                {
                    pack = db.Packs.Where(x => x.Id == model.Id).FirstOrDefault();
                    if (pack == null) return new SuccessResponse(false, "Rod s tem id ne obstaja!");
                    if (pack.Locked) return new SuccessResponse(false, "Rod je zaklenjen!");
                }
                EntityMerger.MergeIgnoringId(pack, model);
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Deactivate(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var pack = db.Packs.Where(x => x.Id == id).FirstOrDefault();
                if (pack == null)
                {
                    response.Message = "Rod s tem id ne obstaja!";
                    return response;
                }
                if (pack.Locked)
                {
                    response.Message = "Rod je zaklenjen!";
                    return response;
                }
                if (pack.Teams != null && pack.Teams.Count > 0)
                {
                    response.Message = "Rod ima ekipe, zato ga ni možno deaktivirati!";
                    return response;
                }
                pack.Active = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Activate(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var pack = db.Packs.Where(x => x.Id == id).FirstOrDefault();
                if (pack == null)
                {
                    response.Message = "Rod s tem id ne obstaja!";
                    return response;
                }
                if (pack.Locked)
                {
                    response.Message = "Rod je zaklenjen!";
                    return response;
                }
                pack.Active = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public bool AddIfNew(string name, bool active, bool inCompetition, string abbr, string town, bool locked)
        {
            using (var db = new Marvin20Db())
            {
                if( db.Packs.Any(x => 
                    x.Name.ToLower() == name.ToLower() 
                    && x.Abbr.ToLower() == abbr.ToLower())) return false;
                var model = new Pack()
                {
                    Name = name,
                    Abbr = abbr,
                    Active = active,
                    InCompetition = inCompetition,
                    Locked = locked,
                    Town = town
                };
                db.Packs.Add(model);
                db.SaveChanges();
            }
            return true;
        }
    }
}
