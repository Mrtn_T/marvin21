﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Mrtn.Marvin21.Bl.Bl;
using Mrtn.Marvin21.Bl.Interface.Run;
using Mrtn.Marvin21.Bl.Services.Teams;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Run;
using Mrtn.Marvin21.DbModel.Tasks;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Run;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Bl.Services.Run
{
    public class RunTaskService : IRunTaskService
    {
        public RunTasksEditModel GetEditModels(int id)
        {
            var model = new RunTasksEditModel
            {
                EditModels = new List<RunTaskEditModel>()
            };
            using (var db = new Marvin20Db())
            {
                var dbTask = db.Tasks.FirstOrDefault(x => x.Id == id);
                if (dbTask == null)
                    throw new Exception(
                        String.Format("ne neajdem naloge z id={0}", id));
                model.Task = ViewModelFactory.TaskViewModel(dbTask, db);


                var teams = new TeamService().Get(new TeamQueryModel { ShowInactive = true });
                foreach (var team in teams)
                {
                    if (!dbTask.TaskToCategories.Any(x => x.Category.Id == team.CategoryId)) continue;
                    var editModel = new RunTaskEditModel
                    {
                        Team = team,
                    };
                    var runTask = dbTask.RunTasks.FirstOrDefault(x => x.Team.Id == team.Id);
                    if (runTask != null)
                    {
                        editModel.FirstEntry = runTask.FirstEntry.ToString(CultureInfo.InvariantCulture);
                        editModel.SecondEntry =
                            dbTask.SecondEntryIsTime
                            ? (new TimeSpan(0, 0, runTask.SecondEntry)).ToString(@"m\:ss")
                            : runTask.SecondEntry.ToString(CultureInfo.InvariantCulture);
                        editModel.Points = runTask.Points;
                        editModel.Id = runTask.Id;
                        editModel.Locked = runTask.Locked;
                    }
                    else
                    {
                        editModel.FirstEntry = ""; //dbTask.FirstEntryDefault.ToString();
                        editModel.SecondEntry = "";
                        //                            dbTask.SecondEntryIsTime
                        //                            ? (new TimeSpan(0, 0, dbTask.SecondEntryDefault)).ToString(@"m\:ss")
                        //                            : dbTask.SecondEntryDefault.ToString();
                        editModel.Points = 0;
                        editModel.Id = 0;
                        editModel.Locked = false;
                    }
                    model.EditModels.Add(editModel);
                }
            }
            return model;
        }
        /// <summary>
        /// Zapisovanje rezultata za določeno ekipo za določeno nalogo
        /// Naenkrat se vpiše samo firstEntry ali samo secondEntry
        /// </summary>
        /// <param name="teamId"></param>
        /// <param name="taskId"></param>
        /// <param name="firstEntry"></param>
        /// <param name="secondEntry"></param>
        /// <returns></returns>
        public SuccessResponse TrySaveCell(int teamId, int taskId, string firstEntry, string secondEntry)
        {
            using (var db = new Marvin20Db())
            {
                bool canInfluenceOthers;

                // Poiščem nalogo in ekipo
                var task = db.Tasks.Where(x => x.Id == taskId).FirstOrDefault();
                if (task == null)
                    return new SuccessResponse(false,
                        String.Format("Naloga z id={0} ne obstaja.", taskId));
                var team = db.Teams.Where(x => x.Id == teamId).FirstOrDefault();
                if (team == null)
                    return new SuccessResponse(false,
                        String.Format("Ekipa z id={0} ne obstaja.", teamId));

                // poiščem vnos, če že obstaja
                var model = db.RunTasks.Where(x => x.Task.Id == taskId && x.Team.Id == teamId).SingleOrDefault();

                //preverim, ali je potrebno vnos izbrisat
                if (String.IsNullOrEmpty(firstEntry) && String.IsNullOrEmpty(secondEntry))
                {
                    //pobriši vnos, če obstaja   
                    if (model != null)
                    {
                        db.RunTasks.Remove(model);
                        db.SaveChanges();
                        if (task.OnTrack) RemoveRunCheckpointConfirmation(task, team, db);

                        RecalculateResults(task, model, db, out canInfluenceOthers);
                        if (canInfluenceOthers) RecalculateAllResultsForThisTaskInCategory(task, team, db);

                    }
                    return new SuccessResponse(true, "");
                }


                int i;
                if (String.IsNullOrEmpty(secondEntry))
                {
                    //prvi vnos mora biti številka
                    if (!int.TryParse(firstEntry, out i))
                        return new SuccessResponse(false,
                            String.Format("Vnos '{0}' ni prepoznan kot število.", firstEntry));
                    if (i > task.FirstEntryMax || i < task.FirstEntryMin)
                        return new SuccessResponse(false,
                            String.Format("Vnos mora biti med {0} in {1}.", task.FirstEntryMin, task.FirstEntryMax));
                    //Prvi vnos je OK, zapišemo 
                    if (model == null)
                    {
                        model = new RunTask
                        {
                            SecondEntry =
                                           task.SecondEntryName == ""
                                               ? 0
                                               : task.SecondEntryDefault,
                            Task = task,
                            Team = team
                        };
                        db.RunTasks.Add(model);
                    }
                    model.FirstEntry = i;
                    db.SaveChanges();
                    AddRunCheckpointConfirmation(task, team, db);

                    RecalculateResults(task, model, db, out canInfluenceOthers);
                    if (canInfluenceOthers) RecalculateAllResultsForThisTaskInCategory(task, team, db);
                    return new SuccessResponse(true, "");
                }
                //drugi vnos mora biti OK
                if (!int.TryParse(secondEntry, out i))
                {
                    if (task.SecondEntryIsTime)
                    {
                        var a = secondEntry.Split(':');
                        if (a.Count() != 2)
                            return new SuccessResponse(false,
                                String.Format("Vnos mora biti v formatu 'm:ss' ali 's' (npr '4:00' pomeni 4 minute, 190 pomeni 3 minute, 10 sekund)."));
                        int s;
                        int m;
                        if (!int.TryParse(a[0], out m) || !int.TryParse(a[1], out s))
                            return new SuccessResponse(false,
                                String.Format("Vnos mora biti v formatu 'm:ss' ali 's' (npr '4:00' pomeni 4 minute, 190 pomeni 3 minute, 10 sekund)."));
                        i = 60 * m + s;
                    }
                    else
                    {
                        return new SuccessResponse(false,
                                String.Format("Vnos '{0}' ni prepoznan kot število.", secondEntry));
                    }
                }

                if (i > task.SecondEntryMax || i < task.SecondEntryMin)
                    return new SuccessResponse(false,
                    String.Format("Vnos mora biti med {0} in {1}.",
                         task.SecondEntryIsTime
                            ? (new TimeSpan(0, 0, task.SecondEntryMin)).ToString(@"m\:ss")
                            : task.SecondEntryMin.ToString(CultureInfo.InvariantCulture),
                         task.SecondEntryIsTime
                            ? (new TimeSpan(0, 0, task.SecondEntryMax)).ToString(@"m\:ss")
                            : task.SecondEntryMax.ToString(CultureInfo.InvariantCulture)));

                //Vse je OK, zapišemo rezultat
                if (model == null)
                {
                    model = new RunTask
                    {
                        FirstEntry = task.FirstEntryDefault,
                        Task = task,
                        Team = team
                    };
                    db.RunTasks.Add(model);
                }
                model.SecondEntry = i;
                db.SaveChanges();
                AddRunCheckpointConfirmation(task, team, db);
                RecalculateResults(task, model, db, out canInfluenceOthers);
                if (canInfluenceOthers) RecalculateAllResultsForThisTaskInCategory(task, team, db);
                return new SuccessResponse(true, "");
            }

        }

        public SuccessResponse Lock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var task = db.RunTasks.Where(x => x.Id == id).FirstOrDefault();
                if (task == null)
                    return new SuccessResponse(false, "Ne najdem zapisa s tem iD-jem");
                task.Locked = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Unlock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var task = db.RunTasks.Where(x => x.Id == id).FirstOrDefault();
                if (task == null)
                    return new SuccessResponse(false, "Ne najdem zapisa s tem iD-jem");
                task.Locked = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse LockCategory(int taskId, int categoryId)
        {
            //tole je malo bolj tricky
            //treba je najti VSE runTracke, ki izoplnjujejo pogoje
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var tasks = db.RunTasks.Where(x => x.Task.Id == taskId && x.Team.Category.Id == categoryId).ToList();
                foreach (var runTask in tasks)
                {
                    runTask.Locked = true;
                }
                db.SaveChanges();
            }
            response.Ok = true;
            return response;

        }

        public SuccessResponse UnlockCategory(int taskId, int categoryId)
        {
            //tole je malo bolj tricky
            //treba je najti VSE runTracke, ki izoplnjujejo pogoje
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var tasks = db.RunTasks.Where(x => x.Task.Id == taskId && x.Team.Category.Id == categoryId).ToList();
                foreach (var runTask in tasks)
                {
                    runTask.Locked = false;
                }
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        /// <summary>
        /// Če je bila naloga na progi, lahko potrdimo da je ekipa bila na določenem KT-ju
        /// </summary>
        /// <param name="task"></param>
        /// <param name="team"></param>
        /// <param name="db"></param>
        private void AddRunCheckpointConfirmation(Task task, Team team, Marvin20Db db)
        {
            var runTrack = team.RunTracks.Where(x => x.Day == task.Day).FirstOrDefault();
            if (runTrack == null)
                //proga za ta dan za to ekipo še ne obstaja, zato jo naredimo
                runTrack = RunTrackCalculator.CreateRunTrackForTeam(team, task.Day, db);
            if (task.Station == null) return;
            var rcp = runTrack.RunTrackCheckpoints
                .Where(x => x.TrackCheckpoint.Station.Id == task.Station.Id)
                .FirstOrDefault();
            if (rcp == null) return;
            if (rcp.IsConfirmedByTask) return;
            rcp.IsConfirmedByTask = true;
            RunTrackCalculator.RecalculateRunTrackPointsForCheckpoints(runTrack, db);
        }

        /// <summary>
        /// če je bil odstranje  rezultat, je potrebno odstraniti tudi "potrditev" da je bila pika pobrana
        /// </summary>
        /// <param name="task"></param>
        /// <param name="team"></param>
        /// <param name="db"></param>
        private void RemoveRunCheckpointConfirmation(Task task, Team team, Marvin20Db db)
        {
            var runTrack = team.RunTracks.Where(x => x.Day == task.Day).FirstOrDefault();
            if (runTrack == null) return;
            if (task.Station == null) return;
            var rcp = runTrack.RunTrackCheckpoints
                .Where(x => x.TrackCheckpoint.Station.Id == task.Station.Id)
                .FirstOrDefault();
            if (rcp == null) return;
            if (!rcp.IsConfirmedByTask) return;
            rcp.IsConfirmedByTask = false;
            if (rcp.IsConfirmedByArrival || rcp.IsConfirmedManualy) return;
            RunTrackCalculator.RecalculateRunTrackPointsForCheckpoints(runTrack, db);
        }

        /// <summary>
        /// Preračuna VSE rezultate za ta tip naloge v tej kategoriji. 
        /// Razlog je običajno ta, da se je spremenilo nekaj, kr vpliva na druge (npr najhitrejši morse)
        /// </summary>
        /// <param name="task"></param>
        /// <param name="team"></param>
        /// <param name="db"></param>
        private void RecalculateAllResultsForThisTaskInCategory(Task task, Team team, Marvin20Db db)
        {
            var runTasks =
                db.RunTasks.Where(x => x.Task.Id == task.Id && x.Team.Category.Id == team.Category.Id).ToList();
            foreach (var runTask in runTasks)
            {
                bool ignore;
                RecalculateResults(task, runTask, db, out ignore);
            }
        }

        /// <summary>
        ///  preračuna rezultat poslane naloge.
        /// 
        /// </summary>
        /// <param name="task"></param>
        /// <param name="runTask"></param>
        /// <param name="db"></param>
        /// <param name="canInfluenceOthers"></param>
        private void RecalculateResults(Task task, RunTask runTask, Marvin20Db db, out bool canInfluenceOthers)
        {

            canInfluenceOthers = false;
            switch (task.CalculatingAlgorythmId)
            {
                case 0:
                    runTask.Points = runTask.FirstEntry + runTask.SecondEntry;
                    break;
                case 1:
                    // ROT verzija za Morse: -3 za vsako napako, do vklj 7 napak, potem 0; 
                    // čas: 60 najboljši, potem linearno navzdol do 4', ostali 0<br />
                    canInfluenceOthers = true;
                    CalculateMorseRot(runTask, db);
                    break;

                case 2:
                    //  ROT verzija za Semafor: -3 za vsako napako,, do vklj 7 napak, potem 0; 
                    // čas: 60 najboljši, potem linearno navzdol do 3', ostali 0<br />
                    canInfluenceOthers = true;
                    CalculateSemaforRot(runTask, db);
                    break;

                case 3:
                    //  ROT verzija za pionirstvo:
                    // prvi vnos se zanemari, računa se le čas
                    // čas: 60 najboljši, potem linearno navzdol do 10', ostali 0<br />
                    canInfluenceOthers = true;
                    CalculatePioneeringRot(runTask, db);
                    break;
                case 4:
                    runTask.Points = (int)(runTask.FirstEntry * 0.6);
                    break;
                case 5:
                    canInfluenceOthers = true;
                    CalculateSemaforGsj(runTask, db);
                    break;
            }
            db.SaveChanges();

            if (runTask.Team != null)
                TeamCalculator.CalculateResult(runTask.Team, db);
            if (runTask.Team != null)
                TeamCalculator.CalculatePlacesInCategory(runTask.Team.Category, db);
            PackCalculator.CalculatePackResults(db);

            db.SaveChanges();

        }

        private static void CalculatePioneeringRot(RunTask runTask, Marvin20Db db)
        {
            //  ROT verzija za pionirstvo:
            // prvi vnos se zanemari, računa se le čas
            // čas: 60 najboljši, potem linearno navzdol do 10', ostali 0<br />

            //nastavitve
            const int maxPoints1 = 0;
            const int maxErrors = 0;
            const int penaltyForError = 0;
            const int maxTime = 600;
            const int maxPointsForTime = 60;

            // prvi vnos se zanemari
            runTask.FirstEntry = 0;

            GenericCalculator1(runTask, db, maxErrors, maxTime, maxPointsForTime, maxPoints1, penaltyForError);
        }


        private static void CalculateSemaforRot(RunTask runTask, Marvin20Db db)
        {
            //  ROT verzija za Semafor: -3 za vsako napako,, do vklj 7 napak, potem 0; 
            // čas: 60 najboljši, potem linearno navzdol do 3', ostali 0<br />

            //nastavitve
            const int maxPoints1 = 60;
            const int maxErrors = 7;
            const int penaltyForError = -3;
            const int maxTime = 180;
            const int maxPointsForTime = 60;

            GenericCalculator1(runTask, db, maxErrors, maxTime, maxPointsForTime, maxPoints1, penaltyForError);
        }


        private static void CalculateSemaforGsj(RunTask runTask, Marvin20Db db)
        {
            //  GSJ verzija za Semafor: -2 za vsako napako,, do vklj 7 napak, potem 0; 
            // čas: 60 najboljši, potem linearno navzdol do 3', ostali 0<br />

            //nastavitve
            const int maxPoints1 = 60;
            const int maxErrors = 7;
            const int penaltyForError = -2;
            const int maxTime = 180;
            const int maxPointsForTime = 60;

            GenericCalculator1(runTask, db, maxErrors, maxTime, maxPointsForTime, maxPoints1, penaltyForError);
        }


        private static void CalculateMorseRot(RunTask runTask, Marvin20Db db)
        {
            // ROT verzija za Morse: -3 za vsako napako, do vklj 7 napak, potem 0; 
            // čas: 60 najboljši, potem linearno navzdol do 4', ostali 0<br />

            //nastavitve
            const int maxPoints1 = 60;
            const int maxErrors = 7;
            const int penaltyForError = -3;
            const int maxTime = 240;
            const int maxPointsForTime = 60;

            GenericCalculator1(runTask, db, maxErrors, maxTime, maxPointsForTime, maxPoints1, penaltyForError);

        }

        private static void GenericCalculator1(RunTask runTask, Marvin20Db db, int maxErrors, int maxTime, int maxPointsForTime,
            int maxPoints1, int penaltyForError)
        {
            // spremenljivke
            var errors = runTask.FirstEntry;
            var time = runTask.SecondEntry;
            runTask.Points = 0; // privzete točke, če ni naloge

            // preverjanje parametrov
            if (errors > maxErrors) return;
            if (time <= 0) return;
            if (time > maxTime) return;
            if (runTask.Task == null) return;
            if (runTask.Team == null) return;
            // minimum časa od te kategorije 
            var minTime = db.RunTasks
                .Where(x =>
                    x.Task.Id == runTask.Task.Id && // rezultati te naloge
                    x.Team.Category.Id == runTask.Team.Category.Id && // iz te kategorije
                    x.FirstEntry <= maxErrors && // samo veljavini
                    x.SecondEntry > 0 && // v veljavnem času
                    x.SecondEntry <= maxTime)
                .OrderBy(x => x.SecondEntry) //najmanjši
                .First().SecondEntry; // eden

            // izračun točk za čas
            var pointsForTime = 0;
            if (minTime < maxTime)
                pointsForTime = maxPointsForTime * (maxTime - time) / (maxTime - minTime);

            // skupne točke
            runTask.Points = maxPoints1 + pointsForTime + errors * penaltyForError;
        }
    }
}
