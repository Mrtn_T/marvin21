﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Mrtn.Marvin21.Bl.Bl;
using Mrtn.Marvin21.Bl.Interface.Run;
using Mrtn.Marvin21.Bl.Services.Tasks;
using Mrtn.Marvin21.Bl.Services.Teams;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Run;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Run;
using Mrtn.Marvin21.Model.Tasks;
using Mrtn.Marvin21.Model.Teams;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Services.Run
{

    public class DashBoardService : IDashBoardService
    {
        public DashBoardViewModel Get(int? requestedDay, bool showInactive = false)
        {


            var model = new DashBoardViewModel
            {
                Headers = new List<DashBoardHeaderViewModel>()
            };
            using (var db = new Marvin20Db())
            {
                //------------------------------
                // dodam ekipe (vrstice)
                //------------------------------
                model.Rows = new List<DashBoardRowViewModel>();
                var teams = new TeamService().Get(new TeamQueryModel { ShowInactive = showInactive });
                foreach (var team in teams)
                {
                    model.Rows.Add(new DashBoardRowViewModel
                    {
                        Team = team,
                        Cells = new List<DashBoardCellViewModel>()
                    });
                }
                var tasks = new TaskService().Get();
                var tracks = new TrackService().GetTracks(new TrackQueryModel { });

                //grem čez stolpce

                for (var day = 1; day < AppSettings.Days + 1; day++)
                {
                    if (!requestedDay.HasValue || requestedDay == 0 || requestedDay == day)
                        InsertColumnsForDay(tracks, day, model, db, tasks);
                    else
                        InsertSumsForDay(tracks, day, model, db, tasks);
                }

                //else InsertColumnsForDay(tracks, requestedDay, model, db, tasks);

            }
            return model;
        }

        /// <summary>
        /// Vstavi vsote za določen dan
        /// </summary>
        /// <param name="tracks"></param>
        /// <param name="day"></param>
        /// <param name="model"></param>
        /// <param name="db"></param>
        /// <param name="tasks"></param>
        private void InsertSumsForDay(List<TrackViewModel> tracks, int day, DashBoardViewModel model, Marvin20Db db, List<TaskViewModel> tasks)
        {
            model.Headers.Add(new DashBoardHeaderViewModel
            {
                Day = day,
                Name = String.Format("Vsota, dan {0}", day),
                Sum = true
            });
            var dayTracks = tracks.Where(x => x.Day == day).ToList();
            foreach (var row in model.Rows)
            {
                var sumPoints = 0;
                if (dayTracks.Count > 0)
                {
                    var track =
                    tracks.SingleOrDefault(x => x.Day == day &&
                                                x.CategoryId == row.Team.CategoryId);
                    if (track != null)
                    {
                        var runTrack = db.RunTracks.FirstOrDefault(x => x.Team.Id == row.Team.Id &&
                                                                        x.Track.Id == track.Id);
                        if (runTrack != null)
                        {
                            //sumPoints += runTrack.PointsForCheckpoints;
                            //sumPoints += runTrack.PointsForTime;
                            sumPoints += runTrack.TrackPoints;
                            sumPoints += runTrack.SpeedTrialPoints;
                        }
                    }
                }
                var dayTasks = tasks.Where(x => x.Day == day).ToList();
                foreach (var task in dayTasks)
                {
                    if (task.Categories.Any(x => x.Key == row.Team.CategoryId))
                    {
                        var runTask = db.RunTasks.SingleOrDefault(x => x.Task.Id == task.Id &&
                                                                       x.Team.Id == row.Team.Id);
                        if (runTask != null)
                        {
                            sumPoints += runTask.Points;
                        }
                    }
                }
                var cell = new DashBoardCellViewModel { TaskPoints = sumPoints };
                row.Cells.Add(cell);
            }

        }

        private static void InsertColumnsForDay(List<TrackViewModel> tracks, int day, DashBoardViewModel model, Marvin20Db db, List<TaskViewModel> tasks)
        {
            //------------------------------
            // za vsak dan...
            //------------------------------
            var dayTracks = tracks.Where(x => x.Day == day).ToList();
            if (dayTracks.Count > 0)
            {
                //---------------------------------------------------------
                //
                //   ta dan ima progo, treba je dodat vse za progo
                //
                //---------------------------------------------------------
                model.Headers.Add(new DashBoardHeaderViewModel
                {
                    Day = day,
                    Name = String.Format("Proga, dan {0}", day),
                    IsTrack = true
                });
                foreach (var row in model.Rows)
                {
                    var cell = new DashBoardCellViewModel
                    {
                        IsTrack = true,
                        Day = day,
                        TeamId = row.Team.Id
                    };

                    var track =
                        tracks.SingleOrDefault(x => x.Day == day &&
                                                    x.CategoryId == row.Team.CategoryId);
                    if (track == null)
                    {
                        //sploh ni proge za to ekipo za ta dan
                        cell.HasNoEntry = true;
                    }
                    else
                    {
                        var runTrack = db.RunTracks.FirstOrDefault(x => x.Team.Id == row.Team.Id &&
                                                                        x.Track.Id == track.Id);
                        if (runTrack == null)
                        {
                            //proga je, vendar še ni vnosa
                            cell.NoResultYet = true;
                        }
                        else
                        {
                            //proga je, vnos je, treba je prikazat...
                            cell.CheckpointsFound = runTrack.CheckpointsFound;
                            cell.PointsForCheckpoints = runTrack.PointsForCheckpoints;
                            cell.PointsForTime = runTrack.PointsForTime;
                            cell.TrackPoints = runTrack.TrackPoints;
                            cell.SpeedTrialPoints = runTrack.SpeedTrialPoints;
                            cell.Locked = runTrack.Locked;
                            cell.RunTrackId = runTrack.Id;
                            cell.StartPredicted = ViewModelFactory.FormatDateTimeToHms(runTrack.StartTime.Predicted);
                        }
                    }
                    row.Cells.Add(cell);
                }
            }
            //--------------------------------------------
            //
            //  dodam še vse naloge za izbrani dan
            //
            //--------------------------------------------
            var dayTasks = tasks.Where(x => x.Day == day).ToList();
            foreach (var task in dayTasks)
            {
                //ToDo: kasneje ZDRUŽI naloge z enako kratico

                //------------------------------
                // header za nalogo
                //------------------------------
                var header = new DashBoardHeaderViewModel
                {
                    Day = day,
                    Name = String.Format(task.Name),
                    TaskId = task.Id,
                };
                if (task.BeforeStart) header.Location = "pred startom";
                else if (task.AfterFinish) header.Location = "po cilju";
                else if (task.Station != null) header.Location = "na točki " + task.Station.ToString();
                else header.Location = "na progi";
                model.Headers.Add(header);
                foreach (var row in model.Rows)
                {
                    //------------------------------
                    // rezultati za nalogo
                    //------------------------------
                    var cell = new DashBoardCellViewModel();
                    if (task.Categories.Where(x => x.Key == row.Team.CategoryId).Any())
                    {
                        var runTask = db.RunTasks.Where(
                            x =>
                                x.Task.Id == task.Id &&
                                x.Team.Id == row.Team.Id).SingleOrDefault();
                        if (runTask == null)
                        {
                            cell.NoResultYet = true;
                        }
                        else
                        {
                            cell.TaskPoints = runTask.Points;
                            cell.Locked = runTask.Locked;
                        }
                    }
                    else
                    {
                        //ta kategorija NIMA tega taska
                        cell.HasNoEntry = true;
                    }
                    row.Cells.Add(cell);
                }
            }
        }
        /// <summary>
        /// Glavna metoda za preverjanje SI čipov
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        public SiReadReportResponse CheckForSiReads(int day)
        {
            var report = new SiReadReportResponse { Day = day };
            try
            {
                using (var db = new Marvin20Db())
                {
                    //Pokličem Si importer
                    report.ImportFromSiRead = LcSportidentImporter.ImportFromSiRead(day, db);

                    var rtReport = report.RunTrackProcessReport;

                    var runTracksToBeProcessed = new List<RunTrack>();
                    foreach (var siRead in db.SiReads.Where(x => !x.HasBeenProcessed && x.Day == day).ToList())
                    {
                        //preverim, če ni slučajno že obdelan in to ni označeno...
                        if (siRead.RunTrack != null)
                        {
                            rtReport.Lines.Add(new SiProcessReportLine()
                            {
                                SiChip = siRead.SiCode,
                                Status = "Warning",
                                Text = "Ima RunTrack, a ni označen  kot obdelan, popravlajm..."
                            });
                            siRead.HasBeenProcessed = true;
                            db.SaveChanges();
                            continue;
                        }

                        //poskusim najti pravo ekipo
                        var teams = db.Teams.Where(x => x.SiCode == siRead.SiCode).ToList();
                        if (teams.Count == 0)
                        {
                            //to ni pravi čip. To se rešuje kasnete/posebej???
                            rtReport.Lines.Add(new SiProcessReportLine()
                            {
                                SiChip = siRead.SiCode,
                                Status = "Error",
                                Text = "Ne najdem ekipe za ta čip, ignoriram"
                            });
                            continue;
                        }
                        if (teams.Count > 1)
                        {
                            var t = String.Join(", ", teams
                                .Select(x => String.Format("{0} {1} ({2})", x.No, x.Category.Abbr, x.Name))
                                .ToList());
                            rtReport.Lines.Add(new SiProcessReportLine()
                            {
                                SiChip = siRead.SiCode,
                                Status = "Fatal Error",
                                Text = "Ta čip ima več kot ena ekipa!   Reši takoj!!!!",
                                Team = t

                            });
                            continue;
                        }
                        // začnem procesirat
                        var team = teams[0];
                        var runTrack = team.RunTracks.FirstOrDefault(x => x.Day == siRead.Day);

                        var line = new SiProcessReportLine
                        {
                            Team = String.Format("{0} {1} ({2})", team.No, team.Category.Abbr, team.Name),
                            SiChip = siRead.SiCode,
                            Day = day
                        };
                        rtReport.Lines.Add(line);

                        if (runTrack == null)
                        {
                            //skreiramo nov RunTrack
                            runTrack = RunTrackCalculator.CreateRunTrackForTeam(team, siRead.Day, db);
                            if (runTrack == null)
                            {
                                line.Status = "Warning";
                                line.Text = string.Format("Ne najdem proge za to ekipo za dan {0}, ne obdelujem...", day);
                                continue;
                            };
                        }

                        // vzpostavimo povezavo med objekti...
                        runTrack.SiRead = siRead;
                        siRead.RunTrack = runTrack;
                        siRead.HasBeenProcessed = true;
                        line.Status = "OK";
                        line.Text = string.Format("gre v obdelavo");
                        //db.SaveChanges();
                        runTracksToBeProcessed.Add(runTrack);

                    }
                    db.SaveChanges();

                    foreach (var runTrack in runTracksToBeProcessed)
                    {
                        RunTrackCalculator.ProcessSiRead(runTrack, db);
                        RunTrackCalculator.RecalculateRunTrackTimes(runTrack, db);
                        RunTrackCalculator.RecalculateRunTrackSpeedTrial(runTrack.Track, db);
                        RunTrackCalculator.RecalculateRunTrackPointsForCheckpoints(runTrack, db);
                        TeamCalculator.CalculateResult(runTrack.Team, db);
                        TeamCalculator.CalculatePlacesInCategory(runTrack.Team.Category, db);
                    }
                    PackCalculator.CalculatePackResults(db);
                }
            }
            catch (Exception ee)
            {
                report.Success = false;
                report.ErrorMessage = ee.Message;
            }
            return report;
        }

        public SuccessResponse DeleteUnusedSiReads()
        {
            var report = new SuccessResponse();
            try
            {
                using (var db = new Marvin20Db())
                {
                    var r = db.SiReads.Where(x => x.HasBeenProcessed == false && x.RunTrack == null);
                    foreach (var siRead in r)
                    {
                        foreach (var siPunch in siRead.SiPunches.ToList())
                        {
                            db.SiPunches.Remove(siPunch);
                        }
                        db.SiReads.Remove(siRead);
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ee)
            {
                report.Ok = false;
                report.Message = ee.Message;
            }
            return report;

        }

        public void RecalculateTeams()
        {
            try
            {
                using (var db = new Marvin20Db())
                {
                    var ts = db.Teams;
                    foreach (var t in ts.ToList())
                    {
                        TeamCalculator.CalculateResult(t, db);
                    }
                    PackCalculator.CalculatePackResults(db);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Debug.Print(e.ToString());
                // ignored
            }

        }
    }
}

