﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Bl;
using Mrtn.Marvin21.Bl.Interface.Run;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Run;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Run;
using Mrtn.Marvin21.Model.RunTrack;
using Mrtn.Marvin21.Model.RunTrack.Support;

namespace Mrtn.Marvin21.Bl.Services.Run
{
    public class RunTrackService : IRunTrackService
    {

        public ViewModelForTrackTables GetModelForTrackTables(int id)
        {
            using (var db = new Marvin20Db())
            {
                var dbTrack = db.Tracks.Where(x => x.Id == id).FirstOrDefault();
                if (dbTrack == null)
                    throw new Exception(String.Format("Ni poti s podanim id={0}!", id));


                return ViewModelFactory.ViewModelForTrackTables(dbTrack, db);
            }
        }


        public RunTrackViewModel Get(int id)
        {
            RunTrackViewModel model;
            using (var db = new Marvin20Db())
            {
                var dbModel = db.RunTracks.Where(x => x.Id == id).FirstOrDefault();
                if (dbModel == null) throw new Exception("RunTrack s tem idjem ni bil najden v bazi.");
                if (dbModel.Team == null) throw new Exception("RunTrack nima ekipe.");
                if (dbModel.Track == null) throw new Exception("RunTrack nima poti.");
                if (dbModel.RunTrackCheckpoints == null) throw new Exception("RunTrack nima KT-jev.");
                // osnovni vnosi v model
                model = ViewModelFactory.RunTrackViewModel(dbModel);
                model.Team = ViewModelFactory.TeamViewModel(dbModel.Team);
                model.Track = ViewModelFactory.TrackViewModel(dbModel.Track, db);
                model.Category = ViewModelFactory.CategoryViewModel(dbModel.Track.Category);
                model.RunTrackCheckpoints = new List<RunTrackCheckpointViewModel>();
                foreach (var dbCheckpoint in dbModel.RunTrackCheckpoints.OrderBy(x => x.No))
                {
                    var rtcp = ViewModelFactory.RunTrackCheckpointViewModel(dbCheckpoint,db);
                    rtcp.TrackCheckpoint = ViewModelFactory.TrackCheckpointViewModel(dbCheckpoint.TrackCheckpoint, db);
                    model.RunTrackCheckpoints.Add(rtcp);
                }

                if (dbModel.SiRead != null)
                {
                    //ta runTrack že ima pridruženo branje SI čipov
                    model.SiRead = ViewModelFactory.SiReadViewModel(dbModel.SiRead);
                    model.SiRead.SiPunches = new List<SiPunchViewModel>();
                    if (dbModel.SiRead.SiPunches != null)
                        foreach (var dbPunch in dbModel.SiRead.SiPunches.OrderBy(x=>x.No))
                        {
                            model.SiRead.SiPunches.Add(ViewModelFactory.SiPunchViewModel(dbPunch));
                        }
                }
                else
                {
                    model.SiRead = new SiReadViewModel
                                       {
                                           SiPunches = new List<SiPunchViewModel>(),
                                       };
                    //poskusim ugotoviti, če je možno pridružit kakšno branje 
                    // (al pa ne, to bo najbrž posebej)
                }


            }
            return model;
        }

        public RunTrackViewModel Add(int teamId, int day)
        {
            RunTrack runTrack;
            using (var db = new Marvin20Db())
            {
                var team = db.Teams.Where(x => x.Id == teamId).FirstOrDefault();
                if (team == null)
                    throw new Exception("Ekipa s tem idjem ni bila najdena v bazi.");
                runTrack = RunTrackCalculator.CreateRunTrackForTeam(team, day, db);
            }
            return Get(runTrack.Id);
        }

        public SuccessResponse AddAll(int day)
        {
            using (var db = new Marvin20Db())
            {

                foreach (var team in db.Teams.ToList())
                {
                   var  runTrack = RunTrackCalculator.CreateRunTrackForTeam(team, day, db);
                    db.SaveChanges();
                }
            }
            return new SuccessResponse();
        }

        public PredictedStartTimeEditModel GetModelForPredictedStartTime(int id)
        {
            var model = new PredictedStartTimeEditModel();
            using (var db = new Marvin20Db())
            {
                var runTrack = db.RunTracks.Where(x => x.Id == id).FirstOrDefault();
                if (runTrack == null)
                    throw new Exception(
                        "Prehod čez progo  s tem idjem ni bil najden v bazi.");
                if (runTrack.Locked)
                    throw new Exception(
                        "Prehod čez progo  s tem idjem je zaklenjen.");
                model.Id = runTrack.Id;
                if (runTrack.StartTime.Predicted.HasValue)
                    model.PredictedStrat = runTrack.StartTime.Predicted.Value.ToString("H:mm:ss");
            }
            return model;
        }

        public SuccessResponse Save(PredictedStartTimeEditModel model)
        {
            var response = new SuccessResponse(true, "");
            using (var db = new Marvin20Db())
            {
                var runTrack = db.RunTracks.Where(x => x.Id == model.Id).FirstOrDefault();
                if (runTrack == null)
                    throw new Exception(
                        "Prehod čez progo  s tem idjem ni bil najden v bazi.");
                if (runTrack.Locked)
                    throw new Exception(
                        "Prehod čez progo  s tem idjem je zaklenjen.");

                if (string.IsNullOrWhiteSpace(model.PredictedStrat))
                {
                    runTrack.StartTime.Predicted = null;
                }
                else
                {
                    DateTime dt;
                    if (DateTime.TryParse(model.PredictedStrat, out dt)) runTrack.StartTime.Predicted = dt;
                }
                db.SaveChanges();
                RunTrackCalculator.CalculatePredictionsForTrack(runTrack, db);
            }
            return response;
        }

        public RejectedManualyEditModel EditRejectManualy(int id)
        {
            var model = new RejectedManualyEditModel();
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.RunTrackCheckpoints.Where(x => x.Id == id).FirstOrDefault();
                if (checkpoint == null)
                    throw new Exception(
                        "Točka na prehodu čez progo  s tem idjem ni bila najdena v bazi.");
                if (checkpoint.Locked || checkpoint.RunTrack.Locked)
                    throw new Exception(
                        "Točka na prehodu čez progo  s tem idjem je zaklenjena.");

                model.Id = checkpoint.Id;
                model.IsRejectedManualy = checkpoint.IsRejectedManualy;
                model.IsRejectedManualyReason = checkpoint.IsRejectedManualyReason;
            }
            return model;
        }

        public SuccessResponse Save(RejectedManualyEditModel model)
        {
            var response = new SuccessResponse(true, "");
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.RunTrackCheckpoints.Where(x => x.Id == model.Id).FirstOrDefault();
                if (checkpoint == null)
                    return new SuccessResponse(false,
                        "Točka na prehodu čez progo  s tem idjem ni bila najdena v bazi.");
                if (checkpoint.Locked || checkpoint.RunTrack.Locked)
                    return new SuccessResponse(false,
                        "Točka na prehodu čez progo  s tem idjem je zaklenjena.");
                if (model.IsRejectedManualy && String.IsNullOrWhiteSpace(model.IsRejectedManualyReason))
                    return new SuccessResponse(false,
                        "Razlog je obvezen.");
                checkpoint.IsRejectedManualy = model.IsRejectedManualy;
                checkpoint.IsRejectedManualyReason = model.IsRejectedManualyReason;
                db.SaveChanges();
                Recalculate(checkpoint.RunTrack, db);
            }
            return response;
        }

        public ConfirmManualyEditModel EditConfirmManualy(int id)
        {
            var model = new ConfirmManualyEditModel();
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.RunTrackCheckpoints.Where(x => x.Id == id).FirstOrDefault();
                if (checkpoint == null)
                    throw new Exception(
                        "Točka na prehodu čez progo  s tem idjem ni bila najdena v bazi.");
                if (checkpoint.Locked || checkpoint.RunTrack.Locked)
                    throw new Exception(
                        "Točka na prehodu čez progo  s tem idjem je zaklenjena.");

                model.Id = checkpoint.Id;
                model.IsConfirmedManualy = checkpoint.IsConfirmedManualy;
                model.IsConfirmedManualyReason = checkpoint.IsConfirmedManualyReason;
            }
            return model;
        }

        public SuccessResponse Save(ConfirmManualyEditModel model)
        {
            var response = new SuccessResponse(true, "");
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.RunTrackCheckpoints.Where(x => x.Id == model.Id).FirstOrDefault();
                if (checkpoint == null)
                    return new SuccessResponse(false,
                        "Točka na prehodu čez progo  s tem idjem ni bila najdena v bazi.");
                if (checkpoint.Locked || checkpoint.RunTrack.Locked)
                    return new SuccessResponse(false,
                        "Točka na prehodu čez progo  s tem idjem je zaklenjena.");
                if (model.IsConfirmedManualy && String.IsNullOrWhiteSpace(model.IsConfirmedManualyReason))
                    return new SuccessResponse(false,
                        "Razlog je obvezen.");
                checkpoint.IsConfirmedManualy = model.IsConfirmedManualy;
                checkpoint.IsConfirmedManualyReason = model.IsConfirmedManualyReason;
                db.SaveChanges();
                Recalculate(checkpoint.RunTrack, db);
            }
            return response;
        }

        public ManualDepartureEditModel ManualDeparture(int id)
        {
            var model = new ManualDepartureEditModel();
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.RunTrackCheckpoints.Where(x => x.Id == id).FirstOrDefault();
                if (checkpoint == null)
                    throw new Exception(
                        "Točka na prehodu čez progo  s tem idjem ni bila najdena v bazi.");
                if (checkpoint.Locked || checkpoint.RunTrack.Locked)
                    throw new Exception(
                        "Točka na prehodu čez progo  s tem idjem je zaklenjena.");
                model.Id = checkpoint.Id;
                if (checkpoint.Departure.Manual.HasValue)
                    model.Time = checkpoint.Departure.Manual.Value.ToString("H:mm:ss");
            }
            return model;
        }

        public SuccessResponse Save(ManualDepartureEditModel model)
        {
            var response = new SuccessResponse(true, "");
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.RunTrackCheckpoints.Where(x => x.Id == model.Id).FirstOrDefault();
                if (checkpoint == null)
                    return new SuccessResponse(false,
                        "Točka na prehodu čez progo  s tem idjem ni bila najdena v bazi.");
                if (checkpoint.Locked || checkpoint.RunTrack.Locked)
                    return new SuccessResponse(false,
                        "Točka na prehodu čez progo  s tem idjem je zaklenjena.");

                if (string.IsNullOrWhiteSpace(model.Time))
                {
                    checkpoint.Departure.Manual = null;
                }
                else
                {
                    DateTime dt;
                    if (DateTime.TryParse(model.Time, out dt)) checkpoint.Departure.Manual = dt;
                }
                db.SaveChanges();

                RunTrackCalculator.RecalculateRunTrackTimes(checkpoint.RunTrack, db);
                RunTrackCalculator.RecalculateRunTrackSpeedTrial(checkpoint.RunTrack.Track, db);
                Recalculate(checkpoint.RunTrack, db);
            }
            return response;
        }

        public ManualArrivalEditModel ManualArrival(int id)
        {
            var model = new ManualArrivalEditModel();
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.RunTrackCheckpoints.Where(x => x.Id == id).FirstOrDefault();
                if (checkpoint == null)
                    throw new Exception(
                        "Točka na prehodu čez progo  s tem idjem ni bila najdena v bazi.");
                if (checkpoint.Locked || checkpoint.RunTrack.Locked)
                    throw new Exception(
                        "Točka na prehodu čez progo  s tem idjem je zaklenjena.");

                model.Id = checkpoint.Id;
                if (checkpoint.Arrival.Manual.HasValue)
                    model.Time = checkpoint.Arrival.Manual.Value.ToString("H:mm:ss");
            }
            return model;
        }

        public SuccessResponse Save(ManualArrivalEditModel model)
        {
            var response = new SuccessResponse(true, "");
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.RunTrackCheckpoints.Where(x => x.Id == model.Id).FirstOrDefault();
                if (checkpoint == null)
                    return new SuccessResponse(false,
                        "Točka na prehodu čez progo  s tem idjem ni bila najdena v bazi.");
                if (checkpoint.Locked || checkpoint.RunTrack.Locked)
                    return new SuccessResponse(false,
                        "Točka na prehodu čez progo  s tem idjem je zaklenjena.");

                if (string.IsNullOrWhiteSpace(model.Time))
                {
                    checkpoint.Arrival.Manual = null;
                }
                else
                {
                    DateTime dt;
                    if (DateTime.TryParse(model.Time, out dt)) checkpoint.Arrival.Manual = dt;
                }
                db.SaveChanges();

                RunTrackCalculator.RecalculateRunTrackTimes(checkpoint.RunTrack, db);
                RunTrackCalculator.RecalculateRunTrackSpeedTrial(checkpoint.RunTrack.Track, db);
                Recalculate(checkpoint.RunTrack, db);
            }
            return response;
        }

        public SuccessResponse Lock(int id)
        {
            var response = new SuccessResponse(true, "");
            using (var db = new Marvin20Db())
            {
                var runTrack = db.RunTracks.Where(x => x.Id == id).FirstOrDefault();
                if (runTrack == null)
                    return new SuccessResponse(false,
                        "Prehod čez progo s tem idjem ni bil najden v bazi.");
                runTrack.Locked = true;
                foreach (var runTrackCheckpoint in runTrack.RunTrackCheckpoints)
                {
                    runTrackCheckpoint.Locked = true;
                }
                db.SaveChanges();
            }
            return response;
        }

        public SuccessResponse DeleteResults(int id)
        {
            var response = new SuccessResponse(true, "");
            //return response;
            using (var db = new Marvin20Db())
            {
                var runTrack = db.RunTracks.Where(x => x.Id == id).FirstOrDefault();
                if (runTrack == null)
                    return new SuccessResponse(false,
                        "Prehod čez progo s tem idjem ni bil najden v bazi.");
                var siRead = runTrack.SiRead;
                var team = runTrack.Team;

                // release reference from SiPunches to RunTrackCheckpoint
                foreach (var siPunch in siRead.SiPunches.ToList())
                {
                    siPunch.RunTrackCheckpoint = null;
                }
                db.SaveChanges();


                // remove RunTrackCheckpoints
                foreach (var runTrackCheckpoint in runTrack.RunTrackCheckpoints.ToList())
                {
                    //runTrackCheckpoint.SiPunchArrival = null;
                    //runTrackCheckpoint.SiPunchDeparture = null;
                    db.RunTrackCheckpoints.Remove(runTrackCheckpoint);
                }
                db.SaveChanges();

                // remove SiPunches
                foreach (var siPunch in siRead.SiPunches.ToList())
                {
                    db.SiPunches.Remove(siPunch);
                }
                db.SaveChanges();

                // release reference to SiRead
                runTrack.SiRead = null;
                db.SaveChanges();

                db.SiReads.Remove(siRead);
                db.RunTracks.Remove(runTrack);
                db.SaveChanges();

                TeamCalculator.CalculateResult(team, db);
                TeamCalculator.CalculatePlacesInCategory(team.Category, db);
                PackCalculator.CalculatePackResults(db);

            }
            return response;
        }



        public SuccessResponse Unlock(int id)
        {
            var response = new SuccessResponse(true, "");
            using (var db = new Marvin20Db())
            {
                var runTrack = db.RunTracks.Where(x => x.Id == id).FirstOrDefault();
                if (runTrack == null)
                    return new SuccessResponse(false,
                        "Prehod čez progo s tem idjem ni bil najden v bazi.");
                runTrack.Locked = false;
                foreach (var runTrackCheckpoint in runTrack.RunTrackCheckpoints)
                {
                    runTrackCheckpoint.Locked = false;
                }
                db.SaveChanges();
            }
            return response;
        }


        private void Recalculate(RunTrack runTrack, Marvin20Db db)
        {
            RunTrackCalculator.RecalculateRunTrackPointsForCheckpoints(runTrack, db);
            TeamCalculator.CalculateResult(runTrack.Team, db);
            TeamCalculator.CalculatePlacesInCategory(runTrack.Team.Category, db);
            PackCalculator.CalculatePackResults(db);


        }
    }


}
