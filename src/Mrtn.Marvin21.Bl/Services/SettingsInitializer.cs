﻿using System.Linq;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Support;

namespace Mrtn.Marvin21.Bl.Services
{
    public static class SettingsInitializer
    {
        public static void InitializeForRot()
        {
            using (var db = new Marvin20Db())
            {
                Save("Days", 3, "", "Št. dni",
                    "Koliko dni traja tekmovanje. Za ROT se vpiše 3, za enodnevna tekmovanja 1.", db);
                Save("MaxFactor", 1.8, "", "Faktor za izračun dvojne časovnice",
                    "Za GSJ 1.7, za ROT 1.8.", db);
                Save("TaskFactor", 2, "", "Faktor za izračun dvojne časovnice, za naloge.",
                    "Za GSJ 1, za ROT 2.", db);
                Save("CompetitionName", "Republiško orientacijsko tekmovanje", "", "Ime tekmovanja", "", db);
                Save("UseMaps", false, "", "Uporablja zemljevide?",
                    "Ali se pri izračunu časovnic uporabljajo zemljevidi?.<br />" +
                    "če je false, potem je treba proge določati ročno!", db);

                CommonInicialization(db);
                db.SaveChanges();
            }
        }

        public static void InitializeForGsj()
        {
            using (var db = new Marvin20Db())
            {
                Save("Days", 1, "", "Št. dni",
                    "Koliko dni traja tekmovanje. Za ROT se vpiše 3, za enodnevna tekmovanja 1.", db);
                Save("MaxFactor", 1.7, "", "Faktor za izračun dvojne časovnice",
                    "Za GSJ 1.7, za ROT 2.", db);
                Save("TaskFactor", 1, "", "Faktor za izračun dvojne časovnice, za naloge.",
                    "Za GSJ 1, za ROT 2.", db);
                Save("CompetitionName", "Glas svobodne Jelovice", "", "Ime tekmovanja", "", db);
                Save("UseMaps", true, "", "Uporablja zemljevide?",
                    "Ali se pri izračunu časovnic uporabljajo zemljevidi?.<br />" +
                    "če je false, potem je treba proge določati ročno!", db);

                CommonInicialization(db);
                db.SaveChanges();

                //Save("Days", 1, "", "Št. dni",
                //    "Koliko dni traja tekmovanje. Za ROT se vpiše 3, za enodnevna tekmovanja 1.", db);
                //Save("MaxFactor", 1.7, "", "Faktor za izračun dvojne časovnice",
                //    "Za GSJ 1.7, za ROT 2.", db);
                //Save("TaskFactor", 1, "", "Faktor za izračun dvojne časovnice, za naloge.",
                //    "Za GSJ 1, za ROT 2.", db);
                //Save("CompetitionName", "Glas svobodne Jelovice", "", "Ime tekmovanja", "", db);
                //Save("UseMaps", true, "", "Uporablja zemljevide?",
                //    "Ali se pri izračunu časovnic uporabljajo zemljevidi?.<br />" +
                //    "če je false, potem je treba proge določati ročno!", db);

                //CommonInicialization(db);
                //db.SaveChanges();
            }
        }

        private static void CommonInicialization(Marvin20Db db)
        {
            Save("MapDpi", 300, "dpi", "Ločljivost zemljevidov",
                "Kolikšna je ločljivost temljevida (v točkah na palec). Običajno je 300 dpi.<br />" +
                "če ta podatek ni vnešen pravilno, program narobe računa razdalje!", db);
            Save("MapScale", 25000, "1:N", "Merilo zemljevidov",
                "Merilo zemljevida. Običajno je 1:25000. V tem primeru vnesi 25000. <br />" +
                "če ta podatek ni vnešen pravilno, program narobe računa razdalje!", db);
            Save("RenderMap.KtMarkAlpha", 200, "", "Neprosojnost krogca na zemljevidu",
                "Določa prosojnost krogca na zemljevidu, ki predstavlja kontrolno točko. 0 pomeni popolnoma prosojen, 255 popolnoma neprosojen (\"solid\").", db);
            Save("RenderMap.KtMarkDiameter", 80, "px", "Premer krogca na zemljevidu",
                "Premer krogca na zemljevidu, ki predstavlja kontrolno točko. Podaja se v pikslih na zemljevidu.", db);
            Save("RenderMap.KtMarkCenterDiameter", 4, "px", "Premer centra na zemljevidu",
                "Premer krogca na zemljevidu, ki predstavlja center kontrolne točke. Podaja se v pikslih na zemljevidu. če vneseč 0, krogca v centru ne bo", db);
            Save("RenderMap.KtMarkColor","#ff0000", "","Barva krogca na zemljevidu",
                "Barva krogca na zemljevidu, ki predstavlja kontrolno točko. Podaja se v formatu za internetne barve #rrggbb.", db);
            Save("RenderMap.KtMarkLineWidth",4, "px","Širina črte pri krogcu na zemljevidu",
                "Širina črte pri krogcu na zemljevidu, ki predstavlja kontrolno točko. Podaja se v pikslih na zemljevidu.", db);
            Save("RenderMap.KtTextAlpha",200, "","Neprosojnost besedila za oznako KT na zemljevidu",
                "Določa (ne)prosojnost besedila za oznako KT na zemljevidu. 0 pomeni popolnoma prosojen, 255 popolnoma neprosojen (\"solid\").", db);
            Save("RenderMap.KtTextColor","#ff0000", "","Barva besedila za oznako KT na zemljevidu",
                "Barva besedila za oznako KT na zemljevidu. Podaja se v formatu za internetne barve #rrggbb.", db);
            Save("RenderMap.KtTextBgAlpha",100, "","Neprosojnost ozadja besedila za oznako KT na zemljevidu",
                "Za besedilom  na zemljevidu se nariše še ozadje, za n pikslov zamaknjena senca."
                + " Ta nastavitev določa (ne)prosojnost ozadja besedila za oznako KT na zemljevidu."
                + " 0 pomeni popolnoma prosojen, 255 popolnoma neprosojen (\"solid\").", db);
            Save("RenderMap.KtTextBgColor","#ffffff", "","Barva ozadja besedila za oznako KT na zemljevidu",
                "Za besedilom  na zemljevidu se nariše še ozadje, za n pikslov zamaknjena senca."
                + "Ta nastavitev določa barvo ozadja besedila za oznako KT na zemljevidu. "
                + "Podaja se v formatu za internetne barve #rrggbb.", db);
            Save("RenderMap.KtTextFontName","Arial", "","Tipografija za oznako KT na zemljevidu",
                "Tipografija za oznako KT na zemljevidu. "
                + " podaja se ime finta, npr \"Arial\".", db);
            Save("RenderMap.KtTextFontSize",30, "px","Velikost fontov za oznako KT na zemljevidu",
                "Velikost fontov za oznako KT na zemljevidu."
                + " Podaja se v pikslih na zemljevidu.", db);
            Save("RenderMap.KtTextBgOffset", 2, "", "Zamik ozadja besedila za oznako KT na zemljevidu",
                "Za besedilom  na zemljevidu se nariše še ozadje, za n pikslov zamaknjena senca."
                + " Ta nastavitev določi za koliko pik je to ozadje zamaknjeno."
                + " 0 pomeni da sence ne bo.", db);
            Save("RenderMap.KtTextXOffset",+20, "","Premik besedila za oznako KT na zemljevidu",
                "Premik besedila za oznako KT na zemljevidu."
                + " Ta nastavitev določa za koliko pik je napis odmaknjen od centra KT po x osi.", db);
            Save("RenderMap.KtTextYOffset", -30, "","Premik besedila za oznako KT na zemljevidu",
                "Premik besedila za oznako KT na zemljevidu."
                + " Ta nastavitev določa za koliko pik je napis odmaknjen od centra KT po y osi.", db);
            Save("RenderMap.StartTextXOffset",-100, "","Premik besedila za oznako Start na zemljevidu",
                "Premik besedila za oznako Start na zemljevidu."
                + " Ta nastavitev določa za koliko pik je napis odmaknjen od centra Starta po x osi.", db);
            Save("RenderMap.StartTextYOffset",-30, "","Premik besedila za oznako Start na zemljevidu",
                "Premik besedila za oznako Start na zemljevidu."
                + " Ta nastavitev določa za koliko pik je napis odmaknjen od centra Starta po y osi.", db);
            Save("RenderMap.FinishTextXOffset",+20, "","Premik besedila za oznako Cilj na zemljevidu",
                "Premik besedila za oznako Cilj na zemljevidu."
                + " Ta nastavitev določa za koliko pik je napis odmaknjen od centra Cilja po x osi.", db);
            Save("RenderMap.FinishTextYOffset",-30, "","Premik besedila za oznako Cilj na zemljevidu",
                "Premik besedila za oznako Cilj na zemljevidu."
                + " Ta nastavitev določa za koliko pik je napis odmaknjen od centra Cilja po y osi.", db);
            Save("RenderMap.KtHiddenRectangleDimension",200, "m","Dimenzija področja, na katerem se lahko skriva kontrolor",
                "Dimenzija področja, na katerem se lahko skriva kontrolor."
                + " če je točka \"skrita\" (primer ranjenca na GSJ), bo na zemljevidu namesto točne točke narisan kvadrat s stranico, določeno v tej nastavitvi."
                + " Dimenzija se podaja v metrih!!!", db);

  

            
        }


        private static void Save(string code, object defaultValue, string unit, string name, string description, Marvin20Db db)
        {
            var setting = db.Settings.SingleOrDefault(x => x.Code == code);
            if (setting == null)
            {
                setting = new Setting();
                db.Settings.Add(setting);
            }
            setting.Code = code;
            setting.Value = defaultValue.ToString();
            setting.Description = description;
            setting.Name = name;
            setting.Unit = unit;
        }
    }
}