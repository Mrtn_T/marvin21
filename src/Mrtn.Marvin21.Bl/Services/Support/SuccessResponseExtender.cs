﻿using Mrtn.Marvin21.Model.Common;

namespace Mrtn.Marvin21.Bl.Services.Support
{
    static class SuccessResponseExtender
    {
        public static void AddToMessage(this SuccessResponse response, string ss)
        {
            if (response.Message != null && response.Message.Trim() != "") response.Message += "<br />";
            response.Message += ss;
        }
    }
}
