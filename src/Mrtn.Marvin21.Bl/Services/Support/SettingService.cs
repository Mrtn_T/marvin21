﻿using Mrtn.Marvin21.Bl.Interface.Support;

namespace Mrtn.Marvin21.Bl.Services.Support
{
    public class SettingsService : ISettingsService
    {

        public int GetDays()
        {
            return AppSettings.Days;
        }

        public int GetMapDpi()
        {
            return AppSettings.MapDpi;
        }

        public int GetMapScale()
        {
            return AppSettings.MapScale;
        }

        public double PixelsPerMeter()
        {
            return AppSettings.PixelsPerMeter;
        }

        public double MetersPerPixel()
        {
            return AppSettings.MetersPerPixel;
        }
    }
}