﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Bl;
using Mrtn.Marvin21.Bl.Interface.Tasks;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Run;
using Mrtn.Marvin21.DbModel.Tasks;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Tasks;

namespace Mrtn.Marvin21.Bl.Services.Tasks
{
    public class TaskService : ITaskService
    {
        //private class SiCodeCount
        //{
        //    public string Code { get; set; }
        //    public int Count { get; set; }
        //}

        public List<TaskViewModel>
            Get()
        {

            var tasks = new List<TaskViewModel>();
            using (var db = new Marvin20Db())
            {
                var dbTasks = db.Tasks
                    .OrderBy(x => x.Day)
                    .ThenByDescending(y => y.BeforeStart)
                    .ThenByDescending(y => y.OnTrack)
                    .ThenBy(y => y.Station == null ? "" : (
                        y.Station.IsStart 
                        ? "@" 
                        : y.Station.Name))
                    .ThenBy(y => y.AfterFinish)
                    .ToList();
                foreach (var dbItem in dbTasks)
                {
                    tasks.Add(ViewModelFactory.TaskViewModel(dbItem, db));
                }
            }
            return tasks;

        }

        /// <summary>
        /// Iskanje Edit modela za urejanje naloge
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TaskEditModel Edit(int? id)
        {
            TaskEditModel model;
            using (var db = new Marvin20Db())
            {
                Task task;
                if (id.HasValue)
                {
                    // iskanje taska, preverjanje...
                    task = db.Tasks.FirstOrDefault(x => x.Id == id);
                    if (task == null) throw new Exception("Naloga s tem id ne obstaja!");
                    if (task.Locked) throw new Exception("Naloga je zaklenjena!");
                }
                else
                {
                    // kreiranje novega taska
                    task = new Task
                    {
                        Day = 1,
                        Name = "vnesi ime naloge",
                        BeforeStart = true,
                        FirstEntryName = "Točke",
                        FirstEntryMax = 100
                    };
                }
                model = EditModelFactory.TaskEditModel(task, db);
            }
            return model;
        }

        public SuccessResponse Save(TaskEditModel model)
        {
            if (model.Name == "testError") return new SuccessResponse(false, "testError");
            if (model.Categories == null || model.Categories.Count <= 0)
                return new SuccessResponse(false, "Nobena kategorija ni izbrana.");
            if (string.IsNullOrEmpty(model.Abbr))
                return new SuccessResponse(false, "Kratica je obvezna.");
            if (model.Day < 0 && model.Day > AppSettings.Days)
                return new SuccessResponse(false,
                    String.Format("Dan mora biti med 0 in {0}.", AppSettings.Days));
            if (model.OnTrack && model.StationId == 0)
                return new SuccessResponse(false,
                    String.Format("Če izbereš \"Na poti\", je potrebo izbrati tudi točko!"));

            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                Task task;
                if (model.Id == 0)
                {
                    //dodajam 
                    task = new Task();
                    task.TaskToCategories = new List<TaskToCategory>();
                    db.Tasks.Add(task);
                }
                else
                {
                    task = db.Tasks.Where(x => x.Id == model.Id).FirstOrDefault();
                    if (task == null) return new SuccessResponse(false, "Naloga s tem id ne obstaja!");
                    if (task.Locked) return new SuccessResponse(false, "Naloga je zaklenjena!");
                }
                EntityMerger.MergeIgnoringId(task, model);

                //uredim povezavo s kategorijami
                var categoriesHasBeenChanged = false;
                var toBeRemoved = new List<TaskToCategory>();
                //precerim če je kaj za odstranit
                foreach (var taskToCategory in task.TaskToCategories)
                {
                    var catId = model.Categories.Where(x => x == taskToCategory.Category.Id).FirstOrDefault();
                    if (catId == 0)
                    {
                        categoriesHasBeenChanged = true;
                        toBeRemoved.Add(taskToCategory);
                    }
                    else
                        model.Categories.Remove(catId);
                }
                foreach (var taskToCategory in toBeRemoved)
                {
                    if (taskToCategory.Locked)
                        return new SuccessResponse(false,
                            String.Format("Kategorija {0} ne more biti odstranjena, ker je povezava zaklenjena.",
                                taskToCategory.Category.Name));
                    task.TaskToCategories.Remove(taskToCategory);
                }

                foreach (var catId in model.Categories)
                {
                    var category = db.Categories.Where(x => x.Id == catId).FirstOrDefault();
                    if (category != null)
                    {
                        //ali je ta kategorija uporabljena kje drugod, v nalogi z enako okrajšavo?
                        if (db.TasksToCategories.Where(
                            x =>
                            x.Task.Abbr == task.Abbr &&
                            x.Category.Id == category.Id
                            ).Any())
                            return new SuccessResponse(false,
                                String.Format("Kategorija {0} je že uporabljena v nalogi z enako kratico {1}.",
                                    category.Name,
                                    task.Abbr));
                        categoriesHasBeenChanged = true;
                        task.TaskToCategories.Add(new TaskToCategory { Category = category, Task = task });

                    }
                }

                //uredim točko na zemljevidu
                var oldStation = task.Station;
                Station station = null;
                if (model.OnTrack && model.StationId > 0)
                    station = db.Stations.Where(x => x.Id == model.StationId).FirstOrDefault();
                task.Station = station;


                var oldTimeFortask = task.TimeForTask;
                TimeSpan ts;
                task.TimeForTask =
                    TimeSpan.TryParse("0:" + model.TimeForTaskString, out ts)
                        ? (int)ts.TotalSeconds
                        : 0;
                foreach (var taskTaskToCategory in task.TaskToCategories)
                {
                    CategoryCalculator.RecalculateMaxPoints(taskTaskToCategory.Category, db);
                }

                db.SaveChanges();

                var tracksToBeRecalculated = new List<Track>();
                //preverim, katere tracke je potrebno preračunat
                if (station != oldStation && oldStation != null)
                {
                    //vse na starem stationu
                    tracksToBeRecalculated.AddRange(db.Tracks
                        .Where(x => x.TrackCheckpoints
                            .Where(y => y.Station.Id == oldStation.Id)
                            .Any())
                        .ToList());
                }
                if (station != null
                    && (station != oldStation
                        || oldTimeFortask != task.TimeForTask
                        || categoriesHasBeenChanged))
                {
                    //preračunaj proge na novem tasku
                    foreach (var track in db.Tracks
                        .Where(x => x.TrackCheckpoints
                            .Where(y => y.Station.Id == station.Id)
                            .Any())
                        .ToList())
                    {
                        if (!tracksToBeRecalculated.Contains(track)) tracksToBeRecalculated.Add(track);
                    }
                }
                foreach (var track in tracksToBeRecalculated)
                {
                    TrackCalculator.RecalculateTrackTimes(track, db);
                }
                db.SaveChanges();

            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Lock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var task = db.Tasks.Where(x => x.Id == id).FirstOrDefault();
                if (task == null)
                    return new SuccessResponse(false, "Ne najdem naloge s tem iD-jem");
                task.Locked = true;
                foreach (var ttc in task.TaskToCategories)
                {
                    ttc.Locked = true;
                }
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Unlock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var task = db.Tasks.Where(x => x.Id == id).FirstOrDefault();
                if (task == null)
                    return new SuccessResponse(false, "Ne najdem naloge s tem iD-jem");
                task.Locked = false;
                foreach (var ttc in task.TaskToCategories)
                {
                    ttc.Locked = false;
                }
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public bool Add(
            string name, string abbr, string description, int day, bool beforeStart, bool onTrack, bool afterFinish,
            string firstEntryName, int firstEntryMin, int firstEntryMax, int firstEntryDefault,
            string secondEntryName, int secondEntryMin, int secondEntryMax, int secondEntryDefault, bool secondEntryIsTime,
            int timeForTask, int calculatingAlgorythmId,
            bool locked, object trackCheckpoint, object station, string categories)
        {


            using (var db = new Marvin20Db())
            {
                //generate task
                var model = new Task()
                {
                    Name = name,
                    Abbr = abbr,
                    Locked = locked,
                    AfterFinish = afterFinish,
                    BeforeStart = beforeStart,
                    CalculatingAlgorythmId = calculatingAlgorythmId,
                    Day = day,
                    Description = description,
                    FirstEntryDefault = firstEntryDefault,
                    FirstEntryMax = firstEntryMax,
                    FirstEntryMin = firstEntryMin,
                    FirstEntryName = firstEntryName,
                    OnTrack = onTrack,
                    SecondEntryDefault = secondEntryDefault,
                    SecondEntryIsTime = secondEntryIsTime,
                    SecondEntryMax = secondEntryMax,
                    SecondEntryMin = secondEntryMin,
                    SecondEntryName = secondEntryName,
                    TaskToCategories = new List<TaskToCategory>(),
                    TimeForTask = timeForTask

                };

                // find all categories
                var categoryModels = new List<Category>();
                var categoryAbbrevations = categories.Split(',').Select(x => x.Trim());
                foreach (var abbrevation in categoryAbbrevations)
                {
                    var cm = db.Categories.FirstOrDefault(x => x.Abbr == abbrevation);
                    if (cm == null) return false;
                    categoryModels.Add(cm);
                    model.TaskToCategories.Add(new TaskToCategory { Category = cm, Task = model });
                }
                db.Tasks.Add(model);
                db.SaveChanges();


            }
            return true;

        }

        //public TeamEditModel Edit(int? id)
        //{
        //    TeamEditModel teamEditModel;
        //    using (var db = new Marvin20Db())
        //    {
        //        Team team;
        //        if (id.HasValue)
        //        {
        //            team = db.Teams.Where(x => x.Id == id).FirstOrDefault();
        //            if (team == null) throw new Exception("Ekipa s tem id ne obstaja!");
        //            if (team.Locked) throw new Exception("Ekipa je zaklenjena!");
        //        }
        //        else team = new Team();
        //        teamEditModel = EditModelFactory.TeamEditModel(team, db);
        //    }
        //    return teamEditModel;
        //}

        //SuccessResponse ITeamService.Save(TeamEditModel model)
        //{
        //    if (model.Name == "testError") return new SuccessResponse(false, "testError");
        //    var response = new SuccessResponse();
        //    using (var db = new Marvin20Db())
        //    {
        //        Team team;
        //        var category = db.Categories.Where(x => x.Id == model.CategoryId).FirstOrDefault();
        //        if (category == null) return new SuccessResponse(false, "Ne najdem kategorije");
        //        if (!category.Active) return new SuccessResponse(false, "Kategorija je neaktivna!");
        //        var pack = db.Packs.Where(x => x.Id == model.PackId).FirstOrDefault();
        //        if (pack == null) return new SuccessResponse(false, "Ne najdem rodu");
        //        if (!pack.Active) return new SuccessResponse(false, "Rod je neaktiven!");
        //        if (model.Id == 0)
        //        {
        //            //dodajam 
        //            team = new Team();
        //            db.Teams.Add(team);
        //        }
        //        else
        //        {
        //            team = db.Teams.Where(x => x.Id == model.Id).FirstOrDefault();
        //            if (team == null) return new SuccessResponse(false, "Ne najdem ekipe!");
        //        }
        //        EntityMerger.MergeIgnoringId(team, model);
        //        team.Category = category;
        //        team.Pack = pack;
        //        team.SiCode = team.SiCode != null ? team.SiCode.Trim() : null;

        //        db.SaveChanges();
        //    }
        //    response.Ok = true;
        //    return response;
        //}


        //public SuccessResponse Deactivate(int id)
        //{
        //    var response = new SuccessResponse();
        //    using (var db = new Marvin20Db())
        //    {
        //        var team = db.Teams.Where(x => x.Id == id).FirstOrDefault();
        //        if (team == null)
        //        {
        //            response.AddToMessage("Ekipa s tem id ne obstaja!");
        //            return response;
        //        }
        //        if (team.Locked)
        //        {
        //            response.AddToMessage("Ekipa je zaklenjena!");
        //            return response;
        //        }
        //        team.Active = false;
        //        db.SaveChanges();
        //    }
        //    response.Ok = true;
        //    return response;
        //}

        //public SuccessResponse Activate(int id)
        //{
        //    var response = new SuccessResponse();
        //    using (var db = new Marvin20Db())
        //    {
        //        var team = db.Teams.Where(x => x.Id == id).FirstOrDefault();
        //        if (team == null)
        //        {
        //            response.Message = "Ekipa s tem id ne obstaja!";
        //            return response;
        //        }
        //        if (team.Locked)
        //        {
        //            response.Message = "Ekipa je zaklenjena!";
        //            return response;
        //        }
        //        team.Active = true;
        //        db.SaveChanges();
        //    }
        //    response.Ok = true;
        //    return response;
        //}

        //public SuccessResponse SetSiCode(int id, string siCode)
        //{
        //    var response = new SuccessResponse();
        //    using (var db = new Marvin20Db())
        //    {
        //        var team = db.Teams.Where(x => x.Id == id).FirstOrDefault();
        //        if (team == null) return new SuccessResponse(false, "Ekipa s tem id ne obstaja!");
        //        if (team.Locked) return new SuccessResponse(false, "Ekipa je zaklenjena!");
        //        if (team.SiCode == siCode) return new SuccessResponse(false, "Ni spermembe!");
        //        team.SiCode = siCode != null ? siCode.Trim() : null;
        //        db.SaveChanges();
        //        response.Ok = true;
        //        // some aditional tests and warnings
        //        if (!String.IsNullOrWhiteSpace(siCode))
        //        {
        //            var otherTeams = db.Teams.Where(x => x.SiCode == siCode && x.Id != team.Id).Count();
        //            if (otherTeams > 0)
        //                response.AddToMessage(String.Format(
        //                    "Številka čipa ni unikatna. Obstaja še {0} ekip z isto številko.",
        //                    otherTeams));
        //            if (db.SiChips.Where(x => x.SiCode == siCode).Count() == 0)
        //                response.AddToMessage(String.Format(
        //                    "Čip {0} ne obstaja v bazi čipov.",
        //                    siCode));
        //        }
        //    }
        //    return response;
        //}

        //public SuccessResponse SetNo(int id, int? no)
        //{
        //    var response = new SuccessResponse();
        //    using (var db = new Marvin20Db())
        //    {
        //        var team = db.Teams.Where(x => x.Id == id).FirstOrDefault();
        //        if (team == null) return new SuccessResponse(false, "Ekipa s tem id ne obstaja!");
        //        if (team.Locked) return new SuccessResponse(false, "Ekipa je zaklenjena!");

        //        if (team.No == no) return new SuccessResponse(false, "Ni spermembe!");
        //        team.No = no;
        //        db.SaveChanges();
        //        response.Ok = true;
        //        // some aditional tests and warnings
        //        if (no.HasValue)
        //        {
        //            var categoryNo = team.Category.No;
        //            var otherTeams = db.Teams.Where(x => x.No == no.Value && x.Id != team.Id).Count();
        //            if (otherTeams > 0)
        //                response.AddToMessage(String.Format(
        //                    "Številka ekipe ni unikatna. Obstaja še {0} ekip z isto številko.",
        //                    otherTeams));
        //            if ((no.Value / 100) != categoryNo)
        //                response.AddToMessage(String.Format(
        //                    "Številka ekipe je običajno 100*številka kategorije + n, v tem primeru {0}00+n",
        //                    categoryNo));
        //            if ((no.Value % 100) == 0)
        //                response.AddToMessage(String.Format(
        //                    "Številka ekipe naj ne bo večkrtanik števila 100. Prva ekipa v tej kategoriji naj bo {0}01",
        //                    categoryNo));
        //        }
        //    }
        //    return response;
        //}


        //public int GetInactiveTeamsCount()
        //{
        //    using (var db = new Marvin20Db())
        //    {
        //        return db.Teams.Where(x => x.Active == false).Count();
        //    }
        //}

        //public FiltersForTeams GetDataForFilters()
        //{
        //    using (var db = new Marvin20Db())
        //    {
        //        return new FiltersForTeams
        //                   {
        //                       Categories =
        //                           ViewModelFactory.NamedEntityList(
        //                               db.Categories.Where(x => x.Active).OrderBy(x => x.No), true),
        //                       Packs =
        //                           ViewModelFactory.NamedEntityList(
        //                               db.Packs.Where(x => x.Active).OrderBy(x => x.Abbr), true)
        //                   };
        //    }
        //}
    }
}
