﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Bl;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.DbModel.Tracks.Support;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Services.Tracks
{
    public class TrackCheckpointService : ITrackCheckpointService
    {
        public List<TrackCheckpointViewModel> Get(TrackCheckpointQueryModel queryModel)
        {

            var checkpoints = new List<TrackCheckpointViewModel>();
            using (var db = new Marvin20Db())
            {
                IQueryable<TrackCheckpoint> dbCheckpoints = db.TrackCheckpoints;

                //                if (queryModel.Active.HasValue)
                //                    dbPacks = dbPacks.Where(x => x.Active == queryModel.Active.Value);

                foreach (var dbCheckpoint in dbCheckpoints)
                {
                    var pack = ViewModelFactory.TrackCheckpointViewModel(dbCheckpoint, db);
                    //                    pack.TeamCount = dbCheckpoint.Teams.Count;
                    checkpoints.Add(pack);
                }
            }
            return checkpoints;
        }

        /// <summary>
        /// POskusi dodati podano postajo kot Checkpoint v podano progo
        /// </summary>
        /// <param name="id"></param>
        /// <param name="stationId"></param>
        /// <returns></returns>
        public SuccessResponse Add(int id, int stationId)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var track = db.Tracks.Where(x => x.Id == id).FirstOrDefault();
                if (track == null)
                    return new SuccessResponse(false,
                        String.Format("Ne najdem proge z id={0}!", id));
                if(track.Locked)
                    return new SuccessResponse(false,
                        String.Format("Ta proga je zaklenjena!"));
                var station = db.Stations.Where(x => x.Id == stationId).FirstOrDefault();
                if (station == null)
                    return new SuccessResponse(false,
                        String.Format("Ne najdem točke z id={0}!", stationId));
                if (track.Day != station.Day)
                    return new SuccessResponse(false,
                        String.Format("Podana točka ne pripada istemu dnevu kot izbrana proga"));
                if (track.TrackCheckpoints.Where(x => x.Station.Id == stationId).Any())
                    return new SuccessResponse(false,
                        String.Format("Podana točka je že na tej progi"));
                if (station.IsStart &&
                    track.TrackCheckpoints.Where(x => x.Station.IsStart).Any())
                    return new SuccessResponse(false,
                        String.Format("Ta proga že ima start"));
                if (station.IsFinish &&
                    track.TrackCheckpoints.Where(x => x.Station.IsFinish).Any())
                    return new SuccessResponse(false,
                        String.Format("Ta proga že ima cilj"));
                // če sem prišel do sem, točko vsaj načeloma smem dodati progi
                var checkpoint = new TrackCheckpoint
                {
                    FromPrevious = new PathData(),
                    Locked = false,
                    Points = station.DefaultPoints,
                    SpeedTrial = new SpeedTrial(),
                    Time = new Time(),
                    Station = station,
                    Track = track
                };
                // če je start, jo dodam na začetek
                if (station.IsStart)
                {
                    // če je start, jo dodam na začetek
                    checkpoint.No = 1;
                    foreach (var trackCheckpoint in track.TrackCheckpoints.OrderBy(x => x.No))
                    {
                        trackCheckpoint.No = trackCheckpoint.No + 1;
                    }
                }
                else if (station.IsFinish ||
                    !track.TrackCheckpoints.Where(x => x.Station.IsFinish).Any())
                {
                    // če je cilj ali če proga še nima cilja, jo dodam na konec
                    checkpoint.No = track.TrackCheckpoints.Count + 1;
                }
                else
                {
                    // ni cilj, proga že ima cilj, dodam jo tik pred konec
                    checkpoint.No = track.TrackCheckpoints.Count;
                    var cp = track.TrackCheckpoints.Where(x => x.Station.IsFinish).Single();
                    cp.No = cp.No + 1;
                }
                track.TrackCheckpoints.Add(checkpoint);
                db.SaveChanges();

                DoRecalculate(db, track); //kliče lokalni nabor funkcij za preračunavanje
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse MoveUp(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.TrackCheckpoints.Where(x => x.Id == id).FirstOrDefault();
                if (checkpoint == null)
                    return new SuccessResponse(false,
                        String.Format("Ne najdem KT z id={0}!", id));
                var track = checkpoint.Track;
                if (track == null)
                    return new SuccessResponse(false,
                        String.Format("Kt z id={0} ne pripada nobeni progi! kako je to mogoče?!?", id));

                if (checkpoint.Locked)
                    return new SuccessResponse(false,
                        String.Format("Ta točka je zaklenjena!"));
                if (track.Locked)
                    return new SuccessResponse(false,
                        String.Format("Ta proga je zaklenjena!"));

                var previousCheckpoint = track.TrackCheckpoints.Where(x => x.No == checkpoint.No - 1).FirstOrDefault();
                if (previousCheckpoint == null)
                    return new SuccessResponse(false,
                        String.Format("Podani KT nima predhodnika, zato ga ni moč premakniti navzgor"));

                previousCheckpoint.No = previousCheckpoint.No + 1;
                checkpoint.No = checkpoint.No - 1;
                db.SaveChanges();

                DoRecalculate(db, track); //kliče lokalni nabor funkcij za preračunavanje
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse MoveDown(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.TrackCheckpoints.Where(x => x.Id == id).FirstOrDefault();
                if (checkpoint == null)
                    return new SuccessResponse(false,
                        String.Format("Ne najdem KT z id={0}!", id));

                var track = checkpoint.Track;
                if (track == null)
                    return new SuccessResponse(false,
                        String.Format("Kt z id={0} ne pripada nobeni progi! kako je to mogoče?!?", id));

                if (checkpoint.Locked)
                    return new SuccessResponse(false,
                        String.Format("Ta točka je zaklenjena!"));
                if (track.Locked)
                    return new SuccessResponse(false,
                        String.Format("Ta proga je zaklenjena!"));

                var nextCheckpoint = track.TrackCheckpoints.Where(x => x.No == checkpoint.No + 1).FirstOrDefault();
                if (nextCheckpoint == null)
                    return new SuccessResponse(false,
                        String.Format("Podani KT nima predhodnika, zato ga ni moč premakniti navzgor"));

                nextCheckpoint.No = nextCheckpoint.No - 1;
                checkpoint.No = checkpoint.No + 1;
                db.SaveChanges();

                DoRecalculate(db, track); //kliče lokalni nabor funkcij za preračunavanje
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Delete(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.TrackCheckpoints.Where(x => x.Id == id).FirstOrDefault();
                if (checkpoint == null)
                    return new SuccessResponse(false,
                        String.Format("Ne najdem KTja z id={0}!", id));
                if (checkpoint.Locked)
                    return new SuccessResponse(false,
                        String.Format("KT je zaklenjen!"));
                var track = checkpoint.Track;
                if (track.Locked)
                    return new SuccessResponse(false,
                        String.Format("Proga je zaklenjena!"));

                track.TrackCheckpoints.Remove(checkpoint);
                db.TrackCheckpoints.Remove(checkpoint);
                db.SaveChanges();

                DoRecalculate(db, track); //kliče lokalni nabor funkcij za preračunavanje
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Lock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var tcp = db.TrackCheckpoints.Where(x => x.Id == id).FirstOrDefault();
                if (tcp == null)
                    return new SuccessResponse(false,
                        "KT s tem id ne obstaja!");
                tcp.Locked = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Unlock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var tcp = db.TrackCheckpoints.Where(x => x.Id == id).FirstOrDefault();
                if (tcp == null)
                    return new SuccessResponse(false,
                        "KT s tem id ne obstaja!");
                tcp.Locked = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public TrackCheckpointEditModel Edit(int id)
        {
            TrackCheckpointEditModel model;
            using (var db = new Marvin20Db())
            {
                var dbModel = db.TrackCheckpoints.Where(x => x.Id == id).FirstOrDefault();

                if (dbModel == null)
                    throw new Exception(
                        String.Format("KT s tem id ne obstaja!"));

                if (dbModel.Locked)
                    throw new Exception(
                        String.Format("KT je zaklenjena!"));

                if (dbModel.Track.Locked)
                    throw new Exception(
                        String.Format("Proga je zaklenjena!"));


                model = EditModelFactory.TrackCheckpoint(dbModel, db);
            }
            return model;

        }

        public SuccessResponse Save(TrackCheckpointEditModel model)
        {
            return Save(model, -1);
        }

        public SuccessResponse Save(TrackCheckpointEditModel model, double timeFromPrevious)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var dbModel = db.TrackCheckpoints.Where(x => x.Id == model.Id).FirstOrDefault();

                if (dbModel == null)
                    return new SuccessResponse(false,
                        String.Format("KT s tem id ne obstaja!"));

                if (dbModel.Locked)
                    return new SuccessResponse(false,
                        String.Format("KT je zaklenjena!"));

                if (dbModel.Track.Locked)
                    return new SuccessResponse(false,
                        String.Format("Proga je zaklenjena!"));

                EntityMerger.MergeIgnoringId(dbModel, model);
                if (timeFromPrevious > 0)
                {
                    dbModel.Time.FromPrevious = (float) timeFromPrevious;
                    DoRecalculate(db,dbModel.Track);
                }
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        private static void DoRecalculate(Marvin20Db db, Track track)
        {
            TrackCalculator.RenameTracksCheckpoints(track);
            TrackCalculator.RecalculateTrackDistances(track, db);
            TrackCalculator.RecalculateTrackTimes(track, db);
            PathCalculator.DeleteUnusedAutomaticPaths(db);
            CategoryCalculator.RecalculateMaxPoints(track.Category,db);
            db.SaveChanges();
        }

    }
}