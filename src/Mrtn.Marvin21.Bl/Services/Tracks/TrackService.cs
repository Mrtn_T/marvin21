﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Support;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.RunTrack;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Services.Tracks
{
    public class TrackService : ITrackService
    {


        /// <summary>
        /// Vrne 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TrackWithCheckpointsViewModel Get(int id)
        {
            using (var db = new Marvin20Db())
            {
                var dbTrack = db.Tracks.Where(x => x.Id == id).FirstOrDefault();
                if (dbTrack == null)
                    throw new Exception(String.Format("Ni poti s podanim id={0}!", id));
                return ViewModelFactory.TrackWithCheckpointsViewModel(dbTrack, db);
            }
        }

        public List<TrackViewModel> GetTracks(TrackQueryModel queryModel)
        {
            var tracks = new List<TrackViewModel>();
            using (var db = new Marvin20Db())
            {
                var dbTracks = queryModel.Day != 0
                    ? db.Tracks.Where(x => x.Day == queryModel.Day).ToList()
                    : db.Tracks.ToList();
                dbTracks = dbTracks.OrderBy(x => x.Day).ThenBy(x => x.Category.No).ToList();

                tracks.AddRange(dbTracks.Select(
                    dbTrack => ViewModelFactory.TrackViewModel(dbTrack, db)));
            }
            return tracks;
        }

        public List<TrackViewModel> GetTrackForEachCategorByDay(TrackQueryModel queryModel)
        {
            var tracks = new List<TrackViewModel>();
            using (var db = new Marvin20Db())
            {
                var dbTracks = db.Tracks.Where(x => x.Day == queryModel.Day).ToList();
                var dbCategories = db.Categories.OrderBy(x => x.No).ToList();
                foreach (var category in dbCategories)
                {
                    var dbTrack = dbTracks.Where(x => x.Category == category).FirstOrDefault();
                    if (dbTrack == null)
                        tracks.Add(new TrackViewModel
                        {
                            Id = 0,
                            CategoryId = category.Id,
                            CategoryAbbr = category.Abbr,
                        });
                    else
                    {
                        var track = ViewModelFactory.TrackViewModel(dbTrack, db);
                        tracks.Add(track);
                    }
                }
            }
            return tracks;
        }

        public SuccessResponse Lock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var track = db.Tracks.Where(x => x.Id == id).FirstOrDefault();
                if (track == null)
                    return new SuccessResponse(false,
                        "Proga na poti s tem id ne obstaja!");
                track.Locked = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Unlock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var track = db.Tracks.Where(x => x.Id == id).FirstOrDefault();
                if (track == null) 
                    return new SuccessResponse(false,
                        "Proga na poti s tem id ne obstaja!");
                //ToDo: preveriti če se sploh sme odklenit
                track.Locked = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse RemoveSpeedTrial(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var track = db.Tracks.Where(x => x.Id == id).FirstOrDefault();
                if (track == null)
                    return new SuccessResponse(false,
                        String.Format("Ne najdem Poti z id={0}!", id));
                if (track.Locked)
                    return new SuccessResponse(false,
                        String.Format("Pot je zaklenjena!"));
                
                //preverim, če je start zaklenjen
                var starts = track.TrackCheckpoints.Where(x => x.SpeedTrial.IsStart).ToList();
                if(starts.Where(x=>x.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Start HE je zaklenjen!"));

                //preverim, če je cilj zaklenjen
                var finishes = track.TrackCheckpoints.Where(x => x.SpeedTrial.IsFinish).ToList();
                if (finishes.Where(x => x.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Cilj HE je zaklenjen!"));

                // dejansko pobrišem
                foreach (var start in starts)
                {
                    start.SpeedTrial.IsStart = false;
                }
                foreach (var finish in finishes)
                {
                    finish.SpeedTrial.IsFinish = false;
                }

                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        /// <summary>
        /// Po logiki stvari nastavlja ali briše SpeedTrial.IsStart in SpeedTrial.IsFinish
        ///  - če je točka že start ali cilj HE, ji ta status vzame
        ///  - če še ni, potem preveri točke pred to točko
        ///  - če prej ni nič:
        ///      - nastavi to točko na start
        ///      - če še ni cilja, ga nastavi za naslednjo točko
        ///  - če prej obstaja start, nastavi to na cilj in izbriše morebitni drugi cilj
        /// </summary>
        /// <param name="checkpointId"></param>
        /// <returns></returns>
        public SuccessResponse SetSpeedTrial(int checkpointId)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var checkpoint = db.TrackCheckpoints.Where(x => x.Id == checkpointId).FirstOrDefault();
                if (checkpoint == null)
                    return new SuccessResponse(false,
                        String.Format("Ne najdem KT-ja z id={0}!", checkpointId));
                if (checkpoint.Locked)
                    return new SuccessResponse(false,
                        String.Format("Kt je zaklenjen!"));
                var track = checkpoint.Track;
                if (track == null)
                    return new SuccessResponse(false,
                        String.Format("Ne najdem Poti!"));
                if (track.Locked)
                    return new SuccessResponse(false,
                        String.Format("Pot je zaklenjena!"));

                if (checkpoint.SpeedTrial.IsStart)
                {
                    //pobrišem in grem ven
                    checkpoint.SpeedTrial.IsStart = false;
                    db.SaveChanges();
                    response.Ok = true;
                    return response;
                }

                if (checkpoint.SpeedTrial.IsFinish)
                {
                    //pobrišem in grem ven
                    checkpoint.SpeedTrial.IsFinish = false;
                    db.SaveChanges();
                    response.Ok = true;
                    return response;
                }


                //preverim, če je kaj podvojenega!!!!
                if (track.TrackCheckpoints.Where(x => x.SpeedTrial.IsStart).Count() > 1)
                    //preveč startov HE, pobrišem vse
                    foreach (var cp in track.TrackCheckpoints.Where(x => x.SpeedTrial.IsStart))
                        cp.SpeedTrial.IsStart = false;
                if (track.TrackCheckpoints.Where(x => x.SpeedTrial.IsFinish).Count() > 1)
                    //preveč ciljec HE, pobrišem vse
                    foreach (var cp in track.TrackCheckpoints.Where(x => x.SpeedTrial.IsFinish))
                        cp.SpeedTrial.IsFinish = false;


                var cpStart = track.TrackCheckpoints.Where(x => x.SpeedTrial.IsStart).FirstOrDefault();
                var cpFinish = track.TrackCheckpoints.Where(x => x.SpeedTrial.IsFinish).FirstOrDefault();

                if (cpStart == null)
                {
                    if (cpFinish == null)
                    {
                        //preprosto nastavim start
                        checkpoint.SpeedTrial.IsFinish = false;
                        checkpoint.SpeedTrial.IsStart = true;
                        //poskusim nastavit naslednjo za cilj
                        cpFinish = track.TrackCheckpoints.Where(x => x.No == checkpoint.No + 1).FirstOrDefault();
                        if (cpFinish != null && !cpFinish.Locked)
                        {
                            cpFinish.SpeedTrial.IsStart = false;
                            cpFinish.SpeedTrial.IsFinish = true;
                        }
                    }
                    else
                    {
                        //imamo cilj. Ali bo treba menjat?
                        if (cpFinish.No < checkpoint.No)
                        {
                            //ja treba bo menjat
                            if (cpFinish.Locked)
                                return new SuccessResponse(false,
                                    String.Format("Obstoječi cilj zaklenjen!"));
                            cpFinish.SpeedTrial.IsFinish = false;
                            cpFinish.SpeedTrial.IsStart = true;
                            checkpoint.SpeedTrial.IsFinish = true;
                            checkpoint.SpeedTrial.IsStart = false;
                        }
                        else
                        {
                            //ne, ok, cilj je ZA startom
                            checkpoint.SpeedTrial.IsFinish = false;
                            checkpoint.SpeedTrial.IsStart = true;
                        }

                    }
                }
                else
                {
                    //start že obstaja! Ali je pred mano?
                    if (cpStart.No < checkpoint.No)
                    {
                        //Start je spredaj, ta točka je cilj. Ali cilj obstaja?
                        if (cpFinish != null)
                        {
                            //tudi obstaja
                            if (cpFinish.Locked)
                                return new SuccessResponse(false,
                                    String.Format("Obstoječi cilj zaklenjen!"));
                            cpFinish.SpeedTrial.IsFinish = false;
                            cpFinish.SpeedTrial.IsStart = false;
                            checkpoint.SpeedTrial.IsFinish = true;
                            checkpoint.SpeedTrial.IsStart = false;
                        }
                        else
                        {
                            //cilj še ne onstaja, ta točka bo cilj
                            checkpoint.SpeedTrial.IsFinish = true;
                            checkpoint.SpeedTrial.IsStart = false;
                        }

                    }
                    else
                    {
                        //start je za mano. Tole bo start!
                        if (cpStart.Locked)
                            return new SuccessResponse(false,
                                String.Format("Obstoječi start zaklenjen!"));
                        checkpoint.SpeedTrial.IsFinish = false;
                        checkpoint.SpeedTrial.IsStart = true;
                        cpStart.SpeedTrial.IsFinish = false;
                        cpStart.SpeedTrial.IsStart = false;
                    }

                }

                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }



        /// <summary>
        /// Ustvari novo progo za podano kategorijo za podani dan, če le ta še ne obstaja.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <param name="day"></param>
        /// <returns></returns>
        public SuccessResponse Add(int categoryId, int day)
        {
            var settings = new SettingsService();
            if (day > settings.GetDays() || day < 1)
                return new SuccessResponse(false, String.Format("Dan mora biti med 1 in {0}.", settings.GetDays()));
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var category = db.Categories.Where(x => x.Id == categoryId).FirstOrDefault();
                if (category == null)
                    return new SuccessResponse(false, String.Format("Ne najdem kategorije z id={0}!", categoryId));
                if (db.Tracks.Where(x => x.Day == day && x.Category.Id == category.Id).Any())
                    return new SuccessResponse(false, String.Format(
                        "Proga za kategorijo {0} za dan {1} že obstaja!", category.Name, day));
                var track = new Track
                                {
                                    Category = category,
                                    Day = day,
                                    Locked = false,
                                    CumulativeDistance = 0,
                                    CumulativeDownhill = 0,
                                    CumulativeUphill = 0,
                                    OptimalTime = 0
                                };
                db.Tracks.Add(track);
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }


    }
}
