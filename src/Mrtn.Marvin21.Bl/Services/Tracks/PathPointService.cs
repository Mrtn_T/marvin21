﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Bl;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Support;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.DbModel.Tracks.Support;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Services.Tracks
{
    public class PathPointService : IPathPointService
    {

        public PathPointEditModel Edit(int id)
        {
            PathPointEditModel editModel;
            using (var db = new Marvin20Db())
            {
                var pathPoint = db.PathPoints.Where(x => x.Id == id).FirstOrDefault();
                if (pathPoint == null) throw new Exception("Točka na poti s tem id ne obstaja!");
                if (pathPoint.Locked) throw new Exception("Točka na poti je zaklenjena!");
                if (pathPoint.Path.Locked) throw new Exception("Pot, kateri pripada ta točka, je zaklenjena!");

                var tcps = db.TrackCheckpoints.Where(x => x.PathFromPrevious.Id == pathPoint.Path.Id).ToList();

                if (tcps.Where(x => x.Locked).Any())
                    throw new Exception(
                        String.Format("Vsaj en KT, za katerega je ta pot uporabljena, je zaklenjen!"));

                if (tcps.Where(x => x.Track.Locked).Any())
                    throw new Exception(
                        String.Format("Vsaj ena proga, za katero je ta pot uporabljena, je zaklenjena!"));

                editModel = EditModelFactory.PathPointEditModel(pathPoint, db);
            }
            return editModel;
        }

        public SuccessResponse Save(PathPointEditModel model)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var point = db.PathPoints.Where(x => x.Id == model.Id).FirstOrDefault();
                if (point == null) return new SuccessResponse(false,
                    "Točka na poti s tem id ne obstaja!");
                if (point.Locked) return new SuccessResponse(false,
                    "Točka na poti je zaklenjena!");
                if (point.Path.Locked) return new SuccessResponse(false,
                    "Pot, kateri pripada ta točka, je zaklenjena!");

                var tcps = db.TrackCheckpoints.Where(x => x.PathFromPrevious.Id == point.Path.Id).ToList();

                if (tcps.Where(x => x.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Vsaj en KT, za katerega je ta pot uporabljena, je zaklenjen!"));

                if (tcps.Where(x => x.Track.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Vsaj ena proga, za katero je ta pot uporabljena, je zaklenjena!"));

                point.MapPosition.ZisDirty = model.MapPositionZisDirty;
                if (point.MapPosition.Z != model.MapPositionZ)
                    point.MapPosition.ZisDirty = false;
                point.MapPosition.Z = model.MapPositionZ;

                RecalculatePathAndTracks(point.Path, db);
                db.SaveChanges();

            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Add(int pathId, int putAfter, float x, float y)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var path = db.Paths.Where(p => p.Id == pathId).FirstOrDefault();
                if (path == null)
                    return new SuccessResponse(false, 
                        String.Format("Ne najdem poti s podanim id={0}", pathId));
                if (path.Locked)
                    return new SuccessResponse(false, 
                        String.Format("Pot je zaklenjena, zato ni možno dodajanje točk", pathId));
                if (putAfter > 0 && !path.PathPoints.Where(pp => pp.No == putAfter).Any())
                    return new SuccessResponse(false,
                        String.Format(
                        "Na podani poti ne najdem točke s številko {0}, za katero naj bi postavil novo točko"
                        , putAfter));


                var tcps = db.TrackCheckpoints.Where(t => t.PathFromPrevious.Id == path.Id).ToList();

                if (tcps.Where(t => t.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Vsaj en KT, za katerega je ta pot uporabljena, je zaklenjen!"));

                if (tcps.Where(t => t.Track.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Vsaj ena proga, za katero je ta pot uporabljena, je zaklenjena!"));
                //povečam zaporedno številko vsem naslednjim točkam

                foreach (var point in path.PathPoints.Where(pp => pp.No > putAfter))
                    point.No = point.No + 1;
                var pathPoint = new PathPoint()
                                    {
                                        No = putAfter + 1,
                                        MapPosition = new MapPosition { X = x, Y = y },
                                    };
                path.PathPoints.Add(pathPoint);
                RecalculatePathAndTracks(path, db);
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Delete(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var pathPoint = db.PathPoints.Where(x => x.Id == id).FirstOrDefault();
                if (pathPoint == null)
                    return new SuccessResponse(false, 
                        String.Format("Ne najdem točke s podanim id={0}", id));
                if (pathPoint.Locked)
                    return new SuccessResponse(false, 
                        String.Format("Točka je zaklenjena, zato ni možno brisanje točk"));
                var path = pathPoint.Path;
                if (path == null)
                    return new SuccessResponse(false, 
                        String.Format("Ne najdem poti, kateri pripada točka"));
                if (path.Locked)
                    return new SuccessResponse(false, 
                        String.Format("Pot je zaklenjena, zato ni možno brisanje točk"));

                var tcps = db.TrackCheckpoints.Where(x => x.PathFromPrevious.Id == pathPoint.Path.Id).ToList();

                if (tcps.Where(x => x.Locked).Any())
                    return new SuccessResponse(false, 
                    String.Format("Vsaj en KT, za katerega je ta pot uporabljena, je zaklenjen!"));

                if (tcps.Where(x => x.Track.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Vsaj ena proga, za katero je ta pot uporabljena, je zaklenjena!"));


                path.PathPoints.Remove(pathPoint);
                db.PathPoints.Remove(pathPoint);
                PathCalculator.FixOrderNumbersInPath(path);

                RecalculatePathAndTracks(path, db);
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Move(int id, float x, float y)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var point = db.PathPoints.Where(X => X.Id == id).FirstOrDefault();
                if (point == null)
                    return new SuccessResponse(false, 
                        String.Format("Ne najdem točke na poti s podanim id={0}", id));
                if (point.Locked)
                    return new SuccessResponse(false, 
                        String.Format("Točka na poti je zaklenjena, zato je ni možno premakniti"));
                if (point.Path == null)
                    return new SuccessResponse(false, 
                        String.Format("Ta točka ne pripada nobeni poti. Nekaj je hudo narobe v bazi. PathPoint.id={0}", id));
                if (point.Path.Locked)
                    return new SuccessResponse(false, 
                        String.Format("Pot, kateri pripada ta točka je zaklenjena, , zato je ni možno premakniti"));
                if (point.MapPosition.X == x && point.MapPosition.Y == y)
                    return new SuccessResponse(false, 
                        String.Format("Ni premika"));

                var tcps = db.TrackCheckpoints.Where(t => t.PathFromPrevious.Id == point.Path.Id).ToList();

                if (tcps.Where(t => t.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Vsaj en KT, za katerega je ta pot uporabljena, je zaklenjen!"));

                if (tcps.Where(trackCheckpoint => trackCheckpoint.Track.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Vsaj ena proga, za katero je ta pot uporabljena, je zaklenjena!"));

                var settings = new SettingsService();
                double moveDistance = 10000;
                if (point.MapPosition.X.HasValue && point.MapPosition.Y.HasValue)
                {
                    moveDistance = Math.Sqrt(
                        (point.MapPosition.X.Value - x) * (point.MapPosition.X.Value - x) +
                        (point.MapPosition.Y.Value - y) * (point.MapPosition.Y.Value - y)) * settings.MetersPerPixel();
                }
                if (point.MapPosition.Z.HasValue && moveDistance > 10) point.MapPosition.ZisDirty = true;
                point.MapPosition.X = x;
                point.MapPosition.Y = y;

                RecalculatePathAndTracks(point.Path, db);
                db.SaveChanges();
            }

            response.Ok = true;
            return response;
        }

        public SuccessResponse LockAll(int pathId)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var pathPoints = db.PathPoints.Where(x => x.Path.Id == pathId);
                foreach (var pathPoint in pathPoints)
                    pathPoint.Locked = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse UnlockAll(int pathId)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var pathPoints = db.PathPoints.Where(x => x.Path.Id == pathId);
                foreach (var pathPoint in pathPoints)
                    pathPoint.Locked = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Lock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var point = db.PathPoints.Where(x => x.Id == id).FirstOrDefault();
                if (point == null) return new SuccessResponse(false, 
                    "Točka na poti s tem id ne obstaja!");

                point.Locked = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Unlock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var point = db.PathPoints.Where(x => x.Id == id).FirstOrDefault();
                if (point == null) return new SuccessResponse(false,
                    "Točka na poti s tem id ne obstaja!");

                point.Locked = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        private static void RecalculatePathAndTracks(Path path, Marvin20Db db)
        {
            PathCalculator.CalculatePathValues(path);
            foreach (var track in db.Tracks.Where(x => x.TrackCheckpoints.Any(y => y.PathFromPrevious.Id == path.Id)))
            {
                TrackCalculator.RecalculateTrackDistances(track, db);
                TrackCalculator.RecalculateTrackTimes(track, db);
            }
            PathCalculator.DeleteUnusedAutomaticPaths(db);
        }

    }
}