﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Services.Support;
using Mrtn.Marvin21.Bl.Bl;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.DbModel.Tracks.Support;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Services.Tracks
{
    public class PathService : IPathService
    {
        public List<PathViewModel> Get(PathQueryModel queryModel)
        {
            var paths = new List<PathViewModel>();
            using (var db = new Marvin20Db())
            {
                IQueryable<Path> dbPaths = db.Paths.Where(x => x.FirstStation.Day == queryModel.Day);
                foreach (var dbPath in dbPaths
                    .OrderByDescending(x => x.FirstStation.IsStart)
                    .ThenBy(x => x.FirstStation.IsFinish)
                    .ThenBy(x => x.FirstStation.Name)
                    .ThenByDescending(x => x.SecondStation.IsStart)
                    .ThenBy(x => x.SecondStation.IsFinish)
                    .ThenBy(x => x.SecondStation.Name))
                {
                    var path = ViewModelFactory.PathViewModel(dbPath);
                    paths.Add(path);
                }
            }
            return paths;
        }

        public PathViewModel Get(int id)
        {
            PathViewModel path;
            using (var db = new Marvin20Db())
            {
                var dbPath = db.Paths.Where(x => x.Id == id).FirstOrDefault();
                if (dbPath == null) throw new Exception("Ne najdem poti s tem Id.");
                 path = ViewModelFactory.PathViewModel(dbPath);
            }
            return path;
        }


        /// <summary>
        /// Dodajanje poti med dvema točkama
        /// </summary>
        /// <param name="id1"></param>
        /// <param name="id2"></param>
        /// <returns></returns>
        public SuccessResponse Add(int id1, int id2)
        {
            if (id1 == id2)
                return new SuccessResponse(false, String.Format("Začetna in konča točka morata biti različni"));
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                if (db.Paths.Where(x =>
                               (x.FirstStation.Id == id1 && x.SecondStation.Id == id2) ||
                               (x.FirstStation.Id == id2 && x.SecondStation.Id == id1)).Any())
                    return new SuccessResponse(false, String.Format("Pot, ki vsebuje ti dve točki, že obstaja!"));

                var station1 = db.Stations.Where(x => x.Id == id1).FirstOrDefault();
                if (station1 == null)
                    return new SuccessResponse(false, String.Format("Ne najdem točke z id={0}!", id1));

                var station2 = db.Stations.Where(x => x.Id == id2).FirstOrDefault();
                if (station2 == null)
                    return new SuccessResponse(false, String.Format("Ne najdem točke z id={0}!", id2));
                if (station1.Day != station2.Day)
                    return new SuccessResponse(false, String.Format("Točki ne pripadata istemu dnevu"));

                var path = new Path
                               {
                                   FirstStation = station1,
                                   SecondStation = station2,
                                   ManualData = new PathData(),
                                   AutomaticData = new PathData()
                               };
                db.Paths.Add(path);
                PathCalculator.CalculatePathValues(path);
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public PathEditModel Edit(int id)
        {
            PathEditModel pathEditModel;
            using (var db = new Marvin20Db())
            {
                var path = db.Paths.Where(x => x.Id == id).FirstOrDefault();
                if (path == null) throw new Exception("Pot s tem id ne obstaja!");
                if (path.Locked) throw new Exception("Pot je zaklenjena!");

                var tcps = db.TrackCheckpoints.Where(x => x.PathFromPrevious.Id == path.Id).ToList();

                if (tcps.Where(x => x.Locked).Any())
                    throw new Exception(
                        String.Format("Vsaj en KT, za katerega je ta pot uporabljena, je zaklenjen!"));

                if (tcps.Where(x => x.Track.Locked).Any())
                    throw new Exception(
                        String.Format("Vsaj ena proga, za katero je ta pot uporabljena, je zaklenjena!"));

                pathEditModel = EditModelFactory.PathEditModel(path, db);
            }
            return pathEditModel;
        }

        public SuccessResponse Save(PathEditModel model)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var path = db.Paths.Where(x => x.Id == model.Id).FirstOrDefault();
                if (path == null)
                    return new SuccessResponse(false,
                        "Pot s tem id ne obstaja!");
                if (path.Locked)
                    return new SuccessResponse(false,
                        "Pot je zaklenjena!");

                var tcps = db.TrackCheckpoints.Where(x => x.PathFromPrevious.Id == path.Id).ToList();

                if (tcps.Where(x => x.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Vsaj en KT, za katerega je ta pot uporabljena, je zaklenjen!"));

                if (tcps.Where(x => x.Track.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Vsaj ena proga, za katero je ta pot uporabljena, je zaklenjena!"));

                path.IsManual = model.IsManual;
                if (model.IsManual)
                {
                    path.ManualData.Distance = model.ManualDataDistance;
                    path.ManualData.Zlist = model.ManualDataZlist;
                    path.ManualData.ManualUpDown = model.ManualDataManualUpDown;
                    if (model.ManualDataManualUpDown)
                    {
                        path.ManualData.Downhill = model.ManualDataDownhill;
                        path.ManualData.Uphill = model.ManualDataUphill;
                    }
                }
                RecalculatePathAndTracks(path, db);
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }



        public SuccessResponse LockAll(int day)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var paths = db.Paths.Where(x => x.FirstStation.Day == day);
                foreach (var path in paths)
                    path.Locked = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }
        public SuccessResponse UnlockAll(int day)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var paths = db.Paths.Where(x => x.FirstStation.Day == day);
                foreach (var path in paths)
                    path.Locked = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }
        public SuccessResponse Lock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var path = db.Paths.Where(x => x.Id == id).FirstOrDefault();
                if (path == null)
                {
                    response.AddToMessage("Pot s tem id ne obstaja!");
                    return response;
                }
                path.Locked = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }
        public SuccessResponse Unlock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var path = db.Paths.Where(x => x.Id == id).FirstOrDefault();
                if (path == null)
                {
                    response.AddToMessage("Pot s tem id ne obstaja!");
                    return response;
                }
                path.Locked = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Invert(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var path = db.Paths.Where(x => x.Id == id).FirstOrDefault();
                if (path == null)
                {
                    response.AddToMessage("Pot s tem id ne obstaja!");
                    return response;
                }

                if (db.Paths.Where(x => x.FirstStation.Id == path.SecondStation.Id
                                     && x.SecondStation.Id == path.FirstStation.Id).Any())
                {
                    response.AddToMessage("Obratna pot že obstaja!");
                    return response;
                }
                var inversePath = PathCalculator.GetInversePath(path);
                db.Paths.Add(inversePath);
                db.SaveChanges();
                //ker  je to nova pot, še ni uporabljena na nobeni progi
            }
            response.Ok = true;
            return response;
        }



        private static void RecalculatePathAndTracks(Path path, Marvin20Db db)
        {
            PathCalculator.CalculatePathValues(path);
            foreach (var track in db.Tracks.Where(x => x.TrackCheckpoints.Any(y => y.PathFromPrevious.Id == path.Id)))
            {
                TrackCalculator.RecalculateTrackDistances(track, db);
                TrackCalculator.RecalculateTrackTimes(track, db);
            }
            PathCalculator.DeleteUnusedAutomaticPaths(db);
        }
    }
}