﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Bl.Services.Support;
using Mrtn.Marvin21.Bl.Bl;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.DbModel.Tracks.Support;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.RunTrack;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl.Services.Tracks
{
    public class  StationService : IStationService
    {

        public StationDetailsViewModel GetModelForStation(int id)
        {

            using (var db = new Marvin20Db())
            {
                var station = db.Stations.Where(x => x.Id == id).FirstOrDefault();

                if (station == null)
                    throw new Exception(String.Format("Točka s tem id ne obstaja!"));


                return ViewModelFactory.StationDetailsViewModel(station, db);
            }
        }
           

        
        public List<StationViewModel> Get()
        {
            var stations = new List<StationViewModel>();
            using (var db = new Marvin20Db())
            {
                IQueryable<Station> dbStations = db.Stations;
                foreach (var dbStation in dbStations
                    .OrderByDescending(x => x.IsStart)
                    .ThenBy(x => x.IsFinish)
                    .ThenBy(x => x.Name))
                {
                    var station = ViewModelFactory.StationViewModel(dbStation);
                    //                    pack.TeamCount = dbCheckpoint.Teams.Count;
                    stations.Add(station);
                }
            }
            return stations;
        }

        public List<StationViewModel> Get(StationQueryModel queryModel)
        {

            var stations = new List<StationViewModel>();
            using (var db = new Marvin20Db())
            {
                IQueryable<Station> dbStations = db.Stations.Where(x => x.Day == queryModel.Day);

                foreach (var dbStation in dbStations
                    .OrderByDescending(x => x.IsStart)
                    .ThenBy(x => x.IsFinish)
                    .ThenBy(x => x.Name))
                {
                    var station = ViewModelFactory.StationViewModel(dbStation);
                    //                    pack.TeamCount = dbCheckpoint.Teams.Count;
                    stations.Add(station);
                }
            }
            return stations;
        }

        public SuccessResponse ChangeCoordinates(int id, float xCoord, float yCoord)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var station = db.Stations.Where(x => x.Id == id).FirstOrDefault();

                if (station == null)
                    return new SuccessResponse(false,
                        String.Format("Točka s tem id ne obstaja!"));

                if (station.Locked)
                    return new SuccessResponse(false,
                        String.Format("Točka je zaklenjena!"));

                if (station.TrackCheckpoints.Where(x => x.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Vsaj en KT, za katerega je ta točka uporabljena, je zaklenjen!"));

                if (station.TrackCheckpoints.Where(x => x.Track.Locked).Any())
                    return new SuccessResponse(false,
                        String.Format("Vsaj ena proga, za katero je ta točka uporabljena, je zaklenjena!"));


                station.MapPosition.X = xCoord;
                station.MapPosition.Y = yCoord;
                station.MapPosition.ZisDirty = true;

                RecalculatePathsAndTracks(db, station);

                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Lock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var station = db.Stations.Where(x => x.Id == id).FirstOrDefault();
                if (station == null)
                {
                    response.AddToMessage("Točka s tem id ne obstaja!");
                    return response;
                }
                station.Locked = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse Unlock(int id)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var station = db.Stations.Where(x => x.Id == id).FirstOrDefault();
                if (station == null)
                {
                    response.AddToMessage("Točka s tem id ne obstaja!");
                    return response;
                }
                station.Locked = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse LockAll(int day)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var stations = db.Stations.Where(x => x.Day == day);
                foreach (var station in stations)
                    station.Locked = true;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public SuccessResponse UnlockAll(int day)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var stations = db.Stations.Where(x => x.Day == day);
                foreach (var station in stations)
                    station.Locked = false;
                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        public StationEditModel GetModelForAdd(int day)
        {
            StationEditModel stationEditModel;
            using (var db = new Marvin20Db())
            {
                //getting automatic name
                string name;
                var stations = db.Stations
                    .Where(x => x.Day == day && !x.IsStart && !x.IsFinish)
                    .ToList();
                var i = 65;
                do
                {
                    name = ((char)i).ToString();
                    i++;
                } while (stations.Where(x => x.Name == name).Any());

                var station = new Station
                {
                    Day = day,
                    MapPosition = new MapPosition { X = 0, Y = 0, Z = 300, ZisDirty = true },
                    Name = name,
                    DefaultPoints = 100

                };
                //db.Stations.Add(station);
                //db.SaveChanges();
                stationEditModel = EditModelFactory.StationEditModel(station, db);
            }
            return stationEditModel;
        }

        public SuccessResponse Delete(int id)
        {
            using (var db = new Marvin20Db())
            {
                var station = db.Stations.FirstOrDefault(x => x.Id == id);

                if (station == null) 
                    return new SuccessResponse(false,"Točka s tem id ne obstaja!");

                if (station.Locked) 
                    return new SuccessResponse(false,"Točka je zaklenjena!");

                if (station.TrackCheckpoints!=null && station.TrackCheckpoints.Count>0) 
                    return new SuccessResponse(false,"Vsaj en KT, za katerega je ta točka uporabljena, je zaklenjen!");

                if(db.Paths.Any(x=>x.FirstStation.Id==station.Id))
                    return new SuccessResponse(false,"Točka je start vsaj ene poti med točkama!");

                if (db.Paths.Any(x => x.SecondStation.Id == station.Id))
                    return new SuccessResponse(false,"Točka je cilj vsaj ene poti med točkama!");

                db.Stations.Remove(station);
                db.SaveChanges();
            }
            return new SuccessResponse(true,"");
        }

        public SuccessResponse Add(string type, float xCoord, float yCoord, int day)
        {
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                List<Station> stations;
                //getting automatic name
                var name = "";
                var i = 1;
                switch (type)
                {
                    case "start":
                        stations = db.Stations
                            .Where(x => x.Day == day && x.IsStart && !x.IsFinish && x.Name.StartsWith("Start"))
                            .ToList();
                        name = "Start";
                        while (stations.Where(x => x.Name == name).Any())
                        {
                            name = "Start " + i;
                            i++;
                        }

                        break;
                    case "finish":
                        stations = db.Stations
                            .Where(x => x.Day == day && !x.IsStart && x.IsFinish && x.Name.StartsWith("Cilj"))
                            .ToList();
                        name = "Cilj";
                        while (stations.Where(x => x.Name == name).Any())
                        {
                            name = "Cilj " + i;
                            i++;
                        }
                        break;
                    case "live":
                    case "kt":
                        stations = db.Stations
                            .Where(x => x.Day == day && !x.IsStart && !x.IsFinish)
                            .ToList();
                        i = 65;
                        do
                        {
                            name = ((char)i).ToString();
                            i++;
                        } while (stations.Where(x => x.Name == name).Any());
                        break;
                }

                var station = new Station
                                  {
                                      Day = day,
                                      MapPosition = new MapPosition { X = xCoord, Y = yCoord, Z = 300, ZisDirty = true },
                                      Name = name,
                                      DefaultPoints = 100

                                  };
                switch (type)
                {
                    case "start":
                        station.IsStart = true;
                        station.DefaultPoints = 0;
                        break;
                    case "finish":
                        station.IsFinish = true;
                        station.DefaultPoints = 0;
                        break;
                    case "live":
                        station.IsAlive = true;
                        break;
                }
                db.Stations.Add(station);
                db.SaveChanges();
            }
            response.Ok = true;
            return response;


        }

        public StationEditModel Edit(int id)
        {
            StationEditModel stationEditModel;
            using (var db = new Marvin20Db())
            {
                var station = db.Stations.Where(x => x.Id == id).FirstOrDefault();

                if (station == null)
                    throw new Exception(
                        String.Format("Točka s tem id ne obstaja!"));

                if (station.Locked)
                    throw new Exception(
                        String.Format("Točka je zaklenjena!"));

                if (station.TrackCheckpoints.Where(x => x.Locked).Any())
                    throw new Exception(
                        String.Format("Vsaj en KT, za katerega je ta točka uporabljena, je zaklenjen!"));

                if (station.TrackCheckpoints.Where(x => x.Track.Locked).Any())
                    throw new Exception(
                        String.Format("Vsaj ena proga, za katero je ta točka uporabljena, je zaklenjena!"));

                stationEditModel = EditModelFactory.StationEditModel(station, db);
            }
            return stationEditModel;
        }

        public SuccessResponse Save(StationEditModel model)
        {
            //if (model.Id == 0) return SaveNew(model);
            if (model.Name == "testError") return new SuccessResponse(false, "testError");
            var response = new SuccessResponse();
            using (var db = new Marvin20Db())
            {
                var station = db.Stations.Where(x => x.Id == model.Id).FirstOrDefault();

                if (station == null)
                {
                    station = new Station();
                    db.Stations.Add(station);
                }

                if (station.Locked)
                    return new SuccessResponse(false,
                        String.Format("Točka je zaklenjena!"));

                if (station.TrackCheckpoints!=null && station.TrackCheckpoints.Any(x => x.Locked))
                    return new SuccessResponse(false,
                        String.Format("Vsaj en KT, za katerega je ta točka uporabljena, je zaklenjen!"));

                if (station.TrackCheckpoints != null && station.TrackCheckpoints.Any(x => x.Track.Locked))
                    return new SuccessResponse(false,
                        String.Format("Vsaj ena proga, za katero je ta točka uporabljena, je zaklenjena!"));

                if (model.Day == 0) model.Day = station.Day;
                if (station.Day != model.Day)
                    return new SuccessResponse(false, String.Format("Dneva ni možno naknadno spremeniti!"));


                EntityMerger.MergeIgnoringId(station, model);
                if (station.MapPosition == null) station.MapPosition = new MapPosition();
                station.MapPosition.ZisDirty = model.ZisDirty;
                if (station.IsStart)
                    station.IsFinish = false;
                if (station.MapPosition.Z != model.Z)
                    station.MapPosition.ZisDirty = false;
                if (model.Z != null)
                    station.MapPosition.Z = model.Z;

                RecalculatePathsAndTracks(db, station);

                db.SaveChanges();
            }
            response.Ok = true;
            return response;
        }

        private static SuccessResponse SaveNew(StationEditModel model)
        {
            var station = new Station();
            EntityMerger.MergeIgnoringId(station, model);
            using (var db = new Marvin20Db())
            {
                db.Stations.Add(station);
                db.SaveChanges();
            }
            var response = new SuccessResponse {Ok = true};
            return response;

        }

        private void RecalculatePathsAndTracks(Marvin20Db db, Station station)
        {
            var paths = db.Paths.Where(x => x.FirstStation.Id == station.Id ||
                                            x.SecondStation.Id == station.Id);
            foreach (var path in paths)
            {
                PathCalculator.CalculatePathValues(path);
                var checkpoints = db.TrackCheckpoints.Where(x => x.PathFromPrevious.Id == path.Id);
                foreach (var checkpoint in checkpoints)
                {
                    TrackCalculator.RecalculateTrackDistances(checkpoint.Track, db);
                    TrackCalculator.RecalculateTrackTimes(checkpoint.Track, db);
                }
            }
            PathCalculator.DeleteUnusedAutomaticPaths(db);
        }

    }
}