﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.DbModel.Tasks;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.Model.Si;
using Mrtn.Marvin21.Model.Tasks;
using Mrtn.Marvin21.Model.Teams;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Bl
{
    public static class EditModelFactory
    {
        public static PackEditModel PackEditModel(Pack pack)
        {
            return new PackEditModel
            {
                Id = pack.Id,
                Name = pack.Name,
                Town = pack.Town,
                Abbr = pack.Abbr,
                InCompetition = pack.InCompetition,
            };
        }

        public static TeamEditModel TeamEditModel(Team team, Marvin20Db db)
        {
            var model = new TeamEditModel
            {
                Active = team.Active,
                Id = team.Id,
                InCompetition = team.InCompetition,
                Name = team.Name,
                CategoryId = team.Category == null ? 0 : team.Category.Id,
                PackId = team.Pack == null ? 0 : team.Pack.Id,
                PrivateSiCode = team.PrivateSiCode
            };
            var shouldAddEmpty = (team.Id == 0);
            model.Categories =
                ViewModelFactory.NamedEntityList(
                    db.Categories.Where(x => x.Active).OrderBy(x => x.No), shouldAddEmpty);
            model.Packs =
                ViewModelFactory.NamedEntityList(
                    db.Packs.Where(x => x.Active).OrderBy(x => x.Abbr), shouldAddEmpty);
            return model;
        }

        public static SiChipEditModel SiChipEditModel(SiChip siChip, Marvin20Db db)
        {
            return new SiChipEditModel
            {
                Id = siChip.Id,
                SiCode = siChip.SiCode,
                Private = siChip.Private,
                Comment = siChip.Comment,
                //Active = siChip.Active
            };
        }

        public static StationEditModel StationEditModel(Station ts, Marvin20Db db)
        {
            return new StationEditModel
                       {
                           Day = ts.Day,
                           Description = ts.Description,
                           Id = ts.Id,
                           IsAlive = ts.IsAlive,
                           IsHidden = ts.IsHidden,
                           IsFinish = ts.IsFinish,
                           IsStart = ts.IsStart,
                           Name = ts.Name,
                           SiCodeArrival = ts.SiCodeArrival,
                           SiCodeDeparture = ts.SiCodeDeparture,
                           Z = ts.MapPosition.Z,
                           ZisDirty = ts.MapPosition.ZisDirty,
                           DefaultPoints = ts.DefaultPoints

                       };

        }

        public static PathEditModel PathEditModel(Path path, Marvin20Db db)
        {
            return new PathEditModel
                       {
                           Id = path.Id,
                           Locked = path.Locked,
                           IsManual = path.IsManual,
                           AutomaticDataDistance = path.AutomaticData.Distance, //readonly
                           AutomaticDataZlist = path.AutomaticData.Zlist, //readonly

                           ManualDataDistance = path.ManualData.Distance,
                           ManualDataZlist = path.ManualData.Zlist,
                           ManualDataDownhill = path.ManualData.Downhill,
                           ManualDataUphill = path.ManualData.Uphill,
                           ManualDataManualUpDown = path.ManualData.ManualUpDown,

                           FirstStationName = path.FirstStation.Name,
                           FirstStationZ = path.FirstStation.MapPosition.Z,
                           FirstStationZisDirty = path.FirstStation.MapPosition.ZisDirty,

                           SecondStationName = path.SecondStation.Name,
                           SecondStationZ = path.SecondStation.MapPosition.Z,
                           SecondStationZisDirty = path.SecondStation.MapPosition.ZisDirty,


                       };
        }

        public static PathPointEditModel PathPointEditModel(PathPoint pp, Marvin20Db db)
        {
            return new PathPointEditModel
                       {
                           Id = pp.Id,
                           MapPositionZ = pp.MapPosition.Z,
                           MapPositionZisDirty = pp.MapPosition.ZisDirty
                       };
        }

        public static CategoryEditModel CategoryEditModel(Category category)
        {
            return new CategoryEditModel
                       {
                           Abbr = category.Abbr,
                           Active = category.Active,
                           Color = category.Color,
                           DownhillSpeed = category.DownhillSpeed,
                           Id = category.Id,
                           Name = category.Name,
                           No = category.No,
                           Speed = category.Speed,
                           UphillSpeed = category.UphillSpeed
                       };
        }

        public static TaskEditModel TaskEditModel(Task task, Marvin20Db db)
        {
            var model = new TaskEditModel
                            {
                                Id = task.Id,
                                Abbr = task.Abbr,
                                Name = task.Name,
                                Description = task.Description,
                                Day = task.Day,
                                BeforeStart = task.BeforeStart,
                                OnTrack = task.OnTrack,
                                AfterFinish = task.AfterFinish,
                                CalculatingAlgorythmId = task.CalculatingAlgorythmId,
                                FirstEntryName = task.FirstEntryName,
                                FirstEntryMin = task.FirstEntryMin,
                                FirstEntryMax = task.FirstEntryMax,
                                FirstEntryDefault = task.FirstEntryDefault,
                                SecondEntryName = task.SecondEntryName,
                                SecondEntryIsTime = task.SecondEntryIsTime,
                                SecondEntryMin = task.SecondEntryMin,
                                SecondEntryMax = task.SecondEntryMax,
                                SecondEntryDefault = task.SecondEntryDefault,
                                Stations =
                                    ViewModelFactory.NamedEntityList(
                                        db.Stations.Where(x => x.Day == task.Day && (x.IsAlive || x.IsStart)), true),
                                StationId = task.Station == null
                                    ? 0
                                    : task.Station.Id,
                                CategoryList = ViewModelFactory.NamedEntityList(db.Categories, false),
                                Categories = (task.TaskToCategories == null)
                                    ? new List<int>()
                                    : task.TaskToCategories.Select(x => x.Category.Id).ToList(),
                                MaxPoints= task.MaxPoints
            };
            var ts = new TimeSpan(0, 0, task.TimeForTask);
            model.TimeForTaskString = ts.ToString(@"m\:ss");
            return model;
        }

        public static TrackCheckpointEditModel TrackCheckpoint(TrackCheckpoint dbModel, Marvin20Db db)
        {
            var ts = new TimeSpan(0, 0, (int)dbModel.Time.FromPrevious);
            return new TrackCheckpointEditModel
            {
                Id = dbModel.Id,
                Description = dbModel.Description,
                Points = dbModel.Points,

                TimeFromPrevious = ts.ToString(@"m\:ss")
            };

        }
    }
}
