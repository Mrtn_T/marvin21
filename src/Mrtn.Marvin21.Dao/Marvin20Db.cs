﻿using System.Data.Entity;
using Mrtn.Marvin21.DbModel;
using Mrtn.Marvin21.DbModel.Results;
using Mrtn.Marvin21.DbModel.Run;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.DbModel.Support;
using Mrtn.Marvin21.DbModel.Tasks;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Tracks;

namespace Mrtn.Marvin21.Dao
{
    public class Marvin20Db : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Pack> Packs { get; set; }

        public DbSet<SiChip> SiChips { get; set; }

        public DbSet<Track> Tracks { get; set; }
        public DbSet<TrackCheckpoint> TrackCheckpoints { get; set; }
        public DbSet<Station> Stations { get; set; }

        public DbSet<Path> Paths { get; set; }
        public DbSet<PathPoint> PathPoints { get; set; }

        public DbSet<Setting> Settings { get; set; }

        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskToCategory> TasksToCategories { get; set; }

        public DbSet<RunTask> RunTasks { get; set; }
        public DbSet<RunTrackCheckpoint> RunTrackCheckpoints { get; set; }
        public DbSet<RunTrack> RunTracks { get; set; }

        public DbSet<PackResult> PackResults { get; set; }
        public DbSet<TeamResult> TeamResults { get; set; }

        public DbSet<SiRead> SiReads { get; set; }
        public DbSet<SiPunch> SiPunches { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>().HasOptional(t => t.Result).WithRequired();
            modelBuilder.Entity<Pack>().HasOptional(t => t.Result).WithRequired();
            modelBuilder.Entity<RunTrack>().HasOptional(t => t.SiRead).WithOptionalPrincipal();
            modelBuilder.Entity<SiRead>().HasOptional(t => t.RunTrack).WithOptionalPrincipal();
            base.OnModelCreating(modelBuilder);

        }
    }
}
