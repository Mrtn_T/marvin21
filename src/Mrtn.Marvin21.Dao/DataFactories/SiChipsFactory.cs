﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.DbModel.Si;

namespace Mrtn.Marvin21.Dao.DataFactories
{
    class SiChipsFactory
    {
        public static void FillSiChips(Marvin20Db db)
        {
            db.SiChips.Add(new SiChip { SiCode = "232462" });
            db.SiChips.Add(new SiChip { SiCode = "235877" });
            db.SiChips.Add(new SiChip { SiCode = "235895" });
            db.SiChips.Add(new SiChip { SiCode = "235898" });
            db.SiChips.Add(new SiChip { SiCode = "349771" });
            db.SiChips.Add(new SiChip { SiCode = "406734" });
            db.SiChips.Add(new SiChip { SiCode = "422503" });
            db.SiChips.Add(new SiChip { SiCode = "422513" });
            db.SiChips.Add(new SiChip { SiCode = "422515" });
            db.SiChips.Add(new SiChip { SiCode = "422519" });
            db.SiChips.Add(new SiChip { SiCode = "422527" });
            db.SiChips.Add(new SiChip { SiCode = "422528" });
            db.SiChips.Add(new SiChip { SiCode = "422529" });
            db.SiChips.Add(new SiChip { SiCode = "422533" });
            db.SiChips.Add(new SiChip { SiCode = "422535" });
            db.SiChips.Add(new SiChip { SiCode = "422536" });
            db.SiChips.Add(new SiChip { SiCode = "422537" });
            db.SiChips.Add(new SiChip { SiCode = "435621" });
            db.SiChips.Add(new SiChip { SiCode = "435622" });
            db.SiChips.Add(new SiChip { SiCode = "435624" });
            db.SiChips.Add(new SiChip { SiCode = "435628" });
            db.SiChips.Add(new SiChip { SiCode = "435632" });
            db.SiChips.Add(new SiChip { SiCode = "435636" });
            db.SiChips.Add(new SiChip { SiCode = "435638" });
            db.SiChips.Add(new SiChip { SiCode = "435640" });
            db.SiChips.Add(new SiChip { SiCode = "435645" });
            db.SiChips.Add(new SiChip { SiCode = "435646" });
            db.SiChips.Add(new SiChip { SiCode = "435647" });
            db.SiChips.Add(new SiChip { SiCode = "435650" });
            db.SiChips.Add(new SiChip { SiCode = "435653" });
            db.SiChips.Add(new SiChip { SiCode = "435654" });
            db.SiChips.Add(new SiChip { SiCode = "442801" });
            db.SiChips.Add(new SiChip { SiCode = "442805" });
            db.SiChips.Add(new SiChip { SiCode = "442807" });
            db.SiChips.Add(new SiChip { SiCode = "442808" });
            db.SiChips.Add(new SiChip { SiCode = "442810" });
            db.SiChips.Add(new SiChip { SiCode = "442811" });
            db.SiChips.Add(new SiChip { SiCode = "442812" });
            db.SiChips.Add(new SiChip { SiCode = "442813" });
            db.SiChips.Add(new SiChip { SiCode = "442815" });
            db.SiChips.Add(new SiChip { SiCode = "442816" });
            db.SiChips.Add(new SiChip { SiCode = "442819" });
            db.SiChips.Add(new SiChip { SiCode = "442821" });
            db.SiChips.Add(new SiChip { SiCode = "442823" });
            db.SiChips.Add(new SiChip { SiCode = "442824" });
            db.SiChips.Add(new SiChip { SiCode = "442825" });
            db.SiChips.Add(new SiChip { SiCode = "442827" });
            db.SiChips.Add(new SiChip { SiCode = "442829" });
            db.SiChips.Add(new SiChip { SiCode = "442830" });
            db.SiChips.Add(new SiChip { SiCode = "442831" });
            db.SiChips.Add(new SiChip { SiCode = "442832" });
            db.SiChips.Add(new SiChip { SiCode = "442836" });
            db.SiChips.Add(new SiChip { SiCode = "442839" });
            db.SiChips.Add(new SiChip { SiCode = "442840" });
            db.SiChips.Add(new SiChip { SiCode = "999987", Private = true });
            db.SiChips.Add(new SiChip { SiCode = "999988", Private = true });
            db.SiChips.Add(new SiChip { SiCode = "999989", Private = true });
            db.SiChips.Add(new SiChip { SiCode = "999990", Private = true });
        }
    }
}
