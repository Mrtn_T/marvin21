﻿using System;
using System.Globalization;

namespace Mrtn.Marvin21.Dao.DataFactories.Support
{
    /// <summary>
    /// Pretvorbe iz stringov v določene podatkovne tipe .
    /// Uporablja se za TESTNO polnjenje podatkov, predvsem v TrackFactory
    /// </summary>
    public static class Converter
    {
        public static int ToInt(string s)
        {
            int i;
            return Int32.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out i)
                       ? i
                       : 0;
        }
        public static float? ToNullableFloat(string s)
        {
            s = s.Trim().Replace(',', '.');
            if (s == "") return null;
            float f;
            return Single.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out f) 
                       ? (float?) f 
                       : null;
        }
        public static bool ToBool(string s)
        {
            s = s.Trim().ToUpper();
            return s == "TRUE";
        }
        public static string ToNullableString(string s)
        {
            s = s.Trim();
            return s == "NULL" ? null : s;
        }
        public static int? ToNullableInt(string s)
        {
            int i;
            return Int32.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out i)
                       ? (int?)i
                       : null;
        }

        public static float ToFloat(string s)
        {
            double f;
            return (float) (Double.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out f)
                                ? f
                                : 0);
        }
    }
}
