﻿using Mrtn.Marvin21.DbModel.Support;

namespace Mrtn.Marvin21.Dao.DataFactories
{
    class SettingsFactory
    {
        public static void FillSettings(Marvin20Db db)
        {
            db.Settings.Add(new Setting
            {
                Code = "Days",
                Name = "Št. dni",
                Value = "2",
                Description = "Koliko dni traja tekmovanje. Za ROT se vpiše 2, za enodnevna tekmovanja 1."
            });
            db.Settings.Add(new Setting
            {
                Code = "MapDpi",
                Name = "Ločljivost zemljevidov",
                Value = "300",
                Unit = "dpi",
                Description = "Kolikšna je ločljivost temljevida (v točkah na palec). Običajno je 300 dpi.<br />" +
                    "Če ta podatek ni vnešen pravilno, program narobe računa razdalje!"
            });
            db.Settings.Add(new Setting
            {
                Code = "MapScale",
                Name = "Merilo zemljevidov",
                Value = "25000",
                Unit = "1:N",
                Description = "Merilo zemljevida. Običajno je 1:25000. V tem primeru vnesi 25000. <br />"+
                    "Če ta podatek ni vnešen pravilno, program narobe računa razdalje!"
            });
        }
    }
}
