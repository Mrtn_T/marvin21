﻿
using System.Linq;
using Mrtn.Marvin21.Dao.DataFactories.Support;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.DbModel.Tracks.Support;

namespace Mrtn.Marvin21.Dao.DataFactories
{
    /// <summary>
    /// Ta razred vsebuje metode za inicialno polnjenje podatkovne baze, 
    /// bodisi po avtomatskem rekreranju ali pa za kakšne teste
    /// </summary>
    static class TrackFactory
    {
        public static void Fill(Marvin20Db db)
        {
            //FillStations(db);
            //db.SaveChanges();
            //FillPaths(db);
            //db.SaveChanges();
            //FillPathPoints(db);
            //db.SaveChanges();
            //FillTracks(db);
            //db.SaveChanges();
            //FillTrackCheckpoints(db);
            //db.SaveChanges();

        }
        //#region Stations
        //private static void FillStations(Marvin20Db db)
        //{

        //    db.Stations.Add(CreateStationFromString("1	Start	NULL	NULL	True	False	131,2549	170,769623	455	False	True	NULL	1	False"));
        //    db.Stations.Add(CreateStationFromString("2	Cilj	NULL	NULL	False	True	106,801758	170,196426	455	False	True	a	1	False"));
        //    db.Stations.Add(CreateStationFromString("3	A	101	102	False	False	782,627747	568,7778	705	False	True	NULL	1	False"));
        //    db.Stations.Add(CreateStationFromString("4	B	103	NULL	False	False	801,738953	1161,48889	880	False	False	NULL	1	False"));
        //    db.Stations.Add(CreateStationFromString("5	C	104	105	False	False	1286,11108	1173,76111	645	False	True	NULL	1	False"));
        //    db.Stations.Add(CreateStationFromString("6	D	NULL	NULL	False	False	1740,28882	737,8389	620	False	True	NULL	1	False"));
        //    db.Stations.Add(CreateStationFromString("7	E	NULL	NULL	False	False	2075,91016	491,40744	630	False	False	NULL	1	False"));
        //    db.Stations.Add(CreateStationFromString("8	F	NULL	NULL	False	False	1128,83887	493,2778	708	False	False	NULL	1	False"));
        //    db.Stations.Add(CreateStationFromString("9	Start	NULL	NULL	True	False	507,543427	496,425781	300	True	False	NULL	2	True"));
        //    db.Stations.Add(CreateStationFromString("10	Cilj	NULL	NULL	False	True	2117,18115	530,262	300	True	False	NULL	2	True"));
        //    db.Stations.Add(CreateStationFromString("11	A	NULL	NULL	False	False	1276,10913	1235,989	300	True	True	NULL	2	True"));
        //    db.Stations.Add(CreateStationFromString("12	B	NULL	NULL	False	False	746,8139	977,3836	300	True	False	NULL	2	True"));
        //    db.Stations.Add(CreateStationFromString("13	C	NULL	NULL	False	False	1699,49451	969,1445	450	False	False	NULL	2	False"));
        //    db.Stations.Add(CreateStationFromString("14	D	NULL	NULL	False	False	1831,12769	956,9722	300	True	False	NULL	2	True"));
        //    db.Stations.Add(CreateStationFromString("15	E	NULL	NULL	False	False	1603,77234	1026,70007	300	True	False	NULL	2	False"));

        //}
        //private static Station CreateStationFromString(string s)
        //{
        //    var ss = s.Split('\t');
        //    return new Station
        //               {
        //                   Name = ss[1],
        //                   SiCodeArrival = Converter.ToNullableString(ss[2]),
        //                   SiCodeDeparture = Converter.ToNullableString(ss[3]),
        //                   IsStart = Converter.ToBool(ss[4]),
        //                   IsFinish = Converter.ToBool(ss[5]),
        //                   MapPosition = new MapPosition
        //                        {
        //                            X = Converter.ToNullableFloat(ss[6]),
        //                            Y = Converter.ToNullableFloat(ss[7]),
        //                            Z = Converter.ToNullableFloat(ss[8]),
        //                            ZisDirty = Converter.ToBool(ss[9]),
        //                        },
        //                   IsAlive = Converter.ToBool(ss[10]),
        //                   Description = ss[11],
        //                   Day = Converter.ToInt(ss[12]),
        //                   Locked = Converter.ToBool(ss[13]),
        //               };
        //}
        //#endregion

        //#region Paths
        //private static void FillPaths(Marvin20Db db)
        //{
        //    db.Paths.Add(CreatePathFromString("1	False	NULL	NULL	0	0	False	False		1759	0	250	False	False	True	3	1", db));
        //    db.Paths.Add(CreatePathFromString("2	False	NULL	NULL	0	0	False	False		1511	0	175	False	False	False	4	3", db));
        //    db.Paths.Add(CreatePathFromString("3	False	NULL	NULL	0	0	False	False	730	1714	235	0	False	False	False	5	4", db));
        //    db.Paths.Add(CreatePathFromString("4	False	NULL	NULL	0	0	False	False	460	2077	245	160	False	False	False	6	3", db));
        //    db.Paths.Add(CreatePathFromString("5	False	NULL	NULL	0	0	False	False	450	2278	259	180	False	False	False	7	8", db));
        //    db.Paths.Add(CreatePathFromString("6	False	NULL	NULL	0	0	False	False	675, 700	858	30	34	False	False	False	8	3", db));
        //    db.Paths.Add(CreatePathFromString("7	False		NULL	0	0	False	False		1839	250	0	False	False	False	1	3", db));
        //    db.Paths.Add(CreatePathFromString("8	False		NULL	0	0	False	False		1512	175	0	False	False	True	3	4", db));
        //    db.Paths.Add(CreatePathFromString("9	False		NULL	0	0	False	False	730	1709	0	235	False	False	False	4	5", db));
        //    db.Paths.Add(CreatePathFromString("10	False	NULL	NULL	0	0	False	False		1332	0	25	False	False	False	5	6", db));
        //    db.Paths.Add(CreatePathFromString("11	False	NULL	NULL	0	0	False	False		1393	89	0	False	False	False	6	8", db));
        //    db.Paths.Add(CreatePathFromString("12	False	NULL	NULL	0	0	False	False		2268	0	254	False	False	False	8	2", db));
        //    db.Paths.Add(CreatePathFromString("13	False	NULL	NULL	0	0	False	False		2219	254	0	False	False	False	1	8", db));
        //    db.Paths.Add(CreatePathFromString("14	False	NULL	NULL	0	0	False	False		1478	0	64	False	False	False	8	5", db));
        //    db.Paths.Add(CreatePathFromString("15	False	NULL	NULL	0	0	False	False		1660	0	250	False	False	False	3	2", db));
        //    db.Paths.Add(CreatePathFromString("16	False	NULL	NULL	0	0	False	False		1665	60	0	False	False	False	5	3", db));
        //    db.Paths.Add(CreatePathFromString("17	False	NULL	NULL	0	0	False	False		2562	0	425	False	False	False	4	2", db));
        //    db.Paths.Add(CreatePathFromString("18	False		NULL	0	0	False	False	450, 715	2324	265	344	False	False	False	8	7", db));
        //    db.Paths.Add(CreatePathFromString("19	False	NULL	NULL	0	0	False	False		881	0	10	False	False	False	7	6", db));
        //    db.Paths.Add(CreatePathFromString("20	False		NULL	0	0	False	False		1478	64	0	False	False	False	5	8", db));
        //}
        //private static Path CreatePathFromString(string s, Marvin20Db db)
        //{
        //    var ss = s.Split('\t');
        //    var id15 = Converter.ToInt(ss[15]);
        //    var id16 = Converter.ToInt(ss[16]);
        //    return new Path
        //    {
        //        IsManual = Converter.ToBool(ss[1]),
        //        ManualData = new PathData
        //                        {
        //                            Zlist = Converter.ToNullableString(ss[2]),
        //                            Distance = Converter.ToNullableInt(ss[3]),
        //                            Uphill = Converter.ToInt(ss[4]),
        //                            Downhill = Converter.ToInt(ss[5]),
        //                            ZisDirty = Converter.ToBool(ss[6]),
        //                            ManualUpDown = Converter.ToBool(ss[7])
        //                        },
        //        AutomaticData = new PathData
        //                        {
        //                            Zlist = Converter.ToNullableString(ss[8]),
        //                            Distance = Converter.ToNullableInt(ss[9]),
        //                            Uphill = Converter.ToInt(ss[10]),
        //                            Downhill = Converter.ToInt(ss[11]),
        //                            ZisDirty = Converter.ToBool(ss[12]),
        //                            ManualUpDown = Converter.ToBool(ss[13])
        //                        },
        //        Locked = Converter.ToBool(ss[14]),
        //        FirstStation = db.Stations.Where(x => x.Id == id15).Single(),
        //        SecondStation = db.Stations.Where(x => x.Id == id16).Single(),
        //    };
        //}

        //#endregion

        //#region PathPoints
        //private static void FillPathPoints(Marvin20Db db)
        //{
        //    db.PathPoints.Add(CreatePathPointFromString("1	1	454,823944	287,506	NULL	False	False	1", db));
        //    db.PathPoints.Add(CreatePathPointFromString("2	2	121,794342	145,97496	NULL	False	False	1", db));
        //    db.PathPoints.Add(CreatePathPointFromString("3	3	125,333336	160,333328	NULL	False	False	1", db));
        //    db.PathPoints.Add(CreatePathPointFromString("4	1	356,37854	288,157959	NULL	False	False	1", db));
        //    db.PathPoints.Add(CreatePathPointFromString("5	8	790,3097	870,6506	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("6	10	747,3906	779,8399	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("7	9	780,1799	833,7644	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("8	11	762,5303	626,4457	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("9	12	802,4994	608,5575	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("10	7	819,949	874,5802	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("11	6	829,6667	903	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("12	1	748,2777	1133,93762	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("13	3	807,522156	1030,65137	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("14	2	774,7588	1074,29541	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("15	5	815,3333	966	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("16	4	815,6634	997,266052	NULL	False	False	2", db));
        //    db.PathPoints.Add(CreatePathPointFromString("17	6	1055,16663	1304,3	NULL	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("18	5	1085,83337	1251,8667	NULL	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("19	4	1229,50562	1217,42773	NULL	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("20	3	1239,30554	1183,42224	NULL	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("21	2	1264,37219	1181,26111	NULL	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("22	1	1269,90552	1166,84448	NULL	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("23	8	936,0667	1349,72229	NULL	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("24	12	812,7366	1174,58179	NULL	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("25	11	786,6222	1315,21118	NULL	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("26	10	827,4912	1399,17993	NULL	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("27	9	899,0111	1374,8833	730	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("28	7	985,676	1308,3031	NULL	False	False	3", db));
        //    db.PathPoints.Add(CreatePathPointFromString("29	1	1444	747,6667	460	False	False	4", db));
        //    db.PathPoints.Add(CreatePathPointFromString("30	8	1493,85852	527,125	NULL	False	False	5", db));
        //    db.PathPoints.Add(CreatePathPointFromString("31	11	1302,68311	507,394379	NULL	False	False	5", db));
        //    db.PathPoints.Add(CreatePathPointFromString("32	10	1365,7157	501,729675	NULL	False	False	5", db));
        //    db.PathPoints.Add(CreatePathPointFromString("33	9	1485,33337	549,3333	450	False	False	5", db));
        //    db.PathPoints.Add(CreatePathPointFromString("34	4	1632,7041	646,1116	NULL	False	False	5", db));
        //    db.PathPoints.Add(CreatePathPointFromString("35	6	1550,3927	601,09436	NULL	False	False	5", db));
        //    db.PathPoints.Add(CreatePathPointFromString("36	7	1516,303	571,062	NULL	False	False	5", db));
        //    db.PathPoints.Add(CreatePathPointFromString("37	5	1588,81	632,328369	NULL	False	False	5", db));
        //    db.PathPoints.Add(CreatePathPointFromString("38	3	1689,76965	661,341553	NULL	False	False	5", db));
        //    db.PathPoints.Add(CreatePathPointFromString("39	1	1888,198	626,6511	NULL	False	False	5", db));
        //    db.PathPoints.Add(CreatePathPointFromString("40	2	1788,47388	664,324646	NULL	False	False	5", db));
        //    db.PathPoints.Add(CreatePathPointFromString("41	9	797,06665	574,3611	NULL	False	False	6", db));
        //    db.PathPoints.Add(CreatePathPointFromString("42	6	928,2778	517,822266	700	False	False	6", db));
        //    db.PathPoints.Add(CreatePathPointFromString("43	8	847,838867	551,427734	NULL	False	False	6", db));
        //    db.PathPoints.Add(CreatePathPointFromString("44	7	888,3944	532,96106	NULL	False	False	6", db));
        //    db.PathPoints.Add(CreatePathPointFromString("45	4	1006,88885	466,549957	NULL	False	False	6", db));
        //    db.PathPoints.Add(CreatePathPointFromString("46	5	972,9167	508,5222	NULL	False	False	6", db));
        //    db.PathPoints.Add(CreatePathPointFromString("47	3	1039,52771	480,916626	NULL	False	False	6", db));
        //    db.PathPoints.Add(CreatePathPointFromString("48	1	1097,17773	456,43335	675	False	False	6", db));
        //    db.PathPoints.Add(CreatePathPointFromString("49	2	1069,01672	473,344452	NULL	False	False	6", db));
        //    db.PathPoints.Add(CreatePathPointFromString("50	1	120,632683	160,833023	NULL	False	False	7", db));
        //    db.PathPoints.Add(CreatePathPointFromString("51	2	121,794342	145,97496	NULL	False	False	7", db));
        //    db.PathPoints.Add(CreatePathPointFromString("52	3	344	278	NULL	False	False	7", db));
        //    db.PathPoints.Add(CreatePathPointFromString("53	4	579,454346	289,160248	NULL	False	False	7", db));
        //    db.PathPoints.Add(CreatePathPointFromString("54	1	802,4994	608,5575	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("55	2	762,5303	626,4457	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("56	3	747,3906	779,8399	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("57	4	780,1799	833,7644	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("58	5	790,3097	870,6506	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("59	6	819,949	874,5802	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("60	7	829,8394	901,05	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("61	8	813,3342	965,7768	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("62	9	815,6634	997,266052	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("63	10	807,522156	1030,65137	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("64	11	774,7588	1074,29541	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("65	12	748,2777	1133,93762	NULL	False	False	8", db));
        //    db.PathPoints.Add(CreatePathPointFromString("66	1	812,7366	1174,58179	NULL	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("67	2	786,6222	1315,21118	NULL	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("68	3	827,4912	1399,17993	NULL	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("69	4	899,0111	1374,8833	730	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("70	5	936,0667	1349,72229	NULL	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("71	6	990,455566	1311,34448	NULL	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("72	7	1055,16663	1304,3	NULL	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("73	8	1085,83337	1251,8667	NULL	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("74	9	1229,50562	1217,42773	NULL	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("75	10	1239,30554	1183,42224	NULL	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("76	11	1264,37219	1181,26111	NULL	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("77	12	1269,90552	1166,84448	NULL	False	False	9", db));
        //    db.PathPoints.Add(CreatePathPointFromString("78	1	1302,68311	507,394379	NULL	False	False	18", db));
        //    db.PathPoints.Add(CreatePathPointFromString("79	2	1365,7157	501,729675	NULL	False	False	18", db));
        //    db.PathPoints.Add(CreatePathPointFromString("80	3	1485,13586	546,1624	450	False	False	18", db));
        //    db.PathPoints.Add(CreatePathPointFromString("81	4	1493,85852	527,125	NULL	False	False	18", db));
        //    db.PathPoints.Add(CreatePathPointFromString("82	5	1516,303	571,062	NULL	False	False	18", db));
        //    db.PathPoints.Add(CreatePathPointFromString("83	6	1550,3927	601,09436	NULL	False	False	18", db));
        //    db.PathPoints.Add(CreatePathPointFromString("84	7	1588,81	632,328369	NULL	False	False	18", db));
        //    db.PathPoints.Add(CreatePathPointFromString("85	8	1632,7041	646,1116	NULL	False	False	18", db));
        //    db.PathPoints.Add(CreatePathPointFromString("86	9	1689,76965	661,341553	NULL	False	False	18", db));
        //    db.PathPoints.Add(CreatePathPointFromString("87	10	1788,47388	664,324646	NULL	False	False	18", db));
        //    db.PathPoints.Add(CreatePathPointFromString("88	11	1888,198	626,6511	NULL	False	False	18", db));
        //    db.PathPoints.Add(CreatePathPointFromString("89	12	2038,46423	585,7203	715	False	False	18", db));
        //}
        //private static PathPoint CreatePathPointFromString(string s, Marvin20Db db)
        //{
        //    var ss = s.Split('\t');
        //    var id7 = Converter.ToInt(ss[7]);
        //    return new PathPoint
        //    {
        //        No = Converter.ToInt(ss[1]),
        //        MapPosition = new MapPosition
        //                        {
        //                            X = Converter.ToNullableFloat(ss[2]),
        //                            Y = Converter.ToNullableFloat(ss[3]),
        //                            Z = Converter.ToNullableFloat(ss[4]),
        //                            ZisDirty = Converter.ToBool(ss[5]),
        //                        },
        //        Locked = Converter.ToBool(ss[6]),
        //        Path = db.Paths.Where(x => x.Id == id7).Single(),
        //    };
        //}

        //#endregion

        //#region Tracks
        //private static void FillTracks(Marvin20Db db)
        //{
        //    db.Tracks.Add(CreateTrackFromString("1	1	8806	489	489	14379	14379	False	1", db));
        //    db.Tracks.Add(CreateTrackFromString("2	1	8582	489	489	15954	15954	False	2", db));
        //    db.Tracks.Add(CreateTrackFromString("3	1	9161	764	764	14553	14553	False	3", db));
        //}
        //private static Track CreateTrackFromString(string s, Marvin20Db db)
        //{
        //    var ss = s.Split('\t');
        //    var id8 = Converter.ToInt(ss[8]);
        //    return new Track()
        //               {
        //                   Day = Converter.ToInt(ss[1]),
        //                   CumulativeDistance = Converter.ToNullableFloat(ss[2]),
        //                   CumulativeUphill = Converter.ToNullableFloat(ss[3]),
        //                   CumulativeDownhill = Converter.ToNullableFloat(ss[4]),
        //                   OptimalTime = Converter.ToNullableInt(ss[5]),
        //                   OptimalPathTime = Converter.ToNullableInt(ss[6]),
        //                   Locked = Converter.ToBool(ss[7]),
        //                   Category = db.Categories.Where(x => x.Id == id8).FirstOrDefault()
        //               };
        //}
        //#endregion

        //#region TrackCheckpoints
        //private static void FillTrackCheckpoints(Marvin20Db db)
        //{
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("1	1	Start	0	NULL	0	1800	0	0	0	0	0	0	NULL	NULL	0	0	False	False	False	False	False	1	1	NULL", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("2	2	KT1	100	NULL	0	8776	3721	3721	3721	1471	2250	0		1839	250	0	False	False	False	False	False	1	3	7", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("3	3	KT2	100	NULL	0	13995	6505	6505	2784	1209	1575	0		1512	175	0	False	False	True	False	False	1	4	8", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("4	4	KT3	100	NULL	0	19202	9283	9283	2777	1367	0	1410	730	1709	0	235	False	False	False	True	False	1	5	9", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("6	5	KT4	100	NULL	0	22498	11041	11041	1758	1182	576	0		1478	64	0	False	False	False	False	False	1	8	20", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("7	6	Cilj	0	NULL	0	28756	14379	14379	3338	1814	0	1524		2268	0	254	False	False	False	False	False	1	2	12", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("8	1	Start	0	NULL	0	1800	0	0	0	0	0	0	NULL	NULL	0	0	False	False	False	False	False	2	1	NULL", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("9	2	KT1	100	NULL	0	10498	4609	4609	4609	1997	2612	0		2219	254	0	False	False	False	False	False	2	8	13", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("10	3	KT2	100	NULL	0	13798	6358	6358	1749	1330	0	418		1478	0	64	False	False	False	False	False	2	5	14", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("11	4	KT3	100	NULL	0	21270	10318	10318	3959	1542	2417	0	730	1714	235	0	False	False	False	False	False	2	4	3", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("12	5	KT4	100	NULL	0	25997	12823	12823	2505	1359	0	1145		1511	0	175	False	False	False	False	False	2	3	2", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("13	6	Cilj	0	NULL	0	31904	15954	15954	3130	1494	0	1636		1660	0	250	False	False	False	False	False	2	2	15", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("14	1	Start	0	NULL	0	1800	0	0	0	0	0	0	NULL	NULL	0	0	False	False	False	False	False	3	1	NULL", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("15	2	KT1	100	NULL	0	7956	3281	3281	3281	1452	1828	0		2219	254	0	False	False	False	False	False	3	8	13", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("16	3	KT2	100	NULL	0	16971	8086	8086	4805	1521	1908	1376	450, 715	2324	265	344	False	False	False	False	False	3	7	18", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("17	4	KT3	100	NULL	0	18127	8703	8703	616	576	0	40		881	0	10	False	False	False	False	False	3	6	19", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("18	5	KT4	100	NULL	0	25188	12466	12466	3763	1359	1764	640	460	2077	245	160	False	False	False	False	False	3	3	4", db));
        //    db.TrackCheckpoints.Add(CreateTrackCheckpointFromString("19	6	Cilj	0	NULL	0	29102	14553	14553	2086	1086	0	1000		1660	0	250	False	False	False	False	False	3	2	15", db));
        //}
        //private static TrackCheckpoint CreateTrackCheckpointFromString(string s, Marvin20Db db)
        //{
        //    var ss = s.Split('\t');
        //    var id22 = Converter.ToInt(ss[22]);
        //    var id23 = Converter.ToInt(ss[23]);
        //    var id24 = Converter.ToInt(ss[24]);
        //    return new TrackCheckpoint()
        //    {
        //        No = Converter.ToInt(ss[1]),
        //        Name = ss[2],
        //        Points = Converter.ToInt(ss[3]),
        //        Description = ss[4],
        //        Time = new Time
        //                  {
        //                      ForTask = Converter.ToInt(ss[5]),
        //                      MaxFromStart = Converter.ToInt(ss[6]),
        //                      OptimalArrival = Converter.ToInt(ss[7]),
        //                      OptimalDeparture = Converter.ToInt(ss[8]),
        //                      FromPrevious = Converter.ToFloat(ss[9]),
        //                      FromPreviousForDistance = Converter.ToFloat(ss[10]),
        //                      FromPreviousForUphill = Converter.ToFloat(ss[11]),
        //                      FromPreviousForDownhill = Converter.ToFloat(ss[12])
        //                  },
        //        FromPrevious = new PathData
        //                         {
        //                             Zlist = ss[13],
        //                             Distance = Converter.ToNullableInt(ss[14]),
        //                             Uphill = Converter.ToInt(ss[15]),
        //                             Downhill = Converter.ToInt(ss[16]),
        //                             ZisDirty = Converter.ToBool(ss[17]),
        //                             ManualUpDown = Converter.ToBool(ss[18]),
        //                         },
        //        SpeedTrial = new SpeedTrial
        //                        {
        //                            IsStart = Converter.ToBool(ss[19]),
        //                            IsFinish = Converter.ToBool(ss[20]),
        //                        },
        //        Locked = Converter.ToBool(ss[21]),
        //        Track = db.Tracks.Where(x => x.Id == id22).FirstOrDefault(),
        //        Station = db.Stations.Where(x => x.Id == id23).FirstOrDefault(),
        //        PathFromPrevious = db.Paths.Where(x => x.Id == id24).FirstOrDefault()

        //    };
        //}
        //#endregion

    }
}
