﻿using System.Linq;
using Mrtn.Marvin21.DbModel.Teams;

namespace Mrtn.Marvin21.Dao.DataFactories
{
    class TeamsFactory
    {
        /// <summary>
        /// Napolni osnovnih 6 kategorij za Glas svobodne Jelovice
        /// </summary>
        /// <param name="db"></param>
        public static void FillCategories(Marvin20Db db)
        {
            var ggmCat = new Category
            {
                Abbr = "GGM",
                Active = true,
                Name = "Gozdovniki",
                No = 1,
                Color = "00cc00",
                DownhillSpeed = 600,
                Speed = (float)4.5,
                UphillSpeed = 400
            };
            var ggzCat = new Category
            {
                Abbr = "GGŽ",
                Active = true,
                Name = "Gozdovnice",
                No = 2,
                Color = "33ff00",
                DownhillSpeed = 550,
                Speed = 4,
                UphillSpeed = 350
            }; var ppmCat = new Category
            {
                Abbr = "PPM",
                Active = true,
                Name = "Popotniki",
                No = 3,
                Color = "0000cc",
                DownhillSpeed = 900,
                Speed = (float)5.5,
                UphillSpeed = 500
            }; var ppzCat = new Category
            {
                Abbr = "PPŽ",
                Active = true,
                Name = "Popotnice",
                No = 4,
                Color = "3333ff",
                DownhillSpeed = 700,
                Speed = 5,
                UphillSpeed = 450
            }; var grceCat = new Category
            {
                Abbr = "GRČ",
                Active = true,
                Name = "Grče",
                No = 5,
                Color = "cc00cc",
                DownhillSpeed = 1000,
                Speed = 6,
                UphillSpeed = 550
            };
            var overFourtyCat = new Category
            {
                Abbr = "40+",
                Active = true,
                Name = "40plus",
                No = 6,
                Color = "996633",
                DownhillSpeed = 800,
                Speed = (float)5.5,
                UphillSpeed = 500
            };
            db.Categories.Add(ggmCat);
            db.Categories.Add(ggzCat);
            db.Categories.Add(ppmCat);
            db.Categories.Add(ppzCat);
            db.Categories.Add(grceCat);
            db.Categories.Add(overFourtyCat);
        }

        /// <summary>
        /// Napolni nekaj testnih rodov
        /// </summary>
        /// <param name="db"></param>
        public static void FillPacks(Marvin20Db db)
        {
            var rskPack = new Pack
                              {
                                  Abbr = "RSK",
                                  Active = true,
                                  InCompetition = false,
                                  Name = "Rod svobodnega Kamnitnika",
                                  Town = "Škofja Loka"
                              };
            var rsoPack = new Pack
                              {
                                  Abbr = "RSO",
                                  Active = true,
                                  InCompetition = true,
                                  Name = "Rod stražnih ognjev",
                                  Town = "Kranj"
                              };

            var rstPack = new Pack
                              {
                                  Abbr = "RST",
                                  Active = true,
                                  InCompetition = true,
                                  Name = "Rod skalnih taborov",
                                  Town = "Domžale"
                              };

            var rzzPack = new Pack
                              {
                                  Abbr = "RZŽ",
                                  Active = true,
                                  InCompetition = true,
                                  Name = "Rod zelenega Žirka",
                                  Town = "Žiri"
                              };

            var rzsPack = new Pack
                              {
                                  Abbr = "RZS",
                                  Active = true,
                                  InCompetition = true,
                                  Name = "Rod zelene sreče",
                                  Town = "Železniki"
                              };

            db.Packs.Add(rskPack);
            db.Packs.Add(rsoPack);
            db.Packs.Add(rstPack);
            db.Packs.Add(rzzPack);
            db.Packs.Add(rzsPack);
        }

        /// <summary>
        /// Doda par testnih ekip
        /// </summary>
        /// <param name="db"></param>
        public static void FillTeams(Marvin20Db db)
        {
            var ggmCat = db.Categories.Where(x => x.Abbr == "GGM").Single();
            var rskPack = db.Packs.Where(x => x.Abbr == "RSK").Single();
            var ggzCat = db.Categories.Where(x => x.Abbr == "GGŽ").Single();
            var rstPack = db.Packs.Where(x => x.Abbr == "RST").Single();
            var ppmCat = db.Categories.Where(x => x.Abbr == "PPM").Single();
            var rsoPack = db.Packs.Where(x => x.Abbr == "RSO").Single();

            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ggmCat,
                                 Pack = rskPack,
                                 Name = "Luzerji",
                                 InCompetition = false
                             });
            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ggzCat,
                                 Pack = rstPack,
                                 Name = "Winnerji",
                                 InCompetition = true,
                                 SiCode = "123456"
                             });
            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ppmCat,
                                 Pack = rskPack,
                                 Name = "flekci",
                                 InCompetition = false
                             });
            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ggzCat,
                                 Pack = rsoPack,
                                 Name = "bombice",
                                 InCompetition = true
                             });
            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ppmCat,
                                 Pack = rskPack,
                                 Name = "tralala",
                                 InCompetition = false
                             });
            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ggzCat,
                                 Pack = rsoPack,
                                 Name = "Frajle",
                                 InCompetition = true
                             });
            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ggmCat,
                                 Pack = rstPack,
                                 Name = "Lopovi",
                                 InCompetition = false,
                                 SiCode = "232462"
                             });
            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ppmCat,
                                 Pack = rsoPack,
                                 Name = "Winnerji",
                                 InCompetition = true,
                                 SiCode = "235877"
                             });
            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ggmCat,
                                 Pack = rskPack,
                                 Name = "Luzerji",
                                 InCompetition = false,
                                 SiCode = "235895"
                             });
            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ggzCat,
                                 Pack = rstPack,
                                 Name = "Winnerji",
                                 InCompetition = true,
                                 SiCode = "235898"
                             });
            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ggmCat,
                                 Pack = rskPack,
                                 Name = "Luzerji",
                                 InCompetition = false,
                                 SiCode = "422529"
                             });
            db.Teams.Add(new Team
                             {
                                 Active = true,
                                 Category = ggzCat,
                                 Pack = rstPack,
                                 Name = "Winnerji",
                                 InCompetition = true,
                                 SiCode = "422529"
                             });

        }
    }
}
