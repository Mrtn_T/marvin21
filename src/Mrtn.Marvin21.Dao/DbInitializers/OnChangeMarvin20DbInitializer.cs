﻿using System.Data.Entity;
using Mrtn.Marvin21.Dao.DataFactories;

namespace Mrtn.Marvin21.Dao.DbInitializers
{
    public class OnChangeMarvin20DbInitializer :
        DropCreateDatabaseIfModelChanges<Marvin20Db>
    {
        protected override void Seed(Marvin20Db db)
        {
            base.Seed(db);
            TeamsFactory.FillCategories(db);
            TeamsFactory.FillPacks(db);
            db.SaveChanges();
            TeamsFactory.FillTeams(db);
            SiChipsFactory.FillSiChips(db);
            TrackFactory.Fill(db);
            SettingsFactory.FillSettings(db);
            db.SaveChanges();

       
        }
    }
}