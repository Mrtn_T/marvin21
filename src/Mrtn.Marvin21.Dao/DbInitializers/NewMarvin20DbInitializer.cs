﻿using System.Data.Entity;
using Mrtn.Marvin21.Dao.DataFactories;

namespace Mrtn.Marvin21.Dao.DbInitializers
{
    public class NewMarvin20DbInitializer :
        DropCreateDatabaseAlways<Marvin20Db>
    {
        protected override void Seed(Marvin20Db db)
        {
            base.Seed(db);
            TeamsFactory.FillCategories(db);
            TeamsFactory.FillPacks(db);
            SettingsFactory.FillSettings(db);
            db.SaveChanges();
            TeamsFactory.FillTeams(db);
            SiChipsFactory.FillSiChips(db);
            db.SaveChanges();

            TrackFactory.Fill(db);
            db.SaveChanges();


        }
    }
}