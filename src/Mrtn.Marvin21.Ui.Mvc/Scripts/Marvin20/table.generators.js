﻿function TableGenerator() {

}




//
// Ta funkcija na novo naredi tabelo točk
//
TableGenerator.UpdateStationsTable = function (data) {
    $("#stationsDiv").html("");
    //prepare table
    var table = $("<table id='stationsTable' width='100%'></table>")
        .append("<tr><th colspan='2'>Ime</th><th>Kode</th><th>Z</th><th>Točke</th><th colspan='2'></th ></tr>");
    for (var i in data) {
        station = data[i];


        //prepare row
        var tr = $("<tr id='stationsRow" + station.Id + "' data-id='" + station.Id + "' data-locked='" + station.Locked + "'></tr>");

        //icon
        //prepare class for icon
        var cl = ""
        if (station.IsStart == true) cl = "startButton";
        else if (station.IsFinish == true) cl = "finishButton";
        else if (station.IsAlive == true) cl = "aliveButton";
        else cl = "ktButton";
        var a = $("<a href='#' data-id='" + station.Id + "' class='" + cl + " selectStationButton'></a>")
        tr.append(
            $("<td style='padding:3px 2px 0px 0px;'></td>").append(a)
        );

        //name
        tr.append($("<td>" + station.Name + "</td>"));

        //codes
        tr.append("<td>" + (station.SiCodeArrival || "") + " - " + (station.SiCodeDeparture || "") + "</td>")


        //Z
        var cssClass = ""
        if (station.ZisDirty && station.ZisDirty == true) cssClass = " class='error' ";
        tr.append("<td " + cssClass + ">" + (station.Z || "") + " m</td>")
        //points
        td = $("<td></td>").append(station.DefaultPoints);
        tr.append(td);




        // links
        td = $("<td></td>");
        td.append($("<a href='" + UrlObject.EditStation + "/" + station.Id + "' class='linkAsButton ajaxDialog'>Uredi</a>")
                .attr("data-popup-name", "Urejanje točke '<i>" + station.Name + "</i>'")
            );
        tr.append(td);

        // icons
        if (station.Locked == true) {
            a = $("<a href='" + UrlObject.UnlockStation + "/" + station.Id + "'></a>")
                .addClass("ajaxLink lockButton");
        } else {
            a = $("<a href='" + UrlObject.LockStation + "/" + station.Id + "'></a>")
                .addClass("ajaxLink unlockButton");
        }
        tr.append(
            $("<td style='padding:3px 2px 0px 2px;'></td>").append(a)
        );

        table.append(tr);
    }
    $("#stationsDiv").append(table);
}   // end function updateStationsTable


//
// Ta funkcija na novo naredi tabelo poti
// Podan je json spisek poti (Path)
//
TableGenerator.UpdatePathsTable = function (data) {
    //prepare table
    console.debug("obnavljam tabelo poti");
    var table = $("<table id='pathsTable' width='100%' style='white-space:nowrap;'></table>")
            .append("<tr><th colspan='2'>Pot med:</th><th>Kode</th><th>Z</th><th colspan='2'></th ></tr>");
    for (var i in data) {
        path = data[i];

        //prepare row
        var tr = $("<tr id='pathRow" + path.Id + "' data-id='" + path.Id + "' data-locked='" + path.Locked + "'></tr>");

        //ikona
        var a = $("<a href='#' data-id='" + path.Id + "' class='selectPathButton pathButton'></a>");
        tr.append(
                $("<td style='padding:2px 2px 2px 2px;'></td>").append(a)
            );

        //name
        tr.append($("<td>" + path.FirstStationName + " &rarr; " + path.SecondStationName + "</td>"));

        // razdalja man, auto
        if (path.IsManual == true)
            tr.append($("<td>(man)</td>"));
        else
            tr.append($("<td>(auto)</td>"));

        // razdalja
        if (!path.ValidDataDistance || path.ValidDataDistance == "" || path.ValidDataDistance == 0)
            tr.append($("<td> - </td>"));
        else
            tr.append($("<td>" + path.ValidDataDistance + "m</td>"));

        // dvig, spust
        var errorClass = "";
        if (path.ValidDataZisDirty == true) errorClass = " class='error'";
        tr.append($("<td " + errorClass + " >&#8599;" + path.ValidDataUphill + "m &#8600;" + path.ValidDataDownhill + "m</td>"));

        // links
        td = $("<td></td>");
        td.append($("<a href='" + UrlObject.EditPath + "/" +
                    +path.Id + "' class='linkAsButton ajaxDialog'>Uredi</a>")
                    .attr("data-popup-name", "Urejanje poti '<i>" + path.FirstStationName + " &rarr; " + path.SecondStationName + "</i>'")
                    .attr("data-popup-width", "850")
                );
        tr.append(td);
        //gumb za izbiro ali za ustvarjanje OBRATNE poti
        a = $("<a href='#' data-id=" + path.Id + "></a>")
                    .addClass("rotateButton selectInversePath");
        tr.append($("<td style='padding:3px 2px 0px 2px;'></td>").append(a));

        // lock, unlock
        if (path.Locked == true) {
            a = $("<a href='" + UrlObject.UnlockPath + "/" + path.Id + "'></a>")
                    .addClass("ajaxLink lockButton");
        } else {
            a = $("<a href='" + UrlObject.LockPath + "/" + path.Id + "'></a>")
                    .addClass("ajaxLink unlockButton");
        }
        tr.append($("<td style='padding:3px 2px 0px 2px;'></td>").append(a));

        table.append(tr);
    }
    $("#pathsDiv").html("").append(table);
}  // end function updateStationsTable


TableGenerator.UpdatePathPointsTable = function (data) {
    $("#pathPointsDiv").html("");
    if (!data) return;
    //prepare table
    //var tableTr = $("<table id='pathPointsTable' width='100%' style='white-space:nowrap;'><tr></tr></table>")
    var table1 = $("<table id='pathPointsTable1' width='100%' style='white-space:nowrap;'></table>")
        .append("<tr><th></th><th>Z</th><th colspan='3'></th ></tr>");
    //var table2 = $("<table id='pathPointsTable2' width='100%' style='white-space:nowrap;'></table>")
    //    .append("<tr><th></th><th>Z</th><th colspan='3'></th ></tr>");
    //var table3 = $("<table id='pathPointsTable3' width='100%'></table>").append("<tr><th>No</th><th>Z</th><th colspan='2'></th ></tr>");
    //tableTr.append($("<td style='width: 33%;'></td>").append(table1));
    //tableTr.append($("<td style='width: 33%;'></td>").append(table2));
    //tableTr.append($("<td style='width: 33%;'></td>").append(table3));
    for (var i in data.PathPoints) {
        pathPoint = data.PathPoints[i];

        //prepare row
        var tr = $("<tr id='pathPointRow" + pathPoint.Id + "' data-id='" + pathPoint.Id + "' data-locked='" + pathPoint.Locked + "'></tr>");

        //icon
        //prepare class for icon
        var cl = ""
        if (pathPoint.MapPositionZ) {
            if (pathPoint.Locked == true || data.Locked == true)
                cl = "pathPointZButtonLocked";
            else
                cl = "pathPointZButton";
        }
        else {
            if (pathPoint.Locked == true || data.Locked == true)
                cl = "pathPointButtonLocked";
            else
                cl = "pathPointButton";
        }
        //dodam ikono
        var a = $("<a href='#' data-id='" + pathPoint.Id + "' class='" + cl + " selectPathPointButton'></a>")
        tr.append($("<td style='padding:3px 2px 0px 0px;'></td>").append(a));

        // višina
        var cssClass = ""
        if (pathPoint.MapPositionZisDirty && pathPoint.MapPositionZisDirty == true) cssClass = " class='error' ";
        var td = $("<td " + cssClass + "></td>");
        if (pathPoint.MapPositionZ) td.html((pathPoint.MapPositionZ || "") + " m");
        else td.html("&nbsp;")
        tr.append(td);

        // links
        a = "";
        if (pathPoint.Locked != true && data.Locked != true)
            a = $("<a href='" + UrlObject.EditPathPoint + "/"
                    + pathPoint.Id + "' class='linkAsButton ajaxDialog'>Uredi</a>")
                    .attr("data-popup-name", "Urejanje poti na poti'<i>" + pathPoint.No + "</i>'")
        td = $("<td></td>").append(a);
        tr.append(td);

        // ikone lock/unlock
        if (pathPoint.Locked == true) {
            a = $("<a href='" + UrlObject.UnlockPathPoint + "/" + pathPoint.Id + "'></a>")
                    .addClass("ajaxLink lockButton");
        } else {
            a = $("<a href='" + UrlObject.LockPathPoint + "/" + pathPoint.Id + "'></a>")
                    .addClass("ajaxLink unlockButton");
        }
        tr.append($("<td style='padding:3px 2px 0px 2px;'></td>").append(a));

        // ikona delete
        a = "";
        if (pathPoint.Locked != true && data.Locked != true)
            a = $("<a href='" + UrlObject.DeletePathPoint + "/" + pathPoint.Id + "'></a>")
                    .addClass("ajaxLink deleteButton");
        tr.append($("<td style='padding:3px 2px 0px 2px;'></td>").append(a));


        //if (i % 2 == 0) 
            table1.append(tr);
        //else if (i % 2 == 1) table2.append(tr);
        //else table3.append(tr);
    }
    //        $("#pathPointsDiv").append(tableTr);
    $("#pathPointsDiv").append(table1);
    var a1 = $("<a href='" + UrlObject.UnlockAllPathPoint + "?pathId=" + data.Id + "' class='ajaxLink unlockButton' ></a>");
    var a2 = $("<a href='" + UrlObject.LockAllPathPoint + "?pathId=" + data.Id + "' class='ajaxLink lockButton' ></a>");
    $("#leftColumnPathPointsTopMenu")
        .html("")
        .append(a1, "&nbsp;", a2);

    //        $(".selectPathButton").click(function(){
    //            selectPath($(this).data("id"));
    //        });
}    // end function updateStationsTable

TableGenerator.UpdateTracksTable = function (data) {
    //prepare table
    console.debug("obnavljam tabelo prog");
    var table = $("<table id='tracksTable' width='100%' style='white-space:nowrap;'></table>")
            .append("<tr><th colspan='2'>Proga</th><th>Pot</th><th> </th><th> </th><th colspan='2'></th ></tr>");
    for (var i in data) {
        track = data[i];

        //prepare row
        var tr = $("<tr data-id='" + track.Id + "' data-locked='" + track.Locked + "'></tr>");
        if (track.Id > 0) tr.attr("id", "trackRow" + track.Id);

        //ikona
        var a = $("<a href='#' data-id='" + track.Id + "' class='selectTrackButton pathButton'></a>");
        tr.append(
                $("<td style='padding:2px 2px 2px 2px;'></td>").append(a)
        );

        //name
        tr.append($("<td>" + track.CategoryAbbr + "</td>"));
        tr.append($("<td>" + track.OptimalPathTime + "</td>"));

        // razdalja man, auto
        //                if (track.IsManual == true)
        //                    tr.append($("<td>(man)</td>"));
        //                else
        //                    tr.append($("<td>(auto)</td>"));

        // razdalja
        if ((!track.CumulativeDistance || track.CumulativeDistance === "") && track.CumulativeDistance != 0)
            tr.append($("<td class='error'>&rarr; ? </td>"));
        else
            tr.append($("<td>&rarr; " + track.CumulativeDistance + " m</td>"));

        // dvig, spust
        //                var errorClass = "";
        //                if (track.ValidDataZisDirty == true) errorClass = " class='error'";
        //                tr.append($("<td " + errorClass + " >&#8599;" + track.ValidDataUphill + "m &#8600;" + track.ValidDataDownhill + "m</td>"));
        if ((!track.CumulativeUphill || track.CumulativeUphill === "") && track.CumulativeUphill != 0)
            tr.append($("<td class='error'>&#8599; ? </td>"));
        else
            tr.append($("<td>&#8599; " + track.CumulativeUphill + " m</td>"));

        if ((!track.CumulativeDownhill || track.CumulativeDownhill === "") && track.CumulativeDownhill != 0)
            tr.append($("<td class='error'>&#8600; ? </td>"));
        else
            tr.append($("<td>&#8600; " + track.CumulativeDownhill + " m</td>"));

        // links
        td = $("<td></td>");
        if ((track.Id > 0)) {
            td.append($("<a href='" + UrlObject.EditTrack + "/" +
                    +track.Id + "' class='linkAsButton ajaxDialog'>Uredi</a>")
                    .attr("data-popup-name", "Urejanje proge '<i>" + track.CategoryAbbr + "</i>'")
                    .attr("data-popup-width", "850")
                );
        }
        else {
            td.append($("<a href='" + UrlObject.AddTrack + "?categoryId=" +
                    +track.CategoryId + "' class='linkAsButton ajaxLink'>Dodaj</a>")
                    .attr("data-popup-name", "Dodajanje nove proge '<i>" + track.CategoryAbbr + "</i>'")
                    .attr("data-popup-width", "850")
                );
        }
        tr.append(td);


        // lock, unlock
        if ((track.Id == 0)) {
            a = "";
        } else if (track.Locked == true) {
            a = $("<a href='" + UrlObject.UnlockTrack + "/" + track.Id + "'></a>")
                    .addClass("ajaxLink lockButton");
        } else {
            a = $("<a href='" + UrlObject.LockTrack + "/" + track.Id + "'></a>")
                    .addClass("ajaxLink unlockButton");
        }
        tr.append(
                $("<td style='padding:3px 2px 0px 2px;'></td>").append(a)
            );

        table.append(tr);
    }
    $("#tracksDiv").html("").append(table);
}       // end function updateStationsTable



TableGenerator.UpdateTrackCheckpointsTable = function (data) {
    //prepare table
    if (!data || !data.TrackCheckpoints) {
        $("#trackCheckpointsDiv").html("");
        return;
    }

    var table = $("<table id='trackCheckpointsTable' width='100%' style='white-space:nowrap;'></table>")
            .append("<tr><th >KT</th><th>Točka</th><th> </th><th colspan='2'></th ></tr>");
    for (var i in data.TrackCheckpoints) {
        checkpoint = data.TrackCheckpoints[i];

        //prepare row
        var tr = $("<tr id=" + checkpoint.Id + " data-id='" + checkpoint.Id + "' data-locked='" + checkpoint.Locked + "'></tr>");

        //icon
        //prepare class for icon
        var cl = ""
        if (checkpoint.StationIsStart == true) cl = "startButton";
        else if (checkpoint.StationIsFinish == true) cl = "finishButton";
        else cl = "ktFlagButton";
        var a = $("<a href='#' data-id='" + checkpoint.Id + "' class='" + cl + " selectCheckpointButton'></a>")
        tr.append(
            $("<td style='padding:3px 2px 0px 0px;'></td>").append(a)
        );



        //ikona
        //        var a = $("<a href='#' data-id='" + track.Id + "' class='selectTrackButton pathButton'></a>");
        //        tr.append(
        //                $("<td style='padding:2px 2px 2px 2px;'></td>").append(a)
        //        );

        //name
        tr.append($("<td>" + checkpoint.Name + "</td>"));

        //oznake
        var s = "";
        if (checkpoint.StationIsStart == false && checkpoint.StationIsFinish == false) {
            s = s + checkpoint.StationName;
            if ((checkpoint.StationSiCodeArrival && checkpoint.StationSiCodeArrival != "")
                || (checkpoint.StationSiCodeDeparture && checkpoint.StationSiCodeDeparture != "")) {
                s = s + " (";
                if (checkpoint.StationSiCodeArrival && checkpoint.StationSiCodeArrival != "") {
                    s = s + checkpoint.StationSiCodeArrival;
                    if (checkpoint.StationSiCodeDeparture && checkpoint.StationSiCodeDeparture != "")
                        s = s + ",";
                }
                if (checkpoint.StationSiCodeDeparture && checkpoint.StationSiCodeDeparture != "")
                    s = s + checkpoint.StationSiCodeDeparture;
                s = s + ")";
            }
        }
        tr.append($("<td>" + s + "</td>"));

        // razdalja man, auto
        //                if (track.IsManual == true)
        //                    tr.append($("<td>(man)</td>"));
        //                else
        //                    tr.append($("<td>(auto)</td>"));


        // razdalja
        if (i == 0) { tr.append($("<td colspan='3'> </td>")); }
        else {
            if ((!checkpoint.FromPreviousDistance || checkpoint.FromPreviousDistance === "") && checkpoint.FromPreviousDistance != 0)
                tr.append($("<td class='error'>&rarr; ? </td>"));
            else
                tr.append($("<td>&rarr; " + checkpoint.FromPreviousDistance + " m</td>"));

            if ((!checkpoint.FromPreviousUphill || checkpoint.FromPreviousUphill === "") && checkpoint.FromPreviousUphill != 0)
                tr.append($("<td class='error'>&#8599; ? </td>"));
            else
                tr.append($("<td>&#8599; " + checkpoint.FromPreviousUphill + " m</td>"));

            if ((!checkpoint.FromPreviousDownhill || checkpoint.FromPreviousDownhill === "") && checkpoint.FromPreviousDownhill != 0)
                tr.append($("<td class='error'>&#8600; ? </td>"));
            else
                tr.append($("<td>&#8600; " + checkpoint.FromPreviousDownhill + " m</td>"));
        }
        // links
        //        td = $("<td></td>");
        //        if ((track.Id > 0)) {
        //            td.append($("<a href='" + UrlObject.EditTrack + "/" +
        //                    +track.Id + "' class='linkAsButton ajaxDialog'>Uredi</a>")
        //                    .attr("data-popup-name", "Urejanje proge '<i>" + track.CategoryAbbr + "</i>'")
        //                    .attr("data-popup-width", "850")
        //                );
        //        }
        //        else {
        //            td.append($("<a href='" + UrlObject.AddTrack + "?categoryId=" +
        //                    +track.CategoryId + "' class='linkAsButton ajaxLink'>Dodaj</a>")
        //                    .attr("data-popup-name", "Dodajanje nove proge '<i>" + track.CategoryAbbr + "</i>'")
        //                    .attr("data-popup-width", "850")
        //                );
        //        }
        //        tr.append(td);


        // lock, unlock
        if (checkpoint.Locked == true) {
            a = $("<a href='" + UrlObject.UnlockPath + "/" + checkpoint.Id + "'></a>")
                    .addClass("ajaxLink lockButton");
        } else {
            a = $("<a href='" + UrlObject.LockPath + "/" + checkpoint.Id + "'></a>")
                    .addClass("ajaxLink unlockButton");
        }
        tr.append($("<td></td>").append(a));
        //up button
        a = "";
        if (i > 0)
            a = $("<a href='" + UrlObject.MoveCheckpointUp + "/" + checkpoint.Id + "'></a>")
                    .addClass("ajaxLink moveUpButton");
        tr.append($("<td></td>").append(a));
        //down button
        a = "";
        if (data.TrackCheckpoints.length > ((i * 1) + 1))
            a = $("<a href='" + UrlObject.MoveCheckpointDown + "/" + checkpoint.Id + "'></a>")
                    .addClass("ajaxLink moveDownButton");
        tr.append($("<td></td>").append(a));
        //delete button
        a = $("<a href='" + UrlObject.DeleteCheckpoint + "/" + checkpoint.Id + "'></a>")
                    .addClass("ajaxLink deleteButton");
        tr.append($("<td></td>").append(a));

        table.append(tr);
    }
    $("#trackCheckpointsDiv").html("").append(table);
}                  // end function updateStationsTable

