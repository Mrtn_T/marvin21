﻿//
//  Odpre link na formo v dialogu. Uporaba je naslednja: 
//  Link: 
//      <a href='url do forme' class='razred za pripenjanje dogodka'>link</a>
//  JS:
//      $("a.razred_za_pripenjanje_dogodka").click( {success: <function> }, openLinkDialog)  ali
//      *** $("a.razred_za_pripenjanje_dogodka").live('click', {success: <function> }, openLinkDialog) ali
//      $("#<container>").delegate("a.razred_za_pripenjanje_dogodka", "click", { success: <function> }, openLinkDialog)
//  *** kot kažejo izkušnje, live ne prenese pravilno success funkcije!!!
//
//  dodatno ima link lahko še nekaj parametrov:
//      <a href='url do forme' class='razred za pripenjanje dogodka'
//         data_popup_width = '450'
//         data_popup_height = '600'
//         data_popup_name = 'Paketno dodajanje čipov' >link</a>
//
var openLinkDialog = function (e) {
    var link = $(this);
    var params = {
        url: this.href,
        name: '...',
        width: 750,
        height: 500,
        success: function () { }
    }
    if (e && e.data && e.data.success)
        params.success = e.data.success;
    if (!params.url) return false;
    if (link.data('popup-name')) params.name = link.data('popup-name');
    if (link.data('popup-width')) params.width = link.data('popup-width');
    if (link.data('popup-height')) params.height = link.data('popup-height');

    openDialog(params);
    return false;
}
//
// Odpre dialog iz podanega linka. Doda gumb a Shrani in Prekliči.
// V primeru shranjevanja submita formo na uploadani vsebini.
// Na koncu pokliče podano "success" funkcijo
//
// Primer klica je spodaj.
// 
// openDialog({
//     url: 'url to call',
//     name: 'title of dialog',
//     width: 750,
//     height: 500,
//     success: function to be executed on success
// });
var openDialog = function (params) {
    var dialog = $('<div />');
    // load remote content
    $.get(params.url,
        function (data) {
            dialog.html(data);
            $('form', dialog).submit(function (e) { //Preprečim samodejni submit pri nekaterih formah (ob pritisku ENTER npr)
                return false;
            });
            dialog.dialog({
                height: params.height,
                width: params.width,
                modal: true,
                buttons: {
                    "Shrani": function () {
                        var form = $("form", this);
                        form.validate();
                        if (form.valid()) {
                            form.ajaxSubmit({
                                success: function (data) {
                                    if (data.Ok == true) {
                                        $(dialog).dialog("close");
                                        params.success.call();
                                        if (data.Message && data.Message != "")
                                            alert(data.Message);
                                    }
                                    else alert(data.Message);
                                }
                            });
                        } else console.debug("form is not valid");
                    },
                    "Prekliči": function () {
                        $(this).dialog("close");
                    }
                },
                open: function (event, ui) {
                    //V naloadanih dialogih jQuery document ready ne dela pravilno. Zato se uporabi funkcija  dialogLoaded.
                    //Če le ta obstaja, se jo pokliče pri open.
                    if (typeof dialogLoaded == 'function') {
                        dialogLoaded();
                    }
                },
                close: function (event, ui) {
                    $(this).remove();
                },
                title: params.name//GetNameForDialog()
            });
            //tole je za validacijo
            $("form").removeData("validator");
            $("form").removeData("unobtrusiveValidation");
            $.validator.unobtrusive.parse("form");
        }
    );
}




// Funkcija pokliče URL iz linka. 
// Če dobi napako prkaže alert
// Če obstaja, na koncu pokliče callback funkcijo
var callAjaxLink = function (e) {

    // message?
    var message = $(this).data("question");
    if (!(typeof message == 'undefined' || message === ''))
        if (!confirm(message)) return false;

    console.debug("callAjaxLink called, param=" + e);
    var url = this.href;
    if (!url) return false;

    var callback;
    if (e && e.success && $.isFunction(e.success)) {
        callback = e.success;
        console.debug("callback:" + callback);
    }
    else if (e && e.data && $.isFunction(e.data.success)) {
        callback = e.data.success;
        console.debug("callback:" + callback);
    }
    $.ajax(url)
        .done(function (data) {
            if (data.Message && data.Message != "")
                alert(data.Message);
            if ($.isFunction(callback))
                callback.call(this, data);
        })
    ;
    return false;
}
