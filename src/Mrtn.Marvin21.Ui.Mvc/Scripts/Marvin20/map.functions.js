﻿/**********************************************************************************/
/**********************************************************************************/
/***                                                                            ***/
/***    Objekt za risanje po zemljevidu                                         ***/
/***                                                                            ***/
/**********************************************************************************/
/**********************************************************************************/
MapDraw = function () {
}
//
// prikaže statione na zemljevidu - potrebuje ga metoda MapObject.ReloadStations
//
MapDraw.StationsOnMap = function (viewport) {
    //console.debug("MapDraw.StationsOnMap called");
    viewport = $(viewport);
    var stations = DataObject.Stations;
    //zbrišem stare
    $(".mapObjectsDiv", viewport).html("");
    for (var i in stations) {
        var station = stations[i];
        var coords = mapCalculations.CoordinatesToPercent({ x: station.X, y: station.Y }, $("#originalSizeMap"));
        var img = $("<div class='mapCheckpoint mapCheckpointDraggable mapCheckpoint" + station.Id + "'></div>")
                .css({ top: coords.y + "%", left: coords.x + "%" })
                .data("id", station.Id)
                .data("station", station);
        var title = $("<div class='mapTitle mapTitle" + station.Id + "'></div>")
                .css({ top: coords.y + "%", left: coords.x + "%" })
                .data("id", station.Id)
                .data("station", station)
                .html(station.Name);
        if (station.IsStart == true) {
            img.addClass("start");
            title.addClass("start");
        }
        else if (station.IsFinish == true) {
            img.addClass("finish");
            title.addClass("finish");
        }
        else if (station.IsAlive == true) {
            img.addClass("alive");
            title.addClass("alive");
        }
        else {
            img.addClass("kt");
            title.addClass("kt");
        }
        if (station.Locked == true)
            img.append("<div class='mapStationLocked'></div>");
        $(".mapObjectsDiv", viewport).append(
                img.clone(true),
                title.clone(true)
            );
        //testiram risanje ranjenca
        if (station.IsHidden == true) {
            drawLine({ x: station.X - 47, y: station.Y - 47 }, { x: station.X - 47, y: station.Y + 47 }, "#ff0000", "divForLineHiddenPoint", 0, true, 120);
            drawLine({ x: station.X + 47, y: station.Y + 47 }, { x: station.X - 47, y: station.Y + 47 }, "#ff0000", "divForLineHiddenPoint", 0, true, 120);
            drawLine({ x: station.X - 47, y: station.Y - 47 }, { x: station.X + 47, y: station.Y - 47 }, "#ff0000", "divForLineHiddenPoint", 0, true, 120);
            drawLine({ x: station.X + 47, y: station.Y + 47 }, { x: station.X + 47, y: station.Y - 47 }, "#ff0000", "divForLineHiddenPoint", 0, true, 120);

        }
    }
    $(".mapCheckpoint").animate({ opacity: 0.6 }, 1000);
    $(".mapTitle").animate({ opacity: 0.6 }, 1000);
    $("#viewport").mousedown(MapObject.FinishAddNewStation); //potreben pri dodajanju nove točke
}    //end function updateStationsOnMap



//
// Če jo najde, nariše izbrano pot
//
MapDraw.Path = function (path, grips, lineColor, keepLines) {

    // Brisanje
    if (keepLines && keepLines == true) { }
    else {
        $("div.divForLine").remove();
        $("div.divForCenterGrip").remove();
        $("div.divForEndGrip").remove();
        $("div.divForEndGripText").remove();
    }
    if (!path || path == null || !path.Id || path.Id == 0) return;

    var drawGrips = true;
    if (grips == false) drawGrips = false;
    if (path.Locked == true) drawGrips = false;

    var color = "00bb00";  //green
    if (path.Locked == true) color = "FF0000"; // red
    if (lineColor && lineColor != "") color = lineColor; // nastavljena ima prednost

    var start = { x: path.FirstStationX, y: path.FirstStationY };
    var finish = { x: path.SecondStationX, y: path.SecondStationY };

    //višina za start:
    if (start) drawContainerForHeight(start);
    var s = "";
    var cssClass = "";
    if (path.FirstStationZ) s = (path.FirstStationZ || "") + "m";
    if (path.FirstStationZisDirty && path.FirstStationZisDirty == true) cssClass = "error";
    $(".temporaryMarkForNewPathDiv")
        .removeClass("temporaryMarkForNewPathDiv").html(s).addClass(cssClass);

    var putAfter = 0;
    if (path.PathPoints && path.PathPoints.length > 0)
        for (var i in path.PathPoints) {
            var point = path.PathPoints[i];
            var current = { x: point.MapPositionX, y: point.MapPositionY };
            // narišem črto
            drawLine(start, current, color, "divForLine selectTrackOnMapClass", path.Id);
            if (drawGrips) {
                //narišem centralno točko za premikanje
                drawCenterGrip(start, current);
                $(".temporaryMarkForNewPathDiv")
                    .removeClass("temporaryMarkForNewPathDiv")
                    .addClass("mapPathDraggable")
                    .data("path-id", path.Id)
                    .data("put-after", putAfter)
                ;
                //narišem točko za premikanje verteksa
                drawEndGrip(current);
                var addClass = "mapPathEndpointDraggable";
                if (point.Locked == true) addClass = "locked"
                $(".temporaryMarkForNewPathDiv")
                    .removeClass("temporaryMarkForNewPathDiv")
                    .addClass("mapPathEndpointClickable " + addClass)
                    .data("path-point-id", point.Id)
                    .each(function () {
                        // dodam višino, če obstaja
                        var s = "";
                        var cssClass = "";
                        if (point.MapPositionZ) s = (point.MapPositionZ || "") + "m";
                        if (point.MapPositionZisDirty && point.MapPositionZisDirty == true) cssClass = "error";
                        $(".divForEndGripText", this).html(s).addClass(cssClass);
                    })
                ;
            }
            start = current;
            var putAfter = point.No;
        }
    // in še zadnja črta
        drawLine(start, finish, color, "divForLine selectTrackOnMapClass", path.Id);
    if (drawGrips) {
        //narišem centralno točko za premikanje
        drawCenterGrip(start, finish);
        $(".temporaryMarkForNewPathDiv")
                .removeClass("temporaryMarkForNewPathDiv")
                .addClass("mapPathDraggable")
                .data("path-id", path.Id)
                .data("put-after", putAfter)
            ;
        //višina za cilj:
        if (finish) drawContainerForHeight(finish);
        var s = "";
        var cssClass = "";
        if (path.SecondStationZ) s = (path.SecondStationZ || "") + "m";
        if (path.SecondStationZisDirty && path.SecondStationZisDirty == true) cssClass = "error";
        $(".temporaryMarkForNewPathDiv")
                .removeClass("temporaryMarkForNewPathDiv")
                .html(s)
                .addClass(cssClass);
    }
}

MapDraw.TrackByPath = false;
MapDraw.Track = function (track) {
    // najprej preverim, če bom rabil poti
    if (MapDraw.TrackByPath == true && MapObject.PathsAreLoaded == false) {
        MapObject.ReloadPaths(function () {
            MapDraw.Track(track)
        });
        return;
    }

    // Brisanje
    $("div.divForLine").remove();
    if (!track || track == null || !track.Id || track.Id == 0 || !track.TrackCheckpoints) return;
    for (var i in DataObject.SelectedTrack.TrackCheckpoints) if (i > 0) {
        if (MapDraw.TrackByPath == true) {
            //rišem progo po pathih
            var tp = DataObject.SelectedTrack.TrackCheckpoints[i];
            var path = MapObject.GetPathById(tp.PathId);
            if (path)
                MapDraw.Path(path, false, "0000ff", true);


        } else {
            var tp1 = DataObject.SelectedTrack.TrackCheckpoints[i - 1];
            var tp2 = DataObject.SelectedTrack.TrackCheckpoints[i];
            if (tp1.X && tp1.Y && tp2.X && tp2.Y) {
                var start = { x: tp1.X, y: tp1.Y };
                var current = { x: tp2.X, y: tp2.Y };
                drawLine(start, current, "#0000ff", "divForLine selectTrackOnMapClass", tp2.PathId);
            }
        }
    }
}




//
// Na vseh layerjih "nariše" črto med podanima točkama
//
var drawLine = function (from, to, color, aditionalClass, id, noArrows, zorder) {
    $(".mapObjectsDiv").each(function (index) {
        var calculationContainer = $(this);
        if (!$(this).hasClass("mapLayer")) calculationContainer = $(this).closest(".mapLayer");

        var containerWidth = calculationContainer.data("image-width");
        var containerHeight = calculationContainer.data("image-height");
        var coords1 = mapCalculations.CoordinatesToPixel(from, calculationContainer);
        var coords2 = mapCalculations.CoordinatesToPixel(to, calculationContainer);

        var parameters = {
            x1: coords1.x,
            y1: coords1.y,
            x2: coords2.x,
            y2: coords2.y,
            width: 3,
            id: id,
            zorder: 100,
            relative: true,
            color: "#" + color.replace("#", ""),
            class: "" + aditionalClass,
            arrows: ["20%", "70%"],
            containerHeight: containerHeight, // uporablja se za preračun v procente, če so koodinate relativne
            containerWidth: containerWidth // uporablja se za preračun v procente, če so koodinate relativne
        }
        if (noArrows == true) parameters.arrows = [];
        if (zorder && zorder != "") parameters.zorder = zorder;
        _drawLine(parameters, this);
    });
}


function _drawLine(o, container) {
    var defaults = {
        x1: 0,
        y1: 0,
        x2: 100,
        y2: 100,
        color: "#777777",
        width: 2,
        id: "",
        zorder: 100,
        class: "",
        arrows: ["25%", "75%"],
        relative: true, //true, če naj bodo koordinate v %, false, če naj bodo v pikslih. 
        containerHeight: $(container).height(), // uporablja se za preračun v procente, če so koodinate relativne
        containerWidth: $(container).width() // uporablja se za preračun v procente, če so koodinate relativne
    }
    o = $.extend(defaults, o || {}); //inherit properties

    var dy = o.y2 - o.y1;
    var dx = o.x2 - o.x1;
    var distance = Math.sqrt((dy * dy) + (dx * dx));
    var angle = 180 - 180 * Math.atan2(dx, dy) / Math.PI;
    var left = o.x1 + dx / 2 - o.width / 2;
    var top = o.y1 + dy / 2 - distance / 2;
    var line = $("<div></div>")
    if (o.class != "") line.addClass(o.class);
    if (o.id != "") line.attr("data-id", o.id);
    if (o.relative == true) {
        var topp = top * 100 / o.containerHeight;
        var distancep = distance * 100 / o.containerHeight;
        var leftp = left * 100 / o.containerWidth;
        line.css({
            top: topp + "%",
            left: leftp + "%",
            height: distancep + "%"
        });
    }
    else {
        line.css({
            top: top + "px",
            left: left + "px",
            height: distance + "px"
        });
    }
    line.css({
        position: "absolute",
        width: o.width + "px",
        border: "0 none"
    });
    for (i = 0; i < o.arrows.length; i++) {
        line.append(createArrow().css({ left: "50%", top: o.arrows[i] }));
    }
    //         line.append(createArrow().css({ left: "50%", top: "50%" }));
    colorize(line, o.color);
    rotate(line, angle);
    $(container).append(line);
    return;
    function rotate(element, angle) {
        $(element).css({
            transform: "rotate(" + angle + "deg)",
            "-ms-transform": "rotate(" + angle + "deg)", /* IE 9 */
            "-moz-transform": "rotate(" + angle + "deg)", /* Firefox */
            "-webkit-transform": "rotate(" + angle + "deg)", /* Safari and Chrome */
            "-o-transform": "rotate(" + angle + "deg)" /* Opera */
        });
    }
    function colorize(element, color) {
        $(element).css({
            "background-color": color,
            color: color
        });
    }
    function createArrow() {
        var arr = $("<div>^</div>");
        arr.css({
            position: "absolute",
            width: "12px",
            height: "20px",
            "margin-left": "-6px",
            "margin-top": "-10px",
            "vertical-align": "middle",
            "text-align": "center",
            "font-size": "20px",
            border: "0 none",
            "font-family": "Arial, Helvetica, sans-serif",
            "font-weight": "bold"
        });
        return arr;
    }
}


//
// Na vseh layerjih "nariše" centralni grip na črti, za dodajanje 
//
var drawCenterGrip = function (from, to) {
    $(".mapObjectsDiv").each(function (index) {
        var calculationContainer = $(this);
        if (!$(this).hasClass("mapLayer")) calculationContainer = $(this).closest(".mapLayer");

        var containerWidth = calculationContainer.data("image-width");
        var containerHeight = calculationContainer.data("image-height");
        var coords1 = mapCalculations.CoordinatesToPixel(from, calculationContainer);
        var coords2 = mapCalculations.CoordinatesToPixel(to, calculationContainer);
        //console.debug("containerWidth=" + containerWidth + ", containerHeight=" + containerHeight
        //    + "coords1(" + coords1.x + "," + coords1.y + ") coords2(" + coords2.x + "," + coords2.y + ")");
        var Y = (coords2.y + coords1.y) / 2;
        var X = (coords2.x + coords1.x) / 2;
        var coordinate = {
            x: 100 * X / containerWidth,
            y: 100 * Y / containerHeight
        }
        var div = $("<div class='divForCenterGrip temporaryMarkForNewPathDiv'></div>")
            .css({
                top: coordinate.y + "%",
                left: coordinate.x + "%"
            }
        );
        $(this).append(div);
    });
}

var drawEndGrip = function (end) {
    $(".mapObjectsDiv").each(function (index) {
        var calculationContainer = $(this);
        if (!$(this).hasClass("mapLayer")) calculationContainer = $(this).closest(".mapLayer");

        var containerWidth = calculationContainer.data("image-width");
        var containerHeight = calculationContainer.data("image-height");
        var coords = mapCalculations.CoordinatesToPixel(end, calculationContainer);
        //console.debug("containerWidth=" + containerWidth + ", containerHeight=" + containerHeight
        //    + "coords1(" + coords1.x + "," + coords1.y + ") coords2(" + coords2.x + "," + coords2.y + ")");
        var coordinate = {
            x: 100 * coords.x / containerWidth,
            y: 100 * coords.y / containerHeight
        }
        var div = $("<div class='divForEndGrip temporaryMarkForNewPathDiv'></div>")
            .css({
                top: coordinate.y + "%",
                left: coordinate.x + "%"
            }
        );
        // dodam še en div za morebiten napis
        div.append($("<div class='divForEndGripText'></div>"))
        $(this).append(div);
    });
}

var drawContainerForHeight = function (crds) {
    $(".mapObjectsDiv").each(function (index) {
        var calculationContainer = $(this);
        if (!$(this).hasClass("mapLayer")) calculationContainer = $(this).closest(".mapLayer");

        var containerWidth = calculationContainer.data("image-width");
        var containerHeight = calculationContainer.data("image-height");
        var coords = mapCalculations.CoordinatesToPixel(crds, calculationContainer);
        var coordinate = {
            x: 100 * coords.x / containerWidth,
            y: 100 * coords.y / containerHeight
        }
        var div = $("<div class='divForEndGripText temporaryMarkForNewPathDiv'></div>")
            .css({
                top: coordinate.y + "%",
                left: coordinate.x + "%"
            }
        );
        $(this).append(div);
    });
}





