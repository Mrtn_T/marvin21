﻿


var mapCalculations = new Object();

//
// Funkcija vrne  x,y koordinate na kliknjenem objeku (merjeno z zgornjega levega kota)
// Uporablja se po kliku z miško, da ugotovimo točno kje na objektu je bil "klik"
//
mapCalculations.ObjectCoordinatesFromClick = function (coordinates, object) {
    var $object = $(object);
    var position = $object.offset();
    var ret =  {};
    ret.y = coordinates.y - position.top;
    ret.x = coordinates.x - position.left;
    return ret;
}

//
// Funkcija vrne x,y koordinate (merjeno z zgornjega levega kota), 
// preračunane na originalno velikost slike, če je bila slika "resizana" v browserju.
//
// Podani objekt mora imeti s sabo podatek o originalni velikosti.
// Ta podatek se pričakuje v attributih 
//  - data-image-width  //velikost slike, kot je bila poslana browserju
//  - data-image-height //velikost slike, kot je bila poslana browserju
// Če katerega od podanih podatkov ni, se vrne podana vrednost
//
mapCalculations.PixelCoordinatesToOriginalSize = function (coordinates, object) {
    var $object = $(object);
    var ret = {};
    ret.y = coordinates.y;
    ret.x = coordinates.x;
    if ($object.data("image-width") == null || $object.data("image-width") == "") return ret;
    if ($object.data("image-height") == null || $object.data("image-height") == "") return ret;
    ret.y = coordinates.y * $object.data("image-height") / $object.height();
    ret.x = coordinates.x * $object.data("image-width") / $object.width();
    return ret;
}
//inverse
mapCalculations.OriginalSizeToPixelCoordinates = function (coordinates, object)
{
    var $object = $(object);
    var ret = {};
    ret.y = coordinates.y;
    ret.x = coordinates.x;
    if ($object.data("image-width") == null || $object.data("image-width") == "") return ret;
    if ($object.data("image-height") == null || $object.data("image-height") == "") return ret;
    ret.y = coordinates.y * $object.height() / $object.data("image-height");
    ret.x = coordinates.x * $object.width() / $object.data("image-width") ;
    return ret;
}


//
// Funkcija vrne x,y koordinate PIKSLA na sliki kot je bila prejeta s strežnika, 
// če je bila izvirna slika premaknjena iz izhodišča.
// Primer je, o sliko "fitamo" v neko velikost brez distorzije tako, da jo recimo centriramo.
//
// Podani objekt mora imeti s sabo podatek o tem, za koliko je slika premaknjena.
// Ta podatek se pričakuje v attributih 
//  - data-padded-left in 
//  - data-padded-top.
//
// OPOMBA: Če je bila slika "resizana" v browserju, je treba koordinate iz browserja
//         najprej preračunat na originalne koordinate.
//         za to se uporablja ObjectCoordinatesToOriginalSize
//
mapCalculations.PixelCoordinatesFromPaddedImage  = function (coordinates, object)
{
    var $object = $(object);
    var paddedLeft = $object.data("padded-left") || 0;
    var paddedTop = $object.data("padded-top") || 0;
    var ret = {};
    ret.y = coordinates.y - paddedTop;
    ret.x = coordinates.x - paddedLeft;
    return ret;
}
//inverse
mapCalculations.PixelOnPaddedImageFromCoordinates = function (coordinates, object)
{
    var $object = $(object);
    var paddedLeft = $object.data("padded-left") || 0;
    var paddedTop = $object.data("padded-top") || 0;
    var ret = {};
    ret.y = coordinates.y + paddedTop;
    ret.x = coordinates.x + paddedLeft;
    return ret;
}


//
// Funkcija vrne x,y koordinate PIKSLA na ORIGINALNI sliki, če je bila le ta predhodno 
// "resizana" na strežniku. 
// Koordinate se pretvorijo V PRAVE KARTEZIČNE KOORDINATE. Se pravi je izhodišče LEVO SPODAJ!!!
// 
// Kot vhod pričakuje koordinate PIKSLA na sliki, kot je bila prejeta s strežnika.
//
// Ker je potrebno dobiti "resize factor" po x in po y, funkcija potrebuje naslednje podatke:
//  - data-width-resize-factor
//  - data-height-resize-factor
//  - data-original-image-height.
// Če katerega od podanih podatkov ni, se vrne podana vrednost
//
// OPOMBA: Če je bila slika "resizana" v browserju, padana ali kako drugače deformirana, je 
//         treba te koordinate najprej preračunat na originalne koordinate na aktivnem 
//         delu slike.
//         za to se uporabljajo prejšnje funkcije
//
mapCalculations.OriginalPixelCoordinates = function (coordinates, object)
{
    var $object = $(object);
    var widthResize = $object.data("width-resize-factor") || 1;
    var heightResize = $object.data("height-resize-factor") || 1;
    var originalImageHeight = $object.data("original-image-height") || 1;

    var ret = {};
    ret.y = originalImageHeight - (coordinates.y / heightResize);
    ret.x = coordinates.x / widthResize;
    return ret;
}
// inverse
mapCalculations.PixelCoordinatesToImageCoordinates = function (coordinates, object)
{
    var $object = $(object);
    var widthResize = $object.data("width-resize-factor") || 1;
    var heightResize = $object.data("height-resize-factor") || 1;
    var originalImageHeight = $object.data("original-image-height") || 1;

    var ret = {};
    ret.y = (originalImageHeight - coordinates.y) * heightResize;
    ret.x = coordinates.x * widthResize;
    return ret;
}

//
// Funkcija sprejme koordinate (piksle) v podanem objektu in vrne PRAVE KARTEZIČNE KOORDINATE
// na originalnem zemljevidu. 
//
// dejansko zapored kliče
//  - PixelCoordinatesToOriginalSize
//  - PixelCoordinatesFromPaddedImage
//  - OriginalPixelCoordinates
mapCalculations.PixelToCoordinates = function (coordinates, object)
{
    coordinates = mapCalculations.PixelCoordinatesToOriginalSize(coordinates, object);
    coordinates = mapCalculations.PixelCoordinatesFromPaddedImage(coordinates, object);
    coordinates = mapCalculations.OriginalPixelCoordinates(coordinates, object);
    return coordinates;
}

//
// Funkcija sprejme koordinate, prejete pri KLIK-u na objekt, in vrne PRAVE KARTEZIČNE KOORDINATE
// na originalnem zemljevidu. 
//
// dejansko zapored kliče
//  - PixelCoordinatesToOriginalSize
//  - PixelCoordinatesFromPaddedImage
//  - OriginalPixelCoordinates
mapCalculations.ObjectClickToCoordinates = function (coordinates, object)
{
    coordinates = mapCalculations.ObjectCoordinatesFromClick(coordinates, object);
    coordinates = mapCalculations.PixelCoordinatesToOriginalSize(coordinates, object);
    coordinates = mapCalculations.PixelCoordinatesFromPaddedImage(coordinates, object);
    coordinates = mapCalculations.OriginalPixelCoordinates(coordinates, object);
    return coordinates;
}

// inverse
//
// Funkcija sprejme KARTEZIČNE KOORDINATE točke in vrne kordinato točke (piksli) v podanem objektu
//
mapCalculations.CoordinatesToPixel = function (coordinates, object)
{
    coordinates = mapCalculations.PixelCoordinatesToImageCoordinates(coordinates, object);
    coordinates = mapCalculations.PixelOnPaddedImageFromCoordinates(coordinates, object);
    //coordinates = mapCalculations.OriginalSizeToPixelCoordinates(coordinates, object);
    return coordinates;
}

//
// Funkcija sprejme kordinato točke (piksli) v podanem objektu
//  in vrne PROCENTUALNE kordinate točke (%) v podanem objektu
//
mapCalculations.PixelToPercent = function (coordinates, object) {
//    coordinates.y = 100 * coordinates.y / $(object).height(); //.data("image-height");
//    coordinates.x = 100 * coordinates.x / $(object).width(); // data("image-width");
    coordinates.y = 100 * coordinates.y / $(object).data("image-height");
    coordinates.x = 100 * coordinates.x / $(object).data("image-width");
    return coordinates;
}
//
// Funkcija sprejme KARTEZIČNE KOORDINATE točke in vrne PROCENTUALNE kordinate točke (%) v podanem objektu
//
mapCalculations.CoordinatesToPercent = function (coordinates, object)
{
    coordinates = mapCalculations.CoordinatesToPixel(coordinates, object);
    coordinates = mapCalculations.PixelToPercent(coordinates, object);
    return coordinates;
}