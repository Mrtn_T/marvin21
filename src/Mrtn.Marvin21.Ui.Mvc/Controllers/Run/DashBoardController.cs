﻿using System;
using System.Web.Mvc;
using System.Web.Services.Discovery;
using Mrtn.Marvin21.Bl.Interface.Run;
using Mrtn.Marvin21.Bl.Services.Run;
using Mrtn.Marvin21.Ui.Mvc._infrastructure;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Run
{
    public class DashBoardController : Controller
    {
        readonly IDashBoardService _dashBoardService = new DashBoardService();

        //
        // GET: /DashBoard/

        public ActionResult Index(int? day)
        {
            // prepare datepicker
            ViewBag.Days = ApplicationSettings.Days();
            ViewBag.IncludeAllDays = true;
            ViewBag.CurrentDay = ApplicationSettings.GetCurrentDay(Session, day); ;
            if (ViewBag.CurrentDay > 0) day = ViewBag.CurrentDay;

            var model = _dashBoardService.Get(day);
            return View("IndexDashboard", model);
        }

        public ActionResult Print(int day)
        {
            // prepare datepicker
            ViewBag.CurrentDay = day ;
            ViewBag.TimePrint = DateTime.Now.ToString("ddd, H:mm:ss");
            ViewBag.Print = true;

            var model = _dashBoardService.Get(day);
            return View("IndexDashboard", model);
        }
        public ActionResult CheckForSiReads(int day)
        {
            var model = _dashBoardService.CheckForSiReads(day);
            return PartialView(model);
        }        
        
        
        public ActionResult RecalculateTeams()
        {
            _dashBoardService.RecalculateTeams();
            return new EmptyResult();
        }

        public JsonResult DeleteUnusedSiReads()
        {
            var response = _dashBoardService.DeleteUnusedSiReads();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
