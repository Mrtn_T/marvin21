﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Run;
using Mrtn.Marvin21.Bl.Interface.Run;
using Mrtn.Marvin21.Bl.Services.Run;
using Mrtn.Marvin21.Model.RunTrack.Support;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Run
{
    public class RunTrackController : Controller
    {
        private readonly IRunTrackService _runTrackService = new RunTrackService();

        //
        // GET: /RunTrack/

        public ActionResult View(int id)
        {
            //id = runTrackId
            var model = _runTrackService.Get(id);
            return View("Index", model);
        }
        public ActionResult Add(int teamId, int day)
        {
            var model = _runTrackService.Add(teamId, day);
            return Redirect(Url.Action("View", new { id = model.Id }));
            //return Redirect(RunTrackController("View", new { id = model.Id }));
        }
        public ActionResult AddAll(int day)
        {
            var model = _runTrackService.AddAll(day);
            return Redirect(Url.Action("Index", "DashBoard", new { day = day }));
        }


        public JsonResult Lock(int id)
        {
            var response = _runTrackService.Lock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Unlock(int id)
        {
            var response = _runTrackService.Unlock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteResults(int id)
        {
            var response = _runTrackService.DeleteResults(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult EditRejectManualy(int id)
        {
            RejectedManualyEditModel model;
            try
            {
                model = _runTrackService.EditRejectManualy(id);
            }
            catch (Exception e)
            {
                return Content(e.Message);
            }
            return PartialView("_EditRejectManualy", model);

        }
        [HttpPost]
        public JsonResult EditRejectManualy(RejectedManualyEditModel model)
        {
            var response = _runTrackService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditConfirmManualy(int id)
        {
            ConfirmManualyEditModel model;
            try
            {
                model = _runTrackService.EditConfirmManualy(id);
            }
            catch (Exception e)
            {
                return Content(e.Message);
            }
            return PartialView("_EditConfirmManualy", model);
        }
        [HttpPost]
        public JsonResult EditConfirmManualy(ConfirmManualyEditModel model)
        {
            var response = _runTrackService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult SetPredictedStartTime(int id)
        {
            PredictedStartTimeEditModel model;
            try
            {
                model = _runTrackService.GetModelForPredictedStartTime(id);
            }
            catch (Exception e)
            {
                return Content(e.Message);
            }
            return PartialView("_SetPredictedStartTime", model);
        }
        [HttpPost]
        public JsonResult SetPredictedStartTime(PredictedStartTimeEditModel model)
        {
            var response = _runTrackService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult ManualDeparture(int id)
        {





            ManualDepartureEditModel model;
            try
            {
                model = _runTrackService.ManualDeparture(id);
            }
            catch (Exception e)
            {
                return Content(e.Message);
            }
            return PartialView("_ManualDeparture", model);


        }
        [HttpPost]
        public JsonResult ManualDeparture(ManualDepartureEditModel model)
        {
            var response = _runTrackService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ManualArrival(int id)
        {



            ManualArrivalEditModel model;
            try
            {
                model = _runTrackService.ManualArrival(id);
            }
            catch (Exception e)
            {
                return Content(e.Message);
            }
            return PartialView("_ManualArrival", model);



        }
        [HttpPost]
        public JsonResult ManualArrival(ManualArrivalEditModel model)
        {
            var response = _runTrackService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
