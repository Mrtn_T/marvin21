﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Bl.Interface.Run;
using Mrtn.Marvin21.Bl.Services.Run;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Run
{
    public class RunTaskController : Controller
    {
        readonly IRunTaskService _runTaskService = new RunTaskService();
        //
        // GET: /RunTask/

        public ActionResult Edit(int id)
        {
            var model = _runTaskService.GetEditModels(id);
            return View(model);
        }

        public ActionResult TrySaveCell(int id, int taskId, string firstEntry, string secondEntry)
        {
            var response = _runTaskService.TrySaveCell(id, taskId, firstEntry, secondEntry);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Lock(int id)
        {
            var response = _runTaskService.Lock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Unlock(int id)
        {
            var response = _runTaskService.Unlock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LockCategory(int taskId , int  categoryId)
        {
            var response = _runTaskService.LockCategory(taskId, categoryId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UnlockCategory(int taskId, int categoryId)
        {
            var response = _runTaskService.UnlockCategory(taskId, categoryId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
