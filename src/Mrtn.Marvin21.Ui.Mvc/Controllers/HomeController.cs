﻿using System.Web.Mvc;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("IndexHome");
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
