﻿using System;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Teams;
using Mrtn.Marvin21.Bl.Services.Teams;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers
{
    public class PackController : Controller
    {

        readonly IPackService _packService = new PackService();


        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            try
            {
                var model = _packService.Edit(id);
                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }

        [HttpPost]
        public JsonResult Edit(PackEditModel model)
        {
            var response = _packService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetCategories(bool active)
        {
            var queryModel = new PackQueryModel { Active = active };
            var model = _packService.Get(queryModel);
            if (active) return PartialView("_ListActive", model);
            return PartialView("_ListInactive", model);
        }

        public JsonResult Deactivate(int id)
        {
            var response = _packService.Deactivate(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Activate(int id)
        {
            var response = _packService.Activate(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }

}
