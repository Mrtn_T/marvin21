﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Teams;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Teams;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Teams;
using Mrtn.Marvin21.Model.Tracks;
using Mrtn.Marvin21.Ui.Mvc._infrastructure;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers
{
    public class TrackManualController : Controller
    {
        private readonly ITrackService _trackService = new TrackService();
        private readonly ITrackCheckpointService _trackCheckpointService = new TrackCheckpointService();
        private readonly ICategoryService _categoryService = new CategoryService();
        private readonly IStationService _stationService = new StationService();

        public ActionResult Index(int? id)
        {
            ViewBag.Days = ApplicationSettings.Days();
            ViewBag.CurrentDay = ApplicationSettings.GetCurrentDay(Session);
            ViewBag.Id = id ?? 0;
            return View("IndexTrackManual");

        }

        public ActionResult GetTracks()
        {
            var days = new List<int>();
            for (var i = 1; i < ApplicationSettings.Days() + 1; i++)
            {
                if (_stationService.Get(new StationQueryModel() { Day = i }).Count > 0) days.Add(i);
            }
            ViewBag.Days = days;
            ViewBag.Categories = _categoryService.GetAll();
            var model = _trackService.GetTracks(new TrackQueryModel());
            return PartialView("_Tracks", model);

        }
        /// <summary>
        /// vrne posamezen track
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult GetTrack(int id)
        {
            if (id == 0) return Content("Izberi progo");
            var model = _trackService.Get(id);
            return PartialView("_Track", model);
        }

        public JsonResult AddToTrack(int id, int stationId)
        {
            var response = _trackCheckpointService.Add(id, stationId);
            //var response = new SuccessResponse(false, "notimplemented");
            return Json(response, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public ActionResult EditCheckpoint(int id)
        {
            try
            {
                var model = _trackCheckpointService.Edit(id);
                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }

        [HttpPost]
        public JsonResult EditCheckpoint(TrackCheckpointEditModel model)
        {
            TimeSpan t;

            if (!TimeSpan.TryParse("0:" + model.TimeFromPrevious, out t))
                return Json(new SuccessResponse(false, "Preveri format pri času!"), JsonRequestBehavior.AllowGet);


            var response = _trackCheckpointService.Save(model, t.TotalSeconds);
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public JsonResult AddTrack(int category, int day)
        {
            var response = _trackService.Add(category, day);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}