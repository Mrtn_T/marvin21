﻿using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers
{
    public class TrackController : Controller
    {
        readonly ITrackService _trackService = new TrackService();

        public ActionResult Index(int? id)
        {
            ViewBag.Id = id.GetValueOrDefault();
            return View();
        }


        public ActionResult GetTracks(TrackQueryModel queryModel)
        {
            if (queryModel == null) queryModel = new TrackQueryModel();
            var model = _trackService.GetTracks(queryModel);
            return PartialView("_List", model);
        }

        public ActionResult GetTrack(int id)
        {
            if (id == 0) return Content("Izberi progo");
            var model = _trackService.Get(id);
            return PartialView("_Track", model);
        }

        //[HttpGet]
        //public ActionResult Edit(int? id)
        //{
        //    try
        //    {
        //        var model = _trackService.Edit(id);
        //        return PartialView("_Edit", model);
        //    }
        //    catch (Exception ee)
        //    {
        //        return PartialView("exception", ee);
        //    }
        //}

        //[HttpPost]
        //public JsonResult Edit(TrackEditModel model)
        //{
        //    var response = _trackService.Save(model);
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}



        public JsonResult RemoveSpeedTrial(int id)
        {
            //id=idTrack
            var response = _trackService.RemoveSpeedTrial(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SetSpeedTrial(int id)
        {
            //id = idCheckpoint
            var response = _trackService.SetSpeedTrial(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Lock(int id)
        {
            var response = _trackService.Lock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Unlock(int id)
        {
            var response = _trackService.Unlock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
