﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mrtn.Marvin21.Model.Teams;
using Mrtn.Marvin21.Bl.Interface.Tasks;
using Mrtn.Marvin21.Bl.Interface.Teams;
using Mrtn.Marvin21.Bl.Services.Tasks;
using Mrtn.Marvin21.Bl.Services.Teams;
using Mrtn.Marvin21.Model.Tasks;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers
{
    public class TaskController : Controller
    {

        readonly ITaskService _taskService = new TaskService();
        readonly ICategoryService _categoryService = new CategoryService();


        public ActionResult Index(int? id)
        {
            ViewBag.Id = id.GetValueOrDefault();
            return View();
        }

        public ActionResult GetTasks()
        {
            var model = _taskService.Get();
            ViewBag.Categories = _categoryService.GetAll();
            return PartialView("_List", model);
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            try
            {
                var model = _taskService.Edit(id);

                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }

        [HttpPost]
        public JsonResult Edit(TaskEditModel model, bool? useSecondEntry, string location)
        {
            if(useSecondEntry==null ||useSecondEntry.Value==false)
            {
                model.SecondEntryName = "";
            }
            if(location=="BeforeStart")
            {
                model.BeforeStart = true;
                model.OnTrack = false;
                model.AfterFinish = false;
                model.StationId = 0;
            }
            else if (location == "AfterFinish")
            {
                model.BeforeStart = false;
                model.OnTrack = false;
                model.AfterFinish = true;
                model.StationId = 0;
            }
            else
            {
                model.BeforeStart = false;
                model.OnTrack = true;
                model.AfterFinish = false;
            }

            var response = _taskService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Lock(int id)
        {
            var response = _taskService.Lock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Unlock(int id)
        {
            var response = _taskService.Unlock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }



    }
}
