﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mrtn.Marvin21.Model.Si;
using Mrtn.Marvin21.Bl.Interface.Teams;
using Mrtn.Marvin21.Bl.Services.Teams;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers
{
    public class CategoryController : Controller
    {

        readonly ICategoryService _categoryService = new CategoryService();

        /// <summary>
        ///  Vrne "okvir" za Index in potem takoj naloži tabelo
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View("IndexCategory");
        }

        public ActionResult GetCategories()
        {
            var model = _categoryService.GetAll();
            //var queryModel = new SiChipQueryModel { };
            //var model = _siChipService.Get(queryModel);
            return PartialView("_List", model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                var model = _categoryService.Edit(id);
                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }


        [HttpPost]
        public JsonResult Edit(CategoryEditModel model)
        {
            var response = _categoryService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }


    }
}
