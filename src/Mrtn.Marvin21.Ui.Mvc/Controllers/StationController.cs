﻿using System;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Model.Tracks;
using Mrtn.Marvin21.Ui.Mvc._infrastructure;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers
{
    public class StationController : Controller
    {
        readonly IStationService _stationService = new StationService();

        /// <summary>
        ///  Vrne "okvir" za Index in potem takoj naloži tabelo
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Days = ApplicationSettings.Days();
            ViewBag.CurrentDay = ApplicationSettings.GetCurrentDay(Session);
            return View("IndexStation");
        }

        public ActionResult GetStations(int day)
        {
            var model = _stationService.Get(new StationQueryModel() { Day = day });
            return PartialView("_List", model);
        }

        public JsonResult Lock(int id)
        {
            var response = _stationService.Lock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Unlock(int id)
        {
            var response = _stationService.Unlock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LockAll(int day)
        {
            var response = _stationService.LockAll(day);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UnlockAll(int day)
        {
            var response = _stationService.UnlockAll( day);
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                var model = _stationService.Edit(id);
                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }

        [HttpPost]
        public JsonResult Edit(StationEditModel model, bool? zIsOk)
        {
            if (zIsOk.HasValue && zIsOk.Value) model.ZisDirty = false;
            var response = _stationService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Add(int day)
        {
            try
            {
                var model = _stationService.GetModelForAdd(day);
                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }
        [HttpPost]
        public JsonResult Add(StationEditModel model, bool? zIsOk)
        {
            var response = _stationService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            var response = _stationService.Delete(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}