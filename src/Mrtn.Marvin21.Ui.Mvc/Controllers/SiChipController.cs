﻿using System;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Si;
using Mrtn.Marvin21.Bl.Services.Si;
using Mrtn.Marvin21.Model.Si;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers
{
    public class SiChipController : Controller
    {

        readonly ISiChipService _siChipService = new SiChipService();
        
        /// <summary>
        ///  Vrne "okvir" za Index in potem takoj naloži tabelo
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetSiChips()
        {
            var queryModel = new SiChipQueryModel { };
            var model = _siChipService.Get(queryModel);
            return PartialView("_List", model);
        }
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            try
            {
                var model = _siChipService.Edit(id);
                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }


        [HttpPost]
        public JsonResult Edit(SiChipEditModel model)
        {
            var response = _siChipService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddPacket(bool @private)
        {
            return PartialView("_AddPacket", @private);
        }
        [HttpPost]
        public ActionResult AddPacket(bool @private, string siCodes)
        {
            var response = _siChipService.AddPacket(siCodes, @private);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            var response = _siChipService.Delete(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Deactivate(int id)
        {
            var response = _siChipService.Deactivate(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Activate(int id)
        {
            var response = _siChipService.Activate(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }



    }
}
