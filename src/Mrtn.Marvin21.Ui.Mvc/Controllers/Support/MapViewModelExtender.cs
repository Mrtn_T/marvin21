﻿using System.Text;
using Mrtn.Marvin21.Ui.Mvc.Models;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Support
{
    /// <summary>
    /// Metode tu vračajo koščke HTML kode za MapViewModel in MapLayerViewModel
    /// </summary>
    public static class MapViewModelExtender
    {
        /// <summary>
        /// Doda "data" tage s podatki o originalni sliki 
        /// Uporablja se na divih , ki imajo class "mapLayer"
        /// To so divi, ki OVIJAJO vsak layer zemljevida
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string AditionalData(this MapViewModel model )
        {
            var sb = new StringBuilder();
            sb.AppendLine(string.Format("data-original-image-height=\"{0}\"", model.OriginalImageHeight));
            sb.Append(string.Format("data-original-image-width=\"{0}\"", model.OriginalImageWidth));
            return sb.ToString();
        }

        /// <summary>
        /// Doda "data" tage z dodatnimi podatki za posamezen layer
        /// Uporablja se na divih , ki imajo class "mapLayer"
        /// To so divi, ki OVIJAJO vsak layer zemljevida
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string AditionalData(this MapLayerViewModel model)
        {
            var sb = new StringBuilder();
            sb.AppendLine(string.Format("data-image-height=\"{0}\"", model.Height));
            sb.AppendLine(string.Format("data-image-width=\"{0}\"", model.Width));
            sb.AppendLine(string.Format("data-padded-left=\"{0}\"", model.PaddedLeft));
            sb.AppendLine(string.Format("data-padded-top=\"{0}\"", model.PaddedTop));
            sb.AppendLine(string.Format("data-width-resize-factor=\"{0}\"",
                model.WidthResizeFactor.ToString().Replace(',', '.')));
            sb.Append(string.Format("data-height-resize-factor=\"{0}\"",
                model.HeightResizeFactor.ToString().Replace(',', '.')));
            return sb.ToString();
        }

        /// <summary>
        /// Stil za "spodnje" layerje.
        /// Za dive, ki  ki OVIJAJO vsak layer zemljevida.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string Style(this MapLayerViewModel model )
        {
            return string.Format("height: {0}px; width: {1}px;", 
                model.Height, model.Width);
        }

        /// <summary>
        /// Stil za "osnovni" layer. Doda tudi sliko v background.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string StyleWithBackground(this MapLayerViewModel model)
        {
            return string.Format("background: url({2}) no-repeat; height: {0}px; width: {1}px;",
                model.Height, model.Width, model.ImgUrl);
        }




    }
}