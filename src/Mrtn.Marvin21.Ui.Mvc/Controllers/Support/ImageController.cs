﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Support
{
    public class ImageController : Controller
    {
        private const string BasePath = "~/Content/img/";

        public ActionResult GetImage(string filename, int? width, int? height)
        {
            if (!width.HasValue || width.Value == 0) width = 100;
            if (!height.HasValue || height.Value == 0) height = 100;

            var phisicalPath = GetPhisicalPath(filename);

            Image img = null;
            Image thumbNail = null;
            try
            {
                //Debug.Print(String.Format("nalagam {0} za velikost ({1},{2})", filename, height, width));
                img = new Bitmap(phisicalPath);
                if (img.Height == height && img.Width == width)
                {
                    //Debug.Print(String.Format("Vračam original {0} za velikost ({1},{2})", filename, height, width));
                    Response.ContentType = "image/png";
                    img.Save(Response.OutputStream, ImageFormat.Png);
                    return new EmptyResult();
                }
                thumbNail = new Bitmap(width.Value, height.Value, img.PixelFormat);
                var graphic = Graphics.FromImage(thumbNail);
                graphic.CompositingQuality = CompositingQuality.HighQuality;
                graphic.SmoothingMode = SmoothingMode.HighQuality;
                graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var rectangle = new Rectangle(0, 0, width.Value, height.Value);
                graphic.DrawImage(img, rectangle);

                Response.ContentType = "image/png";
                thumbNail.Save(Response.OutputStream, ImageFormat.Png);
                //Debug.Print(String.Format("Vračam resize {0} za velikost ({1},{2})", filename, height, width));
                return new EmptyResult();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                //Debug.Print(String.Format("Disposing {0} za velikost ({1},{2})", filename, height, width));
                //Debug.Print("---------------------");
                if (img != null) img.Dispose();
                if (thumbNail != null) thumbNail.Dispose();
            }
        }

        /// <summary>
        ///  example:
        ///     <img src="/image/FitInto/map.png?width=300&height=200"  style="border: 1px solid #000000"/>
        ///     <img src="/image/FitInto/map.png?width=300&height=200&verticalAlign=bottom&bgColor=ff0000&horizontalAlign=right"  style="border: 1px solid #000000"/>
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="verticalAlign"></param>
        /// <param name="horizontalAlign"></param>
        /// <param name="bgColor"></param>
        /// <returns></returns>
        public ActionResult FitInto(
            string filename,
            int? width, int? height,
            string verticalAlign, string horizontalAlign,
            string bgColor)
        {
            // preverim vhodne parametre
            if (!width.HasValue || width.Value == 0) width = 100;
            if (!height.HasValue || height.Value == 0) height = 100;
            if (verticalAlign == null) verticalAlign = "center";
            if (horizontalAlign == null) horizontalAlign = "center";
            if (string.IsNullOrEmpty(bgColor)) bgColor = "ffffff";

            var phisicalPath = GetPhisicalPath(filename);
            Image img = null;
            Image thumbNail = null;
            try
            {
                Response.Cache.SetExpires(DateTime.Now.AddDays(1));
                Debug.Print(String.Format("nalagam {0} za velikost ({1},{2})", filename, height, width));
                img = new Bitmap(phisicalPath);
                if (img.Height == height && img.Width == width)
                {
                    Debug.Print(String.Format("Vračam original {0} za velikost ({1},{2})", filename, height, width));
                    Response.ContentType = "image/png";
                    img.Save(Response.OutputStream, ImageFormat.Png);
                    return new EmptyResult();
                }
                thumbNail = new Bitmap(width.Value, height.Value, img.PixelFormat);
                var graphic = Graphics.FromImage(thumbNail);
                graphic.Clear(ColorTranslator.FromHtml("#" + bgColor));

                var widthRatio = width.Value / (double)img.Width;
                var heightRatio = height.Value / (double)img.Height;
                var ratio = Math.Min(widthRatio, heightRatio);
                var widthValue = (int)(img.Width * ratio);
                var heightValue = (int)(img.Height * ratio);
                var top = 0;
                var left = 0;

                if (widthRatio < heightRatio)
                {
                    //padding top, bottom
                    switch (verticalAlign)
                    {
                        case "center":
                            top = (height.Value - heightValue) / 2;
                            break;
                        case "bottom":
                            top = (height.Value - heightValue);
                            break;
                    }
                }
                else if (widthRatio > heightRatio)
                {
                    //padding left, right
                    switch (horizontalAlign)
                    {
                        case "center":
                            left = (width.Value - widthValue) / 2;
                            break;
                        case "right":
                            left = (width.Value - widthValue);
                            break;
                    }
                }
                else
                {
                    //no padding
                }

                var rectangle = new Rectangle(left, top, widthValue, heightValue);
                if (ratio>0)
                {
                    graphic.CompositingQuality = CompositingQuality.HighSpeed;
                    graphic.SmoothingMode = SmoothingMode.HighSpeed;
                    graphic.InterpolationMode = InterpolationMode.Low;
                    graphic.DrawImage(img, rectangle);
                    Response.ContentType = "image/jpg";
                    thumbNail.Save(Response.OutputStream, ImageFormat.Jpeg);
                }
                else
                {
                    graphic.CompositingQuality = CompositingQuality.HighQuality;
                    graphic.SmoothingMode = SmoothingMode.HighQuality;
                    graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphic.DrawImage(img, rectangle);
                    Response.ContentType = "image/png";
                    thumbNail.Save(Response.OutputStream, ImageFormat.Png);
                }
                Debug.Print(String.Format("Vračam resize {0} za velikost ({1},{2})", filename, height, width));
                return new EmptyResult();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Debug.Print(String.Format("Disposing {0} za velikost ({1},{2})", filename, height, width));
                Debug.Print("---------------------");
                if (img != null) img.Dispose();
                if (thumbNail != null) thumbNail.Dispose();
            }
        }


        /*************** private stuff ****************/
        private static string GetPhisicalPath(string filename)
        {
            var virtualPath = BasePath + filename;
            var phisicalPath = HostingEnvironment.MapPath(virtualPath);
            if (phisicalPath == null || !System.IO.File.Exists(phisicalPath))
                throw new HttpException(404, String.Format("File '{0}' not found.", phisicalPath));
            return phisicalPath;
        }

    }
}
