﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Services;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Bl.Interface.Serialization;
using Mrtn.Marvin21.Bl.Services.DatabaseInit;
using Mrtn.Marvin21.Bl.Services.Serialization;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Support
{
    public class DatabaseInitController : Controller
    {
        readonly ISerializer _serializer = new Serializer();
        private readonly IInitializer _initializer = new Initializer();

        public JsonResult DeleteAllEntites()
        {

            var response = _serializer.DeleteAllEntites();
            return Json(response, JsonRequestBehavior.AllowGet);

        }


        public ActionResult LoadCategoriesForGsj()
        {
            //var response = new SuccessResponse()
            //{
            //    Ok = false,
            //    Message = "not implemented"
            //};
            var response = _initializer.LoadCategoriesForGsj();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadCategoriesForRot()
        {
            //var response = new SuccessResponse()
            //{
            //    Ok = false,
            //    Message = "not implemented"
            //};
            var response = _initializer.LoadCategoriesForRot();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadPacks()
        {
            var response = _initializer.LoadPacks();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadTasksForGsj()
        {
            var response = _initializer.LoadTasksForGsj();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadTasksForRot()
        {
            var response = _initializer.LoadTasksForRot();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StationsTemplateForGsj()
        {
            var response = _initializer.StationsTemplateForGsj();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult StationsTemplateForRot()
        {
            var response = _initializer.StationsTemplateForRot();
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadSettingsForGsj()
        {
            var response = new SuccessResponse(true, "OK");
            try
            {
                SettingsInitializer.InitializeForGsj();
            }
            catch (Exception e)
            {
                response.Ok = false;
                response.Message = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadSettingsForRot()
        {
            var response = new SuccessResponse(true, "OK");
            try
            {
            SettingsInitializer.InitializeForRot();
            }
            catch (Exception e)
            {
                response.Ok = false;
                response.Message = e.Message;
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
