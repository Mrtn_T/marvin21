﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Run;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Run;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Model.RunTrack;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Support
{

    public class PrintController : Controller
    {
        readonly ITrackService _trackService = new TrackService();
        readonly IRunTrackService _runTrackService = new RunTrackService();
        readonly IStationService _stationService = new StationService();
            
        // GET: /Print/

        public ActionResult TrackTable(int id)
        {
            TrackWithCheckpointsViewModel model = _trackService.Get(id);
            return View(model);
        }
        public ActionResult TrackTables(int id)
        {
            ViewModelForTrackTables
                model = _runTrackService.GetModelForTrackTables(id);

            return View(model);
        }

        public ActionResult Station(int id)
        {
            var model=_stationService.GetModelForStation(id);
            return View(model);
        }
    }
}
