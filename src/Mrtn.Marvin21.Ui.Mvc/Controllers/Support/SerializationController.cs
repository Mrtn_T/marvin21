﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Serialization;
using Mrtn.Marvin21.Bl.Services.Serialization;
using Mrtn.Marvin21.Model.Common;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Support
{
    public class SerializationController : Controller
    {
        readonly ISerializer _serializer = new Serializer();
        public ActionResult Index()
        {
            return View("IndexSerialization");
        }

        public ActionResult Tracks(string extension)
        {
            var model = _serializer.SerializeTracks();
            Response.ContentType = "application/octet-stream";
            Response.Write(model);
            return new EmptyResult();
        }

        public ActionResult Tasks(string extension)
        {
            var model = _serializer.SerializeTasks();
            Response.ContentType = "application/octet-stream";
            Response.Write(model);
            return new EmptyResult();
        }
        public ActionResult Teams(string extension)
        {
            var model = _serializer.SerializeTeams();
            Response.ContentType = "application/octet-stream";
            Response.Write(model);
            return new EmptyResult();
        }


        [HttpGet]
        public ActionResult UploadTeams()
        {
            return PartialView("_UploadTeams");
        }
        [HttpPost]
        public JsonResult UploadTeams(HttpPostedFileBase file)
        {
            var response = new SuccessResponse(false, "Ni datoteke!");
            if (file != null && file.ContentLength > 0)
            {
                var reader = new StreamReader(file.InputStream);
                var myJson = reader.ReadToEnd();
                response = _serializer.DeserializeTeams(myJson);

            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult UploadTracks()
        {
            return PartialView("_UploadTracks");
        }
        [HttpPost]
        public JsonResult UploadTracks(HttpPostedFileBase file)
        {
            var response = new SuccessResponse(false, "Ni datoteke!");
            if (file != null && file.ContentLength > 0)
            {
                var reader = new StreamReader(file.InputStream);
                var myJson = reader.ReadToEnd();
                response = _serializer.DeserializeTracks(myJson);

            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult UploadTasks()
        {
            return PartialView("_UploadTasks");
        }
        [HttpPost]
        public JsonResult UploadTasks(HttpPostedFileBase file)
        {
            var response = new SuccessResponse(false, "Ni datoteke!");
            if (file != null && file.ContentLength > 0)
            {
                var reader = new StreamReader(file.InputStream);
                var myJson = reader.ReadToEnd();
                response = _serializer.DeserializeTasks(myJson);

            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }


    }
}
