﻿using System;
using System.Drawing;
using System.IO;
using System.Web.Hosting;


namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Support
{
    /// <summary>
    /// Operacije nad datoteko z zemljevidom
    /// </summary>
    public static class MapFileHelper
    {
        /// <summary>
        /// Metoda prebere zemljevid z diska in vrne njegovo velikost
        /// </summary>
        /// <returns>Velikost zemljevida</returns>
        public static Size GetMapSize(int day)
        {
            var size = new Size();
            Image img = null;
            var virtualPath = GetMapVirtualPath(day);
            var physicalPath = HostingEnvironment.MapPath(virtualPath);
            if (physicalPath == null)
                throw new FileNotFoundException("Ne morem zmapirati podane virtualne poti v fizično pot!", virtualPath);
            if (!File.Exists(physicalPath))
                throw new FileNotFoundException("Ne najdem zemljevida. Naložiti je potrebno zemljevid!", physicalPath);
            try
            {
                img = new Bitmap(physicalPath);
                size.Height = img.Height;
                size.Width = img.Width;
            }
            catch (Exception ee)
            {
                throw ee;
            }
            finally
            {
                if (img != null) img.Dispose();
            }
            return size;
        }

        /// <summary>
        /// Vrne fizično pot do zemljevida za določen dan. 
        /// Trenutno vrača samo enega (ToDo za več)
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        public static string GetMapPhysicalPath(int day)
        {
            var virtualPath = GetMapVirtualPath(day);
            var physicalPath = HostingEnvironment.MapPath(virtualPath);
            if (physicalPath == null || !File.Exists(physicalPath))
                throw new System.Web.HttpException(404, String.Format("File '{0}' not found.", virtualPath));
            return physicalPath;
        }


        /// <summary>
        /// Vrne virtualno pot(vključno z imenom) do zemljevida za določen dan. 
        /// Trenutno vrača samo enega (ToDo za več)
        /// </summary>
        /// <param name="day"></param>
        /// <returns>Virtualna pot do zemljevida, vključno z imenom datoteke</returns>
        public static string GetMapVirtualPath(int day)
        {
            return "~/Content/img/" + GetMapFilename(day);
        }
        /// <summary>
        /// Vrne ime datoteke z zemljevidom za določen dan. 
        /// Trenutno vrača samo enega (ToDo za več)
        /// </summary>
        /// <param name="day"></param>
        /// <returns>Ime datoteke z zemljevidom za določen dan</returns>
        public static string GetMapFilename(int day)
        {
            //ToDo: možno je vračati različne zemljevide za različne dni 
            return "map.png";
        }
    }

}