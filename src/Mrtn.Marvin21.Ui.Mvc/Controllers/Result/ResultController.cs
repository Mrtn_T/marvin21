﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Mrtn.Marvin21.Bl.Interface.Result;
using Mrtn.Marvin21.Bl.Services.Result;
using Mrtn.Marvin21.Ui.Mvc._infrastructure;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Result
{
    public class ResultController : Controller
    {
        readonly IResultService _resultService = new ResultService();


        #region meni, rezultati za na ekran

        //**********************************************
        //****   meni, rezultati za na ekran
        //**********************************************

        /// <summary>
        /// Osnovni seznam rezultatov
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.Days = ApplicationSettings.Days();
            ViewBag.Days = 3;
            return View();
        }

        /// <summary>
        /// Rezultati rodov
        /// </summary>
        /// <returns></returns>
        public ActionResult Packs()
        {
            var model = _resultService.PackResults();
            return View(model);
        }
        /// <summary>
        /// Rezultati rodov
        /// </summary>
        /// <returns></returns>
        public ActionResult PacksPrint()
        {
            var model = _resultService.PackResults();
            ViewBag.Print = true;
            return View("Packs", model);
        }


        /// <summary>
        /// Rezultati ekip po dnevih, za na ekran
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        public ActionResult TeamsByDay(int day)
        {
            var model = _resultService.TeamsByDayResult(day);
            return View("_ProjectionByDay", model);
        }


        #region Neki ekranski izpisi za GSJ, vsi so enaki ...
        /// <summary>
        /// nekaj za GSJ
        /// </summary>
        /// <returns></returns>
        public ActionResult Teams()
        {
            var model = _resultService.TeamResults();
            return View(model);
        }
        /// <summary>
        /// nekaj za GSJ
        /// </summary>
        /// <returns></returns>
        public ActionResult TeamsPp()
        {
            var model = _resultService.TeamPpResults();
            return View("Teams", model);
        }
        /// <summary>
        /// nekaj za GSJ
        /// </summary>
        /// <returns></returns>
        public ActionResult TeamsTopo()
        {
            var model = _resultService.TeamTopoResults();
            return View("Teams", model);
        }
        /// <summary>
        /// nekaj za GSJ
        /// </summary>
        /// <returns></returns>
        public ActionResult TeamsTt()
        {
            var model = _resultService.TeamTtResults();
            return View("Teams", model);
        }

        #endregion

        #endregion

        #region Projekcija za GSJ - projection
        //**********************************************
        //****   Projekcija za GSJ - projection
        //**********************************************

        /// <summary>
        /// Osnovna akcija za projekcijo GSJ
        /// </summary>
        /// <returns></returns>
        public ActionResult Projection()
        {
            return View();
        }
        public ActionResult ProjectionStarWars()
        {
            return View();
        }



        /// <summary>
        /// servis, ki dejansko vrne HTML z rezultati za GSJ
        /// </summary>
        /// <returns></returns>
        public ActionResult ReloadResults()
        {
            // osnovni rezultati
            var model = _resultService.TeamResults();

            // rezultati po rodovih
            var packResults = _resultService.PackResults();
            ViewBag.PackResualt = packResults;

            return PartialView(model);
        }
        #endregion

        #region Projekcija za ROT po dnevih - projection
        //**********************************************
        //****   Projekcija za ROT po dnevih - projection
        //**********************************************

        /// <summary>
        /// Osnovna akcija za projekcijo GSJ
        /// </summary>
        /// <returns></returns>
        public ActionResult ProjectionByDay(int day)
        {
            ViewBag.Day = day;
            return View();
        }

        /// <summary>
        /// servis, ki dejansko vrne HTML z rezultati za ROT
        /// </summary>
        /// <returns></returns>
        public ActionResult ReloadProjectionByDay(int day)
        {
            // osnovni rezultati
            var model = _resultService.TeamsByDayResult(day);

            // rezultati po rodovih
            var packResults = _resultService.PackResults();
            ViewBag.PackResult = packResults;
            ViewBag.HideTeamNames = true;
            ViewBag.TimeGenerated = DateTime.Now.ToString("H:mm:ss");

            return PartialView("_ProjectionByDay", model);
        }
        #endregion


        public ActionResult TeamsByDayJson()
        {
            var model = _resultService.TeamResults();
            var result = new Dictionary<string, object>();
            var categoryCounter = 1;
            // čez vse kategorije:
            foreach (var category in model.Categories)
            {
                var resultsForCategory = new Dictionary<string, object>();

                // izdelaj glavo
                var resultsHead = new List<string>();
                resultsHead.Add("#");
                resultsHead.Add("Topo");
                resultsHead.Add("Vris");
                resultsHead.Add("SEM");
                resultsHead.Add("PP");
                resultsHead.Add("KT");
                resultsHead.Add("Čas");
                resultsHead.Add("HE");
                resultsHead.Add("TT");
                resultsHead.Add("Sum");
                resultsHead.Add("Ime ekipe");
                var resultForTeamHead = new Dictionary<string, string[]>
                    {
                        { "title", resultsHead.ToArray() }
                    };

                resultsForCategory.Add("0", resultForTeamHead);

                // Čez vse ekipe
                var noCnt = -1;
                foreach (var teamResult in category.TeamResults)
                {
                    var results = new List<string>();
                    results.Add(teamResult.Place.ToString());
                    results.Add(teamResult.TopoPoints.ToString());
                    results.Add(teamResult.VrisPoints.ToString());
                    results.Add(teamResult.SemPoints.ToString());
                    results.Add(teamResult.PpPoints.ToString());
                    results.Add(teamResult.TrackPoints.ToString());
                    results.Add(teamResult.TimePoints.ToString());
                    results.Add(teamResult.SpeedTrialPoints.ToString());
                    results.Add(teamResult.TtPoints.ToString());
                    results.Add(teamResult.Points.ToString());
                    results.Add(teamResult.Name.ToString());

                    var resultForTeam = new Dictionary<string, string[]>
                    {
                        { "data", results.ToArray() }
                    };

                    var no = teamResult.No;
                    if (!no.HasValue)
                    {
                        no = noCnt;
                        noCnt--;
                    }

                    resultsForCategory.Add(no.ToString(), resultForTeam);
                }
                result.Add(category.Category.No.ToString(), resultsForCategory);
                categoryCounter++;
            }



            var packModel = _resultService.PackResults();

            var packResults = new Dictionary<string, object>();

            // izdelaj glavo
            var packResultsHead = new List<string>();
            packResultsHead.Add("#");
            packResultsHead.Add("Rod");
            packResultsHead.Add("Sum");
            packResultsHead.Add("Ekipe");
            var packResultHead = new Dictionary<string, string[]>
                    {
                        { "title", packResultsHead.ToArray() }
                    };

            packResults.Add("0", packResultHead);

            // Čez vse rodove
            var pckCnt = 1;
            foreach (var pack in packModel)
            {
                var results = new List<string>();

                results.Add(pack.Place.ToString());
                results.Add(pack.PackNam + " " + pack.PackTown);
                results.Add(pack.Points.ToString());
                results.Add(pack.Teams.Replace("<i>","").Replace("</i>",""));

                var resultForPack = new Dictionary<string, string[]>
                    {
                        { "data", results.ToArray() }
                    };



                packResults.Add(pckCnt.ToString(), resultForPack);
                pckCnt++;
            }


            result.Add(categoryCounter.ToString(), packResults);
            var res = Json(result, JsonRequestBehavior.AllowGet);
            var serializer = new JavaScriptSerializer();     
            var output = serializer.Serialize(res.Data);
            System.IO.File.WriteAllText(HttpRuntime.AppDomainAppPath + @"\results.txt", output); 
            return res;
        }

    }
}
