﻿using System;
using System.Web.Mvc;
using Mrtn.Marvin21.Ui.Mvc.Controllers.Support;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Tracks;
using Mrtn.Marvin21.Ui.Mvc._infrastructure;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Map
{
    public class MapPathController : Controller
    {
        readonly IPathService _pathService = new PathService();
        
        /// <summary>
        /// Vrača glavno masko, v kateri se potem vse ureja
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int? id)
        {
            if (!Request.IsAjaxRequest())
                Response.Redirect(Url.Action("Index", "Map", new { id = id, content = "MapPath" }));
            ViewBag.Days = ApplicationSettings.Days();
            ViewBag.CurrentDay = ApplicationSettings.GetCurrentDay(Session);
            Session["ContentToBeLoadedInMap"] = "MapPath";
            return PartialView("_Index");
        }

        /// <summary>
        /// Vrne vse že definirane poti za določen dan
        /// </summary>
        /// <returns></returns>
        public JsonResult Get()
        {
            var currentDay = ApplicationSettings.GetCurrentDay(Session);
            var queryModel = new PathQueryModel { Day = currentDay };
            var model = _pathService.Get(queryModel);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Poskusi dodati path moed podanima stationama
        /// </summary>
        /// <param name="id1"></param>
        /// <param name="id2"></param>
        /// <returns></returns>
        public  JsonResult Add(int id1, int id2)
        {
            var response = _pathService.Add(id1, id2);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                var model = _pathService.Edit(id);
                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }

        [HttpPost]
        public JsonResult Edit(PathEditModel model)
        {
            var response = _pathService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        public JsonResult Invert(int id)
        {
            var response = _pathService.Invert(id);
            return Json(response, JsonRequestBehavior.AllowGet);
            //return Json(null, JsonRequestBehavior.AllowGet);
        }


        public JsonResult LockAll()
        {
            var response = _pathService.LockAll(ApplicationSettings.GetCurrentDay(Session));
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UnlockAll()
        {
            var response = _pathService.UnlockAll(ApplicationSettings.GetCurrentDay(Session));
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Lock(int id)
        {
            var response = _pathService.Lock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Unlock(int id)
        {
            var response = _pathService.Unlock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeDay(int? day)
        {
            try
            {
                ApplicationSettings.GetCurrentDay(Session, day);
            }
            catch (Exception ee)
            {
                return Json(new SuccessResponse { Message = ee.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new SuccessResponse { Ok = true }, JsonRequestBehavior.AllowGet);
        }

        

    }
}
