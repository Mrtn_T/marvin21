﻿using System;
using System.Web.Mvc;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Ui.Mvc.Controllers.Support;
using Mrtn.Marvin21.Ui.Mvc.Models;
using Mrtn.Marvin21.Ui.Mvc._infrastructure;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Map
{
    public class MapController : Controller
    {
        public ActionResult Index(string content, int? day, int? id)
        {
            if (content != "") Session["ContentToBeLoadedInMap"] = content;
            ViewBag.PathId = 0;
            ViewBag.TrackId = 0;
            ViewBag.StationId = 0;
            if (content == null) content = "";
            switch (content.ToUpper())
            {
                case "MAPTRACK":
                    ViewBag.TrackId = id.HasValue ? id.Value : 0;
                    break;
                case "MAPPATH":
                    ViewBag.PathId = id.HasValue ? id.Value : 0;
                    break;
                case "MAPSTATION":
                    ViewBag.StationId = id.HasValue ? id.Value : 0;
                    break;

            }
            ViewBag.Days = ApplicationSettings.Days();
            ViewBag.CurrentDay = ApplicationSettings.GetCurrentDay(Session, day);
            return View();
        }

        public ActionResult Map(int width = 0, int height = 0)
        {
            // uredim velikosti
            width = Math.Max(500, width - 4);
            height = Math.Max(300, height - 1);

            //pripravim spremenljivke
            var day = ApplicationSettings.GetCurrentDay(Session);

            var model = new MapViewModel();
            try
            {
                //pridobim dimenzije
                var dimensions = MapFileHelper.GetMapSize(day);

                //izračunam razmerja
                var widthRatio = width / (double)dimensions.Width;
                var heightRatio = height / (double)dimensions.Height;
                var ratio = Math.Min(widthRatio, heightRatio);

                //dejanska velikost zemljevida na disku
                model.OriginalImageHeight = dimensions.Height;
                model.OriginalImageWidth = dimensions.Width;

                //pripravim vse potrebno za malo sliko
                model.BasicLayer.Width = width;         //hkrati tudi velikost okna
                model.BasicLayer.Height = height;    //hkrati tudi velikost okna
                model.BasicLayer.PaddedLeft = (int)((width - dimensions.Width * ratio) / 2);
                model.BasicLayer.PaddedTop = (int)((height - dimensions.Height * ratio) / 2);
                model.BasicLayer.WidthResizeFactor = ratio;
                model.BasicLayer.HeightResizeFactor = ratio;
                model.BasicLayer.ImgUrl = Url.Content(
                    String.Format("~/imageFitInto/{1}/{2}/{0}",
                    MapFileHelper.GetMapFilename(day),
                    model.BasicLayer.Width,
                    model.BasicLayer.Height));
                /*               String.Format("~/image/FitInto/{0}?width={1}&height={2}",
                                   MapFileHelper.GetMapFilename(day),
                                   model.BasicLayer.Width,
                                   model.BasicLayer.Height));*/
                model.BasicLayer.DivId = "basicLayer";

                //pripravim vse potrebno za 1:1 sliko
                var layer = new MapLayerViewModel();
                model.Layers.Add(layer);

                layer.Width = (int)(width / ratio);
                layer.Height = (int)(height / ratio);
                layer.PaddedLeft = (layer.Width - dimensions.Width) / 2;
                layer.PaddedTop = (layer.Height - dimensions.Height) / 2;
                layer.WidthResizeFactor = 1;
                layer.HeightResizeFactor = 1;
                layer.ImgUrl = Url.Content(
                String.Format("~/image/FitInto/{0}?width={1}&height={2}",
                    MapFileHelper.GetMapFilename(day),
                    layer.Width,
                    layer.Height));
                layer.DivId = "originalSizeMap";

                //pripravim vse potrebno za 3X VELIKO sliko
                var layer1 = new MapLayerViewModel();
                model.Layers.Add(layer1);

                layer1.Width = (int)(3 * width / ratio);
                layer1.Height = (int)(3 * height / ratio);
                layer1.PaddedLeft = (layer1.Width - 3 * dimensions.Width) / 2;
                layer1.PaddedTop = (layer1.Height - 3 * dimensions.Height) / 2;
                layer1.WidthResizeFactor = 3;
                layer1.HeightResizeFactor = 3;

                layer1.ImgUrl = layer.ImgUrl;

            }
            catch (Exception e)
            {
                return Content(e.Message);
            }
            return PartialView("_Map", model);
        }

        public JsonResult ChangeDay(int? day)
        {
            try
            {
                ApplicationSettings.GetCurrentDay(Session, day);
            }
            catch (Exception ee)
            {
                return Json(new SuccessResponse { Message = ee.Message }, JsonRequestBehavior.AllowGet);
            }
            return Json(new SuccessResponse { Ok = true }, JsonRequestBehavior.AllowGet);
        }

    }

    class MapControllerImpl : MapController
    {
    }
}