﻿using System;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Model.Tracks;
using Mrtn.Marvin21.Ui.Mvc.Controllers.Support;
using Mrtn.Marvin21.Ui.Mvc._infrastructure;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Map
{
    public class MapStationController : Controller
    {
        readonly IStationService _stationService = new StationService();

        public ActionResult Index(int? id)
        {
            if (!Request.IsAjaxRequest())
                Response.Redirect(Url.Action("Index", "Map", new { id = id, content = "MapStation" }));
            ViewBag.Days = ApplicationSettings.Days();
            ViewBag.CurrentDay = ApplicationSettings.GetCurrentDay(Session);
            Session["ContentToBeLoadedInMap"] = "MapStation";
            return PartialView("_Index");
        }

        public JsonResult Get()
        {
            var currentDay = ApplicationSettings.GetCurrentDay(Session);
            var queryModel = new StationQueryModel { Day = currentDay };
            var model = _stationService.Get(queryModel);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(string type, float x, float y)
        {
            var response = _stationService.Add(type, x, y, ApplicationSettings.GetCurrentDay(Session));
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                var model = _stationService.Edit(id);
                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }

        [HttpPost]
        public JsonResult Edit(StationEditModel model, bool? zIsOk)
        {
            if (zIsOk.HasValue && zIsOk.Value) model.ZisDirty = false;
            var response = _stationService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TryChangeCoordinates(int id, float x, float y)
        {
            var size = MapFileHelper.GetMapSize(ApplicationSettings.GetCurrentDay(Session));
            x = Math.Max(0, x);
            x = Math.Min(size.Width, x);
            y = Math.Max(0, y);
            y = Math.Min(size.Height, y);
            var response = _stationService.ChangeCoordinates(id, x, y);
            return Json(response, JsonRequestBehavior.AllowGet);
        }



        public JsonResult Lock(int id)
        {
            var response = _stationService.Lock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Unlock(int id)
        {
            var response = _stationService.Unlock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LockAll()
        {
            var response = _stationService.LockAll(ApplicationSettings.GetCurrentDay(Session));
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UnlockAll()
        {
            var response = _stationService.UnlockAll(ApplicationSettings.GetCurrentDay(Session));
            return Json(response, JsonRequestBehavior.AllowGet);
        }



    }
}
