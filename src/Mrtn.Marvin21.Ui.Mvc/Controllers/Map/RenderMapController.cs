﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mrtn.Marvin21.Model.Extenders;
using Mrtn.Marvin21.Bl.Interface.Teams;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services;
using Mrtn.Marvin21.Bl.Services.Teams;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Model.Tracks;
using Mrtn.Marvin21.Ui.Mvc.Controllers.Support;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Map
{
    /// <summary>
    /// Tu so metode, ki vračajo zrisane zemljevide
    /// </summary>
    public class RenderMapController : Controller
    {

        readonly ITrackService _trackService = new TrackService();
        readonly ICategoryService _categoryService = new CategoryService();
        private readonly IStationService _stationService = new StationService();
        private readonly IPathService _pathService = new PathService();

        /// <summary>
        /// Pri poskusu risanja kart za leto 2014 so iz neznanega vzroka vsi fonti postali MNOGO PREVELIKI
        /// Rešujem s tem faktorjem
        //ToDo: ugotoviti razlog!!!
        /// </summary>
        //private const float FontScaleFactor = (float)0.4;//0.25;
        private const float FontScaleFactor = (float)1;

        public ActionResult GetMap(int id)
        {
            var track = _trackService.Get(id);
            if (track == null) throw new HttpException(404, String.Format("map with id={0} not found", id));
            var day = track.Day;

            var physicalPath = MapFileHelper.GetMapPhysicalPath(day);

            Image img = null;
            try
            {
                Debug.Print(String.Format("nalagam {0}", physicalPath));
                img = new Bitmap(physicalPath);
                var height = img.Height;
                var width = img.Width;
                var g = Graphics.FromImage(img);

                #region Risanje KTjev
                foreach (var checkpoint in track.TrackCheckpoints)
                {
                    if (!checkpoint.X.HasValue) continue;
                    if (!checkpoint.Y.HasValue) continue;
                    var x = checkpoint.X.Value;
                    var y = (height - checkpoint.Y.Value);
                    var name = checkpoint.Name;

                    if (checkpoint.StationIsStart)
                    {
                        DrawCircle(g, x, y);
                        var xOffset = AppSettings.RenderMapStartTextXOffset;
                        var yOffset = AppSettings.RenderMapStartTextYOffset;
                        DrawKtMark(g, name, x + xOffset, y - yOffset);
                    }
                    else if (checkpoint.StationIsFinish)
                    {
                        DrawCircle(g, x, y);
                        var xOffset = AppSettings.RenderMapFinishTextXOffset;
                        var yOffset = AppSettings.RenderMapFinishTextYOffset;
                        DrawKtMark(g, name, x + xOffset, y - yOffset);
                    }
                    else if (checkpoint.StationIsHidden)
                    {
                        DrawRectangle(g, x, y);
                        var xOffset = AppSettings.RenderMapKtTextXOffset;
                        var yOffset = AppSettings.RenderMapKtTextYOffset;
                        DrawKtMark(g, name, x + xOffset, y - yOffset);

                    }
                    else
                    {
                        DrawCircle(g, x, y);
                        var xOffset = AppSettings.RenderMapKtTextXOffset;
                        var yOffset = AppSettings.RenderMapKtTextYOffset;
                        DrawKtMark(g, name, x + xOffset, y - yOffset);
                    }

                }

                #endregion

                #region Risanje legende
                var category = _categoryService.Get(track.CategoryId);
                var color = ColorTranslator.FromHtml("#777777");
                if (category.ColorIsValid())
                    color = ColorTranslator.FromHtml(category.DarkerColor());
                var pen = new Pen(color, 6);
                g.DrawRectangle(pen, 3, 3, width - 6, height - 6);


                //narišem legendo
                var top = 0;                //zgoraj
                //top = height - 400;       //spodaj
                var left = 0;               //levo
                left = width - 1000;        //desno
                //Ozadje
                Brush brushBg = new SolidBrush(Color.FromArgb(200,
                ColorTranslator.FromHtml(category.BrighestColor())));
                g.FillRectangle(brushBg, left, top, 1000, 400);
                g.DrawRectangle(pen, left, top, 1000, 400);

                //ime tekmovanja
                Brush brushText = new SolidBrush(ColorTranslator.FromHtml("#000000"));
                var font = new Font("Arial narrow ", 40 * FontScaleFactor);
                g.DrawString(AppSettings.CompetitionName, font, brushText,
                    left + 10, top + 15);


                //kategorija
                font = new Font("Arial", 25 * FontScaleFactor);
                g.DrawString("Kategorija: ", font, brushText,
                    left + 15, top + 115);



                font = new Font("Arial", 45 * FontScaleFactor, FontStyle.Bold);
                var brushTextCategory = new SolidBrush(ColorTranslator.FromHtml(category.DarkestColor()));
                g.DrawString(category.Name, font, brushTextCategory, left + 250, top + 95);

                //Ravnilce
                font = new Font("Arial", 20 * FontScaleFactor);
                var rulerPen = new Pen(Color.Black, 4);
                var xRuller = left + 20;
                var yRuller = top + 240;
                g.DrawLine(rulerPen, xRuller, yRuller + 40,
                    xRuller + (float)(2000 * AppSettings.PixelsPerMeter), yRuller + 40);
                for (var i = 0; i < 5; i++)
                {
                    var x = (float)(xRuller + i * 500 * AppSettings.PixelsPerMeter);
                    g.DrawString((i * 500).ToString(), font, brushText,
                        x - 15 - 8 * i, yRuller - 40);
                    g.DrawLine(rulerPen, x, yRuller, x, yRuller + 40);
                }
                rulerPen = new Pen(Color.Black, 2);
                for (var i = 0; i < 5; i++)
                {
                    var x = (float)(xRuller + i * 100 * AppSettings.PixelsPerMeter);
                    g.DrawLine(rulerPen, x, yRuller + 20, x, yRuller + 40);
                }
                for (var i = 0; i < 20; i++)
                {
                    var x = (float)(xRuller + i * 25 * AppSettings.PixelsPerMeter);
                    g.DrawLine(rulerPen, x, yRuller + 25, x, yRuller + 35);
                }
                //Merilo
                font = new Font("Arial", 30 * FontScaleFactor);
                brushText = new SolidBrush(ColorTranslator.FromHtml("#000000"));
                g.DrawString("Merilo 1:" + AppSettings.MapScale, font,
                    brushText, left + 640, top + 290);
                //ekvidistanca
                font = new Font("Arial", 25 * FontScaleFactor);
                g.DrawString("Ekvidistanca: 10 m",
                    font, brushText, left + 10, top + 290);
                //izohipse
                font = new Font("Arial", 15 * FontScaleFactor);
                g.DrawString("Glavne izohipse: 50 m, pomožne izohipse: 5 m in 2,5 m ",
                    font, brushText, left + 10, top + 360);
                g.DrawString("Tekmovalni dan " + track.Day, font, brushText,
                    left + 780, top + 360);
                #endregion

                //narišem rob okrog karte
                g.DrawRectangle(pen, 3, 3, width - 6, height - 6);

                Debug.Print(String.Format("Vračam progo id={0} za velikost ({1},{2})", id, height, width));
                Response.ContentType = "image/png";
                img.Save(Response.OutputStream, ImageFormat.Png);
                return new EmptyResult();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Debug.Print(String.Format("Disposing id={0} za velikost", id));
                Debug.Print("---------------------");
                if (img != null) img.Dispose();
            }


        }

        private class StationPair
        {
            public int FirstStationId;
            public int SecondStationId;
            public string Color;
            public int No;
        }
        private class Arrow
        {
            public float X1;
            public float X2;
            public float Y1;
            public float Y2;
            public string Color;
            public int No;
            public bool InverseDirection;
            public int MovedBy;

            public void MoveEndpoint(int length, int perpendict)
            {
                var point = GetMovepoint(length, perpendict);

                X2 = X2 + point.X;
                Y2 = Y2 + point.Y;
            }


            public void MoveStartpoint(int length, int perpendict)
            {
                var point = GetMovepoint(length, perpendict);
                X1 = X1 + point.X;
                Y1 = Y1 + point.Y;
            }

            public IEnumerable<Point> GetArrowPoints()
            {
                const int arrowLenght = 25;
                const int arrowHalfWidth = 8;
                var points = new List<Point>();
                if (!InverseDirection)
                {

                    points.Add(new Point((int)X2, (int)Y2));
                    var point = GetMovepoint(-arrowLenght, arrowHalfWidth);
                    points.Add(new Point((int)X2 + point.X, (int)Y2 + point.Y));
                    point = GetMovepoint(-arrowLenght, -arrowHalfWidth);
                    points.Add(new Point((int)X2 + point.X, (int)Y2 + point.Y));
                }
                else
                {
                    points.Add(new Point((int)X1, (int)Y1));
                    var point = GetMovepoint(arrowLenght, arrowHalfWidth);
                    points.Add(new Point((int)X1 + point.X, (int)Y1 + point.Y));
                    point = GetMovepoint(arrowLenght, -arrowHalfWidth);
                    points.Add(new Point((int)X1 + point.X, (int)Y1 + point.Y));
                }
                return points;
            }
            private Point GetMovepoint(int length, int perpendict)
            {
                var point = new Point();
                var deltaX = (X2 - X1);
                var deltaY = (Y2 - Y1);
                var distance = (float)Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
                point.X = (int)(deltaX * length / distance + deltaY * perpendict / distance);
                point.Y = (int)(deltaY * length / distance - deltaX * perpendict / distance);
                return point;
            }
        }


        public ActionResult GetMastercard(int day)
        {

            // naložim postaje
            var stations = _stationService.Get(new StationQueryModel { Day = day });

            //pripravim "puščice"
            var stationPairs = new List<StationPair>();
            var tracks = _trackService.GetTracks(new TrackQueryModel { Day = day });
            foreach (var trackWithCheckpoints in tracks.Select(track => _trackService.Get(track.Id)))
            {
                var category = _categoryService.Get(trackWithCheckpoints.CategoryId);
                var color = category.Color;
                foreach (var trackCheckpoint in trackWithCheckpoints.TrackCheckpoints)
                {
                    if (trackCheckpoint.PathId > 0)
                    {
                        var path = _pathService.Get(trackCheckpoint.PathId);
                        var stationPair = new StationPair
                        {
                            FirstStationId = path.FirstStationId,
                            SecondStationId = path.SecondStationId,
                            Color = color,
                            No = category.No
                        };
                        stationPairs.Add(stationPair);
                    }
                }
            }
            //Poenotenje "bljižnjih" pik (start in cilj
            for (var i = 0; i < stations.Count; i++)
            {
                for (var j = i + 1; j < stations.Count; j++)
                {
                    var station1 = stations[i];
                    var station2 = stations[j];
                    if (!station1.X.HasValue) continue;
                    if (!station1.Y.HasValue) continue;
                    if (!station2.X.HasValue) continue;
                    if (!station2.Y.HasValue) continue;
                    if (DistanceBetween(station1, station2) >= 30) continue;
                    var toChangeFirstPoint = stationPairs
                        .Where(x => x.FirstStationId == station1.Id)
                        .ToList();
                    foreach (var stationPair in toChangeFirstPoint)
                        stationPair.FirstStationId = station2.Id;
                    var toChangeSecondPoint = stationPairs
                        .Where(x => x.SecondStationId == station1.Id)
                        .ToList();
                    foreach (var stationPair in toChangeSecondPoint)
                        stationPair.SecondStationId = station2.Id;
                }
            }


            var physicalPath = MapFileHelper.GetMapPhysicalPath(day);

            Image img = null;
            try
            {
                Debug.Print(String.Format("nalagam {0}", physicalPath));
                img = new Bitmap(physicalPath);
                var height = img.Height;
                var width = img.Width;
                var g = Graphics.FromImage(img);


                #region Risanje puščic

                for (var i = 0; i < stations.Count; i++)
                {
                    for (var j = i + 1; j < stations.Count; j++)
                    {
                        var arrows = new List<Arrow>();
                        var station1 = stations[i];
                        var station2 = stations[j];
                        if (!station1.X.HasValue) continue;
                        if (!station1.Y.HasValue) continue;
                        if (!station2.X.HasValue) continue;
                        if (!station2.Y.HasValue) continue;
                        foreach (var stationPair in stationPairs
                            .Where(x => x.FirstStationId == station1.Id
                                && x.SecondStationId == station2.Id).ToList())
                        {
                            arrows.Add(new Arrow
                            {
                                Color = stationPair.Color,
                                No = stationPair.No,
                                InverseDirection = false,
                                X1 = station1.X.Value,
                                Y1 = station1.Y.Value,
                                X2 = station2.X.Value,
                                Y2 = station2.Y.Value
                            });
                        }
                        foreach (var stationPair in stationPairs
                            .Where(x => x.FirstStationId == station2.Id
                                && x.SecondStationId == station1.Id).ToList())
                        {
                            arrows.Add(new Arrow
                            {
                                Color = stationPair.Color,
                                No = -stationPair.No,
                                InverseDirection = true,
                                X1 = station1.X.Value,
                                Y1 = station1.Y.Value,
                                X2 = station2.X.Value,
                                Y2 = station2.Y.Value
                            });
                        }
                        if (arrows.Count > 0)
                        {
                            arrows = arrows.OrderBy(x => x.No).ToList();
                            for (var k = 0; k < arrows.Count; k++)
                                arrows[k].MovedBy = 1 - arrows.Count + 2 * k;

                            foreach (var arrow in arrows)
                            {
                                arrow.MoveEndpoint(-50, arrow.MovedBy * 8);
                                arrow.MoveStartpoint(50, arrow.MovedBy * 8);
                                var mycolor = Color.FromArgb(
                                    200,
                                    ColorTranslator.FromHtml("#" + arrow.Color));
                                var myPen = new Pen(mycolor, 6);
                                var myBrush = new SolidBrush(mycolor);
                                g.DrawLine(myPen, arrow.X1, height - arrow.Y1, arrow.X2, height - arrow.Y2);
                                var points = arrow
                                    .GetArrowPoints()
                                    .Select(point => new Point(point.X, height - point.Y))
                                    .ToArray();
                                g.FillPolygon(myBrush, points);
                                g.DrawPolygon(myPen, points);
                            }

                            Debug.Print("from " + stations[i].Name + " to " + stations[j].Name + " puščic: " + arrows.Count);
                        }
                    }
                }

                #endregion

                #region Risanje KTjev

                foreach (var station in stations)
                {
                    if (!station.X.HasValue) continue;
                    if (!station.Y.HasValue) continue;
                    var x = station.X.Value;
                    var y = (height - station.Y.Value);

                    if (station.IsStart)
                    {
                        DrawCircle(g, x, y);
                        var xOffset = AppSettings.RenderMapStartTextXOffset;
                        var yOffset = AppSettings.RenderMapStartTextYOffset;
                        DrawSmallKtMark(g, station.ToString(), x + xOffset, y - yOffset);
                    }
                    else if (station.IsFinish)
                    {
                        DrawCircle(g, x, y);
                        var xOffset = AppSettings.RenderMapFinishTextXOffset;
                        var yOffset = AppSettings.RenderMapFinishTextYOffset;
                        DrawSmallKtMark(g, station.ToString(), x + xOffset, y - yOffset);
                    }
                    else if (station.IsHidden)
                    {
                        DrawRectangle(g, x, y);
                        var xOffset = AppSettings.RenderMapKtTextXOffset;
                        var yOffset = AppSettings.RenderMapKtTextYOffset;
                        DrawSmallKtMark(g, station.ToString(), x + xOffset, y - yOffset);

                    }
                    else
                    {
                        DrawCircle(g, x, y);
                        var xOffset = AppSettings.RenderMapKtTextXOffset;
                        var yOffset = AppSettings.RenderMapKtTextYOffset;
                        DrawSmallKtMark(g, station.ToString(), x + xOffset, y - yOffset);
                    }

                }

                #endregion

                #region Risanje legende
                //var category = _categoryService.Get(track.CategoryId);
                var color = ColorTranslator.FromHtml("#777777");
                var pen = new Pen(color, 6);
                g.DrawRectangle(pen, 3, 3, width - 6, height - 6);


                //narišem legendo
                var top = 0;            //zgoraj
                //top = height - 400;   //spodaj
                var left = 0;           //levo
                left = width - 1000;      //desno

                //Ozadje
                Brush brushBg = new SolidBrush(Color.FromArgb(200,
                ColorTranslator.FromHtml("#ffffff")));
                g.FillRectangle(brushBg, left, top, 1000, 400);
                g.DrawRectangle(pen, left, top, 1000, 400);

                //ime tekmovanja
                Brush brushText = new SolidBrush(ColorTranslator.FromHtml("#000000"));
                var font = new Font("Arial narrow ", 40 * FontScaleFactor);
                g.DrawString(AppSettings.CompetitionName, font, brushText,
                    left + 10, top + 15);


                //kategorija
                font = new Font("Arial", 25 * FontScaleFactor);
                g.DrawString("Kategorija: ", font, brushText,
                    left + 15, top + 115);



                font = new Font("Arial", 45 * FontScaleFactor, FontStyle.Bold);
                var brushTextCategory = new SolidBrush(ColorTranslator.FromHtml("#000000"));
                g.DrawString("MasterCard", font, brushTextCategory, left + 250, top + 95);

                //Ravnilce
                font = new Font("Arial", 20 * FontScaleFactor);
                var rulerPen = new Pen(Color.Black, 4);
                var xRuller = left + 20;
                var yRuller = top + 240;
                g.DrawLine(rulerPen, xRuller, yRuller + 40,
                    xRuller + (float)(2000 * AppSettings.PixelsPerMeter), yRuller + 40);
                for (var i = 0; i < 5; i++)
                {
                    var x = (float)(xRuller + i * 500 * AppSettings.PixelsPerMeter);
                    g.DrawString((i * 500).ToString(), font, brushText,
                        x - 15 - 8 * i, yRuller - 40);
                    g.DrawLine(rulerPen, x, yRuller, x, yRuller + 40);
                }
                rulerPen = new Pen(Color.Black, 2);
                for (var i = 0; i < 5; i++)
                {
                    var x = (float)(xRuller + i * 100 * AppSettings.PixelsPerMeter);
                    g.DrawLine(rulerPen, x, yRuller + 20, x, yRuller + 40);
                }
                for (var i = 0; i < 20; i++)
                {
                    var x = (float)(xRuller + i * 25 * AppSettings.PixelsPerMeter);
                    g.DrawLine(rulerPen, x, yRuller + 25, x, yRuller + 35);
                }
                //Merilo
                font = new Font("Arial", 30 * FontScaleFactor);
                brushText = new SolidBrush(ColorTranslator.FromHtml("#000000"));
                g.DrawString("Merilo 1:" + AppSettings.MapScale, font,
                    brushText, left + 640, top + 290);
                //ekvidistanca
                font = new Font("Arial", 25 * FontScaleFactor);
                g.DrawString("Ekvidistanca: 10 m",
                    font, brushText, left + 10, top + 290);
                //izohipse
                font = new Font("Arial", 15 * FontScaleFactor);
                g.DrawString("Glavne izohipse: 50 m, pomožne izohipse: 5 m in 2,5 m ",
                    font, brushText, left + 10, top + 360);
                g.DrawString("Tekmovalni dan " + day, font, brushText,
                    left + 780, top + 360);
                #endregion

                //narišem rob okrog karte
                g.DrawRectangle(pen, 3, 3, width - 6, height - 6);

                Debug.Print(String.Format("Vračam progo dan={0} za velikost ({1},{2})", day, height, width));
                Response.ContentType = "image/png";
                img.Save(Response.OutputStream, ImageFormat.Png);
                return new EmptyResult();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            finally
            {
                Debug.Print(String.Format("Disposing dan={0} za velikost", day));
                Debug.Print("---------------------");
                if (img != null) img.Dispose();
            }


        }

        private static int DistanceBetween(StationViewModel station1, StationViewModel station2)
        {
            var dx = station1.X.GetValueOrDefault() - station2.X.GetValueOrDefault();
            var dy = station1.Y.GetValueOrDefault() - station2.Y.GetValueOrDefault();
            return (int)Math.Sqrt(dx * dx + dy * dy);
        }


        /*************** private stuff ****************/
        private static void DrawSmallKtMark(Graphics g, string name, float x, float y)
        {
            //Pripravim brushe
            Brush brushText = new SolidBrush(Color.FromArgb(
                AppSettings.RenderMapKtTextAlpha,
                ColorTranslator.FromHtml("#" + AppSettings.RenderMapKtTextColor.Replace("#", ""))));
            Brush brushBg = new SolidBrush(Color.FromArgb(
                AppSettings.RenderMapKtTextBgAlpha,
                ColorTranslator.FromHtml("#" + AppSettings.RenderMapKtTextBgColor.Replace("#", ""))));

            //Pripravim font
            var font = new Font(
                AppSettings.RenderMapKtTextFontName,
                AppSettings.RenderMapKtTextFontSize * FontScaleFactor / 2);
            //Naredim ozadje
            var bgOffset = AppSettings.RenderMapKtTextBgOffset;
            if (bgOffset > 0)
            {
                g.DrawString(name, font, brushBg, x + bgOffset, y + bgOffset);
                g.DrawString(name, font, brushBg, x - bgOffset, y + bgOffset);
                g.DrawString(name, font, brushBg, x - bgOffset, y - bgOffset);
                g.DrawString(name, font, brushBg, x + bgOffset, y - bgOffset);
            }
            g.DrawString(name, font, brushText, x, y);
        }


        private static void DrawKtMark(Graphics g, string name, float x, float y)
        {
            //Pripravim brushe
            Brush brushText = new SolidBrush(Color.FromArgb(
                AppSettings.RenderMapKtTextAlpha,
                ColorTranslator.FromHtml("#" + AppSettings.RenderMapKtTextColor.Replace("#", ""))));
            Brush brushBg = new SolidBrush(Color.FromArgb(
                AppSettings.RenderMapKtTextBgAlpha,
                ColorTranslator.FromHtml("#" + AppSettings.RenderMapKtTextBgColor.Replace("#", ""))));

            //Pripravim font
            var font = new Font(
                AppSettings.RenderMapKtTextFontName,
                AppSettings.RenderMapKtTextFontSize * FontScaleFactor);
            //Naredim ozadje
            var bgOffset = AppSettings.RenderMapKtTextBgOffset;
            if (bgOffset > 0)
            {
                g.DrawString(name, font, brushBg, x + bgOffset, y + bgOffset);
                g.DrawString(name, font, brushBg, x - bgOffset, y + bgOffset);
                g.DrawString(name, font, brushBg, x - bgOffset, y - bgOffset);
                g.DrawString(name, font, brushBg, x + bgOffset, y - bgOffset);
            }
            g.DrawString(name, font, brushText, x, y);
        }
        private static void DrawCircle(Graphics g, float x, float y)
        {
            // Iz nastavitev potegne dimenzije in barvo za oznako KT-ja
            var radius = (float)AppSettings.RenderMapKtMarkDiameter;
            var centerRadius = (float)AppSettings.RenderMapKtMarkCenterDiameter;
            var pen = new Pen(Color.FromArgb(
                AppSettings.RenderMapKtMarkAlpha,
                ColorTranslator.FromHtml("#" + AppSettings.RenderMapKtMarkColor.Replace("#", ""))),
                              AppSettings.RenderMapKtMarkLineWidth);

            // Nariše vse skup
            g.DrawEllipse(pen, x - radius / 2, y - radius / 2, radius, radius);
            if (centerRadius > 0)
                g.DrawEllipse(pen, x - centerRadius / 2, y - centerRadius / 2, centerRadius, centerRadius);
        }
        private static void DrawRectangle(Graphics g, float x, float y)
        {
            // Iz nastavitev potegne dimenzije in barvo za oznako KT-ja
            var dimension = (float)(AppSettings.RenderMapKtHiddenRectangleDimension * AppSettings.PixelsPerMeter);
            var pen = new Pen(Color.FromArgb(
                AppSettings.RenderMapKtMarkAlpha,
                ColorTranslator.FromHtml("#" + AppSettings.RenderMapKtMarkColor.Replace("#", ""))),
                              AppSettings.RenderMapKtMarkLineWidth);

            // Nariše vse skup
            g.DrawRectangle(pen, x - dimension / 2, y - dimension / 2, dimension, dimension);
        }



    }
}
