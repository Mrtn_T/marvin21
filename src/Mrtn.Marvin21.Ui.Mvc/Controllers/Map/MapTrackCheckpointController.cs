﻿using System;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Map
{
    public class MapTrackCheckpointController : Controller
    {
        readonly ITrackCheckpointService _trackCheckpointService = new TrackCheckpointService();

        public JsonResult Add(int id, int stationId)
        {
            var model = _trackCheckpointService.Add(id, stationId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult MoveUp(int id)
        {
            //id= id checkopint
            //kasneje premakni v svoj kontroler
            var model = _trackCheckpointService.MoveUp(id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult MoveDown(int id)
        {
            //id= id checkopint
            //kasneje premakni v svoj kontroler
            var model = _trackCheckpointService.MoveDown(id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Delete(int id)
        {
            //id= id checkopint
            //kasneje premakni v svoj kontroler
            var model = _trackCheckpointService.Delete(id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Lock(int id)
        {
            var response = _trackCheckpointService.Lock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Unlock(int id)
        {
            var response = _trackCheckpointService.Unlock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                var model = _trackCheckpointService.Edit(id);
                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }

        [HttpPost]
        public JsonResult Edit(TrackCheckpointEditModel model)
        {
            var response = _trackCheckpointService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }


    }
}
