﻿using System;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Map
{
    public class MapPathPointController : Controller
    {
        readonly IPathPointService _pathPointService = new PathPointService();

        public JsonResult Add(int pathId, int putAfter, float x, float y)
        {
            var response = _pathPointService.Add(pathId, putAfter, x, y);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult Move(int id, float x, float y)
        {
            var response = _pathPointService.Move(id, x, y);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int id)
        {
            var response = _pathPointService.Delete(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        
        [HttpGet]
        public ActionResult Edit(int id)
        {
            try
            {
                var model = _pathPointService.Edit(id);
                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }

        [HttpPost]
        public JsonResult Edit(PathPointEditModel model, bool? zIsOk)
        {
            if (zIsOk.HasValue && zIsOk.Value) model.MapPositionZisDirty = false;
            var response = _pathPointService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        public JsonResult LockAll(int pathId)
        {
            var response = _pathPointService.LockAll( pathId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UnlockAll(int pathId)
        {
            var response = _pathPointService.UnlockAll( pathId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Lock(int id)
        {
            var response = _pathPointService.Lock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Unlock(int id)
        {
            var response = _pathPointService.Unlock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
