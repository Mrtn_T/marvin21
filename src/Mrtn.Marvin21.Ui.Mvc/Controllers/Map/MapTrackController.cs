﻿using System.Web.Mvc;
using Mrtn.Marvin21.Ui.Mvc.Controllers.Support;
using Mrtn.Marvin21.Bl.Interface.Tracks;
using Mrtn.Marvin21.Bl.Services.Tracks;
using Mrtn.Marvin21.Model.Tracks;
using Mrtn.Marvin21.Ui.Mvc._infrastructure;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers.Map
{
    public class MapTrackController : Controller
    {
        readonly ITrackService _trackService = new TrackService();

        /// <summary>
        /// Vrača glavno masko, v kateri se potem vse ureja
        /// </summary>
        /// <returns></returns>
        public ActionResult Index(int? id)
        {
            if (!Request.IsAjaxRequest()) 
                Response.Redirect(Url.Action("Index", "Map", new { id = id, content="MapTrack" }));
            ViewBag.Days = ApplicationSettings.Days();
            ViewBag.CurrentDay = ApplicationSettings.GetCurrentDay(Session);
            Session["ContentToBeLoadedInMap"] = "MapTrack";
            return PartialView("_Index");
        }

        /// <summary>
        /// Vrne vse že definirane poti za določen dan
        /// </summary>
        /// <returns></returns>
        public JsonResult GetTracks()
        {
            var currentDay = ApplicationSettings.GetCurrentDay(Session);
            var queryModel = new TrackQueryModel { Day = currentDay };
            var model = _trackService.GetTrackForEachCategorByDay(queryModel);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Vrne podrobnosti o določeni poti: TrackWithCheckpointsViewModel
        /// </summary>
        /// <returns></returns>
        public JsonResult Get(int id)
        {
            if (id == 0)
                return Json(null, JsonRequestBehavior.AllowGet);
            var model = _trackService.Get(id);
            foreach (var trackCheckpoint in model.TrackCheckpoints)
            {
                trackCheckpoint.Tasks = null;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Ustvari pot za določeno kategorijo za določen dan
        /// </summary>
        /// <returns></returns>
        public JsonResult Add(int categoryId)
        {
            var currentDay = ApplicationSettings.GetCurrentDay(Session);
            var model = _trackService.Add(categoryId, currentDay);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Lock(int id)
        {
            var response = _trackService.Lock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Unlock(int id)
        {
            var response = _trackService.Unlock(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}
