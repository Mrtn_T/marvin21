﻿using System;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Teams;
using Mrtn.Marvin21.Bl.Services.Teams;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers
{
    public class TeamController : Controller
    {

        readonly ITeamService _teamService = new TeamService();

        public ActionResult Index(TeamQueryModel queryModel)
        {
            if (queryModel == null) queryModel = new TeamQueryModel();
            ViewBag.DataForFilters = _teamService.GetDataForFilters();
            return View(queryModel);
        }
        public ActionResult GetTeams(TeamQueryModel queryModel)
        {
            if (queryModel == null) queryModel = new TeamQueryModel();
            queryModel.ShowInactive = GetShowInactiveTeamFromSession();
            var model = _teamService.Get(queryModel);
            ViewBag.InactiveCount = _teamService.GetInactiveTeamsCount();
            ViewBag.ShowInactive = GetShowInactiveTeamFromSession();
            return PartialView("_List", model);
        }


        [HttpGet]
        public ActionResult Edit(int? id)
        {
            try
            {
                var model = _teamService.Edit(id);
                return PartialView("_Edit", model);
            }
            catch (Exception ee)
            {
                return PartialView("exception", ee);
            }
        }

        [HttpPost]
        public JsonResult Edit(TeamEditModel model)
        {
            var response = _teamService.Save(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Deactivate(int id)
        {
            var response = _teamService.Deactivate(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Activate(int id)
        {
            var response = _teamService.Activate(id);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ShowInactive(bool show)
        {
            Session["ShowInactiveTeams"] = show;
            return Json(new SuccessResponse { Ok = true }, JsonRequestBehavior.AllowGet);
        }

        /*
        public ActionResult GetCategories(bool active)
        {
            var queryModel = new PackQueryModel { Active = active };
            var model = _teamService.Get(queryModel);
            if (active) return PartialView("_IndexActive", model);
            return PartialView("_IndexInactive", model);
        }


*/

        private bool GetShowInactiveTeamFromSession()
        {

            if (Session["ShowInactiveTeams"] == null)
                Session["ShowInactiveTeams"] = true;
            return (bool)Session["ShowInactiveTeams"];
        }

    }

}
