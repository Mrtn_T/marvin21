﻿using System;
using System.Web.Mvc;
using Mrtn.Marvin21.Bl.Interface.Teams;
using Mrtn.Marvin21.Bl.Services.Teams;
using Mrtn.Marvin21.Model.Common;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Ui.Mvc.Controllers
{
    public class TeamSpreadsheetController : Controller
    {

        readonly ITeamService _teamService = new TeamService();
        //
        // GET: /TeamSperadsheet/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult EditSpreadsheet()
        {
            var queryModel = new TeamQueryModel { ShowInactive = GetShowInactiveTeamFromSession() };
            var model = _teamService.Get(queryModel);
            ViewBag.InactiveCount = _teamService.GetInactiveTeamsCount();
            ViewBag.ShowInactive = GetShowInactiveTeamFromSession();
            return PartialView("_Spreadsheet", model);
        }

        public JsonResult TrySaveCell(int id, string no, string siCode)
        {
            try
            {
                var response= new SuccessResponse(false,"Nobena lastnost ni bila nastavljena");
                if (siCode != null)
                {
                    response = _teamService.SetSiCode(id, siCode);
                }
                if (no != null)
                {
                    int? intNo = null;
                    no = no.Trim();
                    if (no != "")
                    {
                        int i;
                        if (Int32.TryParse(no, out i))
                            intNo = i;
                        else
                            return Json(new SuccessResponse { Ok = false, Message = "Vnesti je potrebno številko" }, JsonRequestBehavior.AllowGet);
                    }
                    response = _teamService.SetNo(id, intNo);
                }
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ee)
            {
                return Json(new SuccessResponse
                {
                    Ok = false,
                    Message = ee.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        private bool GetShowInactiveTeamFromSession()
        {

            if (Session["ShowInactiveTeams"] == null)
                Session["ShowInactiveTeams"] = true;
            return (bool)Session["ShowInactiveTeams"];
        }
    }
}
