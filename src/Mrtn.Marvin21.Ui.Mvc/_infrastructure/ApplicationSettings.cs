﻿using System;
using System.Web;
using Mrtn.Marvin21.Bl.Interface.Support;
using Mrtn.Marvin21.Bl.Services.Support;

namespace Mrtn.Marvin21.Ui.Mvc._infrastructure
{
    public static class ApplicationSettings
    {
        public static int Days()
        {
            ISettingsService checkpointService = new SettingsService();
            return checkpointService.GetDays();
        }
        public static int GetCurrentDay(HttpSessionStateBase session, int? day)
        {
            var currentDay = 1;
            var sessionDay = session["CurrentDay"];
            if (sessionDay != null)
                Int32.TryParse(sessionDay.ToString(), out currentDay);
            if (day.HasValue /*&& day.Value>0*/) currentDay = day.Value;
            session["CurrentDay"] = currentDay;
            return currentDay;
        }

        public static int GetCurrentDay(HttpSessionStateBase session)
        {
            return GetCurrentDay(session,null);
        }
    }
}