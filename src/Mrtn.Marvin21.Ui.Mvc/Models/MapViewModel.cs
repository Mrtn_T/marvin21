﻿using System.Collections.Generic;
using System.Text;

namespace Mrtn.Marvin21.Ui.Mvc.Models
{
    public class MapViewModel
    {
        public MapLayerViewModel BasicLayer = new MapLayerViewModel();
        public List<MapLayerViewModel> Layers = new List<MapLayerViewModel>();

        /// <summary>
        /// Dejanska višina ORIGINALNEGA zemljevida, kot je  na disku
        /// </summary>
        public int OriginalImageHeight;

        /// <summary>
        /// Dejanska širina ORIGINALNEGA zemljevida, kot je  na disku
        /// </summary>
        public int OriginalImageWidth;

    }
}