﻿using System.Text;

namespace Mrtn.Marvin21.Ui.Mvc.Models
{
    public class MapLayerViewModel
    {
/*
<div id="viewport">
    <div id="<DivId>"                                   <-- ta ima DivId pri najmanjšem zemljevidu
                class="mapLayer mapObjectsDiv" ...>     <-- TU so pripeti podatki
        basic layer
    </div>
    ...
    <div id="<DivId>"                                   <-- ta ima DivId pri ostalih zemljevidih
                class="mapLayer" ...>                   <-- TU so pripeti podatki
        <img ... >
        <div id="<InnerDivId>"                          <-- ta ima InnerDivId pri ostalih zemljevidih
                class="mapcontent mapObjectsDiv" ... >
            other layers  
        </div>
    </div>
    ...
</div>
*/
        /// <summary>
        /// URL slike
        /// </summary>
        public string ImgUrl;

        /// <summary>
        /// Višina slike (zemljevid + paddingi) v pikslih 
        /// </summary>
        public int Height;

        /// <summary>
        /// Širina slike (zemljevid + paddingi) v pikslih 
        /// </summary>
        public int Width;

        /// <summary>
        /// Umik zemljevida na sliki v levo
        /// </summary>
        public int PaddedLeft;

        /// <summary>
        /// Umik zemljevida na sliki navzdol
        /// </summary>
        public int PaddedTop;

        /// <summary>
        /// Faktor povečave/pomanjšave tega layerja po širini
        /// </summary>
        public double WidthResizeFactor;

        /// <summary>
        /// Faktor povečave/pomanjšave tega layerja po širini
        /// </summary>
        public double HeightResizeFactor;

        /// <summary>
        /// Id diva, ki ima class "mapLayer"
        /// To so divi, ki OVIJAJO vsak layer zemljevida
        /// </summary>
        public string DivId;

        /// <summary>
        /// se (še) ne uporablja
        /// </summary>
        public string InnerDivId;


    }
}