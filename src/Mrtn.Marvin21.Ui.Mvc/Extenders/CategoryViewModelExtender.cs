﻿using System;
using System.Text;
using System.Web;
using Mrtn.Marvin21.Model.Extenders;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Ui.Mvc.Extenders
{
    public static class CategoryViewModelExtender
    {
        public static HtmlString ColorSamples(this CategoryViewModel model)
        {
            var sb = new StringBuilder();
            if (model.ColorIsValid())
            {
                sb.Append(
                    String.Format(
                        "<div style=' margin-left: 10px; width:20px;height:20px;border:1px solid black; float:left; background-color: {0}'></div> &nbsp;", model.DarkestColor()));
                sb.Append(
                    String.Format(
                        "<div style=' margin-left: 10px; width:20px;height:20px;border:1px solid black; float:left; background-color: {0}'></div> &nbsp;", model.DarkerColor()));
                sb.Append(
                    String.Format(
                        "<div style=' margin-left: 10px; width:20px;height:20px;border:1px solid black; float:left; background-color: {0}'></div> &nbsp;", model.ThisColor()));
                sb.Append(
                    String.Format(
                        "<div style=' margin-left: 10px; width:20px;height:20px;border:1px solid black; float:left; background-color: {0}'></div> &nbsp;", model.BrighterColor()));
                sb.Append(
                    String.Format(
                        "<div style=' margin-left: 10px; width:20px;height:20px;border:1px solid black; float:left; background-color: {0}'></div> &nbsp;", model.BrighestColor()));
            }

            return new HtmlString(sb.ToString());
        }


    }
}