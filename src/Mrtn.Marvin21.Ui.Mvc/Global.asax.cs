﻿using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Routing;
using Mrtn.Marvin21.Bl;

namespace Mrtn.Marvin21.Ui.Mvc
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // simple route for image resizing
            // URL example: /imageResize/imagename.png?width=122&height=155
            // it distorts image!
            routes.MapRoute(
                "ImageResize", // Route name
                "imageResize/{filename}", // URL with parameters
                new { controller = "Image", action = "GetImage" }
            );

            /*
             * public ActionResult FitInto(
            string filename,
            int? width, int? height,
            string verticalAlign, string horizontalAlign,
            string bgColor)*/

            // more desicated route for image resizing
            // URL example: /image/<action>/imagename.png?parameters
            routes.MapRoute(
                "Image", // Route name
                "imageFitInto/{width}/{height}/{filename}", // URL with parameters
                new { controller = "Image", action = "FitInto" }
            );

            // more desicated route for image resizing
            // URL example: /image/<action>/imagename.png?parameters
            routes.MapRoute(
                "ImageFitInto", // Route name
                "image/{action}/{filename}", // URL with parameters
                new { controller = "Image" }
            );

            routes.MapRoute(
                "FileDownload", // Route name
                "{controller}/{action}/{filename}.{extension}", // URL with parameters
                new {} // Parameter defaults
            );
            //Standard route
            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );

        }

        protected void Application_Start()
        {
            Initalizer.InitalizeDatabase();
            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
        }
    }
}