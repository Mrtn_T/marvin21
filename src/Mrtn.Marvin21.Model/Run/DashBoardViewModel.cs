﻿using System.Collections.Generic;

namespace Mrtn.Marvin21.Model.Run
{
    public class DashBoardViewModel
    {
        public List<DashBoardRowViewModel> Rows { get; set; }
        public List<DashBoardHeaderViewModel> Headers { get; set; }

    }
}