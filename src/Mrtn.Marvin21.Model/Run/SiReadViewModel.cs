﻿using System;
using System.Collections.Generic;

namespace Mrtn.Marvin21.Model.Run
{
    public class SiReadViewModel
    {
        public int Id { get; set; }

        public bool Locked { get; set; }

        public int Day { get; set; }
        public string SiCode { get; set; }
        public string Clear { get; set; }
        public string Check { get; set; }
        public string Start { get; set; }
        public string Finish { get; set; }
        public string ReadAt { get; set; }

        public bool HasBeenProcessed { get; set; }
        public virtual List<SiPunchViewModel> SiPunches { get; set; }

    }
}