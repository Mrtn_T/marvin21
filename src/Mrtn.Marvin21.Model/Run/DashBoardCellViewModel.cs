﻿using System;

namespace Mrtn.Marvin21.Model.Run
{
    public class DashBoardCellViewModel
    {

        /// <summary>
        /// Pove da tu sploh ne bo rezultatov. 
        /// Bodisi da gre za progo, ik je ta ekipa (kategorija?) ta dan nima, 
        /// ali pa gre zua nalogo, ki je ta ekipa (kategorija?) ne opravlja
        /// </summary>
        public bool HasNoEntry { get; set; }

        /// <summary>
        /// Tu bodo morali biti rezultati pred zaključkom, vendar jih še ni
        /// </summary>
        public bool NoResultYet { get; set; }

        public int CheckpointsFound { get; set; }
        public int PointsForCheckpoints { get; set; }
        public int PointsForTime { get; set; }
        public int TrackPoints { get; set; }
        public int SpeedTrialPoints { get; set; }

        public bool Locked { get; set; }
        public int TaskPoints { get; set; }

        public bool IsTrack { get; set; }

        public int RunTrackId { get; set; }
        public int Day { get; set; }
        public int TeamId { get; set; }
        public string StartPredicted { get; set; }
    }
}