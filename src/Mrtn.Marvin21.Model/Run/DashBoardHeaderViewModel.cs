﻿using System;

namespace Mrtn.Marvin21.Model.Run
{
    public class DashBoardHeaderViewModel
    {
        public bool IsTrack { get; set; }
        public string Name { get; set; }
        public int Day { get; set; }

        public int TaskId { get; set; }

        public string Location { get; set; }
        public bool Sum { get; set; }
    }
}