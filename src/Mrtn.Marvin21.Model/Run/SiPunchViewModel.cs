﻿using System;

namespace Mrtn.Marvin21.Model.Run
{
    public class SiPunchViewModel
    {
        public int Id { get; set; }
        public bool Locked { get; set; }

        public int No { get; set; }
        public string SiCode { get; set; }
        public string Time { get; set; }

        public bool IsValid { get; set; }
        public String ValidFor { get; set; }


    }
}