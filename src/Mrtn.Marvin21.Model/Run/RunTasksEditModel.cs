﻿using System.Collections.Generic;
using Mrtn.Marvin21.Model.Tasks;

namespace Mrtn.Marvin21.Model.Run
{
    public class RunTasksEditModel
    {
        public TaskViewModel Task { get; set; }
        public List<RunTaskEditModel> EditModels { get; set; }
    }
}