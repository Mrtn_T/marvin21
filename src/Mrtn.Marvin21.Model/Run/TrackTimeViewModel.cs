﻿using System;

namespace Mrtn.Marvin21.Model.Run
{
    public class TrackTimeViewModel
    {
        public String Predicted { get; set; }
        public string MaxPredicted { get; set; }
        public String Manual { get; set; }
        public String FromSi { get; set; }

    }
}