﻿using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Model.Run
{
    public class RunTaskEditModel
    {
        public int Id { get; set; }
        public bool Locked { get; set; }


        public TeamViewModel Team { get; set; }

        public string FirstEntry { get; set; }
        public string SecondEntry { get; set; }
        public int Points { get; set; }

    }
}