﻿using System.Collections.Generic;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Model.Run
{
    public class DashBoardRowViewModel
    {
        public TeamViewModel Team { get; set; }
        public List<DashBoardCellViewModel> Cells { get; set; }
    }
}