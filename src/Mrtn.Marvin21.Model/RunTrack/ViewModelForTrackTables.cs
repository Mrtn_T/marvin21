﻿using System.Collections.Generic;

namespace Mrtn.Marvin21.Model.RunTrack
{
    public class ViewModelForTrackTables
    {
        public List<ViewModelForTrackTable> Tables= new List<ViewModelForTrackTable>();
    }
    public class ViewModelForTrackTable
    {
        public int? No{ get; set; }
        public string CategoryName { get; set; }
        public string CategoryAbbr { get; set; }
        public string PredictedStart { get; set; }
        public List<RunTrackCheckpointViewModel> RunTrackCheckPoints = new List<RunTrackCheckpointViewModel>();
    }
    public class StationDetailsViewModel
    {
        public string Name { get; set; }
        public int Day { get; set; }
        public int DefaultPoints { get; set; }
        public string Description { get; set; }
        public bool IsStart { get; set; }
        public bool IsFinish { get; set; }
        public bool IsHidden { get; set; }
        public string SiCodeArrival { get; set; }
        public string SiCodeDeparture { get; set; }
        public List<StationDetailsLineViewModel> Lines { get; set; }
    }
    public class StationDetailsLineViewModel
    {
        public int? TeamNo{ get; set; }
        public string CategoryAbbr { get; set; }
        public string CheckPointName { get; set; }
        public string Arrival { get; set; }
        public string ArrivalMax { get; set; }
    }

}