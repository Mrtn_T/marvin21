﻿using System.ComponentModel;

namespace Mrtn.Marvin21.Model.RunTrack.Support
{
    public class ManualDepartureEditModel
    {
        public int Id { get; set; }
        [DisplayName("Čas odhoda s starta ali KT")]
        public string Time { get; set; }
    }
}