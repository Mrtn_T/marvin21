﻿using System.ComponentModel;

namespace Mrtn.Marvin21.Model.RunTrack.Support
{
    public class ConfirmManualyEditModel
    {
        public int Id { get; set; }
        [DisplayName("Točka je ročno potrjena")]
        public bool IsConfirmedManualy { get; set; }
        [DisplayName("Razlog zavrnitve")]
        public string IsConfirmedManualyReason { get; set; }
    }
}