﻿using System;
using System.ComponentModel;

namespace Mrtn.Marvin21.Model.RunTrack.Support
{
    public class RejectedManualyEditModel
    {
        public int Id { get; set; }
        [DisplayName("Točka je ročno zavrnjena")]
        public bool IsRejectedManualy { get; set; }
        [DisplayName("Razlog zavrnitve")]
        public string IsRejectedManualyReason { get; set; }
    }
}