﻿using System;
using System.ComponentModel;

namespace Mrtn.Marvin21.Model.RunTrack.Support
{
    public class PredictedStartTimeEditModel
    {
        public int Id { get; set; }

        [DisplayName("Planiran start ekipe")]
        public string  PredictedStrat { get; set; }
    }
}