﻿using System;
using System.ComponentModel;

namespace Mrtn.Marvin21.Model.RunTrack.Support
{
    public class ManualArrivalEditModel
    {
        public int Id { get; set; }
        [DisplayName("Čas prihoda na KT ali cilj")]
        public string Time { get; set; }
    }
}