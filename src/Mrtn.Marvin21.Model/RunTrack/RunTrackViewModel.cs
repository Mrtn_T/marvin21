﻿using System.Collections.Generic;
using Mrtn.Marvin21.Model.Run;
using Mrtn.Marvin21.Model.Teams;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Model.RunTrack
{
    public class RunTrackViewModel
    {
        public int Id;
        public bool Locked;

        //kam je vezan rezultat na progi
        public  int Day { get; set; }
        public TeamViewModel Team { get; set; }
        public TrackViewModel Track { get; set; }
        public CategoryViewModel Category { get; set; }

        //njegovi punchi
        public  List<RunTrackCheckpointViewModel> RunTrackCheckpoints { get; set; }

        //časi 
        public TrackTimeViewModel StartTime { get; set; }
        public TrackTimeViewModel FinishTime { get; set; }
        public string TimeForPath { get; set; }
        public string TimeDelayForPath { get; set; }
        public string TimeForTask { get; set; }
        public string TimeForWait { get; set; }
        public string TimeOnTrack { get; set; }

        //rezultat s proge
        public  int CheckpointsFound { get; set; }
        public  int PointsForCheckpoints { get; set; }
        public  int PointsForTime { get; set; }
        public  int TrackPoints { get; set; }

        //hitrostna etapa
        public TrackTimeViewModel SpeedTrialStart { get; set; }
        public TrackTimeViewModel SpeedTrialFinish { get; set; }
        public string SpeedTrialTimeOnTrack { get; set; }
        public string SpeedTrialTimeDelay { get; set; }
        public int SpeedTrialPoints { get; set; }

        //referenca na SI branje...
        public SiReadViewModel SiRead { get; set; }

    }
}