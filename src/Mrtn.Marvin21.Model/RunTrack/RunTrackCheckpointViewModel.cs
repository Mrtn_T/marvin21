﻿using System;
using System.Collections.Generic;
using Mrtn.Marvin21.Model.Run;
using Mrtn.Marvin21.Model.Tasks;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Model.RunTrack
{
    public class RunTrackCheckpointViewModel
    {
        public int Id { get; set; }
        public bool Locked { get; set; }
        public int No { get; set; }

        // to se uporablja, če ekipi KT skenslamo (zavrnemo) npr v primeru goljufanja.
        // to ima absolutno prednost, če je KT zavrnjena, se ostalo ne upošteva pri izračunu.
        public bool IsRejectedManualy { get; set; }
        public string IsRejectedManualyReason { get; set; }

        // točka je ročno priznana
        // denimo če odpove Si sistem
        // druga najvišja prioriteta: če je ročno potrjena, se šteje razen če je bila eksplicitno zavrnjena
        public bool IsConfirmedManualy { get; set; }
        public string IsConfirmedManualyReason { get; set; }

        public TrackTimeViewModel Arrival { get; set; }
        public TrackTimeViewModel Departure { get; set; }
        public string TimeOnPath { get; set; }
        public string TimeWaitHere { get; set; }
        public string TimeTotal { get; set; }
        
        public bool IsConfirmedByTask { get; set; }
        public  bool IsConfirmedByArrival { get; set; }


        /// <summary>
        /// točke, ki jih priznamo za ta KT
        /// </summary>
        public int Points { get; set; }

        public  int? SiPunchArrivalId { get; set; }
        public  int? SiPunchDepartureId { get; set; }

        public TrackCheckpointViewModel TrackCheckpoint { get; set; }
    }
}