﻿using System.Collections.Generic;
using Mrtn.Marvin21.Model.Tracks;

namespace Mrtn.Marvin21.Model.Tasks
{
    public class TaskViewModel
    {
        public int Id { get; set; }
        public bool Locked { get; set; }

        // Podatki o sami nalogi
        public string Name { get; set; }
        public string Abbr { get; set; }
        public string Description { get; set; }

        // Podatki tem, kdaj in kje se naloga izvaja
        public int Day { get; set; }
        public bool BeforeStart { get; set; }
        public bool OnTrack { get; set; }
        public bool AfterFinish { get; set; }
        public StationViewModel Station { get; set; }

        // Kaj se vnaša
        public string FirstEntryName { get; set; }
        public string FirstEntryMin { get; set; }
        public string FirstEntryMax { get; set; }
        public string FirstEntryDefault { get; set; }

        public string SecondEntryName { get; set; }
        public string SecondEntryMin { get; set; }
        public string SecondEntryMax { get; set; }
        public string SecondEntryDefault { get; set; }
        public bool SecondEntryIsTime { get; set; }

        // Algoritem za računanje točk. 0 pomeni preprost vnos!
        public int CalculatingAlgorythmId { get; set; }

        public Dictionary<int, string> Categories { get; set; }
        public Dictionary<int, string> LockedCategories { get; set; }

        public string TimeForTaskString { get; set; }

        /// <summary>
        /// Martin, 8.1.2020:
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        public virtual int MaxPoints { get; set; }

    }
}
