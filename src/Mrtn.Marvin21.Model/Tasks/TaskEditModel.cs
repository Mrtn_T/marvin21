﻿using System.Collections.Generic;
using System.ComponentModel;
using Mrtn.Marvin21.Model.Common;

namespace Mrtn.Marvin21.Model.Tasks
{
    public class TaskEditModel
    {
        public int Id { get; set; }

        // Podatki o sami nalogi
        [DisplayName("Ime naloge")]
        public string Name { get; set; }
        [DisplayName("Kratica")]
        public string Abbr { get; set; }
        [DisplayName("Opis")]
        public string Description { get; set; }

        // Podatki tem, kdaj in kje se naloga izvaja
        [DisplayName("Dan izvajanja naloge")]
        public int Day { get; set; }
        public  bool BeforeStart { get; set; }
        public  bool OnTrack { get; set; }
        public  bool AfterFinish { get; set; }

        // Kaj se vnaša
        [DisplayName("Ime vnosa")]
        public string FirstEntryName { get; set; }
        [DisplayName("Min vrednost")]
        public int FirstEntryMin { get; set; }
        [DisplayName("Max vrednost")]
        public int FirstEntryMax { get; set; }
        [DisplayName("Privzeta vr.")]
        public int FirstEntryDefault { get; set; }

        [DisplayName("Ime vnosa")]
        public string SecondEntryName { get; set; }
        [DisplayName("Min vrednost")]
        public int SecondEntryMin { get; set; }
        [DisplayName("Max vrednost")]
        public int SecondEntryMax { get; set; }
        [DisplayName("Privzeta vr.")]
        public int SecondEntryDefault { get; set; }
        [DisplayName("Ali je čas?")]
        public bool SecondEntryIsTime { get; set; }

        // Algoritem za računanje točk. 0 pomeni preprost vnos!
        [DisplayName("Algoritem za izračun")]
        public int CalculatingAlgorythmId { get; set; }


        // Seznam točk(postaj) na podani dan, za combo.
        // Če bo uporabnik zamenjal dan, potem se to naloži na novo
        public List<NamedEntity> Stations { get; set; }

        // Trenutno izbrana točka
        public int StationId { get; set; }

        //Seznam vseh kategorij, za izbirnik 
        public List<NamedEntity> CategoryList { get; set; }

        //Seznam trenutno izbranih kategorij, ločenih z vejicami
        public List<int> Categories { get; set; }


        [DisplayName("Čas za nalogo (format m:ss")]
        public string TimeForTaskString { get; set; }



        /// <summary>
        /// Martin, 8.1.2020:
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        [DisplayName("Največ točk")]
        public virtual int MaxPoints { get; set; }


    }
}
