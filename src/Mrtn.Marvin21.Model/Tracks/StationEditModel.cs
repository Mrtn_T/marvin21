﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mrtn.Marvin21.Model.Tracks
{
    public class StationEditModel
    {
        public int Id { get; set; }

        [DisplayName("Ime točke - avtomatsko")]
        [Required(ErrorMessage = "Polje je obvezno.")]
        public string Name { get; set; }

        [DisplayName("Si koda prihod")]
        public string SiCodeArrival { get; set; }

        [DisplayName("Si koda odhod")]
        public string SiCodeDeparture { get; set; }

        [DisplayName("Start?")]
        public bool IsStart { get; set; }

        [DisplayName("Cilj?")]
        public bool IsFinish { get; set; }

        [DisplayName("Živa KT?")]
        public bool IsAlive { get; set; }

        [DisplayName("Nadmorska višina (odčitaj s karte)")]
        public float? Z { get; set; }

        [DisplayName("Opombe (kdo je na piki, GSM,...)")]
        public string Description { get; set; }

        [DisplayName("Tekmovalni dan")]
        public int Day { get; set; }
        public bool ZisDirty { get; set; }

        [DisplayName("Skrita (ranjenec)")]
        public bool IsHidden { get; set; }

        public int DefaultPoints { get; set; }
    }
}
