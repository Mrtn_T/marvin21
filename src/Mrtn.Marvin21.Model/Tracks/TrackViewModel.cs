﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mrtn.Marvin21.Model.Tracks
{
    public class TrackViewModel
    {
        public int Id { get; set; }
        //public bool IsManual { get; set; }
        public bool Locked { get; set; }

        public int CategoryId { get; set; }
        public int CategoryNo { get; set; }
        public string CategoryAbbr { get; set; }

        public float? CumulativeDistance { get; set; }
        public float? CumulativeDownhill { get; set; }

        public float? CumulativeUphill { get; set; }
        public string OptimalTime{ get; set; }
        public string OptimalPathTime { get; set; }

        public int TrackCheckpointsCount { get; set; }

        public int Day { get; set; }

        public string MinTimeForSpeedTrial{ get; set; }


        //public int FirstStationId { get; set; }
        //public float? FirstStationX { get; set; }
        //public float? FirstStationY { get; set; }
        //public float? FirstStationZ { get; set; }
        //public bool FirstStationZisDirty { get; set; }
        //public string FirstStationName { get; set; }

        //public int SecondStationId { get; set; }
        //public float? SecondStationX { get; set; }
        //public float? SecondStationY { get; set; }
        //public float? SecondStationZ { get; set; }
        //public bool SecondStationZisDirty { get; set; }
        //public string SecondStationName { get; set; }

        //public int? AutomaticDataDistance { get; set; }
        //public string AutomaticDataZlist { get; set; }
        //public int AutomaticDataDownhill { get; set; }
        //public int AutomaticDataUphill { get; set; }
        //public bool AutomaticDataZisDirty { get; set; }

        //public int? ManualDataDistance { get; set; }
        //public string ManualDataZlist { get; set; }
        //public int ManualDataDownhill { get; set; }
        //public int ManualDataUphill { get; set; }
        //public bool ManualDataZisDirty { get; set; }

        //public int? ValidDataDistance { get; set; }
        //public string ValidDataZlist { get; set; }
        //public int ValidDataDownhill { get; set; }
        //public int ValidDataUphill { get; set; }
        //public bool ValidDataZisDirty { get; set; }

        //public List<PathPointViewModel> PathPoints { get; set; }

    }
}
