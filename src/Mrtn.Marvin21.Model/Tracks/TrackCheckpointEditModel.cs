﻿namespace Mrtn.Marvin21.Model.Tracks
{
    public class TrackCheckpointEditModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        //public int? FromPreviousDistance { get; set; }
        //public int FromPreviousDownhill { get; set; }
        //public int FromPreviousUphill { get; set; }
        //public bool FromPreviousZisDirty { get; set; }
        //public string FromPreviousZlist { get; set; }
        public int Points { get; set; }
        public string TimeFromPrevious { get; set; }

        //public bool SpeedTrialIsFinish { get; set; }
        //public bool SpeedTrialIsStart { get; set; }
        //public string TimeForTask { get; set; }
        //public string TimeFromPrevious { get; set; }
        //public string TimeFromPreviousForDistance { get; set; }
        //public string TimeFromPreviousForDownhill { get; set; }
        //public string TimeFromPreviousForUphill { get; set; }
        //public string TimeOptimalArrival { get; set; }
        //public string TimeOptimalDeparture { get; set; }
        //public string TimeMaxFromStart { get; set; }


    }
}
