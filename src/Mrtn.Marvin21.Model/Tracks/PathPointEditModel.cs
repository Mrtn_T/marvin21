﻿using System.ComponentModel;

namespace Mrtn.Marvin21.Model.Tracks
{
    public class PathPointEditModel
    {
        public int Id { get; set; }


        [DisplayName("Višina točke")]
        public float? MapPositionZ { get; set; }
        public bool MapPositionZisDirty { get; set; }
    }
}
