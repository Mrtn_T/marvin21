﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.Model.Tasks;

namespace Mrtn.Marvin21.Model.Tracks
{
    public class TrackCheckpointViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public int? FromPreviousDistance { get; set; }
        public int FromPreviousDownhill { get; set; }
        public int FromPreviousUphill { get; set; }
        public bool FromPreviousZisDirty { get; set; }
        public string FromPreviousZlist { get; set; }
        public bool Locked { get; set; }
        public string Name { get; set; }
        public int No { get; set; }
        public int Points { get; set; }
        public bool SpeedTrialIsFinish { get; set; }
        public bool SpeedTrialIsStart { get; set; }
        public string TimeForTask { get; set; }
        public string TimeFromPrevious { get; set; }
        public string TimeFromPreviousForDistance { get; set; }
        public string TimeFromPreviousForDownhill { get; set; }
        public string TimeFromPreviousForUphill { get; set; }
        public string TimeOptimalArrival { get; set; }
        public string TimeOptimalDeparture { get; set; }
        public string TimeMaxFromStart { get; set; }
        public int StationId { get; set; }
        public string StationName { get; set; }
        public string StationString { get; set; }
        public string StationSiCodeArrival { get; set; }
        public string StationSiCodeDeparture { get; set; }
        public bool StationIsStart { get; set; }
        public bool StationIsFinish { get; set; }
        public bool StationIsAlive { get; set; }
        public bool StationIsHidden { get; set; }

        public float? X { get; set; }
        public float? Y { get; set; }

        public int PathId { get; set; }

        public List<TaskViewModel> Tasks { get; set; }
    }
}
