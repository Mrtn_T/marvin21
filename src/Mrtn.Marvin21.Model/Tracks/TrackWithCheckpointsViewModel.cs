﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mrtn.Marvin21.Model.Tracks
{
    public class TrackWithCheckpointsViewModel : TrackViewModel
    {
        public List<TrackCheckpointViewModel> TrackCheckpoints { get; set; }
        public List<StationViewModel> RemainingStations { get; set; }
    }
}
