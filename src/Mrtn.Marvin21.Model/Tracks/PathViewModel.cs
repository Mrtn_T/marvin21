﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mrtn.Marvin21.Model.Tracks
{
    public class PathViewModel
    {
        public int Id { get; set; }
        public bool IsManual { get; set; }
        public bool Locked { get; set; }

        public int FirstStationId { get; set; }
        public float? FirstStationX { get; set; }
        public float? FirstStationY { get; set; }
        public float? FirstStationZ { get; set; }
        public bool FirstStationZisDirty { get; set; }
        public string FirstStationName { get; set; }

        public int SecondStationId { get; set; }
        public float? SecondStationX { get; set; }
        public float? SecondStationY { get; set; }
        public float? SecondStationZ { get; set; }
        public bool SecondStationZisDirty { get; set; }
        public string SecondStationName { get; set; }

        public int? AutomaticDataDistance { get; set; }
        public string AutomaticDataZlist { get; set; }
        public int AutomaticDataDownhill { get; set; }
        public int AutomaticDataUphill { get; set; }
        public bool AutomaticDataZisDirty { get; set; }

        public int? ManualDataDistance { get; set; }
        public string ManualDataZlist { get; set; }
        public int ManualDataDownhill { get; set; }
        public int ManualDataUphill { get; set; }
        public bool ManualDataZisDirty { get; set; }

        public int? ValidDataDistance { get; set; }
        public string ValidDataZlist { get; set; }
        public int ValidDataDownhill { get; set; }
        public int ValidDataUphill { get; set; }
        public bool ValidDataZisDirty { get; set; }

        public List<PathPointViewModel> PathPoints { get; set; }

    }
}
