﻿using System;
using System.Text;

namespace Mrtn.Marvin21.Model.Tracks
{
    public class StationViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public  string SiCodeArrival { get; set; }
        public  string SiCodeDeparture { get; set; }
        public  bool IsStart { get; set; }
        public  bool IsFinish { get; set; }
        public  float? X { get; set; }
        public  float? Y { get; set; }
        public  float? Z { get; set; }
        public bool IsAlive { get; set; }
        public bool IsHidden { get; set; }
        public string Description { get; set; }
        public  int Day { get; set; }

        public bool ZisDirty { get; set; }
        public bool Locked { get; set; }

        public int DefaultPoints { get; set; }
        public string Tracks { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(Name);
            var sa = (SiCodeArrival ?? "").Trim();
            var sd = (SiCodeDeparture ?? "").Trim();
            if (sa == "" && sd == "") return sb.ToString();
            if (sa == "")
            {
                sb.Append(string.Format(" ({0})", sd));
                return sb.ToString();
            }
            if (sd == "")
            {
                sb.Append(string.Format(" ({0})", sa));
                return sb.ToString();
            }
            sb.Append(string.Format(" ({0}-{1})", sa, sd));
            return sb.ToString();
        }
    }
}
