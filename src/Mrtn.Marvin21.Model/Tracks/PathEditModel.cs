﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mrtn.Marvin21.Model.Tracks
{
    public class PathEditModel
    {
        public int Id { get; set; }
        public bool Locked { get; set; }

        [DisplayName("Ročno določanje")]
        public bool IsManual { get; set; }

        public int? AutomaticDataDistance { get; set; }
        public string AutomaticDataZlist { get; set; }

        [DisplayName("Ročno določena razdalja")]
        public int? ManualDataDistance { get; set; }

        [DisplayName("Ročno določene višine")]
        public string ManualDataZlist { get; set; }

        public float? FirstStationZ { get; set; }
        public bool FirstStationZisDirty { get; set; }
        public string FirstStationName { get; set; }

        public float? SecondStationZ { get; set; }
        public bool SecondStationZisDirty { get; set; }
        public string SecondStationName { get; set; }

        [DisplayName("Ročni vnos spusta")]
        public int ManualDataDownhill { get; set; }
        [DisplayName("Ročni vnos dviga")]
        public int ManualDataUphill { get; set; }
        [DisplayName("Dvig in spust se vnašata ročno")]
        public bool ManualDataManualUpDown { get; set; }
    }
}
