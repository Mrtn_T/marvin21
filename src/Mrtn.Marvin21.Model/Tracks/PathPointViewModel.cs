﻿namespace Mrtn.Marvin21.Model.Tracks
{
    public class PathPointViewModel
    {
        public int Id { get; set; }
        public bool Locked { get; set; }
        public int No { get; set; }

        public float? MapPositionX { get; set; }
        public float? MapPositionY { get; set; }
        public float? MapPositionZ { get; set; }
        public bool MapPositionZisDirty { get; set; }
    }
}
