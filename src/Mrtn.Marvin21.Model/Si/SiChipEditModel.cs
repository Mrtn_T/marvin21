﻿namespace Mrtn.Marvin21.Model.Si
{
    public class SiChipEditModel
    {
//        public bool Active { get; set; }
        public int Id { get; set; }
        public string SiCode { get; set; }
        public bool Private { get; set; }
        public string Comment { get; set; }
    }
}
