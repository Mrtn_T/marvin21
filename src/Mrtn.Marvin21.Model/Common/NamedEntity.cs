﻿namespace Mrtn.Marvin21.Model.Common
{
    /// <summary>
    /// Ta razred je namenjen polnjenju raznih selektorjev (drop-down)
    /// Vsaka entiteta predstavlja en vnos in vsebuje ime (Name) in Id
    /// </summary>
    public class NamedEntity 
    {
        public int Id { get; set;}
        public string Name { get; set; }
    }
}
