﻿using System.Collections.Generic;

namespace Mrtn.Marvin21.Model.Common
{
    public class SiReadReportResponse
    {
        public RunTrackProcessReport RunTrackProcessReport= new RunTrackProcessReport();
        public ImportFromSiReadReport ImportFromSiRead=new ImportFromSiReadReport();
        public int Day;
        public bool Success=true;
        public string ErrorMessage;
    }

    public class ImportFromSiReadReport
    {
        public string ConfigurationString;
        public bool Success=true;
        public string ErrorMessage;
        public List<SiReadReportLine> Lines= new List<SiReadReportLine>();
    }

    public class SiReadReportLine
    {
        public string Text;
        public bool IsNew=true;
        public string SiChip;
    }

    public class RunTrackProcessReport
    {
        public List<SiProcessReportLine> Lines = new List<SiProcessReportLine>();
    }

    public class SiProcessReportLine
    {
        public string Text;
        public string SiChip;
        public string Status;
        public string Team;
        public int Day;
    }
}