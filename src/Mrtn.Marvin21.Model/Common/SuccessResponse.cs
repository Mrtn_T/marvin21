﻿namespace Mrtn.Marvin21.Model.Common
{
    public class SuccessResponse
    {
        public SuccessResponse()
        {
        }
        public SuccessResponse(bool ok, string message)
        {
            Ok = ok;
            Message = message;
        }

        public bool Ok { get; set; }
        public string Message { get; set; }
    }
}
