﻿using System.Drawing;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Model.Extenders
{
    public static class CategoryColorExtender
    {
        public static bool ColorIsValid(this CategoryViewModel model)
        {
            if (model.Color == null) return false;
            var color = ColorTranslator.FromHtml("#" + model.Color.Replace("#", ""));
            if (color.A > 0) return true;
            return false;

        }
        public static string ThisColor(this CategoryViewModel model)
        {
            var color = ColorTranslator.FromHtml("#" + model.Color.Replace("#", ""));
            return ColorTranslator.ToHtml(color);
        }

        public static string DarkestColor(this CategoryViewModel model)
        {
            const double factor = 0.3;
            var color = ColorTranslator.FromHtml("#" + model.Color.Replace("#", ""));
            if (color.A == 0) return ColorTranslator.ToHtml(color);
            color = Color.FromArgb((int)(color.R * factor), (int)(color.G * factor), (int)(color.B * factor));
            return ColorTranslator.ToHtml(color);
        }
        public static string DarkerColor(this CategoryViewModel model)
        {
            const double factor = 0.7;
            var color = ColorTranslator.FromHtml("#" + model.Color.Replace("#", ""));
            if (color.A == 0) return ColorTranslator.ToHtml(color);
            color = Color.FromArgb((int)(color.R * factor), (int)(color.G * factor), (int)(color.B * factor));
            return ColorTranslator.ToHtml(color);
        }
        public static string BrighterColor(this CategoryViewModel model)
        {
            const double factor = 0.3;
            var color = ColorTranslator.FromHtml("#" + model.Color.Replace("#", ""));
            if (color.A == 0) return ColorTranslator.ToHtml(color);
            color = Color.FromArgb((int)(color.R + (255 - color.R) * factor), (int)(color.G + (255 - color.G) * factor), (int)(color.B + (255 - color.B) * factor));
            return ColorTranslator.ToHtml(color);
        }
        public static string BrighestColor(this CategoryViewModel model)
        {
            const double factor = 0.7;
            var color = ColorTranslator.FromHtml("#" + model.Color.Replace("#", ""));
            if (color.A == 0) return ColorTranslator.ToHtml(color);
            color = Color.FromArgb((int)(color.R + (255 - color.R) * factor), (int)(color.G + (255 - color.G) * factor), (int)(color.B + (255 - color.B) * factor));
            return ColorTranslator.ToHtml(color);
        }

    }
}
