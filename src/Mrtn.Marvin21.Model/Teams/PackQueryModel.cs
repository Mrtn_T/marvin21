﻿namespace Mrtn.Marvin21.Model.Teams
{
    public class PackQueryModel
    {
        public bool? Active { get; set; }
        public string Town { get; set; }
        public string Abbr { get; set; }

    }
}
