﻿namespace Mrtn.Marvin21.Model.Teams
{
    public class PackViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Town { get;  set; }
        public string Abbr { get;  set; }
        public bool Active { get;  set; }
        public bool InCompetition { get;  set; }
        public int TeamCount{ get;  set; }
    }
}
