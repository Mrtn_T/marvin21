﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Mrtn.Marvin21.Model.Common;

namespace Mrtn.Marvin21.Model.Teams
{
    public class FiltersForTeams
    {

        public List<NamedEntity> Categories { get; set; }
        public List<NamedEntity> Packs { get; set; }

    }
}
