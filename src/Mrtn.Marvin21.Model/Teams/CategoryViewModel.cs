﻿using System.Collections.Generic;

namespace Mrtn.Marvin21.Model.Teams
{
    public class CategoryViewModel
    {

        public int Id { get; set; }
        public int No { get; set; }
        public string Name { get; set; }
        public string Abbr { get; set; }
        public bool Active { get; set; }
        public int TeamCount { get; set; }

        public string Color { get; set; }

        public float Speed { get; set; }
        public float UphillSpeed { get; set; }
        public float DownhillSpeed { get; set; }

        public int TrackCount { get; set; }
        public Dictionary<int, string> Tracks { get; set; }

        /// <summary>
        /// Martin, 8.1.2020:
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        public int MaxPoints { get; set; }

        /// <summary>
        /// Martin, 8.1.2020:
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        public string MaxPointsDescription { get; set; }
    }
}
