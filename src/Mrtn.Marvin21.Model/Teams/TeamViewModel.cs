﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Mrtn.Marvin21.Model.Teams
{
    public class TeamViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get;  set; }
        public bool InCompetition { get;  set; }
        public int? No { get; set; }
        public string SiCode { get; set; }
        public string PrivateSiCode { get; set; }

        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
        public string PackName { get; set; }
        public int PackId { get; set; }
        public string PackAbbr { get; set; }

        // izračunana lastnost
        public string SiCodeError { get; set; }

        public string Points { get; set; }
        public string Place { get; set; }


        /// <summary>
        /// Martin, 8.1.2020:
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:P2}")]
        public Single Percent { get; set; }

        public string PercentString => $"{Percent:P}";
    }
}
