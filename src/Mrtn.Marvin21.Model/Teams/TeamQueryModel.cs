﻿using System;

namespace Mrtn.Marvin21.Model.Teams
{
    public class TeamQueryModel
    {
        public bool ShowInactive { get; set; }
        public int IdCategory { get; set; }
        public int IdPack { get; set; }
        public bool SortByPlace { get; set; }
    }
}
