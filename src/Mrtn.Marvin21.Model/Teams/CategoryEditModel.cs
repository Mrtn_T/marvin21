﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mrtn.Marvin21.Model.Teams
{
    public class CategoryEditModel
    {

        public int Id { get;  set; }
        public int No { get;  set; }
        public string Name { get;  set; }
        public string Abbr { get;  set; }
        public bool Active { get;  set; }

        [DisplayName("Nosilna barva kategorije (format rrggbb)")]
        public string Color { get; set; }

        [DisplayName("Hitrost gibanja po ravnem v tej kategoriji (km/h)")]
        [Required(ErrorMessage = "Polje je obvezno.")]
        public float Speed { get; set; }

        [DisplayName("Hitrost vzpenjanja v tej kategoriji (m/h)")]
        [Required(ErrorMessage = "Polje je obvezno.")]
        public float UphillSpeed { get; set; }

        [DisplayName("Hitrost spuščanja v tej kategoriji (m/h)")]
        [Required(ErrorMessage = "Polje je obvezno.")]
        public float DownhillSpeed { get; set; }

    }
}
