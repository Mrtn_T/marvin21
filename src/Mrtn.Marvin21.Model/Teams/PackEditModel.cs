﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace Mrtn.Marvin21.Model.Teams
{
    public class PackEditModel
    {
        [Range(0, 10000)]
        public int Id { get; set; }

        [DisplayName("Ime rodu")]
        [Required(ErrorMessage = "Polje je obvezno.")]
        public string Name { get; set; }

        [DisplayName("Kraj")]
        [Required(ErrorMessage = "Polje je obvezno.")]
        public string Town { get; set; }

        [DisplayName("Kratica")]
        [Required(ErrorMessage = "Polje je obvezno.")]
        public string Abbr { get; set; }

        [DisplayName("Ali tekmuje v konkurenci")]
        public bool InCompetition { get; set; }
    }
}
