﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Mrtn.Marvin21.Model.Common;

namespace Mrtn.Marvin21.Model.Teams
{
    public class TeamEditModel
    {
        public int Id { get; set; }

        [DisplayName("Ime")]
        [Required(ErrorMessage = "Polje je obvezno.")]
        public string Name { get; set; }

        [DisplayName("Ali tekmuje v konkurenci")]
        public bool InCompetition { get; set; }

        [DisplayName("Aktivna")]
        public bool Active { get; set; }

        [DisplayName("Št. privatnega čipa")]
        public virtual string PrivateSiCode { get; set; }


        public List<NamedEntity> Categories { get; set; }
        public List<NamedEntity> Packs { get; set; }

        [DisplayName("Kategorija")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Potrebno je izbrati kategorijo")]
        [Required(ErrorMessage = "Polje je obvezno.")]
        public int CategoryId { get; set; }

        [DisplayName("Rod")]
        [Range(1, Int32.MaxValue, ErrorMessage = "Potrebno je izbrati rod")]
        [Required(ErrorMessage = "Polje je obvezno.")]
        public int PackId { get; set; }

    }
}
