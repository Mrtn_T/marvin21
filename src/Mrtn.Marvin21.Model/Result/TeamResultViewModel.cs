﻿using System;

namespace Mrtn.Marvin21.Model.Result
{
    public class TeamResultViewModel
    {
        public int Place { get; set; }
        public int? No { get; set; }
        public string Name { get; set; }
        public int TrackPoints { get; set; }
        public int SpeedTrialPoints { get; set; }
        public int TimePoints { get; set; }
        public int? TaskPoints { get; set; }
        public int Points { get; set; }
        public int TopoPoints { get; set; }
        public int PpPoints { get; set; }
        public int TtPoints { get; set; }
        public int FemalePoints { get; set; }
        public int SemPoints { get; set; }
        public int VrisPoints { get; set; }
    }
}