﻿using System.Collections.Generic;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Model.Result
{
    public class TeamResultsViewModel
    {
        public List<TeamResultsByCategoryViewModel> Categories { get; set; }
        public string TimeGenerated { get; set; }
    }

    public class TeamResultsByDayViewModel
    {
        public List<TeamResultsHeaderViewModel> Headers
        { get; set; }

        public List<TeamResultsRowViewModel> Rows { get; set; }
    }

    public class TeamResultsRowViewModel
    {
        public TeamViewModel Team;
        public List<TeamResultsCellViewModel> Cells;
    }

    public class TeamResultsCellViewModel
    {
        public int TaskPoints { get; set; }
        public bool IsTrack { get; set; }
        public int Day { get; set; }
        public int TeamId { get; set; }
        public bool HasNoEntry { get; set; }
        public bool NoResultYet { get; set; }
        public int CheckpointsFound { get; set; }
        public int PointsForCheckpoints { get; set; }
        public int PointsForTime { get; set; }
        public int TrackPoints { get; set; }
        public int SpeedTrialPoints { get; set; }
        public bool Locked { get; set; }
        public int RunTrackId { get; set; }
    }

    public class TeamResultsHeaderViewModel
    {
        public int Day;
        public string Name;
        public bool Sum;
        public bool IsTrack;
        public int TaskId { get; set; }
        public string Location { get; set; }
    }
}