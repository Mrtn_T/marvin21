﻿namespace Mrtn.Marvin21.Model.Result
{
    public class PackResultViewModel
    {
        public int Place { get; set; }
        public int Points { get; set; }
        public string PackAbbr { get; set; }
        public string PackNam { get; set; }
        public string PackTown { get; set; }
        public string Teams { get; set; }
        public float Percent { get; set; }
        public string PercentString => $"{Percent:P}";
    }
}