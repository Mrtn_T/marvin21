﻿using System.Collections.Generic;
using Mrtn.Marvin21.Model.Teams;

namespace Mrtn.Marvin21.Model.Result
{
    public class TeamResultsByCategoryViewModel
    {
        public CategoryViewModel Category { get; set; }
        public List<TeamResultViewModel> TeamResults { get; set; }
    }
}