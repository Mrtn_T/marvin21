﻿using System.Configuration;
using NUnit.Framework;

namespace Mrtn.Marvin21.DbModel.Tests
{
    [TestFixture]
    class CheckConfigSetting
    {
        [Test]
        public void Check_If_Config_File_Is_Read_From_app_dot_config()
        {
            Assert.AreEqual("1234565", ConfigurationManager.AppSettings["configCheck"]);
        }
    }
}
