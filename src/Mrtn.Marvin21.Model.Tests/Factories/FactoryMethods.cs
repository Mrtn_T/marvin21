﻿using System;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.DbModel.Support;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Tracks;
using Mrtn.Marvin21.DbModel.Tracks.Support;

namespace Mrtn.Marvin21.DbModel.Tests.Factories
{
    public static class FactoryMethods
    {
        public static Pack CreatePack()
        {
            return new Pack
            {
                Abbr = "TEST",
                Active = true,
                InCompetition = true,
                Name = "test",
                Town = "testTown"
            };
        }

        public static Category CreateCategory()
        {
            return new Category
                       {
                           Abbr = "PPŽ",
                           Active = true,
                           Name = "Popotnice",
                           No = 4
                       };
        }


        public static Teams.Team CreateTeam(Marvin20Db db)
        {
            return new Teams.Team
                       {
                           Active = true,
                           InCompetition = true,
                           Name = "Ekipa1",
                           Category = PersistetEntitesFactoryMethods.CreatePersistentCategory(db),
                           Pack = PersistetEntitesFactoryMethods.CreatePersistentPack(db),
                           SiCode = "123456"
                       };
        }

        public static Track CreateTrack(Marvin20Db db)
        {
            return new Track
                       {
                           Category = PersistetEntitesFactoryMethods.CreatePersistentCategory(db),
                           Day = 1
                       };
        }

        public static Station CreateStation(Marvin20Db db)
        {
            return new Station()
                       {
                           Day=1,
                           Description = "opis te lokacije",
                           IsAlive = false,
                           IsFinish = false,
                           IsStart = false,
                           SiCodeArrival = "31",
                           MapPosition= new MapPosition()
                       };
        }

        public static TrackCheckpoint CreateTrackCheckpoint(Marvin20Db db)
        {
            return new TrackCheckpoint
                       {

                           Description = "",
                           Name = "KT1",
                           No = 1,
                           Points = 100,
                           Track = PersistetEntitesFactoryMethods.CreatePersistentTrack(db),
                           Station = PersistetEntitesFactoryMethods.CreatePersistentStation(db),
                           FromPrevious = new PathData(),
                           SpeedTrial = new SpeedTrial(),
                           Time = new Time()
                                      {
                                          ForTask = 0,
                                          OptimalArrival = 3600,
                                          OptimalDeparture =  3900
                                      }
                       };
        }

        public static SiChip CreateSiChip()
        {
            return new SiChip
                       {
                           SiCode="123456",
                           Active = false,
                           Private = true
                       };
        }

        public static Setting CreateSetting()
        {
            return new Setting{Code="SettingsCode",  Name = "SettingName" , Value = "x"};
        }

        public static Path CreatePath(Marvin20Db db)
        {
            return new Path
            {
                FirstStation = PersistetEntitesFactoryMethods.CreatePersistentStation(db),
                SecondStation = PersistetEntitesFactoryMethods.CreatePersistentStation(db),
                AutomaticData = new PathData {Distance = 1560, Zlist = "500,300,200"},
                ManualData = new PathData {Distance = 1680, Zlist = "530,330,210"},
                IsManual = false
            };
        }

        public static PathPoint CreatePathPoint(Marvin20Db db)
        {
            return new PathPoint
                       {
                           MapPosition = new MapPosition{X=300,Y = 256},
                           No = 1,
                           Path = PersistetEntitesFactoryMethods.CreatePersistentPath(db),
                       };
        }
    }
}
