﻿using System;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Tracks;

namespace Mrtn.Marvin21.DbModel.Tests.Factories
{
    public static    class PersistetEntitesFactoryMethods
    {
        public static Pack CreatePersistentPack(Marvin20Db db)
        {
            var pack = FactoryMethods.CreatePack();
            db.Packs.Add(pack);
            db.SaveChanges();
            return pack;
        }
        public static Category CreatePersistentCategory(Marvin20Db db)
        {
            var category = FactoryMethods.CreateCategory();
            db.Categories.Add(category);
            db.SaveChanges();
            return category;
        }

        public static Track CreatePersistentTrack(Marvin20Db db)
        {
            var track = FactoryMethods.CreateTrack(db);
            db.Tracks.Add(track);
            db.SaveChanges();
            return track;
        }

        public static Station CreatePersistentStation(Marvin20Db db)
        {
            var station = FactoryMethods.CreateStation(db);
            db.Stations.Add(station);
            db.SaveChanges();
            return station;
        }

        public static Path CreatePersistentPath(Marvin20Db db)
        {
            var path = FactoryMethods.CreatePath(db);
            db.Paths.Add(path);
            db.SaveChanges();
            return path;
        }
    }
}
