﻿using System;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.DbModel.Support;
using Mrtn.Marvin21.DbModel.Tests.Factories;
using NUnit.Framework;

namespace Mrtn.Marvin21.DbModel.Tests.Persistence.Support
{

    [TestFixture]
    public class SettingTest : BasicPersistenceTest<Setting>
    {
        protected override Setting CreateInstance()
        {
            return FactoryMethods.CreateSetting();
        }

        protected override void UpdateInstance(Setting model)
        {
            model.Name = "bu";
        }

    }
}
