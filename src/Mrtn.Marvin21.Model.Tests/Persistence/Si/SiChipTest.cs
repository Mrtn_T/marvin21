﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.DbModel.Tests.Factories;
using NUnit.Framework;

namespace Mrtn.Marvin21.DbModel.Tests.Persistence.Si
{

    [TestFixture]
    public class SiChipTest : BasicPersistenceTest<SiChip>
    {
        protected override SiChip CreateInstance()
        {
            return FactoryMethods.CreateSiChip();
        }

        protected override void UpdateInstance(SiChip model)
        {
            model.Active = true;
        }
    }
}
