﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.DbModel.Tests.Factories;
using Mrtn.Marvin21.DbModel.Tracks;
using NUnit.Framework;

namespace Mrtn.Marvin21.DbModel.Tests.Persistence.Tracks
{
     [TestFixture]
    public class PathTest : BasicPersistenceTest<Path>
    {
         protected override Path CreateInstance()
         {
             return FactoryMethods.CreatePath(_db);
         }

         protected override void UpdateInstance(Path model)
         {
             model.IsManual = true;
         }
    }
}
