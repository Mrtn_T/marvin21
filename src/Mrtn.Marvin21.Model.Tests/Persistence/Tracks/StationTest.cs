﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.DbModel.Tests.Factories;
using Mrtn.Marvin21.DbModel.Tracks;
using NUnit.Framework;

namespace Mrtn.Marvin21.DbModel.Tests.Persistence.Tracks
{
    [TestFixture]
    public class StationTest : BasicPersistenceTest<Station>     
    {
         protected override Station CreateInstance()
         {
             return FactoryMethods.CreateStation(_db);


         }

         protected override void UpdateInstance(Station model)
         {
             model.Description = "KKK";
         }
    }
}
