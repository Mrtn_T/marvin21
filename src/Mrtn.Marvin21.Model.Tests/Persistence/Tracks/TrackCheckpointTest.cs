﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.DbModel.Tests.Factories;
using Mrtn.Marvin21.DbModel.Tracks;
using NUnit.Framework;

namespace Mrtn.Marvin21.DbModel.Tests.Persistence.Tracks
{
    [TestFixture]
    public class TrackCheckpointTest : BasicPersistenceTest<TrackCheckpoint>
    {
        protected override TrackCheckpoint CreateInstance()
        {
            return FactoryMethods.CreateTrackCheckpoint(_db);
        }

        protected override void UpdateInstance(TrackCheckpoint model)
        {
            model.SpeedTrial.IsFinish = true;
        }
    }
}
