﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.DbModel.Tests.Factories;
using Mrtn.Marvin21.DbModel.Tracks;
using NUnit.Framework;

namespace Mrtn.Marvin21.DbModel.Tests.Persistence.Tracks
{
     [TestFixture]
    public class TrackTest : BasicPersistenceTest<Track>
    {
         protected override Track CreateInstance()
         {
             return FactoryMethods.CreateTrack(_db);
         }

         protected override void UpdateInstance(Track model)
         {
             model.Day = 0;
         }
    }
}
