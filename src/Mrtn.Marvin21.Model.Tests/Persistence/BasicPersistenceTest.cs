﻿using System;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.Dao;
using NUnit.Framework;

namespace Mrtn.Marvin21.DbModel.Tests.Persistence
{
    public abstract class BasicPersistenceTest<TModel> where TModel : EntityBase
    {
        protected Marvin20Db _db;

        [TestFixtureSetUp]
        public void Init()
        {
            Debug.Print("TestFixtureSetUp");
            PersistenceDatabaseInitializer.InitializeIfNessesary();

            _db = new Marvin20Db();
        }


        [Test]
        public void Can_save_and_get()
        {
            Console.WriteLine(String.Format("*** Running tests: {0} --> Can_save_and_get ***", GetType().Name));

            if (!String.Equals(typeof(TModel).Name + "Test", GetType().Name, StringComparison.InvariantCultureIgnoreCase))
                Assert.Fail(String.Format("Test class name mismatches tested class name, should be: {0}Test", typeof(TModel).Name));
            try
            {
                var model = CreateInstance();
                Console.WriteLine(String.Format(" - Getting property of type DbSet<{0}>. Should be exactly one", typeof(TModel).Name));
                var propertyInfo = _db
                    .GetType()
                    .GetProperties()
                    .Where(info => info.PropertyType == typeof (DbSet<TModel>))
                    .Single();
                //save new model
                ((DbSet<TModel>)propertyInfo.GetValue(_db, null)).Add(model);
                _db.SaveChanges();
                
                //retrieve model
                var model1 = ((DbSet<TModel>)propertyInfo
                    .GetValue(_db, null))
                    .Where(x=>x.Id == model.Id)
                    .Single();

                //compare models
                Assert.AreEqual(model, model1);
            }
            catch (Exception ee)
            {
                Console.WriteLine(String.Format("*** Exception in test: {0} --> Can_save_and_get ***", GetType().Name));
                Assert.Fail(ee.Message);
            }
        }

        [Test]
        public void Can_Update()
        {
            Console.WriteLine(String.Format("*** Running tests: {0} --> Can_Update ***", GetType().Name));
            try
            {
                var model = CreateInstance();
                Console.WriteLine(String.Format(" - Getting property of type DbSet<{0}>. Should be exactly one",
                                                typeof (TModel).Name));
                var propertyInfo = _db
                    .GetType()
                    .GetProperties()
                    .Where(info => info.PropertyType == typeof (DbSet<TModel>))
                    .Single();
                //save new model
                ((DbSet<TModel>) propertyInfo.GetValue(_db, null)).Add(model);
                _db.SaveChanges();

                //retrieve model
                //retrieve model
                var model1 = ((DbSet<TModel>)propertyInfo
                    .GetValue(_db, null))
                    .Where(x => x.Id == model.Id)
                    .Single();

                UpdateInstance(model1);

                //save changes
                _db.SaveChanges();

                //retrieve model
                //retrieve model
                var model2 = ((DbSet<TModel>)propertyInfo
                    .GetValue(_db, null))
                    .Where(x => x.Id == model.Id)
                    .Single();

                Assert.IsNotNull(model1);
                Assert.AreEqual(model1, model2);
            }
            catch (Exception ee)
            {
                Console.WriteLine(String.Format("*** Exception in test: {0} --> Can_save_and_get ***", GetType().Name));
                Assert.Fail(ee.Message);
            }
        }

        protected abstract TModel CreateInstance();

        protected abstract void UpdateInstance(TModel model);


        [TestFixtureTearDown]
        public void Dispose()
        {
            Debug.Print("TestFixtureTearDown");

            /* ... */
        }
    }


/*

    /// <summary>
    /// A basic persistence test collection for <typeparamref name="TModel"/>. Validates mapping, persisting to database and loading 
    /// </summary>
    /// <typeparam name="TModel">The entity to test</typeparam>
    public abstract class BasicPersistenceTest<TModel> : BasicPersistenceTest where TModel : class
    {
        [Test]

        /// <summary>
        /// Returns <see cref="Microsoft.SqlServer.Management.Smo.Server"/> that does not specify the database to connect to.
        /// Used to check if the DB exists, to drop/create it. 
        /// </summary>
        /// <returns></returns>
        private static Server GetServerWithoutDatabase()
        {
            return string.IsNullOrEmpty(PerNamespaceFixtureSetup.UserId)
                       ? new Server(PerNamespaceFixtureSetup.SqlServerName)
                       : new Server(new ServerConnection(PerNamespaceFixtureSetup.SqlServerName, PerNamespaceFixtureSetup.UserId, PerNamespaceFixtureSetup.Password));
        }

        [Test]
        public void Validate_Mapping()
        {
            Console.WriteLine(String.Format("*** Running tests: {0} --> Validate_Mapping ***", GetType().Name));

            RegisterExplicitLazyLoadingRules();

            var server = GetServerWithoutDatabase();
            var db = server.Databases[PerNamespaceFixtureSetup.DatabaseName];

            var type = typeof(TModel);

            var mapping = PerNamespaceFixtureSetup.NhConfiguration.GetClassMapping(typeof(TModel));

            var schemaName = mapping.Table.Schema == null ? "dbo" : mapping.Table.Schema.Trim('[', ']');
            var tableName = mapping.Table.Name.Trim('[', ']');

            var table = db.Tables
                .OfType<Table>()
                .SingleOrDefault(x => x.Schema == schemaName && x.Name == tableName);

            if (table == null)
                Assert.Fail(String.Format("Table {0} not found in schema {1}", tableName, schemaName));

            var allColumns = table.Columns.OfType<Column>().ToList();
            var unmappedColumns = table.Columns.OfType<Column>().ToList();

            var foreignKeys = table.ForeignKeys.OfType<ForeignKey>().ToList();

            if (!mapping.IsLazy)
                LogError("Lazy loading is 'false', it should be enabled for all Vpisniki entites during development phase.");

            var properties = mapping.PropertyIterator.ToList();
            properties.Insert(0, mapping.IdentifierProperty);

            foreach (var property in properties)
            {
                var classMemberInfo = type.GetProperty(property.Name);

                foreach (var column in property.ColumnIterator)
                {
                    var colName = column.Text.Trim('[', ']');
                    var dbCol = allColumns.SingleOrDefault(x => x.Name == colName);
                    if (dbCol != null)
                    {
                        ForeignKey foreignKey = null;
                        if (dbCol.IsForeignKey)
                            foreignKey = foreignKeys.Single(x =>
                            {
                                var fk = x.Columns.OfType<NamedSmoObject>().SingleOrDefault();
                                return fk != null && fk.Name == dbCol.Name;
                            });

                        unmappedColumns.RemoveAll(x => x.Name == dbCol.Name);

                        if (dbCol.Nullable != property.IsNullable)
                            LogError(String.Format("'{0}', column: '{1}', DB nullable: {2}, NH nullable: {3} ", property.Name, colName, dbCol.Nullable, property.IsNullable));

                        CheckConsistency(dbCol, foreignKey, property, classMemberInfo);
                    }
                    else
                        LogError(String.Format("'{0}', column: '{1}', no column found in DB", property.Name, colName));
                }

                if (property.Type.IsAssociationType)
                {
                    if (property.Type.IsEntityType &&
                        ((NHibernate.Mapping.ToOne)property.Value).IsLazy &&
                        PerNamespaceFixtureSetup.NhConfiguration.GetClassMapping(property.Type.ReturnedClass).IsLazy &&
                        !_registeredEntitiesLazy.Contains(property.Name))
                        LogInfo(String.Format("association '{0}' is lazy loaded (turn off lazy loading or register rule in RegisterExplicitLazyLoadingRules)", property.Name));

                    if (property.Type.IsCollectionType &&
                        !((NHibernate.Mapping.Collection)property.Value).IsLazy &&
                        !_registeredCollectionsNonLazy.Contains(property.Name))
                        LogError(String.Format("collection '{0}' is NOT lazy loaded (make it lazy or register rule in RegisterExplicitLazyLoadingRules (but only if you have a good reason for keeping it eagearly loade))", property.Name));
                }
            }

            if (unmappedColumns.Count > 0)
            {
                foreach (var col in unmappedColumns)
                {
                    if (col.IsForeignKey)
                    {
                        ForeignKey foreignKey = null;
                        if (col.IsForeignKey)
                            foreignKey = foreignKeys.Single(x =>
                            {
                                var fk =
                                    x.Columns.OfType<NamedSmoObject>().
                                        SingleOrDefault();
                                return fk != null && fk.Name == col.Name;
                            });

                        if (col.Nullable)
                            LogWarning(String.Format("column '{0}' has no mapping (is nullable in DB), FK_TABLE: {1}.{2}",
                                col.Name,
                                foreignKey.ReferencedTableSchema,
                                foreignKey.ReferencedTable));
                        else
                            LogError(String.Format("column '{0}' has no mapping (is NOT nullable in DB), FK_TABLE: {1}.{2}",
                                col.Name,
                                foreignKey.ReferencedTableSchema,
                                foreignKey.ReferencedTable));
                    }
                    else
                    {
                        if (col.Nullable)
                            LogWarning(String.Format("column '{0}' has no mapping (is nullable in DB)", col.Name));
                        else
                            LogError(String.Format("column '{0}' has no mapping (is NOT nullable in DB)", col.Name));
                    }
                }
            }

            if (_errorCount > 0 || _warningCount > 0)
            {
                Console.WriteLine(_sbInfo.ToString());
                Console.WriteLine(String.Format("Errors: {0}, Warnings: {1}", _errorCount, _warningCount));
            }

            if (_errorCount > 0)
                Assert.Fail(String.Format("{0} possible errors in mapping: {1}", _errorCount, _sbError));
        }


        protected void SaveFlushAndClear(TModel model)
        {
            Session.Save(model);
            Session.Flush();
            Session.Clear();
        }

        protected void RegisterEntityLazy(Expression<Func<TModel, object>> expression)
        {
            _registeredEntitiesLazy.Add(StrongTypedPropertyHelper.GetProperty(expression).Name);
        }

        protected void RegisterCollectionNonLazy(Expression<Func<TModel, object>> expression)
        {
            _registeredCollectionsNonLazy.Add(StrongTypedPropertyHelper.GetProperty(expression).Name);
        }

        /// <summary>
        /// Override this method to register explicit lazy loading rules like this:
        /// RegisterCollectionNonLazy(x => x.SomeCollection);
        /// RegisterEntityLazy(x => x.SomeEntity);
        /// </summary>
        protected virtual void RegisterExplicitLazyLoadingRules()
        {

        }

        private void LogInfo(string s)
        {
            _sbInfo.AppendLine(String.Format("   OK:      {0}", s));
        }

        private void LogWarning(string s)
        {
            _warningCount++;
            _sbInfo.AppendLine(String.Format("   WARNING: {0}", s));
        }

        private void LogError(string s)
        {
            _errorCount++;
            LogErrorToInfo(s);
            _sbError.AppendLine(String.Format("   ERROR:   {0}; ", s));
        }
        private void LogErrorToInfo(string s)
        {
            _sbInfo.AppendLine(String.Format("   ERROR:   {0}", s));
        }

        private void CheckConsistency(Column dbCol, ForeignKey foreignKey, Property nhProperty, PropertyInfo classMemberInfo)
        {
            //database column data type
            var dbdt = dbCol.DataType;
            //nhibernate mapping property data type
            var nhdt = nhProperty.Type;
            //class property data type
            var cpdt = classMemberInfo.PropertyType;

            var dbNhTypeMatch = false;

            // check value types nullability (match between nh mapping and class property --> 
            // nullable value type should have nullable property behind ... i.e. nullable int field should be mapped to 'int?')
            if (cpdt.IsValueType)
            {
                if (nhProperty.IsNullable !=
                    (Nullable.GetUnderlyingType(cpdt) != null ? cpdt.GetGenericTypeDefinition() == typeof(Nullable<>) : false))
                    LogWarning(String.Format("'{0}', column: '{1}', nullability mismatch (NH: {2} <---> Class: {3})", nhProperty.Name, dbCol.Name, nhProperty.IsNullable, !nhProperty.IsNullable));
            }

            //check if db column type matches mapping type
            if (nhdt.IsEntityType)
            {
                //IsEntityType --> FK
                var foreignEntityType = PerNamespaceFixtureSetup.GetMappedTypesForTableName(foreignKey.ReferencedTable);
                if (foreignEntityType.Contains(nhdt.ReturnedClass))
                    dbNhTypeMatch = true;
            }
            else if (dbdt.Name == DataType.BigInt.Name)
            {
                if (nhdt is Int64Type) dbNhTypeMatch = true;
            }
            else if (dbdt.Name == DataType.Int.Name)
            {
                if (nhdt is Int32Type) dbNhTypeMatch = true;
                else if (nhdt is PersistentEnumType) dbNhTypeMatch = true;
            }
            else if (dbdt.Name == DataType.Bit.Name)
            {
                if (nhdt is BooleanType) dbNhTypeMatch = true;
            }
            else if (dbdt.Name == DataType.DateTime.Name)
            {
                if (nhdt is DateTimeType) dbNhTypeMatch = true;
            }
            else if (dbdt.Name == DataType.Numeric(1, 1).Name)
            {
                if (nhdt is DecimalType) dbNhTypeMatch = true;
            }
            else if (dbdt.Name == DataType.VarBinaryMax.Name)
            {
                if (nhdt is BinaryType) dbNhTypeMatch = true;
            }
            else if (dbdt.Name == DataType.VarCharMax.Name || dbdt.Name == DataType.NVarCharMax.Name)
            {
                if (nhdt is StringType)
                {
                    dbNhTypeMatch = true;
                    var mappingLength = ((StringType)nhdt).SqlType.Length;
                    if (mappingLength > 0 && dbCol.DataType.MaximumLength != mappingLength)
                        LogError(String.Format("'{0}', column: '{1}', varchar:  DB length: {2}, NH length: {3} ", nhProperty.Name, dbCol, dbCol.DataType.MaximumLength, mappingLength));
                }
                else if (nhdt is EnumStringType) dbNhTypeMatch = true;
            }
            else
            {
                LogWarning(String.Format("'{0}', column: '{1}', datatype consistency not checked (DB: {2} <---> NH: {3})", nhProperty.Name, dbCol.Name, dbdt.Name, nhdt.Name));
                return;
            }

            if (!dbNhTypeMatch)
            {
                if (nhdt.IsEntityType)
                    LogError(
                        String.Format(
                            "'{0}', column: '{1}', datatype mismatch (DB: {2}, FK_TABLE: {3}.{4} <---> NH: {5})",
                            nhProperty.Name, dbCol.Name, dbdt.Name, foreignKey.ReferencedTableSchema,
                            foreignKey.ReferencedTable, nhdt.Name));
                else
                    LogError(String.Format("'{0}', column: '{1}', datatype mismatch (DB: {2} <---> NH: {3})",
                                           nhProperty.Name, dbCol.Name, dbdt.Name, nhdt.Name));
            }
            else
            {
                if (nhdt.IsEntityType)
                    LogInfo(String.Format("'{0}', column: '{1}', (DB: {2}, FK_TABLE: {3}.{4} <---> NH: {5})",
                                          nhProperty.Name, dbCol.Name, dbdt.Name, foreignKey.ReferencedTableSchema,
                                          foreignKey.ReferencedTable, nhdt.Name));
                else
                    LogInfo(String.Format("'{0}', column: '{1}', (DB: {2} <---> NH: {3})",
                                          nhProperty.Name, dbCol.Name, dbdt.Name, nhdt.Name));
            }
        }
    }

    public abstract class BasicPersistenceTest
    {
        protected ISession Session;
        protected int _warningCount;
        protected int _errorCount;
        protected StringBuilder _sbInfo;
        protected StringBuilder _sbError;
        protected List<string> _registeredEntitiesLazy = new List<string>();
        protected List<string> _registeredCollectionsNonLazy = new List<string>();

        [SetUp]
        public void SetUpCore()
        {
            _warningCount = 0;
            _errorCount = 0;
            _sbInfo = new StringBuilder();
            _sbError = new StringBuilder();

            Session = PerNamespaceFixtureSetup.Session;

            var languageFilter = Session.EnableFilter("CultureFilter");
            languageFilter.SetParameter("LangId", 1);
            //default admin user in the DB
            //used by SRCNHibernateInterceptor
            Globals.CurrentUserID = 1;
            Globals.CurrentUserName = "admin";
            Globals.SelectedLanguageID = 1;

            Session.BeginTransaction();
        }

        [TearDown]
        public void TearDownCore()
        {
            Session.Transaction.Rollback();
            Session.Dispose();
        }
    }
    */
}
