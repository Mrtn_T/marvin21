﻿using System.Diagnostics;
using System.Data.Entity;
using Mrtn.Marvin21.Dao;
using Mrtn.Marvin21.Dao.DbInitializers;

namespace Mrtn.Marvin21.DbModel.Tests.Persistence
{
    public static class PersistenceDatabaseInitializer
    {
        private static bool _isInitialized=false;

        public static void InitializeIfNessesary()
        {
            if(_isInitialized) return;
            Debug.Print("Database will be recreated");

            Database.SetInitializer(new NewMarvin20DbInitializer());

            _isInitialized = true;
        }
    }
}
