﻿using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Tests.Factories;
using NUnit.Framework;

namespace Mrtn.Marvin21.DbModel.Tests.Persistence.Team
{
    [TestFixture]
    public class CategoryTest : BasicPersistenceTest<Category>
    {
        protected override Category CreateInstance()
        {
            return FactoryMethods.CreateCategory();
        }

        protected override void UpdateInstance(Category model)
        {
            model.Name = "novo ime kategorije";
        }
    }
}
