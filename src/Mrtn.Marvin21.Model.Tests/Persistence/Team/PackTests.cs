﻿using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Tests.Factories;
using NUnit.Framework;

namespace Mrtn.Marvin21.DbModel.Tests.Persistence.Team
{
    [TestFixture]
    public class PackTest : BasicPersistenceTest<Pack>
    {
        protected override Pack CreateInstance()
        {
            return FactoryMethods.CreatePack();
        }
        protected override void UpdateInstance(Pack model)
        {
            model.Name = "new pack name";
        }
    }
}
