﻿using System;
using System.Linq;
using Mrtn.Marvin21.DbModel.Tests.Factories;
using NUnit.Framework;

namespace Mrtn.Marvin21.DbModel.Tests.Persistence.Team
{
    [TestFixture]
    public class TeamTest : BasicPersistenceTest<Teams.Team>
    {
        protected override Teams.Team CreateInstance()
        {
            return FactoryMethods.CreateTeam(_db);
        }

        protected override void UpdateInstance(Teams.Team model)
        {
            model.Name = "spremenjeno ime";
        }




        [Test]
        public void Does_New_Team_Exist_In_Pack()
        {
            var team = FactoryMethods.CreateTeam(_db);
            _db.Teams.Add(team);
            _db.SaveChanges();

            Assert.Contains(team, team.Pack.Teams.ToList());

            var pack = _db.Packs.Where(x => x.Id == team.Pack.Id).Single();
            Assert.Contains(team, pack.Teams.ToList());


        }

        [Test]
        public void Does_New_Team_Exist_In_Category()
        {
            var team = FactoryMethods.CreateTeam(_db);
            _db.Teams.Add(team);
            _db.SaveChanges();

            Assert.Contains(team, team.Category.Teams.ToList());

            var category = _db.Categories.Where(x => x.Id == team.Category.Id).Single();
            Assert.Contains(team, category.Teams.ToList());
        }

        [Test]
        public void Can_Move_Team_To_Other_Pack()
        {
            var team = FactoryMethods.CreateTeam(_db);
            _db.Teams.Add(team);
            _db.SaveChanges();
            var packId = team.Pack.Id;

            var newPack = PersistetEntitesFactoryMethods.CreatePersistentPack(_db);
            var team1 = _db.Teams.Where(x => x.Id == team.Id).Single();
            team1.Pack = newPack;
            _db.SaveChanges();

            Assert.Contains(team1, newPack.Teams.ToList());
            Assert.Contains(team, newPack.Teams.ToList());

            var oldPack = _db.Packs.Where(x => x.Id == packId).Single();
            Assert.False(oldPack.Teams.Contains(team));
            Assert.False(oldPack.Teams.Contains(team1));

        }

        [Test]
        public void Can_Move_Team_To_Other_Category()
        {
            var team = FactoryMethods.CreateTeam(_db);
            _db.Teams.Add(team);
            _db.SaveChanges();
            var categoryId = team.Category.Id;

            var newCategory = PersistetEntitesFactoryMethods.CreatePersistentCategory(_db);
            var team1 = _db.Teams.Where(x => x.Id == team.Id).Single();
            team1.Category = newCategory;
            _db.SaveChanges();

            Assert.Contains(team1, newCategory.Teams.ToList());
            Assert.Contains(team, newCategory.Teams.ToList());

            var oldCategory = _db.Categories.Where(x => x.Id == categoryId).Single();
            Assert.False(oldCategory.Teams.Contains(team));
            Assert.False(oldCategory.Teams.Contains(team1));
        }

    }
}
