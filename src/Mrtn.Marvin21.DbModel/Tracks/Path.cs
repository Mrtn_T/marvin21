﻿using System.Collections.Generic;
using Mrtn.Marvin21.DbModel.Tracks.Support;

namespace Mrtn.Marvin21.DbModel.Tracks
{
    /// <summary>
    /// Predstavlja pot med dvema točkama, na zemljevidu
    /// </summary>
    public class Path : EntityBase
    {
        /// <summary>
        /// Referenca na začetno točko
        /// </summary>
        public virtual Station FirstStation { get; set; }
        /// <summary>
        /// Referenca na končno točko
        /// </summary>
        public virtual Station SecondStation { get; set; }
        public virtual bool IsManual { get; set; }

        /// <summary>
        /// ročni vzponi in spusti
        /// </summary>
        public virtual PathData ManualData { get; set; }
        /// <summary>
        /// avtomatsko izračunani vzponi in spusti
        /// </summary>
        public virtual PathData AutomaticData { get; set; }

        /// <summary>
        /// Napikane točke na zemljevidu
        /// </summary>
        public virtual List<PathPoint> PathPoints { get; set; }

    }
}
