﻿using System.Collections.Generic;
using System.Text;
using Mrtn.Marvin21.DbModel.Interfaces;
using Mrtn.Marvin21.DbModel.Tracks.Support;

namespace Mrtn.Marvin21.DbModel.Tracks
{
    /// <summary>
    /// Ta class predstavlja "Postajo", lokacijo na zemljevidu. 
    /// Lokacija je lahko start ali cilj, lahko je KT za eno ali več prog.
    /// Vsaka lokacija, ki ni start ali cilj, ima vsaj eno SI postajo.
    /// Če je na lokaciji možem "mrtev" čas, potem ima dve SI postaji, eno za prihod in eno za odhod
    /// </summary>
    public class Station : EntityBase, INamedEntity
    {
        private int _day = 1;
        public virtual string Name { get; set; }
        public virtual string SiCodeArrival { get; set; }
        public virtual string SiCodeDeparture { get; set; }
        public virtual bool IsStart { get; set; }
        public virtual bool IsFinish { get; set; }
        public virtual bool IsAlive { get; set; }
        public virtual bool IsHidden { get; set; }
        public virtual MapPosition MapPosition { get; set; }
        public virtual int DefaultPoints { get; set; }

        //{
        //    get { return _task; }
        //    set { _task = value; }
        //}
        public virtual string Description { get; set; }
        public virtual int Day
        {
            get { return _day; }
            set { _day = value; }
        }
        public virtual IList<TrackCheckpoint> TrackCheckpoints { get; set; }
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(Name);
            var sa = (SiCodeArrival ?? "").Trim();
            var sd = (SiCodeDeparture ?? "").Trim();
            if (sa == "" && sd == "") return sb.ToString();
            if (sa == "")
            {
                sb.Append(string.Format(" ({0})", sd));
                return sb.ToString();
            }
            if (sd == "")
            {
                sb.Append(string.Format(" ({0})", sa));
                return sb.ToString();
            }
            sb.Append(string.Format(" ({0}-{1})", sa, sd));
            return sb.ToString();
        }
    }
}
