﻿namespace Mrtn.Marvin21.DbModel.Tracks.Support
{
    public class SpeedTrial:PartialEntiyBase
    {
        public virtual bool IsStart { get; set; }
        public virtual bool IsFinish { get; set; }
    }
}
