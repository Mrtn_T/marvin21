﻿using System;

namespace Mrtn.Marvin21.DbModel.Tracks.Support
{
    /// <summary>
    /// Delna entiteta za TrackCheckpoint, namenjena vsemu, kar se tiče definicije časov: 
    /// </summary>
    public class Time: PartialEntiyBase
    {

        /// <summary>
        /// Število sekund, ki jih na tem KT-ju ekipi odštejemo od časa za opravljanje naloge.
        /// Ta vrednost se prepiše iz taska, ko se le tega dodeli KT-ju
        /// </summary>
        public virtual long ForTask { get; set; }

        /// <summary>
        /// Največje dovoljeno število sekund od starta do te KT, 
        /// upoštevajoč tudi vse čase za naloge
        /// </summary>
        public virtual long MaxFromStart { get; set; }

        /// <summary>
        /// Optimalno število sekund od starta do prihoda na to točko
        /// </summary>
        public long OptimalArrival { get; set; }

        /// <summary>
        /// Optimalno število sekund od starta do odhoda iz te točke. 
        /// Če tu ni naloge s fiksnim časom (ForTask=0), potem je enak OptimalArrival
        /// </summary>
        public long OptimalDeparture { get; set; }

        /// <summary>
        /// Optimalno število sekund od prejšnje KT do te
        /// Spremenjeno v float zaradi kumulativnih napak pri računanju dvojne časovnice
        /// </summary>
        public float FromPrevious { get; set; }

        /// <summary>
        /// Optimalno število sekund za pot po ravnem od prejšnje KT do te
        /// Spremenjeno v float zaradi kumulativnih napak pri računanju dvojne časovnice
        /// </summary>
        public float FromPreviousForDistance { get; set; }

        /// <summary>
        /// Optimalno število sekund za vzpon od prejšnje KT do te
        /// Spremenjeno v float zaradi kumulativnih napak pri računanju dvojne časovnice
        /// </summary>
        public float FromPreviousForUphill { get; set; }

        /// <summary>
        /// Optimalno število sekund za spust od prejšnje KT do te
        /// Spremenjeno v float zaradi kumulativnih napak pri računanju dvojne časovnice
        /// </summary>
        public float FromPreviousForDownhill { get; set; }
    }
}
