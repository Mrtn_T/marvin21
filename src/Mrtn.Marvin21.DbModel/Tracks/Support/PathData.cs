﻿using System;

namespace Mrtn.Marvin21.DbModel.Tracks.Support
{
    /// <summary>
    /// Ta class vsebuje podatke o poti med dvema točkama, bodisi podanima ali od prejšnje do te.
    /// Podatki so lahko bodisi IZRAČUNANI ali VNEŠENI, odvisno od konteksta, v katerem se pojavlja.
    /// 
    /// Obnašanje določa bool ManualUpDown
    ///  - Če je FALSE, se Uphill in downhill računa iz začetne in končne točke ter iz Zliste
    ///  - Če je TRUE, potem se vzpon in spust VNAŠA v Uphill, Downhill
    /// podatek ZisDirty se nastavlja le pri avtomatskem računanju podatkov.
    /// </summary>
    public class PathData : PartialEntiyBase
    {
        /// <summary>
        /// Samo pri ročnem vnosu. 
        /// Če je FALSE, se Uphill in downhill računa iz začetne in končne točke ter iz Zliste
        /// Če je TRUE, potem se vzpon in spust VNAŠA v Uphill, Downhill
        /// </summary>
        public bool ManualUpDown { get; set; }


        /// <summary>
        /// Seznam ekstremnih višin (maksimumov, minimumov) na poti. 
        /// Lahko je vnešen ročno, ali pa izračunan iz PathPoints. 
        /// Skupaj z višino začetne in končne točke se uporablja 
        /// za izračun vzponov in spustov (Uphill, Downhill).
        /// 
        /// Podaja se kot seznam višin(int) v metrih, ločenih z vejicami (CSV)
        /// </summary>
        public virtual string Zlist { get; set; }
        
        /// <summary>
        /// Razdalja v metrih
        /// </summary>
        public virtual int? Distance { get; set; }
        
        /// <summary>
        /// Skupni vzpon v metrih
        /// </summary>
        public int Uphill { get; set; }
        
        /// <summary>
        /// Skupni spust v metrih
        /// </summary>
        public int Downhill { get; set; }

        /// <summary>
        /// Samo pri avtomatskem izračunu:
        /// Ali je vsaj ena izmed točk (začetek, vmesne PathPoints z višinami, cilj)
        /// imela "neveljavno" višino (ZisDirty==true).
        /// To običajno pomeni, da je bila vsaj ena izmed omenjenih točk 
        /// premaknjena na zemljevidu, njena višina pa ni bila spremenjena ali potrjena
        /// </summary>
        public bool ZisDirty{ get; set; }

    }
}
