﻿namespace Mrtn.Marvin21.DbModel.Tracks.Support
{
    public class MapPosition : PartialEntiyBase
    {
        public virtual float? X { get; set; }
        public virtual float? Y { get; set; }
        public virtual float? Z { get; set; }

        public virtual bool ZisDirty { get; set; }
    }
}
