﻿using System;
using System.Collections.Generic;
using Mrtn.Marvin21.DbModel.Run;
using Mrtn.Marvin21.DbModel.Teams;

namespace Mrtn.Marvin21.DbModel.Tracks
{
    public class Track : EntityBase
    {
        private int _day = 1;

        //        private IList<RunTrack> _runtrack;
        public virtual Category Category { get; set; }
        public virtual int Day
        {
            get { return _day; }
            set { _day = value; }
        }
        /// <summary>
        /// Skupna dolžina proge (v metrih) 
        /// </summary>
        public virtual float? CumulativeDistance { get; set; }

        /// <summary>
        /// Skupni vzpon na progi (v metrih)
        /// </summary>
        public virtual float? CumulativeUphill { get; set; }

        /// <summary>
        /// Skupni spust na progi (v metrih)
        /// </summary>
        public virtual float? CumulativeDownhill { get; set; }


        /// <summary>
        /// Število sekund za optimalen prehod proge, Skupaj z upoštevanimi časi za naloge (Time.ForTask)
        /// </summary>
        public virtual int? OptimalTime { get; set; }

        /// <summary>
        /// Število sekund za optimalen prehod proge, brez upoštevanja brez časov za naloge (Time.ForTask)
        /// </summary>
        public int? OptimalPathTime { get; set; }

        /// <summary>
        /// Seznam vseh KT-jev na progi
        /// </summary>
        public virtual IList<TrackCheckpoint> TrackCheckpoints { get; set; }

        /// <summary>
        /// seznam "opravljenih" prog (iz RUN)
        /// </summary>
        public virtual List<RunTrack> RunTracks { get; set; }

        public virtual int? MinTimeForSpeedTrial { get; set; }

        //public virtual IList<RunTrack> RunTrack
        //{
        //    get { return _runtrack; }
        //    set { _runtrack = value; }
        //}

                public override string ToString()
        {
            return String.Format("{0}({1})",Category.Abbr,Day );
        }

    }
}
