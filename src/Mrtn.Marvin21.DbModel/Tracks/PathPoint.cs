﻿using Mrtn.Marvin21.DbModel.Tracks.Support;

namespace Mrtn.Marvin21.DbModel.Tracks
{
    /// <summary>
    /// ena od "napikanih" točk pri poti po zemljevidu
    /// </summary>
    public class PathPoint : EntityBase
    {
        /// <summary>
        /// Pot, kateri pripada
        /// </summary>
        public virtual Path Path { get; set; }

        /// <summary>
        /// Zaporedna številka
        /// </summary>
        public virtual int No { get; set; }

        /// <summary>
        /// pozicija na zemljevidu
        /// </summary>
        public virtual MapPosition MapPosition { get; set; }
    }
}