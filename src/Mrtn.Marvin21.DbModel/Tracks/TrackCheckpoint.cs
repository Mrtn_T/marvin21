﻿using System;
using System.Collections.Generic;
using Mrtn.Marvin21.DbModel.Tasks;
using Mrtn.Marvin21.DbModel.Tracks.Support;

namespace Mrtn.Marvin21.DbModel.Tracks
{
    /// <summary>
    /// Predstavlja eno KT na eni progi 
    /// </summary>
    public class TrackCheckpoint : EntityBase
    {
        private int _points = 100;
        //private Task _task;
        //private IList<RunCheckpoint> _runcheckpoint;

        public virtual Track Track { get; set; }

        public virtual int No { get; set; }
        public virtual string Name { get; set; }
        public virtual int Points
        {
            get { return _points; }
            set { _points = value; }
        }
        public virtual string Description { get; set; }

        //partials
        public virtual Time Time { get; set; }
        public virtual PathData FromPrevious { get; set; }
        public virtual SpeedTrial SpeedTrial { get; set; }

        //linked entites
        public virtual Station Station { get; set; }
        public virtual Path PathFromPrevious { get; set; }
        public virtual List<Task> Tasks { get; set; }

    }
}
