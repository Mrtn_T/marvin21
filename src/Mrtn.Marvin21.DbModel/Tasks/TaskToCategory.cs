﻿using Mrtn.Marvin21.DbModel.Teams;

namespace Mrtn.Marvin21.DbModel.Tasks
{
    public class TaskToCategory : EntityBase
    {
        public virtual Task Task { get; set; }
        public virtual Category Category { get; set; }
    }
}
