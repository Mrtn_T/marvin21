﻿using System.Collections.Generic;
using Mrtn.Marvin21.DbModel.Interfaces;
using Mrtn.Marvin21.DbModel.Run;
using Mrtn.Marvin21.DbModel.Tracks;

namespace Mrtn.Marvin21.DbModel.Tasks
{
    public class Task : EntityBase, INamedEntity
    {

        // Podatki o sami nalogi
        public virtual string Name { get; set; }
        public virtual string Abbr { get; set; }
        public virtual string Description { get; set; }

        // Podatki tem, kdaj in kje se naloga izvaja
        public virtual int Day { get; set; }
        public virtual bool BeforeStart { get; set; }
        public virtual bool OnTrack { get; set; }
        public virtual bool AfterFinish { get; set; }
        public virtual Station Station { get; set; }

        // Kaj se vnaša
        public virtual string FirstEntryName { get; set; }
        public virtual int FirstEntryMin { get; set; }
        public virtual int FirstEntryMax { get; set; }
        public virtual int FirstEntryDefault { get; set; }

        public virtual string SecondEntryName { get; set; }
        public virtual int SecondEntryMin { get; set; }
        public virtual int SecondEntryMax { get; set; }
        public virtual int SecondEntryDefault { get; set; }
        public virtual bool SecondEntryIsTime { get; set; }

        public virtual int TimeForTask { get; set; }

        // Algoritem za računanje točk. 0 pomeni preprost vnos!
        public virtual int CalculatingAlgorythmId { get; set; }

        public virtual List<TaskToCategory> TaskToCategories { get; set; }
        public virtual List<RunTask> RunTasks { get; set; }


        /// <summary>
        /// Martin, 8.1.2020:
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        public virtual int MaxPoints { get; set; }

    }
}
