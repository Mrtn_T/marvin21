﻿using System;
using Mrtn.Marvin21.DbModel.Interfaces;

namespace Mrtn.Marvin21.DbModel.Support
{
    public class Setting : EntityBase, INamedEntity
	{
        public virtual string Name { get; set; }
        public virtual string Code { get; set; }
        public virtual string Value { get; set; }
        public virtual string Unit { get; set; }
        public virtual string Description { get; set; }
    }
}
