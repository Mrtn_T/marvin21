﻿using System;
using System.Collections.Generic;
using Mrtn.Marvin21.DbModel.Interfaces;
using Mrtn.Marvin21.DbModel.Tasks;
using Mrtn.Marvin21.DbModel.Tracks;

namespace Mrtn.Marvin21.DbModel.Teams
{
    public class Category : EntityBase, INamedEntity
    {
        public virtual int No { get; set; }
        public virtual string Name { get; set; }
        public virtual bool Active { get; set; }
        public virtual string Abbr { get; set; }
        public virtual IList<Team> Teams { get; set; }
        public virtual IList<Track> Tracks { get; set; }

        
        public float Speed { get; set; }
        public float UphillSpeed { get; set; }
        public float DownhillSpeed { get; set; }
        public string Color { get; set; }

        public virtual List<TaskToCategory> TaskToCategories { get; set; }

        /// <summary>
        /// Martin, 10.1.2020:
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        public int MaxPoints { get; set; }

        /// <summary>
        /// Martin, 10.1.2020:
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        public string MaxPointsDescription { get; set; }
    }
}
