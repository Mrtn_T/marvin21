﻿using System;
using System.Collections.Generic;
using Mrtn.Marvin21.DbModel.Interfaces;
using Mrtn.Marvin21.DbModel.Results;

namespace Mrtn.Marvin21.DbModel.Teams
{
    public class Pack : EntityBase, INamedEntity
    {
        private bool _active = true;
        private bool _incompetition = true;

        public virtual string Name { get; set; }

        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
        public virtual bool InCompetition
        {
            get { return _incompetition; }
            set { _incompetition = value; }
        }

        public virtual string Abbr { get; set; }

        public virtual string Town { get; set; }

        public virtual PackResult Result { get; set; }
        public virtual IList<Team> Teams { get; set; }
    }
}
