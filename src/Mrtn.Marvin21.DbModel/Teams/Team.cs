﻿using System;
using System.Collections.Generic;
using Mrtn.Marvin21.DbModel.Results;
using Mrtn.Marvin21.DbModel.Run;

namespace Mrtn.Marvin21.DbModel.Teams
{
    public class Team : EntityBase
    {
        private bool _active = true;
        private bool _incompetition = true;
        public virtual string Name { get; set; }
        public virtual int? No { get; set; }
        public virtual string SiCode { get; set; }
        public virtual string PrivateSiCode { get; set; }

        public virtual Pack Pack { get; set; }
        public virtual Category Category { get; set; }

        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
        public virtual bool InCompetition
        {
            get { return _incompetition; }
            set { _incompetition = value; }
        }

        public virtual TeamResult Result { get; set; }
        public virtual List<RunTask> RunTasks { get; set; }
        public virtual List<RunTrack> RunTracks { get; set; }
    }
}
