﻿namespace Mrtn.Marvin21.DbModel
{
    public class EntityBase
    {
        public EntityBase()
        {
            Locked = false;
        }

        public int Id { get; set; }
        public bool Locked { get; set; }
    }
}
