﻿namespace Mrtn.Marvin21.DbModel.Interfaces
{
    public interface INamedEntity
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}
