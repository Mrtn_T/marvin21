﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.DbModel.Teams;

namespace Mrtn.Marvin21.DbModel.Results
{
    public class PackResult 
    {
        [Key]
        public virtual int PackId { get; set; }
        public virtual Pack Pack { get; set; }
        public virtual bool Locked { get; set; }

        public virtual int Points { get; set; }
        public virtual int Place { get; set; }
        public virtual List<TeamResult> TeamResults { get; set; }

        //public virtual string Remarks { get; set; }
        //"GGM:78,23%; GGS:56,17%(66,17%, 46,17%); PP:-;RG:78,15%; 40+:-"


        /// <summary>
        /// Martin, 10.1.2020:
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        public float Percent { get; set; }

        /// <summary>
        /// Martin, 10.1.2020:
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        public string Description { get; set; }


    }
}
