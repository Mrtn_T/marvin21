﻿using System.ComponentModel.DataAnnotations;
using Mrtn.Marvin21.DbModel.Teams;

namespace Mrtn.Marvin21.DbModel.Results
{
    public class TeamResult
    {
        [Key]
        public virtual int TeamId { get; set; }
        public virtual Team Team { get; set; }
        public virtual bool Locked { get; set; }

        public virtual int Points { get; set; }
        public virtual int Place { get; set; }
        public virtual PackResult PackResult { get; set; }


        /// <summary>
        /// Martin, 10.1.2020:
        /// dodano zato, da se lahko pri spremenjenem algoritmu za izračun rodove zmage določi
        /// največje število točk v kategoriji
        /// </summary>
        public float Percent { get; set; }
    }
}
