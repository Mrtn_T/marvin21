﻿using Mrtn.Marvin21.DbModel.Tasks;
using Mrtn.Marvin21.DbModel.Teams;

namespace Mrtn.Marvin21.DbModel.Run
{
    /// <summary>
    /// Entiteta, ki vsebuje rezultat pri eni nalogi za eno ekipo
    /// </summary>
    public class RunTask : EntityBase
    {
        public virtual int FirstEntry { get; set; }
        public virtual int SecondEntry { get; set; }

        /// <summary>
        /// Skupno število točk za to ekipo za ta task
        /// </summary>
        public virtual int Points { get; set; }

        //povezane entitete
        public virtual Team Team { get; set; }
        public virtual Task Task { get; set; }
    }
}
