﻿using System;
using System.Collections.Generic;
using Mrtn.Marvin21.DbModel.Run.Support;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Tracks;

namespace Mrtn.Marvin21.DbModel.Run
{
    public class RunTrack : EntityBase
    {
        //kam je vezan rezultat na progi
        public virtual Team Team { get; set; }
        public virtual Track Track { get; set; }
        public virtual int Day { get; set; }

        //njegovi punchi
        public virtual List<RunTrackCheckpoint> RunTrackCheckpoints { get; set; }

        //časi 
        public virtual TrackTime StartTime { get; set; }
        public virtual TrackTime FinishTime { get; set; }
        public virtual int? TimeForPath { get; set; }
        public virtual int? TimeDelayForPath { get; set; }
        public virtual int? TimeForTask { get; set; }
        public virtual int? TimeForWait { get; set; }
        public virtual int? TimeOnTrack { get; set; }

        //rezultat s proge
        public virtual int CheckpointsFound { get; set; }
        public virtual int PointsForCheckpoints { get; set; }
        public virtual int PointsForTime { get; set; }
        public virtual int TrackPoints { get; set; }
        
        //hitrostna etapa
        public virtual TrackTime SpeedTrialStart { get; set; }
        public virtual TrackTime SpeedTrialFinish { get; set; }
        public virtual int? SpeedTrialTimeOnTrack { get; set; }
        public virtual int SpeedTrialPoints { get; set; }

        //referenca na SI branje...
        public virtual SiRead SiRead { get; set; }
    }
}
