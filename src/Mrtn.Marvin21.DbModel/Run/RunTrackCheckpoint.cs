﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Run.Support;
using Mrtn.Marvin21.DbModel.Si;
using Mrtn.Marvin21.DbModel.Tracks;

namespace Mrtn.Marvin21.DbModel.Run
{
    public class RunTrackCheckpoint : EntityBase
    {
        public virtual int No { get; set; }

        // to se uporablja, če ekipi KT skenslamo (zavrnemo) npr v primeru goljufanja.
        // to ima absolutno prednost, če je KT zavrnjena, se ostalo ne upošteva pri izračunu.
        public virtual bool IsRejectedManualy { get; set; }
        public virtual string IsRejectedManualyReason { get; set; }

        // točka je ročno priznana
        // denimo če odpove Si sistem
        // druga najvišja prioriteta: če je ročno potrjena, se šteje razen če je bila eksplicitno zavrnjena
        public virtual bool IsConfirmedManualy { get; set; }
        public virtual string IsConfirmedManualyReason { get; set; }

        public virtual TrackTime Arrival { get; set; }
        public virtual TrackTime Departure { get; set; }
        public int? TimeOnPath { get; set; }
        public int? TimeWaitHere { get; set; }
        public int? TimeTotal { get; set; }
        // se nastavlja avtomatsko, če je TimeOnPath znotraj okvirjev, predvidenih z individualno časovnico
        public virtual bool IsConfirmedByArrival { get; set; }

        // se nastavlja avtomatsko, če je bil vnešen rezultat za nalogo na tej KT
        public virtual bool IsConfirmedByTask { get; set; }



        /// <summary>
        /// točke, ki jih priznamo za ta KT
        /// Ekipa dobi točke, če KT NI zavrnjena (!IsRejectedManualy) in 
        /// če je potrjena na vsaj en način (IsConfirmedManualy, IsConfirmedByArrival, IsConfirmedByTask)
        /// </summary>
        public int Points { get; set; }

        public virtual RunTrack RunTrack { get; set; }
        public virtual TrackCheckpoint TrackCheckpoint { get; set; }
        public virtual SiPunch SiPunchArrival { get; set; }
        public virtual SiPunch SiPunchDeparture { get; set; }

    }
}
