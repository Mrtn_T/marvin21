﻿using System;

namespace Mrtn.Marvin21.DbModel.Run.Support
{
    public class TrackTime
    {
        public DateTime? Predicted { get; set; }
        public DateTime? MaxPredicted { get; set; }
        public DateTime? Manual { get; set; }
        public DateTime? FromSi { get; set; }

    }
}