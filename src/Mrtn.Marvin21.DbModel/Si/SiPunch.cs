﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.DbModel.Run;

namespace Mrtn.Marvin21.DbModel.Si
{
    public class SiPunch : EntityBase
    {

        public int No { get; set; }
        public virtual string SiCode { get; set; }
        public virtual DateTime Time { get; set; }


        public virtual SiRead SiRead { get; set; }
        public virtual RunTrackCheckpoint RunTrackCheckpoint { get; set; }

    }
}
