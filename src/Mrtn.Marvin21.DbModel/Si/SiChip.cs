﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mrtn.Marvin21.DbModel.Si
{
    public class SiChip : EntityBase
    {
        public virtual bool Private { get; set; }
        public virtual string SiCode { get; set; }
        public virtual string Comment { get; set; }
        public virtual bool Active { get; set; }
    }
}
