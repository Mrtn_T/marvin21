﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mrtn.Marvin21.DbModel.Teams;
using Mrtn.Marvin21.DbModel.Run;

namespace Mrtn.Marvin21.DbModel.Si
{
    public class SiRead : EntityBase
    {

        public virtual int Day { get; set; }
        public virtual string SiCode { get; set; }
        public virtual DateTime? Clear { get; set; }
        public virtual DateTime? Check { get; set; }
        public virtual DateTime? Start { get; set; }
        public virtual DateTime? Finish { get; set; }
        public virtual DateTime? ReadAt { get; set; }

        public virtual bool HasBeenProcessed { get; set; }
        public virtual RunTrack RunTrack { get; set; }
        public virtual List<SiPunch> SiPunches { get; set; }
        
    }
}
